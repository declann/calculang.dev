---
#title: "calculang 🧮💬👩‍💻"
code-fold: true
echo: false
toc: false
format:
  html:
    #link-external-icon: true
    resources:
      - './models/**'
      - '/thumbnails/website.png'
    page-layout: full
    #code-tools: true # breaks?

listing:
  - id: gallery
    template: gallery.ejs
    contents: gallery.yml
---


::: {#hero-banner .column-screen}
::: {.grid .column-page}
::: {.headline .g-col-lg-6 .g-col-12 .g-col-md-12}
::: h1
calculang is a language for calculations
:::


It aims to be:

- shareable, communicable 💬 💌
- transparent, verifiable 🕵️‍♀️ 
- understandable, concise 📖
- flexible, reusable ♻️
<!-- - meaningful, maintainable 🤌🏻 -->




:::

::: {.headline .g-col-lg-6 .g-col-12 .g-col-md-12}

<!--::: h1
symbols
:::-->

[![](./logo4.svg)](https://fosstodon.org/@calculang/110226468383564413)
<!--[![a logo for calculang composed of a speech bubble, + and - mathematical symbols, a code representation and an equals symbol](./logo4.svg)](https://fosstodon.org/@calculang/110226468383564413)--->
<!-- Alt text being displayed bad for easy inclusion? -->

<!--[logo](https://fosstodon.org/@calculang/110226468383564413)-->


:::
:::
:::

::: {.headline .column-page}

### only for calculations <small>⇔ numbers ⇔ workings</small>

calculang doesn't 'do things' ("side-effects") like regular programming languages 💥

This serves to simplify, permitting the aims above for **calculations, numbers,** and their **workings** ✅️

### portable 💨

calculang models compile to **highly-portable [Javascript modules](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Modules)**. Besides private usecases, outputs such as numbers and visualizations can be placed **directly** in a [blog post you read](https://calcwithdec.dev/posts/you-draw-inflation/)^[even if you don't know it!], <!--in the news you read, -->in a dashboard for your teams metrics, in your banking app<!--, in a scientific paper you read-->, and so on.

<!-- say something about linking back to models, tools, or wait until I have a demo? -->

<!-- ::: {.callout-note title="introspection and calculang tools" collapse="true"}-->


### formulas as building blocks 🧱

In calculang, **formulas are the building blocks of calculations**. Formulas are "pure functions" for mathematics or [FP](https://en.wikipedia.org/wiki/Functional_programming) audience; for everyone else: they're just like spreadsheet formulas.

calculang formulas are [flexible](https://github.com/calculang/calculang?tab=readme-ov-file#design-principlesfeatures) ♻️: not only can they be re-used, they can be re-used *with specified changes*: specified at the *formula-level*.

calculang facilitates transparency at the same *formula-level* (because formulas are exported as a rule).

::: {.callout-note title="versus spreadsheets" collapse="true"}

- In calculang, layout and presentation is separated from calculation logic: and there can be many of these.<!-- calculang models are free from layout. There are pros and cons, but a free expresiveness about calculations is a pro.-->

- Every formula is given a name, so that there is no need to debate *v or hlookups* vs. *index-match* 🤯

- Flexible formulas and modularity allow us to give more meaningful and maintainable structure to many common patterns we create in spreadsheets.

- calculang models are "code" and as such can integrate readily with state-of-the-art tools and practices around auditability, version control and testing and development.
<!--Writing and interpreting calculations is *more like* writing and interpreting **formulae in a spreadsheet** rather than more delicate and tricky programming code. ^[but different - more on comparisons here soon!]-->

:::

### tools 🧰

A calculang introspection API makes it convenient to build **tools** (even [environments](https://calculang-at-fosdem.pages.dev/)) that interact with calculang models and their formulas in meaningful ways.

It includes all of the information needed to transpile from calculang source into Javascript output; and more besides.

<details><summary style="font-size: 0.8em">introspection output 📜</summary>
- every calculang formula
  - their source locations
  - and their input dependencies
- every calculang call within calculang code
  - and their source locations
- all calculang relationships (`links`)
- information about source files, calculang scopes^[calculang scopes are a construct for modularity], and how they are related through the module graph

There is some documentation [here](https://github.com/calculang/calculang/tree/main/packages/introspection-api), but you can also see introspection outputs using [this experimental tool](https://finding-calculang-foc.netlify.app/editor).
</details>


::: {.callout-note title="visualization APIs & calcudata" collapse="true"}

<!-- detached from layout speel... -->

One calculang tool is `calcuvizspec`: a visualization API that uses [Vega-Lite](https://vega.github.io/vega-lite/) to map model features (formulas and inputs) **directly** to visual marks.

A presentation including some examples is [here](https://calcuvizspec-presentation.pages.dev/calcuvizspec) (toggle Code blocks).

This API exists due to a need model developers have: *they must be able to very quickly see numbers from a model, and iterate the model and outputs quickly* during the development process.

A separate API: `calcuvegadata`, is more efficient for development- or finalised- model outputs which **respond to interactivity**.

[`calcudata`](https://observablehq.com/@declann/little-calculang-helpers) is a convenient lower-level API for querying numbers.

These APIs include a notion about a `cursor`: representing inputs where a single value is provided, and `domains`: representing inputs where a range of values are provided (and their cross-product is used).

These are opinionated APIs: none of which are prescribed. Different opinions about patterns to use models are [encouraged](https://github.com/calculang/calculang/discussions)!

:::


<br/>

<style>
#chronicles {
  font-size: 0.5em;
  background: lightpink;
  border-radius: 5px;
  padding: 5px 10px;
}

#chronicles > h3 {
  margin: 0;
}
</style>

::: {#chronicles}

### chronicles of calculang, Feb 2025 (*published work*), and status

<details><summary>A long note...</summary>

#### Examples

The examples on this website are dated: from approx. **December 2023**. Here are other **newer** examples and experimental environments that I've been working on since then:

Relatively new/more experimental bits:

- [uh squine and cosquare](https://new-layout-2--finding-calculang-foc.netlify.app/squine)
- [Savings Calculator](https://new-layout-2--finding-calculang-foc.netlify.app/savings) with highlights
- [HowMuchIs13BillionEuros.com](https://howmuchis13billioneuros.com/): a website and environment I experimented with ([context](https://www.linkedin.com/posts/activity-7240329187167776770--nhY?utm_source=share&utm_medium=member_desktop&rcm=ACoAAAstp_QBJcVCzB0sguky_6JeFCMH5wU23UM))


Selected from "[next-calculang-gallery](https://next-calculang-gallery.netlify.app)" ([GitLab](https://gitlab.com/declann/next-calculang-gallery)):

- [Raycasting](https://next-calculang-gallery.netlify.app/raycasting) ([VIDEO 📹](https://www.youtube.com/watch?v=hKVXRACCnqU))
- [bouncing ball ⚽⛹️‍♂️ Scrollytorial](https://next-calculang-gallery.netlify.app/bounce) Here I was experimenting with some tutorial presentation - using scrollytelling and [gemini](https://github.com/uwdata/gemini) transitions

Selected from "[calculang-editables](https://calculang-editables.netlify.app/)" ([GitHub](https://github.com/declann/calculang-develop-with-framework/)):

- [tixy.land clone](https://calculang-editables.netlify.app/tixyish) ([VIDEO 📹](https://www.youtube.com/watch?v=uXUd_-xrycs))
- [Dungeon Generator](https://calculang-editables.netlify.app/dungeon/dungeon)
- [❤️ Calculator](https://calculang-editables.netlify.app/graphing-calcs/graphing-edit-cm) ([VIDEO 📹](https://www.youtube.com/watch?v=Y0_VF-Dbmqw))
- [Generative Spirographs 🌀](https://calculang-editables.netlify.app/spirographs)
- [Sun and Moon Paths WIP](https://calculang-editables.netlify.app/sun-moon-paths)
- [Sun and Moon Paths ii WIP](https://calculang-editables.netlify.app/sun-moon-paths2)
- [Falling Sand](https://calculang-editables.netlify.app/graphing-calcs/graphing-edit-cm-gol-sand)
- [Editable Music](https://calculang-editables.netlify.app/graphing-calcs/graphing-edit-music) (⚠️ sound Low!)
- [Savings Calculator](https://calculang-editables.netlify.app/savings) with working highlighting of workings
  - There is a newer and more informative (for developers) version of this [here](https://new-layout-2--finding-calculang-foc.netlify.app/savings) (noted above)
- [Reactive Inputs](https://calculang-editables.netlify.app/reactive-inputs) This was a nice experiment at reacting to model code changes. You need to checkout [the repo](https://github.com/declann/calculang-develop-with-framework/) to try it

I have other examples that I use in [my blog](https://calcwithdec.dev), though readers might not notice! 💫 My blogs are usually communicative pieces using interactive calculang models to illustrate messages - I do not *show* the formulas as I do in calculang examples. But calculang is operating behind the scenes, and you can find the model sources [on GitLab](https://gitlab.com/declann/calcwithdec.dev/-/tree/main).

- [Pictures of Pensions 🖼️](https://calcwithdec.dev/posts/pictures-pensions/)
- [You Draw Inflation](https://calcwithdec.dev/posts/you-draw-inflation/)
- [Cashflow Profiles and Reserves: Actuarial Terminology I](https://calcwithdec.dev/posts/actuarial-terms-i/)
- [Visualizing Risk](https://calcwithdec.dev/posts/viz-risk/)
- (old) [Hearty maths with calculang ❤️](https://calcwithdec.dev/posts/hearty-maths/)

[calculang-at-fosdem.pages.dev](https://calculang-at-fosdem.pages.dev/) is a website including examples I used for one presentation at FOSDEM 2024 ([recording](https://archive.fosdem.org/2024/schedule/event/fosdem-2024-2617-calc-with-calculang-a-language-for-calculations/)). Using that environment I demoed "reactive workings" for the first time (double click on numbers in the Savings example to activate an overlay).

(old) in 2023 I spoke at BelfastJS: my first time presenting calculang to a crowd 🙂. A website I made is [calculang-talk-belfastjs.pages.dev](https://calculang-talk-belfastjs.pages.dev/notes) where I demoed "three little applications" (plus a bonus)

For completion, there are **older published examples** [on ObservableHQ](https://observablehq.com/collection/@declann/calculang) and [calculang-miscellaneous-models](https://github.com/declann/calculang-miscellaneous-models). Other examples and an early environment in [calcy-quary-vizys-online.pages.dev](https://calcy-quarty-vizys-online.pages.dev/) and [models-on-a-plane.pages.dev](https://models-on-a-plane.pages.dev/stories/). I partially [migrated](https://calcy-quarty-vizys-online.pages.dev/specs/climate-simple/some-visuals-ramp) [ClimateMARGO.jl](https://github.com/ClimateMARGO/ClimateMARGO.jl) which was fun. [index2](/index2.qmd) has easter eggs, including a raymarching model that can use more interactive/explanatory work to be a highlight.

*There has been no consolidation in environments/examples because **calculang is still evolving***.

📢 ***Soon**, a key model release will be a (first ever?) interactive and visual actuarial cashflow model.* Actuarial modelling can push calculang into new and interesting areas, but applications across a wide range of domains is still *fundamental* for a language for calculations.

#### Themes

The examples I developed with calculang seem to cover many very different themes: pension and savings calculators, then gaming algorithms, creative coding environments, then solar system models, and other wild patterns.

**Education** was/is an important theme for much of it. When I develop I aim to have instant feedback as I thinker with models. This along with calculangs pure formulas distinct from general programming, could be an interesting way to introduce coding, or to "play with" and explore particular models. I am happy to discuss this with people who are interested!

Further, models have a problem whenever they do many things: this really crops up with flexibility in models. You need ways to see and interpret all the things. You need this when you are done: to communicate, and critically too while you develop.

This is why I am highly invested in visualization and interaction. It's to solve *modelling problems*. Ability to explore and ability to communicate with models are modelling problems: not (by themselves) distinct new problems or interests.

#### Presentations

FOSDEM 2024 (Feb):

Recordings available, each is ~17mins:

- [calc with calculang, a language for calculations](https://archive.fosdem.org/2024/schedule/event/fosdem-2024-2617-calc-with-calculang-a-language-for-calculations/) given in the Javascript devroom (There are 2x short drops due to technical issues) [slides](https://calculang-at-fosdem.pages.dev/slides) | [website](https://calculang-at-fosdem.pages.dev/)
- [declarative calcs and visualization with calculang](https://archive.fosdem.org/2024/schedule/event/fosdem-2024-2636-declarative-calcs-and-visualization-with-calculang/) given in the Declarative and Minimalistic Computing devroom [slides](https://calculang-at-fosdem.pages.dev/slides2)

[Finding calculang](https://finding-calculang-foc.netlify.app/), a lightning presentation I gave to the Future of Coding community

Presentation about [calcuvizspec](https://calcuvizspec-presentation.pages.dev/calcuvizspec-revealjs#/title-slide), an early visualization API.

LISP Ireland, Aug 2024:

- [Recording](https://www.youtube.com/live/zPVo0d-0G_U?si=VKywrQCp0DyrrIZa&t=540) (poor sound quality warning. Screen share starts at [9mins](https://www.youtube.com/live/zPVo0d-0G_U?si=VKywrQCp0DyrrIZa&t=540), talking starts at [5mins15s](https://www.youtube.com/live/zPVo0d-0G_U?si=VKywrQCp0DyrrIZa&t=315))

2023 BelfastJS talk: [slides and examples](https://calculang-talk-belfastjs.pages.dev/notes) (noted above under examples)

#### Tools

When we streamline so many different models in a consistent language, there is an opportunity to build many tools to explore calculations and models. The environments linked above can be considered tools. "Reactive workings" (mentioned above) is a tool. Other noteworthy tools are:

- [calculationships (wip)](https://observablehq.com/@declann/calculationships-wip): very rough node-link diagrams to see the relationships in calculations. I used this to make [fun](https://fosstodon.org/@calculang/112395292193435109), [and illustrative](https://fosstodon.org/@calculang/112395586583271754), [different](https://fosstodon.org/@calculang/112399416500046571), node-link diagrams. This tool takes a URL to a `.cul.js` file and does its work (this is a pattern that all tools should permit)
- [metal](https://finding-calculang-foc.netlify.app/editor) an environment I use to show people exactly what the compiler does (not working for URL references - TOFIX)

Tools is probably the most interesting way to contribute to calculang. We need tools much better than calculationships to understand/explore models at a high- and low-level - and to manipulate them.

#### Integrations - Python

I basically built an integration for Jupyter notebooks by using the awesome Anywidget. It isn't a good pattern to publish, but I can provide it if requested.

#### Developer branch

calculang is under development, and I am usually using a [different](https://github.com/calculang/calculang/branches/active) branch. Above: the version I use differs from place to place.

Code in the main branch of calculang is *extremely old* and in many places obselete.

Because the compiler now works in the browser, except in old examples you can trace versions used in the source (without considering `node_modules`/`package.json`).

#### Missing/TODO

<!-- NOTE TO SELF: I have a lot of local commits in my fut-dev branch of calculang-develop-with-framework Feb 2025 (most of it is disorganised and ex-latest. Reviving spirographs and sun-and-moon ii). I have some local branches of next-calculang-gallery, likely even more redundant. Same is true for calculang-metal - currently a private repo but it shouldn't be TOFIX -->

[shop](https://new-layout-2--finding-calculang-foc.netlify.app/shop) give this a special shoutout?

*May not be in chronological order; may not be comprehensive.*

</details>

:::

## 👀 Examples


<!--Made with 💖 and calculang, click the link to see the calculang.-->

:::

::: {#gallery .column-page}


:::

::: {.column-page}

<br/>


<!--**made with 💖 and calculang 🤗**-->

calculang is free software available [on GitHub](https://github.com/calculang/calculang).

:::


```{=html}
<style>

#hero-banner {
  //padding-top: 35px;
  //background-color: rgb(237,243,249);
}


.headline {
  font-size: 1.2em;
  //font-weight: 300;
  //padding-bottom: 1em;
}

.headline .h1 {
  font-size: 1.2em;
}
.headline p {
  margin: 0;
  padding-bottom: 0.4rem;
}

.headline a {
  text-decoration: none;
}

.headline ul li {
  //margin-bottom: 0.3em;
}

.logo {
  width: 100px;
  height: 100px;
  padding-top: 2em
}
</style>
```
