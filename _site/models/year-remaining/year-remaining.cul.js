// date_in is the only input, then fractionIntoYear({date_in}) does the calculation for any date (see below)
export const date = () => date_in;

// formulae:

export const year = () => date().getFullYear(); // e.g. 2023

// leap year if Feb has 29 days,
// from https://stackoverflow.com/a/43819507
export const isLeap = () => new Date(year(), 1, 29).getDate() === 29;

export const daysInYear = () => (isLeap() ? 366 : 365);

// no Daylight Savings Time allowance because
// "UTC ... is not subject to DST" \o/
// from https://stackoverflow.com/a/40975730
export const daysIntoYearStartTomorrow = () =>
  (Date.UTC(date().getFullYear(), date().getMonth(), date().getDate()) -
    Date.UTC(date().getFullYear(), 0, 0)) /
  24 /
  60 /
  60 /
  1000;

export const daysIntoYearStartToday = () => daysIntoYearStartTomorrow() - 1;

export const daysIntoYear = () =>
  timing() == "start of today"
    ? daysIntoYearStartToday()
    : daysIntoYearStartTomorrow();

export const fractionIntoYear = () => daysIntoYear() / daysInYear();

export const fractionLeftInYear = () => 1 - fractionIntoYear();

//inputs:
export const timing = () => timing_in; // "start of today" or "start of tomorrow"
