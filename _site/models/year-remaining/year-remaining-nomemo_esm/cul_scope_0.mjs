// date_in is the only input, then fractionIntoYear({date_in}) does the calculation for any date (see below)
export const date = ({ date_in }) => date_in;

// formulae:

export const year = ({ date_in }) => date({ date_in }).getFullYear(); // e.g. 2023

// leap year if Feb has 29 days,
// from https://stackoverflow.com/a/43819507
export const isLeap = ({ date_in }) => new Date(year({ date_in }), 1, 29).getDate() === 29;

export const daysInYear = ({ date_in }) => isLeap({ date_in }) ? 366 : 365;

// no Daylight Savings Time allowance because
// "UTC ... is not subject to DST" \o/
// from https://stackoverflow.com/a/40975730
export const daysIntoYearStartTomorrow = ({ date_in }) =>
(Date.UTC(date({ date_in }).getFullYear(), date({ date_in }).getMonth(), date({ date_in }).getDate()) -
Date.UTC(date({ date_in }).getFullYear(), 0, 0)) /
24 /
60 /
60 /
1000;

export const daysIntoYearStartToday = ({ date_in }) => daysIntoYearStartTomorrow({ date_in }) - 1;

export const daysIntoYear = ({ timing_in, date_in }) =>
timing({ timing_in }) == "start of today" ?
daysIntoYearStartToday({ date_in }) :
daysIntoYearStartTomorrow({ date_in });

export const fractionIntoYear = ({ timing_in, date_in }) => daysIntoYear({ timing_in, date_in }) / daysInYear({ date_in });

export const fractionLeftInYear = ({ timing_in, date_in }) => 1 - fractionIntoYear({ timing_in, date_in });

//inputs:
export const timing = ({ timing_in }) => timing_in; // "start of today" or "start of tomorrow"