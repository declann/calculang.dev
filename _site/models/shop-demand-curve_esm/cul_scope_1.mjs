import { expenses } from "./cul_scope_0.mjs";import { purchase_price } from "./cul_scope_0.mjs";import { sales_price } from "./cul_scope_0.mjs";import { units } from "./cul_scope_0.mjs";import { profit } from "./cul_scope_0.mjs";import { purchases } from "./cul_scope_0.mjs";import { sales } from "./cul_scope_0.mjs"; // shop model, formulae:
export const sales_ = ({ sales_price_in }) =>
units({ sales_price_in }) * sales_price({ sales_price_in });

export const purchases_ = ({ sales_price_in, purchase_price_in }) =>
units({ sales_price_in }) * purchase_price({ purchase_price_in });

export const profit_ = ({ sales_price_in, purchase_price_in, expenses_in }) =>
sales({ sales_price_in }) - purchases({ sales_price_in, purchase_price_in }) - expenses({ expenses_in });

export const units_ = ({ sales_price_in }) =>
7 * 20000 - sales_price({ sales_price_in }) * 20000;

// inputs:
export const sales_price_ = ({ sales_price_in }) => sales_price_in;
export const purchase_price_ = ({ purchase_price_in }) => purchase_price_in;
export const expenses_ = ({ expenses_in }) => expenses_in;