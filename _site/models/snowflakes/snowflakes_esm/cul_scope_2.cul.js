
    import { memoize } from 'underscore';
    //import memoize from 'lru-memoize';
    //import { isEqual } from 'underscore'; // TODO poor tree shaking support, or why is this impact so massive? Move to lodash/lodash-es?
    
    // import/export non-to memo?

    import { angle_ as angle$, x_ as x$, y_ as y$, time_ as time$, p_ as p$, f_ as f$, w_ as w$, radius_ as radius$, initial_angle_ as initial_angle$, size_ as size$, width_ as width$, height_ as height$ } from './snowflake.cul.js?&+memoed'; // there is already-culed stuff in here, why? imports to memo loader include cul_scope_id, what logic should it apply RE passing forward? eliminate? Probably!

    
    
    

////////// start angle memo-loader code //////////
//const angle$m = memoize(999999, isEqual)(angle$);
export const angle$m = memoize(angle$, JSON.stringify);
export const angle = (a) => {
  return angle$m(a);
  // eslint-disable-next-line no-undef
  angle$(); // never run, but here to "trick" calculang graph logic
};
////////// end angle memo-loader code //////////



////////// start x memo-loader code //////////
//const x$m = memoize(999999, isEqual)(x$);
export const x$m = memoize(x$, JSON.stringify);
export const x = (a) => {
  return x$m(a);
  // eslint-disable-next-line no-undef
  x$(); // never run, but here to "trick" calculang graph logic
};
////////// end x memo-loader code //////////



////////// start y memo-loader code //////////
//const y$m = memoize(999999, isEqual)(y$);
export const y$m = memoize(y$, JSON.stringify);
export const y = (a) => {
  return y$m(a);
  // eslint-disable-next-line no-undef
  y$(); // never run, but here to "trick" calculang graph logic
};
////////// end y memo-loader code //////////



////////// start time memo-loader code //////////
//const time$m = memoize(999999, isEqual)(time$);
export const time$m = memoize(time$, JSON.stringify);
export const time = (a) => {
  return time$m(a);
  // eslint-disable-next-line no-undef
  time$(); // never run, but here to "trick" calculang graph logic
};
////////// end time memo-loader code //////////



////////// start p memo-loader code //////////
//const p$m = memoize(999999, isEqual)(p$);
export const p$m = memoize(p$, JSON.stringify);
export const p = (a) => {
  return p$m(a);
  // eslint-disable-next-line no-undef
  p$(); // never run, but here to "trick" calculang graph logic
};
////////// end p memo-loader code //////////



////////// start f memo-loader code //////////
//const f$m = memoize(999999, isEqual)(f$);
export const f$m = memoize(f$, JSON.stringify);
export const f = (a) => {
  return f$m(a);
  // eslint-disable-next-line no-undef
  f$(); // never run, but here to "trick" calculang graph logic
};
////////// end f memo-loader code //////////



////////// start w memo-loader code //////////
//const w$m = memoize(999999, isEqual)(w$);
export const w$m = memoize(w$, JSON.stringify);
export const w = (a) => {
  return w$m(a);
  // eslint-disable-next-line no-undef
  w$(); // never run, but here to "trick" calculang graph logic
};
////////// end w memo-loader code //////////



////////// start radius memo-loader code //////////
//const radius$m = memoize(999999, isEqual)(radius$);
export const radius$m = memoize(radius$, JSON.stringify);
export const radius = (a) => {
  return radius$m(a);
  // eslint-disable-next-line no-undef
  radius$(); // never run, but here to "trick" calculang graph logic
};
////////// end radius memo-loader code //////////



////////// start initial_angle memo-loader code //////////
//const initial_angle$m = memoize(999999, isEqual)(initial_angle$);
export const initial_angle$m = memoize(initial_angle$, JSON.stringify);
export const initial_angle = (a) => {
  return initial_angle$m(a);
  // eslint-disable-next-line no-undef
  initial_angle$(); // never run, but here to "trick" calculang graph logic
};
////////// end initial_angle memo-loader code //////////



////////// start size memo-loader code //////////
//const size$m = memoize(999999, isEqual)(size$);
export const size$m = memoize(size$, JSON.stringify);
export const size = (a) => {
  return size$m(a);
  // eslint-disable-next-line no-undef
  size$(); // never run, but here to "trick" calculang graph logic
};
////////// end size memo-loader code //////////



////////// start width memo-loader code //////////
//const width$m = memoize(999999, isEqual)(width$);
export const width$m = memoize(width$, JSON.stringify);
export const width = (a) => {
  return width$m(a);
  // eslint-disable-next-line no-undef
  width$(); // never run, but here to "trick" calculang graph logic
};
////////// end width memo-loader code //////////



////////// start height memo-loader code //////////
//const height$m = memoize(999999, isEqual)(height$);
export const height$m = memoize(height$, JSON.stringify);
export const height = (a) => {
  return height$m(a);
  // eslint-disable-next-line no-undef
  height$(); // never run, but here to "trick" calculang graph logic
};
////////// end height memo-loader code //////////


    