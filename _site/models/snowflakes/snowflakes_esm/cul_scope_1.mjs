import { random } from "./cul_scope_0.mjs";import { size } from "./cul_scope_0.mjs";import { initial_angle } from "./cul_scope_0.mjs";import { radius } from "./cul_scope_0.mjs";import { p } from "./cul_scope_0.mjs";import { y } from "./cul_scope_0.mjs";import { x } from "./cul_scope_0.mjs";import { angle } from "./cul_scope_0.mjs";import { height } from "./cul_scope_0.mjs";import { width } from "./cul_scope_0.mjs";import { time } from "./cul_scope_0.mjs"; // this model defines the random distributions of inputs used for different snowflakes.
// I separate from the pure dynamics of a falling snowflake, below

import { time_, width_, height_, angle_, x_, y_, p_ } from "./cul_scope_2.mjs";
export { time_, width_, height_, angle_, x_, y_, p_ };

// radius of sine wave (or amplitude in x)
export const radius_ = ({ p_in, width_in }) =>
Math.sqrt(random({ p_in: p({ p_in }) }) * Math.pow(width({ width_in }) / 2, 2));

export const initial_angle_ = ({ p_in }) => random({ p_in: p({ p_in }), v: 2 }) * 2 * Math.PI;

export const size_ = ({ p_in }) =>
random({ p_in: p({ p_in }), v: 0 }) * 2 + random({ p_in: p({ p_in }), v: 1 }) * 3;

// must compile with memoization and must pass appropriate params into random, to ensure purity
export const random_ = ({}) => Math.random();

/* for seeded random numbers use:
import { prng_alea } from "./esm-seedrandom.mjs";
export const random = () => prng_alea("seed123456")();*/