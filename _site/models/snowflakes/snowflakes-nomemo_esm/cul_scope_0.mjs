// this model defines the random distributions of inputs used for different snowflakes.
// I separate from the pure dynamics of a falling snowflake, below

import { time, width, height, angle, x, y, p } from "./cul_scope_1.mjs";
export { time, width, height, angle, x, y, p };

// radius of sine wave (or amplitude in x)
export const radius = ({ p_in, width_in }) =>
Math.sqrt(random({ p_in: p({ p_in }) }) * Math.pow(width({ width_in }) / 2, 2));

export const initial_angle = ({ p_in }) => random({ p_in: p({ p_in }), v: 2 }) * 2 * Math.PI;

export const size = ({ p_in }) =>
random({ p_in: p({ p_in }), v: 0 }) * 2 + random({ p_in: p({ p_in }), v: 1 }) * 3;

// must compile with memoization and must pass appropriate params into random, to ensure purity
export const random = ({}) => Math.random();

/* for seeded random numbers use:
import { prng_alea } from "./esm-seedrandom.mjs";
export const random = () => prng_alea("seed123456")();*/