import { random } from "./cul_scope_0.mjs";import { size } from "./cul_scope_0.mjs";import { initial_angle } from "./cul_scope_0.mjs";import { radius } from "./cul_scope_0.mjs"; // this model defines dynamics of a falling snowflake

export const angle = ({ w_in, f_in, p_in }) => w({ w_in }) * time({ f_in }) + initial_angle({ p_in });

export const x = ({ width_in, p_in, w_in, f_in }) => width({ width_in }) / 2 + radius({ p_in, width_in }) * Math.sin(angle({ w_in, f_in, p_in }));

export const y = ({ f_in, height_in, p_in }) => {
  if (f({ f_in }) == 0 || y({ height_in, p_in, f_in: f({ f_in }) - 1 }) > height({ height_in }) + 20) return -10;else
  return y({ height_in, p_in, f_in: f({ f_in }) - 1 }) + Math.pow(size({ p_in }), 0.5);
};

export const time = ({ f_in }) => f({ f_in }) / 30;

// inputs:
export const p = ({ p_in }) => p_in; // point or snowflake
export const f = ({ f_in }) => f_in; // frame
export const w = ({ w_in }) => w_in; // angular speed
export const radius_ = ({ radius_in }) => radius_in; // radius of sine wave (or amplitude in x)
export const initial_angle_ = ({ initial_angle_in }) => initial_angle_in;
export const size_ = ({ size_in }) => size_in;
export const width = ({ width_in }) => width_in;
export const height = ({ height_in }) => height_in;