// this model defines the random distributions of inputs used for different snowflakes.
// I separate from the pure dynamics of a falling snowflake, below

import { time, width, height, angle, x, y, p } from "./snowflake.cul.js";
export { time, width, height, angle, x, y, p };

// radius of sine wave (or amplitude in x)
export const radius = () =>
  Math.sqrt(random({ p_in: p() }) * Math.pow(width() / 2, 2));

export const initial_angle = () => random({ p_in: p(), v: 2 }) * 2 * Math.PI;

export const size = () =>
  random({ p_in: p(), v: 0 }) * 2 + random({ p_in: p(), v: 1 }) * 3;

// must compile with memoization and must pass appropriate params into random, to ensure purity
export const random = () => Math.random();

/* for seeded random numbers use:
import { prng_alea } from "./esm-seedrandom.mjs";
export const random = () => prng_alea("seed123456")();*/
