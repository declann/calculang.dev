// https://www.youtube.com/watch?v=0yKf8TrLUOw&t=856s @ 16 minutes

// maze_in is an input 2-d matrix of values to collect. We need to go from top left to bottom right
// we can't walk backwards (but we can calculate backwards :)

// value in the maze for x,y
export const value = ({ maze_in, y_in, x_in }) => maze({ maze_in })[y({ y_in })][x({ x_in })];

// dynamic programming solution:

// maximum value collected after reaching/collected at a square in the maze
// for first row and first col easy: there is only one straight path
// otherwise we calculate maximum of approaches
export const max = ({ x_in, y_in, maze_in }) => {
  if (x({ x_in }) == 0 && y({ y_in }) == 0) return value({ maze_in, y_in, x_in });else
  if (x({ x_in }) == 0) return max({ x_in, maze_in, y_in: y({ y_in }) - 1 }) + value({ maze_in, y_in, x_in });else
  if (y({ y_in }) == 0) return max({ y_in, maze_in, x_in: x({ x_in }) - 1 }) + value({ maze_in, y_in, x_in });else

  return Math.max(max({ x_in, maze_in, y_in: y({ y_in }) - 1 }), max({ y_in, maze_in, x_in: x({ x_in }) - 1 })) + value({ maze_in, y_in, x_in });
};

// starting from the end
export const arrow = ({ x_in, y_in, maze_in }) => {
  if (x({ x_in }) == 0) return "^";else
  if (y({ y_in }) == 0) return "<";else
  return max({ x_in, maze_in, y_in: y({ y_in }) - 1 }) > max({ y_in, maze_in, x_in: x({ x_in }) - 1 }) ? "^" : "<";
};

// If pointed at determines inclusion in arrow_path
export const arrow_path = ({ x_in, y_in, maze_in }) => {
  if (x({ x_in }) == 14 && y({ y_in }) == 14) return arrow({ x_in, y_in, maze_in });else
  if (x({ x_in }) == 14 && pointed_at_from_below({ x_in, y_in, maze_in })) return arrow({ x_in, y_in, maze_in });else
  if (y({ y_in }) == 14 && pointed_at_from_right({ x_in, y_in, maze_in })) return arrow({ x_in, y_in, maze_in });else
  if (x({ x_in }) == 14 || y({ y_in }) == 14) return "";else
  if (pointed_at_from_right({ x_in, y_in, maze_in })) return arrow({ x_in, y_in, maze_in });else
  if (pointed_at_from_below({ x_in, y_in, maze_in })) return arrow({ x_in, y_in, maze_in });else
  return "";
};

// just for readability
export const pointed_at_from_below = ({ x_in, y_in, maze_in }) => arrow_path({ x_in, maze_in, y_in: y({ y_in }) + 1 }) == "^";
export const pointed_at_from_right = ({ x_in, y_in, maze_in }) => arrow_path({ y_in, maze_in, x_in: x({ x_in }) + 1 }) == "<";

// starting from the start
// due to direction change arrow is wrong,
// but run with it and use emojis
export const arrow_path_pretty = ({ x_in, y_in, maze_in }) => {
  if (x({ x_in }) == 0 && y({ y_in }) == 0) return "🐣";else
  if (x({ x_in }) == 14 && y({ y_in }) == 14) return "🏆";else
  return arrow_path({ x_in, y_in, maze_in }) == "^" ? "⬇️" : arrow_path({ x_in, y_in, maze_in }) == "<" ? "➡️‍" : "";
};

// it will be useful to try different approaches to path plotting, including recording the path during calculation; cleanly and careful about side-effects, hmmm
// also create context/credibility: show that other paths are inferior. But this wasn't my challenge for now, if you work on this shout!

// inputs:
export const maze = ({ maze_in }) => maze_in;
export const x = ({ x_in }) => x_in;
export const y = ({ y_in }) => y_in;