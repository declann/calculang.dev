(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else {
		var a = factory();
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "value", function() { return value; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "max", function() { return max; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "arrow", function() { return arrow; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "arrow_path", function() { return arrow_path; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pointed_at_from_below", function() { return pointed_at_from_below; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pointed_at_from_right", function() { return pointed_at_from_right; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "arrow_path_pretty", function() { return arrow_path_pretty; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "maze", function() { return maze; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "x", function() { return x; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "y", function() { return y; });
// https://www.youtube.com/watch?v=0yKf8TrLUOw&t=856s @ 16 minutes

// maze_in is an input 2-d matrix of values to collect. We need to go from top left to bottom right
// we can't walk backwards (but we can calculate backwards :)

// value in the maze for x,y
const value = ({ maze_in, y_in, x_in }) => maze({ maze_in })[y({ y_in })][x({ x_in })];

// dynamic programming solution:

// maximum value collected after reaching/collected at a square in the maze
// for first row and first col easy: there is only one straight path
// otherwise we calculate maximum of approaches
const max = ({ x_in, y_in, maze_in }) => {
  if (x({ x_in }) == 0 && y({ y_in }) == 0) return value({ maze_in, y_in, x_in });else
  if (x({ x_in }) == 0) return max({ x_in, maze_in, y_in: y({ y_in }) - 1 }) + value({ maze_in, y_in, x_in });else
  if (y({ y_in }) == 0) return max({ y_in, maze_in, x_in: x({ x_in }) - 1 }) + value({ maze_in, y_in, x_in });else

  return Math.max(max({ x_in, maze_in, y_in: y({ y_in }) - 1 }), max({ y_in, maze_in, x_in: x({ x_in }) - 1 })) + value({ maze_in, y_in, x_in });
};

// starting from the end
const arrow = ({ x_in, y_in, maze_in }) => {
  if (x({ x_in }) == 0) return "^";else
  if (y({ y_in }) == 0) return "<";else
  return max({ x_in, maze_in, y_in: y({ y_in }) - 1 }) > max({ y_in, maze_in, x_in: x({ x_in }) - 1 }) ? "^" : "<";
};

// If pointed at determines inclusion in arrow_path
const arrow_path = ({ x_in, y_in, maze_in }) => {
  if (x({ x_in }) == 14 && y({ y_in }) == 14) return arrow({ x_in, y_in, maze_in });else
  if (x({ x_in }) == 14 && pointed_at_from_below({ x_in, y_in, maze_in })) return arrow({ x_in, y_in, maze_in });else
  if (y({ y_in }) == 14 && pointed_at_from_right({ x_in, y_in, maze_in })) return arrow({ x_in, y_in, maze_in });else
  if (x({ x_in }) == 14 || y({ y_in }) == 14) return "";else
  if (pointed_at_from_right({ x_in, y_in, maze_in })) return arrow({ x_in, y_in, maze_in });else
  if (pointed_at_from_below({ x_in, y_in, maze_in })) return arrow({ x_in, y_in, maze_in });else
  return "";
};

// just for readability
const pointed_at_from_below = ({ x_in, y_in, maze_in }) => arrow_path({ x_in, maze_in, y_in: y({ y_in }) + 1 }) == "^";
const pointed_at_from_right = ({ x_in, y_in, maze_in }) => arrow_path({ y_in, maze_in, x_in: x({ x_in }) + 1 }) == "<";

// starting from the start
// due to direction change arrow is wrong,
// but run with it and use emojis
const arrow_path_pretty = ({ x_in, y_in, maze_in }) => {
  if (x({ x_in }) == 0 && y({ y_in }) == 0) return "🐣";else
  if (x({ x_in }) == 14 && y({ y_in }) == 14) return "🏆";else
  return arrow_path({ x_in, y_in, maze_in }) == "^" ? "⬇️" : arrow_path({ x_in, y_in, maze_in }) == "<" ? "➡️‍" : "";
};

// it will be useful to try different approaches to path plotting, including recording the path during calculation; cleanly and careful about side-effects, hmmm
// also create context/credibility: show that other paths are inferior. But this wasn't my challenge for now, if you work on this shout!

// inputs:
const maze = ({ maze_in }) => maze_in;
const x = ({ x_in }) => x_in;
const y = ({ y_in }) => y_in;

/***/ })
/******/ ]);
});
//# sourceMappingURL=maze-nomemo.js.map