(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else {
		var a = factory();
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return fund_value; });
/* unused harmony export fund_growth */
/* unused harmony export fund_charges */
/* unused harmony export contribution_charge */
/* unused harmony export check_cfs */
/* unused harmony export check_cfs_vs_fund_value */
/* unused harmony export unit_balance */
/* unused harmony export management_charge_rate */
/* unused harmony export management_charge */
/* unused harmony export management_charge_units */
/* unused harmony export contributions */
/* unused harmony export contribution_units */
/* unused harmony export unit_price */
/* unused harmony export missed_contribution_age */
/* unused harmony export empee_contribution */
/* unused harmony export accumulated_empee_contributions */
/* unused harmony export accumulated_empee_contribution_tax_relief */
/* unused harmony export pension_tax_relief_ratio */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return tax_credits; });
/* unused harmony export empee_contribution_tax_relief */
/* unused harmony export empee_contribution_cost */
/* unused harmony export emper_contribution */
/* unused harmony export salary */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return retirement_fund_value; });
/* unused harmony export salaries_per_retirement_fund */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return age; });
/* unused harmony export age_0 */
/* unused harmony export retirement_age */
/* unused harmony export salary_0 */
/* unused harmony export salary_age_0 */
/* unused harmony export salary_inflation_rate */
/* unused harmony export empee_contribution_rate */
/* unused harmony export emper_matching_rate */
/* unused harmony export unit_growth_rate_ */
/* unused harmony export fund_value_0 */
/* unused harmony export contribution_charge_rate */
/* harmony import */ var _monte_carlo_pension_calculator_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1);
/* harmony import */ var _simple_incometax_cul_cul_scope_id_2_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2);

// disclaimer: This is a work-in-progress model released for some calculang/tooling demonstration purposes and numbers shouldn't be relied upon; there are known model issues.





const fund_value = ({ age_in, age_0_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in }) => unit_balance({ age_in, age_0_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in }) * unit_price({ age_in, age_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in }); // not allowing for multiple funds now

const fund_growth = ({ age_in, age_0_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in }) =>
unit_balance({ age_0_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in, age_in: age({ age_in }) - 1 }) * unit_price({ age_in, age_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in }) -
unit_balance({ age_0_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in, age_in: age({ age_in }) - 1 }) * unit_price({ age_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, age_in: age({ age_in }) - 1 });

const fund_charges = ({ contribution_charge_rate_in, age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, management_charge_rate_in }) => contribution_charge({ contribution_charge_rate_in, age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in }) + management_charge({ age_in, age_0_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in });

const contribution_charge = ({ contribution_charge_rate_in, age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in }) =>
-contribution_charge_rate({ contribution_charge_rate_in }) * (empee_contribution({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in }) + emper_contribution({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in }));

const check_cfs = ({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, paye_band_id_in, gross_salary_in, pension_contribution_in, usc_band_id_in, emper_matching_rate_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, contribution_charge_rate_in, management_charge_rate_in }) =>
empee_contribution_cost({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in }) +
empee_contribution_tax_relief({ age_in, salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in, age_0_in, missed_contribution_age_in, empee_contribution_rate_in }) +
emper_contribution({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in }) +
fund_growth({ age_in, age_0_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in }) +
fund_charges({ contribution_charge_rate_in, age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, management_charge_rate_in });

const check_cfs_vs_fund_value = ({ age_in, age_0_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in, paye_band_id_in, gross_salary_in, pension_contribution_in, usc_band_id_in }) =>
fund_value({ age_in, age_0_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in }) - fund_value({ age_0_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in, age_in: age({ age_in }) - 1 }) - check_cfs({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, paye_band_id_in, gross_salary_in, pension_contribution_in, usc_band_id_in, emper_matching_rate_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, contribution_charge_rate_in, management_charge_rate_in });

const unit_balance = ({ age_in, age_0_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in }) => {
  if (age({ age_in }) <= age_0({ age_0_in }) - 1) return fund_value_0({ fund_value_0_in }) / unit_price({ age_in, age_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in });else

  return (
    unit_balance({ age_0_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in, age_in: age({ age_in }) - 1 }) +
    contribution_units({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in }) +
    management_charge_units({ age_in, age_0_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in }));

  // timing = premium received at start of year and allocated immediately
};

const management_charge_rate = ({ management_charge_rate_in }) => management_charge_rate_in;

const management_charge = ({ age_in, age_0_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in }) => {
  if (age({ age_in }) <= age_0({ age_0_in })) return 0;else
  return -fund_value({ age_0_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in, age_in: age({ age_in }) - 1 }) * management_charge_rate({ management_charge_rate_in });
};

const management_charge_units = ({ age_in, age_0_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in }) => management_charge({ age_in, age_0_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in }) / unit_price({ age_in, age_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in });

const contributions = ({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in }) => empee_contribution({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in }) + emper_contribution({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in });

const contribution_units = ({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in }) =>
contributions({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in }) * (1 - contribution_charge_rate({ contribution_charge_rate_in })) / unit_price({ age_in, age_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in }); // todo, AVCs?

const unit_price = ({ age_in, age_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in }) => {
  if (age({ age_in }) <= age_0({ age_0_in })) return 1;else
  return unit_price({ age_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, age_in: age({ age_in }) - 1 }) * (1 + Object(_monte_carlo_pension_calculator_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_0__["unit_growth_rate"])({ random_seed_in, age_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in }));
};

const missed_contribution_age = ({ missed_contribution_age_in }) =>
missed_contribution_age_in != undefined ? missed_contribution_age_in : -1;

const empee_contribution = ({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in }) => {
  if (
  age({ age_in }) <= age_0({ age_0_in }) - 1 ||
  age({ age_in }) == retirement_age({ retirement_age_in }) ||
  age({ age_in }) == missed_contribution_age({ missed_contribution_age_in }))

  return 0;else
  return salary({ salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in, age_in: age({ age_in }) - 1 }) * empee_contribution_rate({ empee_contribution_rate_in });
};

const accumulated_empee_contributions = ({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in }) => {
  if (age({ age_in }) == age_0({ age_0_in }) - 1) return 0;else

  return (
    accumulated_empee_contributions({ age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, age_in: age({ age_in }) - 1 }) +
    empee_contribution({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in }));

};

const accumulated_empee_contribution_tax_relief = ({ age_in, age_0_in, paye_band_id_in, gross_salary_in, pension_contribution_in, usc_band_id_in, salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in, missed_contribution_age_in, empee_contribution_rate_in }) => {
  if (age({ age_in }) == age_0({ age_0_in }) - 1) return 0;else

  return (
    accumulated_empee_contribution_tax_relief({ age_0_in, paye_band_id_in, gross_salary_in, pension_contribution_in, usc_band_id_in, salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in, missed_contribution_age_in, empee_contribution_rate_in, age_in: age({ age_in }) - 1 }) +
    empee_contribution_tax_relief({ age_in, salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in, age_0_in, missed_contribution_age_in, empee_contribution_rate_in }));

};

const pension_tax_relief_ratio = ({ age_in, salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in, age_0_in, missed_contribution_age_in, empee_contribution_rate_in }) =>
empee_contribution_tax_relief({ age_in, salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in, age_0_in, missed_contribution_age_in, empee_contribution_rate_in }) / empee_contribution({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in });

// assumption for tax relief calculation, 2024 values
const tax_credits = ({}) => 1875 + 1875; // single person + empee paye tax creedits

const empee_contribution_tax_relief = ({ age_in, salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in, age_0_in, missed_contribution_age_in, empee_contribution_rate_in }) =>
Object(_simple_incometax_cul_cul_scope_id_2_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_1__[/* income_tax */ "a"])({ age_in,
  gross_salary_in: salary({ salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in, age_in: age({ age_in }) - 1 }),
  //tax_credits_in: 3000, // ex- I specified like this
  pension_contribution_in: 0 }
) -
Object(_simple_incometax_cul_cul_scope_id_2_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_1__[/* income_tax */ "a"])({ age_in,
  gross_salary_in: salary({ salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in, age_in: age({ age_in }) - 1 }),
  //tax_credits_in: 3000, // ex- I specified like this
  pension_contribution_in: empee_contribution({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in }) }
);

const empee_contribution_cost = ({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in }) =>
empee_contribution({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in }) - empee_contribution_tax_relief({ age_in, salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in, age_0_in, missed_contribution_age_in, empee_contribution_rate_in });

// affected by bug: depends on gross_salary_in, for some reason
// issue #102
/*export const accumulated_empee_contribution_tax_relief = () => {
  if (age() == age_0() - 1) return 0;
  else
    return (
      accumulated_empee_contribution_tax_relief({ age_in: age() - 1 }) +
      empee_contribution_tax_relief()
    );
};*/

const emper_contribution = ({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in }) =>
empee_contribution({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in }) * emper_matching_rate({ emper_matching_rate_in });

const salary = ({ age_in, salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in }) => {
  // at end of year
  if (age({ age_in }) == salary_age_0({ salary_age_0_in }) - 1) return salary_0({ salary_0_in });else
  if (age({ age_in }) >= retirement_age({ retirement_age_in })) return 0;else
  if (age({ age_in }) >= salary_age_0({ salary_age_0_in })) return salary({ salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in, age_in: age({ age_in }) - 1 }) * (1 + salary_inflation_rate({ salary_inflation_rate_in }));else
  return salary({ salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in, age_in: age({ age_in }) + 1 }) / (1 + salary_inflation_rate({ salary_inflation_rate_in }));
};

const retirement_fund_value = ({ age_0_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in }) =>
// at retirement:
fund_value({ age_0_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in, age_in: retirement_age({ retirement_age_in }) });

const salaries_per_retirement_fund = ({ age_0_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in }) =>
retirement_fund_value({ age_0_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in }) / salary({ salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in, age_in: retirement_age({ retirement_age_in }) - 1 });


// inputs:

// using age and age_0 (starting age) as inputs, rather than year/time and age_0.
const age = ({ age_in }) => age_in; // input
const age_0 = ({ age_0_in }) => age_0_in;

const retirement_age = ({ retirement_age_in }) => retirement_age_in;
const salary_0 = ({ salary_0_in }) => salary_0_in;
const salary_age_0 = ({ salary_age_0_in }) => salary_age_0_in; // salary benchmark age is detached from age_0 so that results for different age_0 (starting age) are comparable // maybe this is bad naming
const salary_inflation_rate = ({ salary_inflation_rate_in }) => salary_inflation_rate_in;
const empee_contribution_rate = ({ empee_contribution_rate_in }) => empee_contribution_rate_in;
const emper_matching_rate = ({ emper_matching_rate_in }) => emper_matching_rate_in;

const unit_growth_rate_ = ({ unit_growth_rate_in }) => unit_growth_rate_in;

const fund_value_0 = ({ fund_value_0_in }) => fund_value_0_in;

const contribution_charge_rate = ({ contribution_charge_rate_in }) => contribution_charge_rate_in;

/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "unit_growth_rate", function() { return unit_growth_rate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "u1", function() { return u1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "u2", function() { return u2; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "z0", function() { return z0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "random_seed", function() { return random_seed; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "seeded", function() { return seeded; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "random", function() { return random; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "simulation", function() { return simulation; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "unit_growth_rate_std_dev", function() { return unit_growth_rate_std_dev; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "unit_growth_rate_mean", function() { return unit_growth_rate_mean; });
/* harmony import */ var _pension_calculator_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "retirement_fund_value", function() { return _pension_calculator_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__["c"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "age", function() { return _pension_calculator_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__["a"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "fund_value", function() { return _pension_calculator_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__["b"]; });

/* harmony import */ var _esm_seedrandom_mjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3);



// must compile with memoization and must pass appropriate params into random calls, to ensure purity

const unit_growth_rate = ({ random_seed_in, age_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in }) =>
z0({ random_seed_in, age_in, simulation_in }) * unit_growth_rate_std_dev({ unit_growth_rate_std_dev_in }) + unit_growth_rate_mean({ unit_growth_rate_mean_in });
// vs. uniform:
//random({ age_in: age(), simulation_in: simulation() }) * 0.1;

// box-muller transform to generate z0 a standard normally distributed number
// https://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform
const u1 = ({ random_seed_in, age_in, simulation_in }) =>
random({ random_seed_in, age_in: Object(_pension_calculator_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* age */ "a"])({ age_in }), simulation_in: simulation({ simulation_in }), u: 1 });
const u2 = ({ random_seed_in, age_in, simulation_in }) =>
random({ random_seed_in, age_in: Object(_pension_calculator_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* age */ "a"])({ age_in }), simulation_in: simulation({ simulation_in }), u: 2 });
const z0 = ({ random_seed_in, age_in, simulation_in }) =>
Math.sqrt(-2.0 * Math.log(u1({ random_seed_in, age_in, simulation_in }))) * Math.cos(2 * Math.PI * u2({ random_seed_in, age_in, simulation_in }));
// todo use z1 of previous generation?
//export const z1 = () => Math.sqrt(-2.0 * Math.log(u1())) * Math.sin(2 * Math.PI * u2());


/* seeded random numbers: */
const random_seed = ({ random_seed_in }) => random_seed_in;

const seeded = ({ random_seed_in }) => Object(_esm_seedrandom_mjs__WEBPACK_IMPORTED_MODULE_1__[/* prng_alea */ "a"])(random_seed({ random_seed_in }));
const random = ({ random_seed_in }) => seeded({ random_seed_in: random_seed({ random_seed_in }) })();
// alternative:
//export const random = () => Math.random();

const simulation = ({ simulation_in }) => simulation_in;
const unit_growth_rate_std_dev = ({ unit_growth_rate_std_dev_in }) => unit_growth_rate_std_dev_in;
const unit_growth_rate_mean = ({ unit_growth_rate_mean_in }) => unit_growth_rate_mean_in;

/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export tax_year */
/* unused harmony export net_salary */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return income_tax; });
/* unused harmony export effective_rate */
/* unused harmony export prsi_taxable_salary */
/* unused harmony export prsi */
/* unused harmony export prsi_rate */
/* unused harmony export usc_table */
/* unused harmony export usc_band_id */
/* unused harmony export usc_band_end */
/* unused harmony export usc_band_start */
/* unused harmony export usc_rate */
/* unused harmony export usc_taxable_salary */
/* unused harmony export usc_by_band_id */
/* unused harmony export usc */
/* unused harmony export paye_table */
/* unused harmony export paye_band_id */
/* unused harmony export paye_band_end */
/* unused harmony export paye_band_start */
/* unused harmony export paye_rate */
/* unused harmony export percentage_limit */
/* unused harmony export pension_tax_relief */
/* unused harmony export paye_taxable_salary */
/* unused harmony export paye_by_band_id */
/* unused harmony export paye_over_bands */
/* unused harmony export paye */
/* unused harmony export net_salary_plus_pension_contribution */
/* unused harmony export age_ */
/* unused harmony export gross_salary */
/* unused harmony export tax_credits_ */
/* unused harmony export pension_contribution */
/* harmony import */ var _pension_calculator_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var _monte_carlo_pension_calculator_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1);
 // heavily simplified incometax calculation for Irish incometax
// not reliable and not checked; single person only. Many other limitations
// work in progress

const tax_year = ({}) => 2024; // or 2023

const net_salary = ({ gross_salary_in, pension_contribution_in, age_in }) =>
gross_salary({ gross_salary_in }) - pension_contribution({ pension_contribution_in }) - income_tax({ gross_salary_in, pension_contribution_in, age_in });

const income_tax = ({ gross_salary_in, pension_contribution_in, age_in }) => paye({ gross_salary_in, pension_contribution_in, age_in }) + prsi({ gross_salary_in }) + usc({ gross_salary_in });

const effective_rate = ({ gross_salary_in, pension_contribution_in, age_in }) => 1 - net_salary({ gross_salary_in, pension_contribution_in, age_in }) / gross_salary({ gross_salary_in });

const prsi_taxable_salary = ({ gross_salary_in }) => gross_salary({ gross_salary_in });


// Class A PRSI
// missing: "On 1 October 2024, all PRSI contribution rates increase by 0.1%.
// see: https://www.citizensinformation.ie/en/social-welfare/irish-social-welfare-system/social-insurance-prsi/paying-social-insurance
const prsi = ({ gross_salary_in }) =>
prsi_taxable_salary({ gross_salary_in }) * prsi_rate({}) * (gross_salary({ gross_salary_in }) > 352 * 52 ? 1 : 0); // todo feature flag RE threshold

const prsi_rate = ({}) => 0.04;

// USC, should be mostly abstracted to a table loader
// issues: #11 #76
const usc_table = ({}) => [
{ band_id: 1, band_co: 12012, rate: 0.005 },
{ band_id: 2, band_co: tax_year({}) == 2024 ? 25760 : 22920, rate: 0.02 },
{
  band_id: 3,
  band_co: 70044, // this was off by 100e in error for 2023!
  rate: tax_year({}) == 2024 ? 0.04 : 0.045
},
{ band_id: 4, band_co: 0, rate: 0.08 }
// missing: 11% on self-employed income > 100k
];

const usc_band_id = ({ usc_band_id_in }) => usc_band_id_in;

const usc_band_end = ({ usc_band_id_in }) => {
  if (usc_band_id({ usc_band_id_in }) == usc_table({}).length) return 999999999;
  return usc_table({})[usc_band_id({ usc_band_id_in }) - 1].band_co;
};

const usc_band_start = ({ usc_band_id_in }) => {
  if (usc_band_id({ usc_band_id_in }) == 1) return 0;
  return usc_table({})[usc_band_id({ usc_band_id_in }) - 2].band_co;
};

const usc_rate = ({ usc_band_id_in }) => usc_table({})[usc_band_id({ usc_band_id_in }) - 1].rate;

const usc_taxable_salary = ({ gross_salary_in }) => gross_salary({ gross_salary_in }); // pay usc on pension contribution

const usc_by_band_id = ({ usc_band_id_in, gross_salary_in }) =>
usc_rate({ usc_band_id_in }) *
Math.min(
  usc_band_end({ usc_band_id_in }) - usc_band_start({ usc_band_id_in }),
  Math.max(usc_taxable_salary({ gross_salary_in }) - usc_band_start({ usc_band_id_in }), 0)
);

const usc = ({ gross_salary_in }) =>
usc_table({}).reduce(
  (a, v) => a + usc_by_band_id({ gross_salary_in, usc_band_id_in: v.band_id }),
  0
) * (gross_salary({ gross_salary_in }) > 13000 ? 1 : 0);

// PAYE, "
const paye_table = ({}) => [
{ band_id: 1, band_co: tax_year({}) == 2024 ? 42000 : 40000, rate: 0.2 },
{ band_id: 2, band_co: 100000, rate: 0.4 },
{
  band_id: 3,
  band_co: 0,
  rate: 0.4
}];


const paye_band_id = ({ paye_band_id_in }) => paye_band_id_in;

const paye_band_end = ({ paye_band_id_in }) => {
  if (paye_band_id({ paye_band_id_in }) == paye_table({}).length) return 999999999;
  return paye_table({})[paye_band_id({ paye_band_id_in }) - 1].band_co;
};

const paye_band_start = ({ paye_band_id_in }) => {
  if (paye_band_id({ paye_band_id_in }) == 1) return 0;
  return paye_table({})[paye_band_id({ paye_band_id_in }) - 2].band_co;
};

const paye_rate = ({ paye_band_id_in }) => paye_table({})[paye_band_id({ paye_band_id_in }) - 1].rate;

const percentage_limit = ({ age_in }) => {
  if (Object(_pension_calculator_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* age */ "a"])({ age_in }) < 30) return 0.15;else
  if (Object(_pension_calculator_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* age */ "a"])({ age_in }) < 40) return 0.2;else
  if (Object(_pension_calculator_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* age */ "a"])({ age_in }) < 50) return 0.25;else
  if (Object(_pension_calculator_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* age */ "a"])({ age_in }) < 55) return 0.3;else
  if (Object(_pension_calculator_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* age */ "a"])({ age_in }) < 60) return 0.35;else
  return 0.4;
};

const pension_tax_relief = ({ pension_contribution_in, age_in, gross_salary_in }) =>
// "The maximum amount of earnings taken into account for calculating tax relief is 115k per year". See also 26.3 https://www.revenue.ie/en/tax-professionals/tdm/pensions/chapter-26.pdf
// calcs/approach not particularly validated
// but some results in ./check-pensions-tax-relief.png
Math.min(
  pension_contribution({ pension_contribution_in }),
  percentage_limit({ age_in }) * Math.min(115000, gross_salary({ gross_salary_in }))
);

const paye_taxable_salary = ({ gross_salary_in, pension_contribution_in, age_in }) =>
Math.max(0, gross_salary({ gross_salary_in }) - pension_tax_relief({ pension_contribution_in, age_in, gross_salary_in }));

const paye_by_band_id = ({ paye_band_id_in, gross_salary_in, pension_contribution_in, age_in }) =>
paye_rate({ paye_band_id_in }) *
Math.min(
  paye_band_end({ paye_band_id_in }) - paye_band_start({ paye_band_id_in }),
  Math.max(paye_taxable_salary({ gross_salary_in, pension_contribution_in, age_in }) - paye_band_start({ paye_band_id_in }), 0)
);

const paye_over_bands = ({ gross_salary_in, pension_contribution_in, age_in }) =>
Math.max(
  0,
  paye_table({}).reduce(
    (a, v) => a + paye_by_band_id({ gross_salary_in, pension_contribution_in, age_in, paye_band_id_in: v.band_id }),
    0
  ) //- tax_credit() // input not working here => placed outside. Issue #95
);

const paye = ({ gross_salary_in, pension_contribution_in, age_in }) => Math.max(paye_over_bands({ gross_salary_in, pension_contribution_in, age_in }) - Object(_pension_calculator_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* tax_credits */ "d"])({}), 0);

const net_salary_plus_pension_contribution = ({ gross_salary_in, pension_contribution_in, age_in }) =>
net_salary({ gross_salary_in, pension_contribution_in, age_in }) + pension_contribution({ pension_contribution_in });


// inputs:
const age_ = ({ age_in }) => age_in;
const gross_salary = ({ gross_salary_in }) => gross_salary_in;
const tax_credits_ = ({ tax_credits_in }) => tax_credits_in;
const pension_contribution = ({ pension_contribution_in }) => pension_contribution_in;

/***/ }),
/* 3 */
/***/ (function(__webpack_module__, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return prng_alea; });
/* unused harmony export prng_arc4 */
/* unused harmony export prng_tychei */
/* unused harmony export prng_xor128 */
/* unused harmony export prng_xor4096 */
/* unused harmony export prng_xorshift7 */
/* unused harmony export prng_xorwow */
// DN NOT MY FILE FROM node_modules/esm-seedrandom/esm/index.mjs SEE SOURCEMAP

function _prng_restore(prng, xg, opts) {
  let state = opts && opts.state;
  if (state) {
    if (typeof(state) == 'object') xg.copy(state, xg);
    prng.state = () => xg.copy(xg, {});
  }
}

function _prng_xor_core(xg, opts) {
  let prng = () => (xg.next() >>> 0) / 0x100000000;

  prng.double = () => {
    let top, bot, result;
    do {
      top = xg.next() >>> 11;
      bot = (xg.next() >>> 0) / 0x100000000;
      result = (top + bot) / (1 << 21);
    } while (result === 0);
    return result;
  };

  prng.int32 = () => xg.next() | 0;

  prng.quick = prng;

  _prng_restore(prng, xg, opts);
  return prng;
}

// A port of an algorithm by Johannes Baagøe <baagoe@baagoe.com>, 2010
function prng_alea(seed, opts) {
  let xg = new AleaGen(seed);

  let prng = () => xg.next();

  prng.double = () =>
    prng() + (prng() * 0x200000 | 0) * 1.1102230246251565e-16; // 2^-53

  prng.int32 = () => (xg.next() * 0x100000000) | 0;

  prng.quick = prng;

  _prng_restore(prng, xg, opts);
  return prng
}

class AleaGen {
  constructor(seed) {
    if (seed == null) seed = +(new Date);

    let n = 0xefc8249d;

    // Apply the seeding algorithm from Baagoe.
    this.c = 1;
    this.s0 = mash(' ');
    this.s1 = mash(' ');
    this.s2 = mash(' ');
    this.s0 -= mash(seed);
    if (this.s0 < 0) { this.s0 += 1; }
    this.s1 -= mash(seed);
    if (this.s1 < 0) { this.s1 += 1; }
    this.s2 -= mash(seed);
    if (this.s2 < 0) { this.s2 += 1; }

    function mash(data) {
      data = String(data);
      for (let i = 0; i < data.length; i++) {
        n += data.charCodeAt(i);
        let h = 0.02519603282416938 * n;
        n = h >>> 0;
        h -= n;
        h *= n;
        n = h >>> 0;
        h -= n;
        n += h * 0x100000000; // 2^32
      }
      return (n >>> 0) * 2.3283064365386963e-10; // 2^-32
    }
  }

  next() {
    let {c,s0,s1,s2} = this;
    let t = 2091639 * s0 + c * 2.3283064365386963e-10; // 2^-32
    this.s0 = s1;
    this.s1 = s2;
    return this.s2 = t - (this.c = t | 0);
  }

  copy(f, t) {
    t.c = f.c;
    t.s0 = f.s0;
    t.s1 = f.s1;
    t.s2 = f.s2;
    return t;
  }
}

// A Javascript implementaion of the "xor128" prng algorithm by
function prng_xor128(seed, opts) {
  let xg = new Xor128Gen(seed);
  return _prng_xor_core(xg, opts);
}

class Xor128Gen {
  constructor(seed) {
    if (seed == null) seed = +(new Date);

    let strseed = '';

    this.x = 0;
    this.y = 0;
    this.z = 0;
    this.w = 0;

    if (seed === (seed | 0)) {
      // Integer seed.
      this.x = seed;
    } else {
      // String seed.
      strseed += seed;
    }

    // Mix in string seed, then discard an initial batch of 64 values.
    for (let k = 0; k < strseed.length + 64; k++) {
      this.x ^= strseed.charCodeAt(k) | 0;
      this.next();
    }
  }

  next() {
    let {x,y,z,w} = this;
    let t = x ^ (x << 11);
    this.x = y;
    this.y = z;
    this.z = w;
    return this.w = w ^ ((w >>> 19) ^ t ^ (t >>> 8));
  };

  copy(f, t) {
    t.x = f.x;
    t.y = f.y;
    t.z = f.z;
    t.w = f.w;
    return t;
  }
}

// A Javascript implementaion of the "xorwow" prng algorithm by
function prng_xorwow(seed, opts) {
  let xg = new XorWowGen(seed);
  return _prng_xor_core(xg, opts);
}

class XorWowGen {
  constructor(seed) {
    if (seed == null) seed = +(new Date);

    let strseed = '';

    this.x = 0;
    this.y = 0;
    this.z = 0;
    this.w = 0;
    this.v = 0;

    if (seed === (seed | 0)) {
      // Integer seed.
      this.x = seed;
    } else {
      // String seed.
      strseed += seed;
    }

    // Mix in string seed, then discard an initial batch of 64 values.
    for (let k = 0; k < strseed.length + 64; k++) {
      this.x ^= strseed.charCodeAt(k) | 0;
      if (k == strseed.length) {
        this.d = this.x << 10 ^ this.x >>> 4;
      }
      this.next();
    }
  }

  next() {
    let {x,y,z,w,v,d} = this;
    let t = (x ^ (x >>> 2));
    this.x = y;
    this.y = z;
    this.z = w;
    this.w = v;
    return (this.d = (d + 362437 | 0)) +
       (this.v = (v ^ (v << 4)) ^ (t ^ (t << 1))) | 0;
  };

  copy(f, t) {
    t.x = f.x;
    t.y = f.y;
    t.z = f.z;
    t.w = f.w;
    t.v = f.v;
    t.d = f.d;
    return t;
  }
}

// A Javascript implementaion of the "xorshift7" algorithm by
function prng_xorshift7(seed, opts) {
  let xg = new XorShift7Gen(seed);
  return _prng_xor_core(xg, opts);
}


class XorShift7Gen {
  constructor(seed) {
    if (seed == null) seed = +(new Date);

    var j, w, x = [];

    if (seed === (seed | 0)) {
      // Seed state array using a 32-bit integer.
      w = x[0] = seed;
    } else {
      // Seed state using a string.
      seed = '' + seed;
      for (j = 0; j < seed.length; ++j) {
        x[j & 7] = (x[j & 7] << 15) ^
            (seed.charCodeAt(j) + x[(j + 1) & 7] << 13);
      }
    }

    // Enforce an array length of 8, not all zeroes.
    while (x.length < 8) x.push(0);
    for (j = 0; j < 8 && x[j] === 0; ++j);
    if (j == 8) w = x[7] = -1; else w = x[j];

    this.x = x;
    this.i = 0;

    // Discard an initial 256 values.
    for (j = 256; j > 0; --j) {
      this.next();
    }
  }

  next() {
    // Update xor generator.
    let t, v, {x,i} = this;
    t = x[i]; t ^= (t >>> 7); v = t ^ (t << 24);
    t = x[(i + 1) & 7]; v ^= t ^ (t >>> 10);
    t = x[(i + 3) & 7]; v ^= t ^ (t >>> 3);
    t = x[(i + 4) & 7]; v ^= t ^ (t << 7);
    t = x[(i + 7) & 7]; t = t ^ (t << 13); v ^= t ^ (t << 9);
    x[i] = v;
    this.i = (i + 1) & 7;
    return v;
  };

  copy(f, t) {
    t.x = [... f.x];
    t.i = f.i;
    return t;
  }
}

// A Javascript implementaion of Richard Brent's Xorgens xor4096 algorithm.
function prng_xor4096(seed, opts) {
  let xg = new Xor4096Gen(seed);
  return _prng_xor_core(xg, opts);
}


class Xor4096Gen {
  constructor(seed) {
    if (seed == null) seed = +(new Date);

    let t, v, i, j, w, X = [], limit = 128;
    if (seed === (seed | 0)) {
      // Numeric seeds initialize v, which is used to generates X.
      v = seed;
      seed = null;
    } else {
      // String seeds are mixed into v and X one character at a time.
      seed = seed + '\0';
      v = 0;
      limit = Math.max(limit, seed.length);
    }
    // Initialize circular array and weyl value.
    for (i = 0, j = -32; j < limit; ++j) {
      // Put the unicode characters into the array, and shuffle them.
      if (seed) v ^= seed.charCodeAt((j + 32) % seed.length);
      // After 32 shuffles, take v as the starting w value.
      if (j === 0) w = v;
      v ^= v << 10;
      v ^= v >>> 15;
      v ^= v << 4;
      v ^= v >>> 13;
      if (j >= 0) {
        w = (w + 0x61c88647) | 0;     // Weyl.
        t = (X[j & 127] ^= (v + w));  // Combine xor and weyl to init array.
        i = (0 == t) ? i + 1 : 0;     // Count zeroes.
      }
    }
    // We have detected all zeroes; make the key nonzero.
    if (i >= 128) {
      X[(seed && seed.length || 0) & 127] = -1;
    }
    // Run the generator 512 times to further mix the state before using it.
    // Factoring this as a function slows the main generator, so it is just
    // unrolled here.  The weyl generator is not advanced while warming up.
    i = 127;
    for (j = 4 * 128; j > 0; --j) {
      v = X[(i + 34) & 127];
      t = X[i = ((i + 1) & 127)];
      v ^= v << 13;
      t ^= t << 17;
      v ^= v >>> 15;
      t ^= t >>> 12;
      X[i] = v ^ t;
    }
    // Storing state as object members is faster than using closure variables.
    this.w = w;
    this.X = X;
    this.i = i;
  }

  next() {
    let t, v, {w, X, i} = this;
    // Update Weyl generator.
    this.w = w = (w + 0x61c88647) | 0;
    // Update xor generator.
    v = X[(i + 34) & 127];
    t = X[i = ((i + 1) & 127)];
    v ^= v << 13;
    t ^= t << 17;
    v ^= v >>> 15;
    t ^= t >>> 12;
    // Update Xor generator array state.
    v = X[i] = v ^ t;
    this.i = i;
    // Result is the combination.
    return (v + (w ^ (w >>> 16))) | 0;
  }

  copy(f, t) {
    t.i = f.i;
    t.w = f.w;
    t.X = [... f.X];
    return t;
  }
}

// A Javascript implementaion of the "Tyche-i" prng algorithm by
function prng_tychei(seed, opts) {
  let xg = new TycheiGen(seed);
  return _prng_xor_core(xg, opts);
}

class TycheiGen {
  constructor(seed) {
    if (seed == null) seed = +(new Date);

    let strseed = '';

    this.a = 0;
    this.b = 0;
    this.c = 2654435769 | 0;
    this.d = 1367130551;

    if (seed === Math.floor(seed)) {
      // Integer seed.
      this.a = (seed / 0x100000000) | 0;
      this.b = seed | 0;
    } else {
      // String seed.
      strseed += seed;
    }

    // Mix in string seed, then discard an initial batch of 64 values.
    for (let k = 0; k < strseed.length + 20; k++) {
      this.b ^= strseed.charCodeAt(k) | 0;
      this.next();
    }
  }

  next() {
    let {a,b,c,d} = this;
    b = (b << 25) ^ (b >>> 7) ^ c;
    c = (c - d) | 0;
    d = (d << 24) ^ (d >>> 8) ^ a;
    a = (a - b) | 0;
    this.b = b = (b << 20) ^ (b >>> 12) ^ c;
    this.c = c = (c - d) | 0;
    this.d = (d << 16) ^ (c >>> 16) ^ a;
    return this.a = (a - b) | 0;
  };

  copy(f, t) {
    t.a = f.a;
    t.b = f.b;
    t.c = f.c;
    t.d = f.d;
    return t;
  }
}


/* The following is non-inverted tyche, which has better internal
 * bit diffusion, but which is about 25% slower than tyche-i in JS.
 *

class TycheiGenAlt extends TycheiGen {
  next() {
    let {a,b,c,d} = this
    a = (a + b | 0) >>> 0;
    d = d ^ a; d = d << 16 ^ d >>> 16;
    c = c + d | 0;
    b = b ^ c; b = b << 12 ^ d >>> 20;
    this.a = a = a + b | 0;
    d = d ^ a; this.d = d = d << 8 ^ d >>> 24;
    this.c = c = c + d | 0;
    b = b ^ c;
    return this.b = (b << 7 ^ b >>> 25);
  }
}
*/

/*
Copyright 2019 David Bau.

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/
function prng_arc4(seed, opts) {
  let xg = new ARC4Gen(seed);
  let prng = () => xg.next();

  prng.double = prng;

  prng.int32 = () => xg.g(4) | 0;

  prng.quick = () => xg.g(4) / 0x100000000;

  _prng_restore(prng, xg, opts);
  return prng
}


//
// ARC4
//
// An ARC4 implementation.  The constructor takes a key in the form of
// an array of at most (width) integers that should be 0 <= x < (width).
//
// The g(count) method returns a pseudorandom integer that concatenates
// the next (count) outputs from ARC4.  Its return value is a number x
// that is in the range 0 <= x < (width ^ count).
//

//
// The following constants are related to IEEE 754 limits.
//

// const width = 256 // each RC4 output is 0 <= x < 256
// const chunks = 6 // at least six RC4 outputs for each double
const _arc4_startdenom = 281474976710656;     // 256 ** 6 == width ** chunks
const _arc4_significance = 4503599627370496;  // 2 ** 52 significant digits in a double
const _arc4_overflow = 9007199254740992;      // 2 ** 53 == significance * 2


class ARC4Gen {
  constructor(seed) {
    if (seed == null) seed = +(new Date);

    let key = this.mixkey(seed, []);
    let i,j,t, S=[], keylen = key.length;
    this.i = this.j = i = j = 0;
    this.S = S;

    // The empty key [] is treated as [0].
    if (!keylen) { key = [keylen++]; }

    // Set up S using the standard key scheduling algorithm.
    while (i <= 0xff) {
      S[i] = i++;
    }
    for (i = 0; i <= 0xff; i++) {
      S[i] = S[j = 0xff & (j + key[i % keylen] + (t = S[i]))];
      S[j] = t;
    }

    // For robust unpredictability, the function call below automatically
    // discards an initial batch of values.  This is called RC4-drop[256].
    // See http://google.com/search?q=rsa+fluhrer+response&btnI
    this.g(256);
  }

  next() {
    // This function returns a random double in [0, 1) that contains
    // randomness in every bit of the mantissa of the IEEE 754 value.

    let n = this.g(6);                  // Start with a numerator n < 2 ^ 48
    let d = _arc4_startdenom;           //   and denominator d = 2 ^ 48.
    let x = 0;                          //   and no 'extra last byte'.

    while (n < _arc4_significance) {    // Fill up all significant digits (2 ** 52)
      n = (n + x) * 256;                //   by shifting numerator and
      d *= 256;                         //   denominator and generating a
      x = this.g(1);                    //   new least-significant-byte.
    }
    while (n >= _arc4_overflow) {       // To avoid rounding past overflow, before adding
      n /= 2;                           //   last byte, shift everything
      d /= 2;                           //   right using integer math until
      x >>>= 1;                         //   we have exactly the desired bits.
    }
    return (n + x) / d;                 // Form the number within [0, 1).
  }

  g(count) {
    // The "g" method returns the next (count) outputs as one number.
    let t, r = 0, {i,j,S} = this;
    while (count--) {
      t = S[i = 0xff & (i + 1)];
      r = r * 256 + S[0xff & ((S[i] = S[j = 0xff & (j + t)]) + (S[j] = t))];
    }
    this.i = i;
    this.j = j;
    return r;
  }

  copy(f, t) {
    t.i = f.i;
    t.j = f.j;
    t.S = [... f.S];
    return t;
  }

  mixkey(seed, key) {
    seed = seed + '';
    let smear=0, j=0;
    while (j < seed.length) {
      key[0xff & j] =
        0xff & ((smear ^= key[0xff & j] * 19) + seed.charCodeAt(j++));
    }
    return key
  }
}



// DN I COPIED THIS FROM node_modules/esm-seedrandom/esm/index.mjs

//# sourceMappingURL=esm-seedrandom.mjs.map


/***/ })
/******/ ]);
});
//# sourceMappingURL=monte-carlo-pension-calculator-nomemo.js.map