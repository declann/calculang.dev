import {
  retirement_fund_value,
  age,
  fund_value } from
"./cul_scope_1.mjs";
export { retirement_fund_value, age, fund_value };

// must compile with memoization and must pass appropriate params into random calls, to ensure purity

export const unit_growth_rate = ({ random_seed_in, age_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in }) =>
z0({ random_seed_in, age_in, simulation_in }) * unit_growth_rate_std_dev({ unit_growth_rate_std_dev_in }) + unit_growth_rate_mean({ unit_growth_rate_mean_in });
// vs. uniform:
//random({ age_in: age(), simulation_in: simulation() }) * 0.1;

// box-muller transform to generate z0 a standard normally distributed number
// https://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform
export const u1 = ({ random_seed_in, age_in, simulation_in }) =>
random({ random_seed_in, age_in: age({ age_in }), simulation_in: simulation({ simulation_in }), u: 1 });
export const u2 = ({ random_seed_in, age_in, simulation_in }) =>
random({ random_seed_in, age_in: age({ age_in }), simulation_in: simulation({ simulation_in }), u: 2 });
export const z0 = ({ random_seed_in, age_in, simulation_in }) =>
Math.sqrt(-2.0 * Math.log(u1({ random_seed_in, age_in, simulation_in }))) * Math.cos(2 * Math.PI * u2({ random_seed_in, age_in, simulation_in }));
// todo use z1 of previous generation?
//export const z1 = () => Math.sqrt(-2.0 * Math.log(u1())) * Math.sin(2 * Math.PI * u2());


/* seeded random numbers: */
export const random_seed = ({ random_seed_in }) => random_seed_in;
import { prng_alea } from "./esm-seedrandom.mjs";
export const seeded = ({ random_seed_in }) => prng_alea(random_seed({ random_seed_in }));
export const random = ({ random_seed_in }) => seeded({ random_seed_in: random_seed({ random_seed_in }) })();
// alternative:
//export const random = () => Math.random();

export const simulation = ({ simulation_in }) => simulation_in;
export const unit_growth_rate_std_dev = ({ unit_growth_rate_std_dev_in }) => unit_growth_rate_std_dev_in;
export const unit_growth_rate_mean = ({ unit_growth_rate_mean_in }) => unit_growth_rate_mean_in;