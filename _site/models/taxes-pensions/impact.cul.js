/*

This was the first good and practical test of modularity and ♻️ formulae

  The modules are split in meaningful ways:

  - `simple-incometax` is a simplified incometax calc: it's the same model I use in payroll models and in the pension calculator model
  - `incometax-set` does a population-level calculation
  - `proposed` adds control over underlying tax rates, and
  - `impact` is just subtractions: the proposed calc versus the default calc...

  What explains difference between those 2 calcs? From the module relationships graph: the `proposed.cul.js` file entirely encapsulates that, so look there. (the population calc - and by extension the income tax formulae, are shared)

  This code looks mental, but it is repeating the same patterns (clearly can/will be generated).
*/



// reminder to self when I use this: simple-incometax updates added not included in released v

import {
  income_tax_sum as income_tax_sum_current,
  income_tax as income_tax_current,
  effective_rate as effective_rate_current,
  paye_sum as paye_sum_current,
  paye as paye_current,
  usc_sum as usc_sum_current,
  usc as usc_current,
  prsi_sum as prsi_sum_current,
  prsi as prsi_current,
  income_tax_by_taxpayer as income_tax_by_taxpayer_current,
  usc_by_taxpayer as usc_by_taxpayer_current,
  prsi_by_taxpayer as prsi_by_taxpayer_current,
  paye_by_taxpayer as paye_by_taxpayer_current,
} from './incometax-set.cul.js';
import {
  income_tax_sum as income_tax_sum_proposed,
  income_tax as income_tax_proposed,
  effective_rate as effective_rate_proposed,
  paye_sum as paye_sum_proposed,
  paye as paye_proposed,
  usc_sum as usc_sum_proposed,
  usc as usc_proposed,
  prsi_sum as prsi_sum_proposed,
  prsi as prsi_proposed,
  income_tax_by_taxpayer as income_tax_by_taxpayer_proposed,
  usc_by_taxpayer as usc_by_taxpayer_proposed,
  prsi_by_taxpayer as prsi_by_taxpayer_proposed,
  paye_by_taxpayer as paye_by_taxpayer_proposed,
} from './proposed.cul.js';

export { income_tax_sum_current, income_tax_sum_proposed };
export { income_tax_current, income_tax_proposed };
export { usc_sum_current, usc_sum_proposed };
export { prsi_sum_current, prsi_sum_proposed };
export { usc_current, usc_proposed };
export { prsi_current, prsi_proposed };
export { paye_sum_current, paye_sum_proposed };
export { paye_current, paye_proposed };
export { effective_rate_current, effective_rate_proposed };
export {
  paye_by_taxpayer_proposed,
  income_tax_by_taxpayer_proposed,
  usc_by_taxpayer_proposed,
  prsi_by_taxpayer_proposed,
};
export {
  paye_by_taxpayer_current,
  income_tax_by_taxpayer_current,
  usc_by_taxpayer_current,
  prsi_by_taxpayer_current,
};

export const income_tax_by_taxpayer_impact = () =>
  income_tax_by_taxpayer_proposed() - income_tax_by_taxpayer_current();
export const usc_by_taxpayer_impact = () =>
  usc_by_taxpayer_proposed() - usc_by_taxpayer_current();
export const paye_by_taxpayer_impact = () =>
  paye_by_taxpayer_proposed() - paye_by_taxpayer_current();
export const prsi_by_taxpayer_impact = () =>
  prsi_by_taxpayer_proposed() - prsi_by_taxpayer_current();


export const income_tax_sum_impact = () =>
  income_tax_sum_proposed() - income_tax_sum_current();

export const income_tax_impact = () =>
  income_tax_proposed() - income_tax_current();

export const paye_sum_impact = () => paye_sum_proposed() - paye_sum_current();

export const paye_impact = () => paye_proposed() - paye_current();

export const usc_sum_impact = () => usc_sum_proposed() - usc_sum_current();

export const usc_impact = () => usc_proposed() - usc_current();

export const prsi_sum_impact = () => prsi_sum_proposed() - prsi_sum_current();

export const prsi_impact = () => prsi_proposed() - prsi_current();

// test mitigate application refactor
export const pension_contribution = () => 0;