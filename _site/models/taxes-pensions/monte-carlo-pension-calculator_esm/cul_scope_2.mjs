import { unit_growth_rate_mean } from "./cul_scope_0.mjs";import { unit_growth_rate_std_dev } from "./cul_scope_0.mjs";import { simulation } from "./cul_scope_0.mjs";import { random } from "./cul_scope_0.mjs";import { seeded } from "./cul_scope_0.mjs";import { random_seed } from "./cul_scope_0.mjs";import { z0 } from "./cul_scope_0.mjs";import { u2 } from "./cul_scope_0.mjs";import { u1 } from "./cul_scope_0.mjs";import { unit_growth_rate } from "./cul_scope_0.mjs";import { fund_value } from "./cul_scope_0.mjs";import { age } from "./cul_scope_0.mjs";import { retirement_fund_value } from "./cul_scope_0.mjs";
import { memoize } from 'underscore';
//import memoize from 'lru-memoize';
//import { isEqual } from 'underscore'; // TODO poor tree shaking support, or why is this impact so massive? Move to lodash/lodash-es?

// import/export non-to memo?

import { income_tax_ as income_tax$, fund_value_ as fund_value$, fund_growth_ as fund_growth$, fund_charges_ as fund_charges$, contribution_charge_ as contribution_charge$, check_cfs_ as check_cfs$, check_cfs_vs_fund_value_ as check_cfs_vs_fund_value$, unit_balance_ as unit_balance$, management_charge_rate_ as management_charge_rate$, management_charge_ as management_charge$, management_charge_units_ as management_charge_units$, contributions_ as contributions$, contribution_units_ as contribution_units$, unit_price_ as unit_price$, missed_contribution_age_ as missed_contribution_age$, empee_contribution_ as empee_contribution$, accumulated_empee_contributions_ as accumulated_empee_contributions$, accumulated_empee_contribution_tax_relief_ as accumulated_empee_contribution_tax_relief$, pension_tax_relief_ratio_ as pension_tax_relief_ratio$, tax_credits_ as tax_credits$, empee_contribution_tax_relief_ as empee_contribution_tax_relief$, empee_contribution_cost_ as empee_contribution_cost$, emper_contribution_ as emper_contribution$, salary_ as salary$, retirement_fund_value_ as retirement_fund_value$, salaries_per_retirement_fund_ as salaries_per_retirement_fund$, age_ as age$, age_0_ as age_0$, retirement_age_ as retirement_age$, salary_0_ as salary_0$, salary_age_0_ as salary_age_0$, salary_inflation_rate_ as salary_inflation_rate$, empee_contribution_rate_ as empee_contribution_rate$, emper_matching_rate_ as emper_matching_rate$, unit_growth_rate_ as unit_growth_rate$, fund_value_0_ as fund_value_0$, contribution_charge_rate_ as contribution_charge_rate$ } from "./cul_scope_3.mjs"; // there is already-culed stuff in here, why? imports to memo loader include cul_scope_id, what logic should it apply RE passing forward? eliminate? Probably!





////////// start income_tax memo-loader code //////////
//const income_tax$m = memoize(999999, isEqual)(income_tax$);
export const income_tax$m = memoize(income_tax$, JSON.stringify);
export const income_tax = (a) => {
  return income_tax$m(a);
  // eslint-disable-next-line no-undef
  income_tax$({ gross_salary_in, pension_contribution_in, age_in }); // never run, but here to "trick" calculang graph logic
};
////////// end income_tax memo-loader code //////////



////////// start fund_value memo-loader code //////////
//const fund_value$m = memoize(999999, isEqual)(fund_value$);
export const fund_value$m = memoize(fund_value$, JSON.stringify);
export const fund_value_ = (a) => {
  return fund_value$m(a);
  // eslint-disable-next-line no-undef
  fund_value$({ age_in, age_0_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in }); // never run, but here to "trick" calculang graph logic
};
////////// end fund_value memo-loader code //////////



////////// start fund_growth memo-loader code //////////
//const fund_growth$m = memoize(999999, isEqual)(fund_growth$);
export const fund_growth$m = memoize(fund_growth$, JSON.stringify);
export const fund_growth = (a) => {
  return fund_growth$m(a);
  // eslint-disable-next-line no-undef
  fund_growth$({ age_in, age_0_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in }); // never run, but here to "trick" calculang graph logic
};
////////// end fund_growth memo-loader code //////////



////////// start fund_charges memo-loader code //////////
//const fund_charges$m = memoize(999999, isEqual)(fund_charges$);
export const fund_charges$m = memoize(fund_charges$, JSON.stringify);
export const fund_charges = (a) => {
  return fund_charges$m(a);
  // eslint-disable-next-line no-undef
  fund_charges$({ contribution_charge_rate_in, age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, management_charge_rate_in }); // never run, but here to "trick" calculang graph logic
};
////////// end fund_charges memo-loader code //////////



////////// start contribution_charge memo-loader code //////////
//const contribution_charge$m = memoize(999999, isEqual)(contribution_charge$);
export const contribution_charge$m = memoize(contribution_charge$, JSON.stringify);
export const contribution_charge = (a) => {
  return contribution_charge$m(a);
  // eslint-disable-next-line no-undef
  contribution_charge$({ contribution_charge_rate_in, age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in }); // never run, but here to "trick" calculang graph logic
};
////////// end contribution_charge memo-loader code //////////



////////// start check_cfs memo-loader code //////////
//const check_cfs$m = memoize(999999, isEqual)(check_cfs$);
export const check_cfs$m = memoize(check_cfs$, JSON.stringify);
export const check_cfs = (a) => {
  return check_cfs$m(a);
  // eslint-disable-next-line no-undef
  check_cfs$({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, paye_band_id_in, gross_salary_in, pension_contribution_in, usc_band_id_in, emper_matching_rate_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, contribution_charge_rate_in, management_charge_rate_in }); // never run, but here to "trick" calculang graph logic
};
////////// end check_cfs memo-loader code //////////



////////// start check_cfs_vs_fund_value memo-loader code //////////
//const check_cfs_vs_fund_value$m = memoize(999999, isEqual)(check_cfs_vs_fund_value$);
export const check_cfs_vs_fund_value$m = memoize(check_cfs_vs_fund_value$, JSON.stringify);
export const check_cfs_vs_fund_value = (a) => {
  return check_cfs_vs_fund_value$m(a);
  // eslint-disable-next-line no-undef
  check_cfs_vs_fund_value$({ age_in, age_0_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in, paye_band_id_in, gross_salary_in, pension_contribution_in, usc_band_id_in }); // never run, but here to "trick" calculang graph logic
};
////////// end check_cfs_vs_fund_value memo-loader code //////////



////////// start unit_balance memo-loader code //////////
//const unit_balance$m = memoize(999999, isEqual)(unit_balance$);
export const unit_balance$m = memoize(unit_balance$, JSON.stringify);
export const unit_balance = (a) => {
  return unit_balance$m(a);
  // eslint-disable-next-line no-undef
  unit_balance$({ age_in, age_0_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in }); // never run, but here to "trick" calculang graph logic
};
////////// end unit_balance memo-loader code //////////



////////// start management_charge_rate memo-loader code //////////
//const management_charge_rate$m = memoize(999999, isEqual)(management_charge_rate$);
export const management_charge_rate$m = memoize(management_charge_rate$, JSON.stringify);
export const management_charge_rate = (a) => {
  return management_charge_rate$m(a);
  // eslint-disable-next-line no-undef
  management_charge_rate$({ management_charge_rate_in }); // never run, but here to "trick" calculang graph logic
};
////////// end management_charge_rate memo-loader code //////////



////////// start management_charge memo-loader code //////////
//const management_charge$m = memoize(999999, isEqual)(management_charge$);
export const management_charge$m = memoize(management_charge$, JSON.stringify);
export const management_charge = (a) => {
  return management_charge$m(a);
  // eslint-disable-next-line no-undef
  management_charge$({ age_in, age_0_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in }); // never run, but here to "trick" calculang graph logic
};
////////// end management_charge memo-loader code //////////



////////// start management_charge_units memo-loader code //////////
//const management_charge_units$m = memoize(999999, isEqual)(management_charge_units$);
export const management_charge_units$m = memoize(management_charge_units$, JSON.stringify);
export const management_charge_units = (a) => {
  return management_charge_units$m(a);
  // eslint-disable-next-line no-undef
  management_charge_units$({ age_in, age_0_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in }); // never run, but here to "trick" calculang graph logic
};
////////// end management_charge_units memo-loader code //////////



////////// start contributions memo-loader code //////////
//const contributions$m = memoize(999999, isEqual)(contributions$);
export const contributions$m = memoize(contributions$, JSON.stringify);
export const contributions = (a) => {
  return contributions$m(a);
  // eslint-disable-next-line no-undef
  contributions$({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in }); // never run, but here to "trick" calculang graph logic
};
////////// end contributions memo-loader code //////////



////////// start contribution_units memo-loader code //////////
//const contribution_units$m = memoize(999999, isEqual)(contribution_units$);
export const contribution_units$m = memoize(contribution_units$, JSON.stringify);
export const contribution_units = (a) => {
  return contribution_units$m(a);
  // eslint-disable-next-line no-undef
  contribution_units$({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in }); // never run, but here to "trick" calculang graph logic
};
////////// end contribution_units memo-loader code //////////



////////// start unit_price memo-loader code //////////
//const unit_price$m = memoize(999999, isEqual)(unit_price$);
export const unit_price$m = memoize(unit_price$, JSON.stringify);
export const unit_price = (a) => {
  return unit_price$m(a);
  // eslint-disable-next-line no-undef
  unit_price$({ age_in, age_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in }); // never run, but here to "trick" calculang graph logic
};
////////// end unit_price memo-loader code //////////



////////// start missed_contribution_age memo-loader code //////////
//const missed_contribution_age$m = memoize(999999, isEqual)(missed_contribution_age$);
export const missed_contribution_age$m = memoize(missed_contribution_age$, JSON.stringify);
export const missed_contribution_age = (a) => {
  return missed_contribution_age$m(a);
  // eslint-disable-next-line no-undef
  missed_contribution_age$({ missed_contribution_age_in }); // never run, but here to "trick" calculang graph logic
};
////////// end missed_contribution_age memo-loader code //////////



////////// start empee_contribution memo-loader code //////////
//const empee_contribution$m = memoize(999999, isEqual)(empee_contribution$);
export const empee_contribution$m = memoize(empee_contribution$, JSON.stringify);
export const empee_contribution = (a) => {
  return empee_contribution$m(a);
  // eslint-disable-next-line no-undef
  empee_contribution$({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in }); // never run, but here to "trick" calculang graph logic
};
////////// end empee_contribution memo-loader code //////////



////////// start accumulated_empee_contributions memo-loader code //////////
//const accumulated_empee_contributions$m = memoize(999999, isEqual)(accumulated_empee_contributions$);
export const accumulated_empee_contributions$m = memoize(accumulated_empee_contributions$, JSON.stringify);
export const accumulated_empee_contributions = (a) => {
  return accumulated_empee_contributions$m(a);
  // eslint-disable-next-line no-undef
  accumulated_empee_contributions$({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in }); // never run, but here to "trick" calculang graph logic
};
////////// end accumulated_empee_contributions memo-loader code //////////



////////// start accumulated_empee_contribution_tax_relief memo-loader code //////////
//const accumulated_empee_contribution_tax_relief$m = memoize(999999, isEqual)(accumulated_empee_contribution_tax_relief$);
export const accumulated_empee_contribution_tax_relief$m = memoize(accumulated_empee_contribution_tax_relief$, JSON.stringify);
export const accumulated_empee_contribution_tax_relief = (a) => {
  return accumulated_empee_contribution_tax_relief$m(a);
  // eslint-disable-next-line no-undef
  accumulated_empee_contribution_tax_relief$({ age_in, age_0_in, paye_band_id_in, gross_salary_in, pension_contribution_in, usc_band_id_in, salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in, missed_contribution_age_in, empee_contribution_rate_in }); // never run, but here to "trick" calculang graph logic
};
////////// end accumulated_empee_contribution_tax_relief memo-loader code //////////



////////// start pension_tax_relief_ratio memo-loader code //////////
//const pension_tax_relief_ratio$m = memoize(999999, isEqual)(pension_tax_relief_ratio$);
export const pension_tax_relief_ratio$m = memoize(pension_tax_relief_ratio$, JSON.stringify);
export const pension_tax_relief_ratio = (a) => {
  return pension_tax_relief_ratio$m(a);
  // eslint-disable-next-line no-undef
  pension_tax_relief_ratio$({ age_in, salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in, age_0_in, missed_contribution_age_in, empee_contribution_rate_in }); // never run, but here to "trick" calculang graph logic
};
////////// end pension_tax_relief_ratio memo-loader code //////////



////////// start tax_credits memo-loader code //////////
//const tax_credits$m = memoize(999999, isEqual)(tax_credits$);
export const tax_credits$m = memoize(tax_credits$, JSON.stringify);
export const tax_credits = (a) => {
  return tax_credits$m(a);
  // eslint-disable-next-line no-undef
  tax_credits$({}); // never run, but here to "trick" calculang graph logic
};
////////// end tax_credits memo-loader code //////////



////////// start empee_contribution_tax_relief memo-loader code //////////
//const empee_contribution_tax_relief$m = memoize(999999, isEqual)(empee_contribution_tax_relief$);
export const empee_contribution_tax_relief$m = memoize(empee_contribution_tax_relief$, JSON.stringify);
export const empee_contribution_tax_relief = (a) => {
  return empee_contribution_tax_relief$m(a);
  // eslint-disable-next-line no-undef
  empee_contribution_tax_relief$({ age_in, salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in, age_0_in, missed_contribution_age_in, empee_contribution_rate_in }); // never run, but here to "trick" calculang graph logic
};
////////// end empee_contribution_tax_relief memo-loader code //////////



////////// start empee_contribution_cost memo-loader code //////////
//const empee_contribution_cost$m = memoize(999999, isEqual)(empee_contribution_cost$);
export const empee_contribution_cost$m = memoize(empee_contribution_cost$, JSON.stringify);
export const empee_contribution_cost = (a) => {
  return empee_contribution_cost$m(a);
  // eslint-disable-next-line no-undef
  empee_contribution_cost$({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in }); // never run, but here to "trick" calculang graph logic
};
////////// end empee_contribution_cost memo-loader code //////////



////////// start emper_contribution memo-loader code //////////
//const emper_contribution$m = memoize(999999, isEqual)(emper_contribution$);
export const emper_contribution$m = memoize(emper_contribution$, JSON.stringify);
export const emper_contribution = (a) => {
  return emper_contribution$m(a);
  // eslint-disable-next-line no-undef
  emper_contribution$({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in }); // never run, but here to "trick" calculang graph logic
};
////////// end emper_contribution memo-loader code //////////



////////// start salary memo-loader code //////////
//const salary$m = memoize(999999, isEqual)(salary$);
export const salary$m = memoize(salary$, JSON.stringify);
export const salary = (a) => {
  return salary$m(a);
  // eslint-disable-next-line no-undef
  salary$({ age_in, salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in }); // never run, but here to "trick" calculang graph logic
};
////////// end salary memo-loader code //////////



////////// start retirement_fund_value memo-loader code //////////
//const retirement_fund_value$m = memoize(999999, isEqual)(retirement_fund_value$);
export const retirement_fund_value$m = memoize(retirement_fund_value$, JSON.stringify);
export const retirement_fund_value_ = (a) => {
  return retirement_fund_value$m(a);
  // eslint-disable-next-line no-undef
  retirement_fund_value$({ age_0_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in }); // never run, but here to "trick" calculang graph logic
};
////////// end retirement_fund_value memo-loader code //////////



////////// start salaries_per_retirement_fund memo-loader code //////////
//const salaries_per_retirement_fund$m = memoize(999999, isEqual)(salaries_per_retirement_fund$);
export const salaries_per_retirement_fund$m = memoize(salaries_per_retirement_fund$, JSON.stringify);
export const salaries_per_retirement_fund = (a) => {
  return salaries_per_retirement_fund$m(a);
  // eslint-disable-next-line no-undef
  salaries_per_retirement_fund$({ age_0_in, fund_value_0_in, random_seed_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in }); // never run, but here to "trick" calculang graph logic
};
////////// end salaries_per_retirement_fund memo-loader code //////////



////////// start age memo-loader code //////////
//const age$m = memoize(999999, isEqual)(age$);
export const age$m = memoize(age$, JSON.stringify);
export const age_ = (a) => {
  return age$m(a);
  // eslint-disable-next-line no-undef
  age$({ age_in }); // never run, but here to "trick" calculang graph logic
};
////////// end age memo-loader code //////////



////////// start age_0 memo-loader code //////////
//const age_0$m = memoize(999999, isEqual)(age_0$);
export const age_0$m = memoize(age_0$, JSON.stringify);
export const age_0 = (a) => {
  return age_0$m(a);
  // eslint-disable-next-line no-undef
  age_0$({ age_0_in }); // never run, but here to "trick" calculang graph logic
};
////////// end age_0 memo-loader code //////////



////////// start retirement_age memo-loader code //////////
//const retirement_age$m = memoize(999999, isEqual)(retirement_age$);
export const retirement_age$m = memoize(retirement_age$, JSON.stringify);
export const retirement_age = (a) => {
  return retirement_age$m(a);
  // eslint-disable-next-line no-undef
  retirement_age$({ retirement_age_in }); // never run, but here to "trick" calculang graph logic
};
////////// end retirement_age memo-loader code //////////



////////// start salary_0 memo-loader code //////////
//const salary_0$m = memoize(999999, isEqual)(salary_0$);
export const salary_0$m = memoize(salary_0$, JSON.stringify);
export const salary_0 = (a) => {
  return salary_0$m(a);
  // eslint-disable-next-line no-undef
  salary_0$({ salary_0_in }); // never run, but here to "trick" calculang graph logic
};
////////// end salary_0 memo-loader code //////////



////////// start salary_age_0 memo-loader code //////////
//const salary_age_0$m = memoize(999999, isEqual)(salary_age_0$);
export const salary_age_0$m = memoize(salary_age_0$, JSON.stringify);
export const salary_age_0 = (a) => {
  return salary_age_0$m(a);
  // eslint-disable-next-line no-undef
  salary_age_0$({ salary_age_0_in }); // never run, but here to "trick" calculang graph logic
};
////////// end salary_age_0 memo-loader code //////////



////////// start salary_inflation_rate memo-loader code //////////
//const salary_inflation_rate$m = memoize(999999, isEqual)(salary_inflation_rate$);
export const salary_inflation_rate$m = memoize(salary_inflation_rate$, JSON.stringify);
export const salary_inflation_rate = (a) => {
  return salary_inflation_rate$m(a);
  // eslint-disable-next-line no-undef
  salary_inflation_rate$({ salary_inflation_rate_in }); // never run, but here to "trick" calculang graph logic
};
////////// end salary_inflation_rate memo-loader code //////////



////////// start empee_contribution_rate memo-loader code //////////
//const empee_contribution_rate$m = memoize(999999, isEqual)(empee_contribution_rate$);
export const empee_contribution_rate$m = memoize(empee_contribution_rate$, JSON.stringify);
export const empee_contribution_rate = (a) => {
  return empee_contribution_rate$m(a);
  // eslint-disable-next-line no-undef
  empee_contribution_rate$({ empee_contribution_rate_in }); // never run, but here to "trick" calculang graph logic
};
////////// end empee_contribution_rate memo-loader code //////////



////////// start emper_matching_rate memo-loader code //////////
//const emper_matching_rate$m = memoize(999999, isEqual)(emper_matching_rate$);
export const emper_matching_rate$m = memoize(emper_matching_rate$, JSON.stringify);
export const emper_matching_rate = (a) => {
  return emper_matching_rate$m(a);
  // eslint-disable-next-line no-undef
  emper_matching_rate$({ emper_matching_rate_in }); // never run, but here to "trick" calculang graph logic
};
////////// end emper_matching_rate memo-loader code //////////



////////// start unit_growth_rate memo-loader code //////////
//const unit_growth_rate$m = memoize(999999, isEqual)(unit_growth_rate$);
export const unit_growth_rate$m = memoize(unit_growth_rate$, JSON.stringify);
export const unit_growth_rate_ = (a) => {
  return unit_growth_rate$m(a);
  // eslint-disable-next-line no-undef
  unit_growth_rate$({ unit_growth_rate_in }); // never run, but here to "trick" calculang graph logic
};
////////// end unit_growth_rate memo-loader code //////////



////////// start fund_value_0 memo-loader code //////////
//const fund_value_0$m = memoize(999999, isEqual)(fund_value_0$);
export const fund_value_0$m = memoize(fund_value_0$, JSON.stringify);
export const fund_value_0 = (a) => {
  return fund_value_0$m(a);
  // eslint-disable-next-line no-undef
  fund_value_0$({ fund_value_0_in }); // never run, but here to "trick" calculang graph logic
};
////////// end fund_value_0 memo-loader code //////////



////////// start contribution_charge_rate memo-loader code //////////
//const contribution_charge_rate$m = memoize(999999, isEqual)(contribution_charge_rate$);
export const contribution_charge_rate$m = memoize(contribution_charge_rate$, JSON.stringify);
export const contribution_charge_rate = (a) => {
  return contribution_charge_rate$m(a);
  // eslint-disable-next-line no-undef
  contribution_charge_rate$({ contribution_charge_rate_in }); // never run, but here to "trick" calculang graph logic
};
////////// end contribution_charge_rate memo-loader code //////////