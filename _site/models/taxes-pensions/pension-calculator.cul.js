
// disclaimer: This is a work-in-progress model released for some calculang/tooling demonstration purposes and numbers shouldn't be relied upon; there are known model issues.


import { income_tax } from "./simple-incometax.cul.js";
export { income_tax };

export const fund_value = () => unit_balance() * unit_price(); // not allowing for multiple funds now

export const fund_growth = () =>
  unit_balance({ age_in: age() - 1 }) * unit_price() -
  unit_balance({ age_in: age() - 1 }) * unit_price({ age_in: age() - 1 });

export const fund_charges = () => contribution_charge() + management_charge();

export const contribution_charge = () =>
  -contribution_charge_rate() * (empee_contribution() + emper_contribution());

export const check_cfs = () =>
  empee_contribution_cost() +
  empee_contribution_tax_relief() +
  emper_contribution() +
  fund_growth() +
  fund_charges();

export const check_cfs_vs_fund_value = () =>
  fund_value() - fund_value({ age_in: age() - 1 }) - check_cfs();

export const unit_balance = () => {
  if (age() <= age_0() - 1) return fund_value_0() / unit_price();
  else
    return (
      unit_balance({ age_in: age() - 1 }) +
      contribution_units() +
      management_charge_units()
    );
  // timing = premium received at start of year and allocated immediately
};

export const management_charge_rate = () => management_charge_rate_in;

export const management_charge = () => {
  if (age() <= age_0()) return 0;
  else return -fund_value({ age_in: age() - 1 }) * management_charge_rate();
};

export const management_charge_units = () => management_charge() / unit_price();

export const contributions = () => empee_contribution() + emper_contribution();

export const contribution_units = () =>
  (contributions() * (1 - contribution_charge_rate())) / unit_price(); // todo, AVCs?

export const unit_price = () => {
  if (age() <= age_0()) return 1;
  else return unit_price({ age_in: age() - 1 }) * (1 + unit_growth_rate());
};

export const missed_contribution_age = () =>
  missed_contribution_age_in != undefined ? missed_contribution_age_in : -1;

export const empee_contribution = () => {
  if (
    age() <= age_0() - 1 ||
    age() == retirement_age() ||
    age() == missed_contribution_age()
  )
    return 0;
  else return salary({ age_in: age() - 1 }) * empee_contribution_rate();
};

export const accumulated_empee_contributions = () => {
  if (age() == age_0() - 1) return 0;
  else
    return (
      accumulated_empee_contributions({ age_in: age() - 1 }) +
      empee_contribution()
    );
};

export const accumulated_empee_contribution_tax_relief = () => {
  if (age() == age_0() - 1) return 0;
  else
    return (
      accumulated_empee_contribution_tax_relief({ age_in: age() - 1 }) +
      empee_contribution_tax_relief()
    );
};

export const pension_tax_relief_ratio = () =>
  empee_contribution_tax_relief() / empee_contribution();

// assumption for tax relief calculation, 2024 values
export const tax_credits = () => 1875+1875 // single person + empee paye tax creedits

export const empee_contribution_tax_relief = () =>
  income_tax({
    gross_salary_in: salary({ age_in: age() - 1 }),
    //tax_credits_in: 3000, // ex- I specified like this
    pension_contribution_in: 0,
  }) -
  income_tax({
    gross_salary_in: salary({ age_in: age() - 1 }),
    //tax_credits_in: 3000, // ex- I specified like this
    pension_contribution_in: empee_contribution(),
  });

export const empee_contribution_cost = () =>
  empee_contribution() - empee_contribution_tax_relief();

// affected by bug: depends on gross_salary_in, for some reason
// issue #102
/*export const accumulated_empee_contribution_tax_relief = () => {
  if (age() == age_0() - 1) return 0;
  else
    return (
      accumulated_empee_contribution_tax_relief({ age_in: age() - 1 }) +
      empee_contribution_tax_relief()
    );
};*/

export const emper_contribution = () =>
  empee_contribution() * emper_matching_rate();

export const salary = () => {
  // at end of year
  if (age() == salary_age_0() - 1) return salary_0();
  else if (age() >= retirement_age()) return 0;
  else if (age() >= salary_age_0()) return salary({ age_in: age() - 1 }) * (1 + salary_inflation_rate());
  else return salary({ age_in: age() + 1 }) / (1 + salary_inflation_rate());
};

export const retirement_fund_value = () =>
  // at retirement:
  fund_value({ age_in: retirement_age() });

export const salaries_per_retirement_fund = () =>
  retirement_fund_value() / salary({ age_in: retirement_age() - 1 });


// inputs:

// using age and age_0 (starting age) as inputs, rather than year/time and age_0.
export const age = () => age_in; // input
export const age_0 = () => age_0_in;

export const retirement_age = () => retirement_age_in;
export const salary_0 = () => salary_0_in;
export const salary_age_0 = () => salary_age_0_in; // salary benchmark age is detached from age_0 so that results for different age_0 (starting age) are comparable // maybe this is bad naming
export const salary_inflation_rate = () => salary_inflation_rate_in;
export const empee_contribution_rate = () => empee_contribution_rate_in;
export const emper_matching_rate = () => emper_matching_rate_in;

export const unit_growth_rate = () => unit_growth_rate_in;

export const fund_value_0 = () => fund_value_0_in;

export const contribution_charge_rate = () => contribution_charge_rate_in;
