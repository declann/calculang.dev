(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else {
		var a = factory();
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "x", function() { return x; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "dx", function() { return dx; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "drag_x", function() { return drag_x; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "drag_y", function() { return drag_y; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "y", function() { return y; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "dy", function() { return dy; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "t", function() { return t; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "angle", function() { return angle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "power", function() { return power; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return g; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "drag_coefficient", function() { return drag_coefficient; });
const x = ({ t_in, angle_in, power_in, drag_coefficient_in }) => {
  if (t({ t_in }) == 0) return 0;else
  return x({ angle_in, power_in, drag_coefficient_in, t_in: t({ t_in }) - 1 }) + dx({ angle_in, power_in, t_in, drag_coefficient_in });
};

const dx = ({ angle_in, power_in, t_in, drag_coefficient_in }) => Math.sin(angle({ angle_in }) * Math.PI / 180) * power({ power_in }) - drag_x({ t_in, angle_in, power_in, drag_coefficient_in });

// linear drag // test
const drag_x = ({ t_in, angle_in, power_in, drag_coefficient_in }) => x({ angle_in, power_in, drag_coefficient_in, t_in: t({ t_in }) - 1 }) * drag_coefficient({ drag_coefficient_in });
const drag_y = ({ t_in, angle_in, power_in, g_in, drag_coefficient_in }) => y({ angle_in, power_in, g_in, drag_coefficient_in, t_in: t({ t_in }) - 1 }) * drag_coefficient({ drag_coefficient_in });

const y = ({ t_in, angle_in, power_in, g_in, drag_coefficient_in }) => {
  if (t({ t_in }) == 0) return 0;else
  return y({ angle_in, power_in, g_in, drag_coefficient_in, t_in: t({ t_in }) - 1 }) + dy({ angle_in, power_in, t_in, g_in, drag_coefficient_in });
};

const dy = ({ angle_in, power_in, t_in, g_in, drag_coefficient_in }) =>
Math.cos(angle({ angle_in }) * Math.PI / 180) * power({ power_in }) - t({ t_in }) * g({ g_in }) - drag_y({ t_in, angle_in, power_in, g_in, drag_coefficient_in });

// inputs:
const t = ({ t_in }) => t_in;
const angle = ({ angle_in }) => angle_in;
const power = ({ power_in }) => power_in;
const g = ({ g_in }) => g_in;
const drag_coefficient = ({ drag_coefficient_in }) => drag_coefficient_in;

/***/ })
/******/ ]);
});
//# sourceMappingURL=projectile-nomemo.js.map