export const x = () => {
  if (t() == 0) return 0;
  else return x({ t_in: t() - 1 }) + dx();
};

export const dx = () => Math.sin((angle() * Math.PI) / 180) * power() - drag_x();

// linear drag // test
export const drag_x = () => x({ t_in: t() - 1 }) * drag_coefficient();
export const drag_y = () => y({ t_in: t() - 1 }) * drag_coefficient();

export const y = () => {
  if (t() == 0) return 0;
  else return y({ t_in: t() - 1 }) + dy();
};

export const dy = () =>
  Math.cos((angle() * Math.PI) / 180) * power() - t() * g() - drag_y();

// inputs:
export const t = () => t_in;
export const angle = () => angle_in;
export const power = () => power_in;
export const g = () => g_in;
export const drag_coefficient = () => drag_coefficient_in;