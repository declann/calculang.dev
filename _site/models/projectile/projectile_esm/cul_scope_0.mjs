
import { memoize } from 'underscore';
//import memoize from 'lru-memoize';
//import { isEqual } from 'underscore'; // TODO poor tree shaking support, or why is this impact so massive? Move to lodash/lodash-es?

// import/export non-to memo?

import { x_ as x$, dx_ as dx$, drag_x_ as drag_x$, drag_y_ as drag_y$, y_ as y$, dy_ as dy$, t_ as t$, angle_ as angle$, power_ as power$, g_ as g$, drag_coefficient_ as drag_coefficient$ } from "./cul_scope_1.mjs"; // there is already-culed stuff in here, why? imports to memo loader include cul_scope_id, what logic should it apply RE passing forward? eliminate? Probably!





////////// start x memo-loader code //////////
//const x$m = memoize(999999, isEqual)(x$);
export const x$m = memoize(x$, JSON.stringify);
export const x = (a) => {
  return x$m(a);
  // eslint-disable-next-line no-undef
  x$({ t_in, angle_in, power_in, drag_coefficient_in }); // never run, but here to "trick" calculang graph logic
};
////////// end x memo-loader code //////////



////////// start dx memo-loader code //////////
//const dx$m = memoize(999999, isEqual)(dx$);
export const dx$m = memoize(dx$, JSON.stringify);
export const dx = (a) => {
  return dx$m(a);
  // eslint-disable-next-line no-undef
  dx$({ angle_in, power_in, t_in, drag_coefficient_in }); // never run, but here to "trick" calculang graph logic
};
////////// end dx memo-loader code //////////



////////// start drag_x memo-loader code //////////
//const drag_x$m = memoize(999999, isEqual)(drag_x$);
export const drag_x$m = memoize(drag_x$, JSON.stringify);
export const drag_x = (a) => {
  return drag_x$m(a);
  // eslint-disable-next-line no-undef
  drag_x$({ t_in, angle_in, power_in, drag_coefficient_in }); // never run, but here to "trick" calculang graph logic
};
////////// end drag_x memo-loader code //////////



////////// start drag_y memo-loader code //////////
//const drag_y$m = memoize(999999, isEqual)(drag_y$);
export const drag_y$m = memoize(drag_y$, JSON.stringify);
export const drag_y = (a) => {
  return drag_y$m(a);
  // eslint-disable-next-line no-undef
  drag_y$({ t_in, angle_in, power_in, g_in, drag_coefficient_in }); // never run, but here to "trick" calculang graph logic
};
////////// end drag_y memo-loader code //////////



////////// start y memo-loader code //////////
//const y$m = memoize(999999, isEqual)(y$);
export const y$m = memoize(y$, JSON.stringify);
export const y = (a) => {
  return y$m(a);
  // eslint-disable-next-line no-undef
  y$({ t_in, angle_in, power_in, g_in, drag_coefficient_in }); // never run, but here to "trick" calculang graph logic
};
////////// end y memo-loader code //////////



////////// start dy memo-loader code //////////
//const dy$m = memoize(999999, isEqual)(dy$);
export const dy$m = memoize(dy$, JSON.stringify);
export const dy = (a) => {
  return dy$m(a);
  // eslint-disable-next-line no-undef
  dy$({ angle_in, power_in, t_in, g_in, drag_coefficient_in }); // never run, but here to "trick" calculang graph logic
};
////////// end dy memo-loader code //////////



////////// start t memo-loader code //////////
//const t$m = memoize(999999, isEqual)(t$);
export const t$m = memoize(t$, JSON.stringify);
export const t = (a) => {
  return t$m(a);
  // eslint-disable-next-line no-undef
  t$({ t_in }); // never run, but here to "trick" calculang graph logic
};
////////// end t memo-loader code //////////



////////// start angle memo-loader code //////////
//const angle$m = memoize(999999, isEqual)(angle$);
export const angle$m = memoize(angle$, JSON.stringify);
export const angle = (a) => {
  return angle$m(a);
  // eslint-disable-next-line no-undef
  angle$({ angle_in }); // never run, but here to "trick" calculang graph logic
};
////////// end angle memo-loader code //////////



////////// start power memo-loader code //////////
//const power$m = memoize(999999, isEqual)(power$);
export const power$m = memoize(power$, JSON.stringify);
export const power = (a) => {
  return power$m(a);
  // eslint-disable-next-line no-undef
  power$({ power_in }); // never run, but here to "trick" calculang graph logic
};
////////// end power memo-loader code //////////



////////// start g memo-loader code //////////
//const g$m = memoize(999999, isEqual)(g$);
export const g$m = memoize(g$, JSON.stringify);
export const g = (a) => {
  return g$m(a);
  // eslint-disable-next-line no-undef
  g$({ g_in }); // never run, but here to "trick" calculang graph logic
};
////////// end g memo-loader code //////////



////////// start drag_coefficient memo-loader code //////////
//const drag_coefficient$m = memoize(999999, isEqual)(drag_coefficient$);
export const drag_coefficient$m = memoize(drag_coefficient$, JSON.stringify);
export const drag_coefficient = (a) => {
  return drag_coefficient$m(a);
  // eslint-disable-next-line no-undef
  drag_coefficient$({ drag_coefficient_in }); // never run, but here to "trick" calculang graph logic
};
////////// end drag_coefficient memo-loader code //////////