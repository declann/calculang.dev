export const fizz = () => i() % 3 == 0 ? 'Fizz' : '';
export const buzz = () => i() % 5 == 0 ? 'Buzz' : '';

export const fizzbuzz = () => fizz() + buzz();

// inputs:
export const i = () => i_in;