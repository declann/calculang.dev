export const fizz = ({ i_in }) => i({ i_in }) % 3 == 0 ? 'Fizz' : '';
export const buzz = ({ i_in }) => i({ i_in }) % 5 == 0 ? 'Buzz' : '';

export const fizzbuzz = ({ i_in }) => {
  if (fizz({ i_in }) + buzz({ i_in }) != '')
  return fizz({ i_in }) + buzz({ i_in });else
  return i({ i_in });
};

// inputs:
export const i = ({ i_in }) => i_in;