import { balance, deposits, interest, duration, interest_rate_ as interest_rate_expected, annual_payment } from './savings-expected.cul.js';
export { balance, deposits, interest, duration, interest_rate_expected, annual_payment };
  
export const year = () => year_in;

export const actual_interest_rates = () => [0.02, 0.03, 0.03, 0.00, 0.01, 0.01];

export const actual_interest_rate_co = () => actual_interest_rate_co_in;

export const interest_rate = () => {
  if (year() > actual_interest_rate_co())
    return interest_rate_expected();
  else
    return actual_interest_rates()[year()];
}