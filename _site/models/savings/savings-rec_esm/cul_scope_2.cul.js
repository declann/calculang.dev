
    import { memoize } from 'underscore';
    //import memoize from 'lru-memoize';
    //import { isEqual } from 'underscore'; // TODO poor tree shaking support, or why is this impact so massive? Move to lodash/lodash-es?
    
    // import/export non-to memo?

    import { balance_ as balance$, deposits_ as deposits$, interest_ as interest$, annual_payment_ as annual_payment$, duration_ as duration$, interest_rate_ as interest_rate$ } from './savings-expected.cul.js?&+memoed'; // there is already-culed stuff in here, why? imports to memo loader include cul_scope_id, what logic should it apply RE passing forward? eliminate? Probably!

    
    
    

////////// start balance memo-loader code //////////
//const balance$m = memoize(999999, isEqual)(balance$);
export const balance$m = memoize(balance$, JSON.stringify);
export const balance = (a) => {
  return balance$m(a);
  // eslint-disable-next-line no-undef
  balance$(); // never run, but here to "trick" calculang graph logic
};
////////// end balance memo-loader code //////////



////////// start deposits memo-loader code //////////
//const deposits$m = memoize(999999, isEqual)(deposits$);
export const deposits$m = memoize(deposits$, JSON.stringify);
export const deposits = (a) => {
  return deposits$m(a);
  // eslint-disable-next-line no-undef
  deposits$(); // never run, but here to "trick" calculang graph logic
};
////////// end deposits memo-loader code //////////



////////// start interest memo-loader code //////////
//const interest$m = memoize(999999, isEqual)(interest$);
export const interest$m = memoize(interest$, JSON.stringify);
export const interest = (a) => {
  return interest$m(a);
  // eslint-disable-next-line no-undef
  interest$(); // never run, but here to "trick" calculang graph logic
};
////////// end interest memo-loader code //////////



////////// start annual_payment memo-loader code //////////
//const annual_payment$m = memoize(999999, isEqual)(annual_payment$);
export const annual_payment$m = memoize(annual_payment$, JSON.stringify);
export const annual_payment = (a) => {
  return annual_payment$m(a);
  // eslint-disable-next-line no-undef
  annual_payment$(); // never run, but here to "trick" calculang graph logic
};
////////// end annual_payment memo-loader code //////////



////////// start duration memo-loader code //////////
//const duration$m = memoize(999999, isEqual)(duration$);
export const duration$m = memoize(duration$, JSON.stringify);
export const duration = (a) => {
  return duration$m(a);
  // eslint-disable-next-line no-undef
  duration$(); // never run, but here to "trick" calculang graph logic
};
////////// end duration memo-loader code //////////



////////// start interest_rate memo-loader code //////////
//const interest_rate$m = memoize(999999, isEqual)(interest_rate$);
export const interest_rate$m = memoize(interest_rate$, JSON.stringify);
export const interest_rate = (a) => {
  return interest_rate$m(a);
  // eslint-disable-next-line no-undef
  interest_rate$(); // never run, but here to "trick" calculang graph logic
};
////////// end interest_rate memo-loader code //////////


    