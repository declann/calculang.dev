import { interest_rate } from "./cul_scope_0.mjs";import { actual_interest_rate_co } from "./cul_scope_0.mjs";import { actual_interest_rates } from "./cul_scope_0.mjs";import { year } from "./cul_scope_0.mjs";import { annual_payment } from "./cul_scope_0.mjs";import { interest_rate_expected } from "./cul_scope_0.mjs";import { duration } from "./cul_scope_0.mjs";import { interest } from "./cul_scope_0.mjs";import { deposits } from "./cul_scope_0.mjs";import { balance } from "./cul_scope_0.mjs";import { balance_, deposits_, interest_, duration_, interest_rate_ as interest_rate_expected_, annual_payment_ } from "./cul_scope_2.mjs";
export { balance_, deposits_, interest_, duration_, interest_rate_expected_, annual_payment_ };

export const year_ = ({ year_in }) => year_in;

export const actual_interest_rates_ = ({}) => [0.02, 0.03, 0.03, 0.00, 0.01, 0.01];

export const actual_interest_rate_co_ = ({ actual_interest_rate_co_in }) => actual_interest_rate_co_in;

export const interest_rate_ = ({ year_in, actual_interest_rate_co_in }) => {
  if (year({ year_in }) > actual_interest_rate_co({ actual_interest_rate_co_in }))
  return interest_rate_expected({});else

  return actual_interest_rates({})[year({ year_in })];
};