import { interest_rate } from "./cul_scope_0.mjs";import { actual_interest_rate_co } from "./cul_scope_0.mjs";import { actual_interest_rates } from "./cul_scope_0.mjs";import { year } from "./cul_scope_0.mjs";import { annual_payment } from "./cul_scope_0.mjs";import { interest_rate_expected } from "./cul_scope_0.mjs";import { duration } from "./cul_scope_0.mjs";import { interest } from "./cul_scope_0.mjs";import { deposits } from "./cul_scope_0.mjs";import { balance } from "./cul_scope_0.mjs";export const balance_ = ({ year_in, actual_interest_rate_co_in }) => {
  if (year({ year_in }) < 0) return 0;else
  if (year({ year_in }) == 0) return deposits({ year_in });else
  return balance({ actual_interest_rate_co_in, year_in: year({ year_in }) - 1 }) + deposits({ year_in }) + interest({ year_in, actual_interest_rate_co_in });
};

export const deposits_ = ({ year_in }) => {
  if (year({ year_in }) >= 0 && year({ year_in }) < duration({}))
  return annual_payment({});else

  return 0;
};

export const interest_ = ({ year_in, actual_interest_rate_co_in }) => {
  if (year({ year_in }) == 0) return 0;else
  return balance({ actual_interest_rate_co_in, year_in: year({ year_in }) - 1 }) * interest_rate({ year_in, actual_interest_rate_co_in });
};

// inputs:
export const year_ = ({ year_in }) => year_in;
export const annual_payment_ = ({ annual_payment_in }) => annual_payment_in;
export const duration_ = ({ duration_in }) => duration_in; // years
export const interest_rate_ = ({ interest_rate_in }) => interest_rate_in; // annual