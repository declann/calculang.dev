(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else {
		var a = factory();
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "year", function() { return year; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "actual_interest_rates", function() { return actual_interest_rates; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "actual_interest_rate_co", function() { return actual_interest_rate_co; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "interest_rate", function() { return interest_rate; });
/* harmony import */ var _savings_expected_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1);
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "balance", function() { return _savings_expected_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__["b"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "deposits", function() { return _savings_expected_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__["c"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "interest", function() { return _savings_expected_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__["e"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "duration", function() { return _savings_expected_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__["d"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "interest_rate_expected", function() { return _savings_expected_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__["f"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "annual_payment", function() { return _savings_expected_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__["a"]; });




const year = ({ year_in }) => year_in;

const actual_interest_rates = ({}) => [0.02, 0.03, 0.03, 0.00, 0.01, 0.01];

const actual_interest_rate_co = ({ actual_interest_rate_co_in }) => actual_interest_rate_co_in;

const interest_rate = ({ year_in, actual_interest_rate_co_in }) => {
  if (year({ year_in }) > actual_interest_rate_co({ actual_interest_rate_co_in }))
  return Object(_savings_expected_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* interest_rate_ */ "f"])({});else

  return actual_interest_rates({})[year({ year_in })];
};

/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return annual_payment; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return duration; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return interest_rate_; });
/* harmony import */ var _savings_rec_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var _savings_cul_js_cul_scope_id_2_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2);
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "b", function() { return _savings_cul_js_cul_scope_id_2_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_1__["a"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "c", function() { return _savings_cul_js_cul_scope_id_2_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_1__["b"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "e", function() { return _savings_cul_js_cul_scope_id_2_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_1__["c"]; });




const annual_payment = ({}) => 1000;
const duration = ({}) => 5;
const interest_rate_ = ({}) => 0.02;

/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return balance; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return deposits; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return interest; });
/* unused harmony export year_ */
/* unused harmony export annual_payment_ */
/* unused harmony export duration_ */
/* unused harmony export interest_rate_ */
/* harmony import */ var _savings_expected_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1);
/* harmony import */ var _savings_rec_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(0);
const balance = ({ year_in, actual_interest_rate_co_in }) => {
  if (Object(_savings_rec_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_1__["year"])({ year_in }) < 0) return 0;else
  if (Object(_savings_rec_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_1__["year"])({ year_in }) == 0) return deposits({ year_in });else
  return balance({ actual_interest_rate_co_in, year_in: Object(_savings_rec_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_1__["year"])({ year_in }) - 1 }) + deposits({ year_in }) + interest({ year_in, actual_interest_rate_co_in });
};

const deposits = ({ year_in }) => {
  if (Object(_savings_rec_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_1__["year"])({ year_in }) >= 0 && Object(_savings_rec_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_1__["year"])({ year_in }) < Object(_savings_expected_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* duration */ "d"])({}))
  return Object(_savings_expected_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* annual_payment */ "a"])({});else

  return 0;
};

const interest = ({ year_in, actual_interest_rate_co_in }) => {
  if (Object(_savings_rec_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_1__["year"])({ year_in }) == 0) return 0;else
  return balance({ actual_interest_rate_co_in, year_in: Object(_savings_rec_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_1__["year"])({ year_in }) - 1 }) * Object(_savings_rec_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_1__["interest_rate"])({ year_in, actual_interest_rate_co_in });
};

// inputs:
const year_ = ({ year_in }) => year_in;
const annual_payment_ = ({ annual_payment_in }) => annual_payment_in;
const duration_ = ({ duration_in }) => duration_in; // years
const interest_rate_ = ({ interest_rate_in }) => interest_rate_in; // annual

/***/ })
/******/ ]);
});
//# sourceMappingURL=savings-rec-nomemo.js.map