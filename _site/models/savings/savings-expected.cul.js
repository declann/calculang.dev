import { balance, deposits, interest } from './savings.cul.js';
export { balance, deposits, interest };

export const annual_payment = () => 1000;
export const duration = () => 5;
export const interest_rate = () => 0.02;
