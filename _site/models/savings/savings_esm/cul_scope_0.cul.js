export const balance = () => {
  if (year() < 0) return 0;
  else if (year() == 0) return deposits();
  else return balance({ year_in: year() - 1 }) + deposits() + interest();
}

export const deposits = () => {
  if (year() >= 0 && year() < duration())
    return annual_payment();
  else
    return 0;
};

export const interest = () => {
  if (year() == 0) return 0;
  else return balance({ year_in: year() - 1 }) * interest_rate();
}

// inputs:
export const year = () => year_in;
export const annual_payment = () => annual_payment_in;
export const duration = () => duration_in; // years
export const interest_rate = () => interest_rate_in; // annual