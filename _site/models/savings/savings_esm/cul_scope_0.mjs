export const balance = ({ year_in, duration_in, annual_payment_in, interest_rate_in }) => {
  if (year({ year_in }) < 0) return 0;else
  if (year({ year_in }) == 0) return deposits({ year_in, duration_in, annual_payment_in });else
  return balance({ duration_in, annual_payment_in, interest_rate_in, year_in: year({ year_in }) - 1 }) + deposits({ year_in, duration_in, annual_payment_in }) + interest({ year_in, duration_in, annual_payment_in, interest_rate_in });
};

export const deposits = ({ year_in, duration_in, annual_payment_in }) => {
  if (year({ year_in }) >= 0 && year({ year_in }) < duration({ duration_in }))
  return annual_payment({ annual_payment_in });else

  return 0;
};

export const interest = ({ year_in, duration_in, annual_payment_in, interest_rate_in }) => {
  if (year({ year_in }) == 0) return 0;else
  return balance({ duration_in, annual_payment_in, interest_rate_in, year_in: year({ year_in }) - 1 }) * interest_rate({ interest_rate_in });
};

// inputs:
export const year = ({ year_in }) => year_in;
export const annual_payment = ({ annual_payment_in }) => annual_payment_in;
export const duration = ({ duration_in }) => duration_in; // years
export const interest_rate = ({ interest_rate_in }) => interest_rate_in; // annual