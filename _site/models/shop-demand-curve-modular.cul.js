import { sales, purchases, profit, sales_price, purchase_price, expenses } from './shop.cul.js';
export { sales, purchases, profit, sales_price, purchase_price, expenses };

export const units = () =>
  7 * 20000 - sales_price() * 20000;