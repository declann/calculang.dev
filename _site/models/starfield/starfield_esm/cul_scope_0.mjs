
import { memoize } from 'underscore';
//import memoize from 'lru-memoize';
//import { isEqual } from 'underscore'; // TODO poor tree shaking support, or why is this impact so massive? Move to lodash/lodash-es?

// import/export non-to memo?

import { random_seed_ as random_seed$, seeded_ as seeded$, random_ as random$, frame_ as frame$, star_ as star$, x0_ as x0$, y0_ as y0$, z0_ as z0$, speed_ as speed$, z_ as z$, visible_ as visible$, x_ as x$, y_ as y$, px_ as px$, py_ as py$ } from "./cul_scope_1.mjs"; // there is already-culed stuff in here, why? imports to memo loader include cul_scope_id, what logic should it apply RE passing forward? eliminate? Probably!





////////// start random_seed memo-loader code //////////
//const random_seed$m = memoize(999999, isEqual)(random_seed$);
export const random_seed$m = memoize(random_seed$, JSON.stringify);
export const random_seed = (a) => {
  return random_seed$m(a);
  // eslint-disable-next-line no-undef
  random_seed$({ random_seed_in }); // never run, but here to "trick" calculang graph logic
};
////////// end random_seed memo-loader code //////////



////////// start seeded memo-loader code //////////
//const seeded$m = memoize(999999, isEqual)(seeded$);
export const seeded$m = memoize(seeded$, JSON.stringify);
export const seeded = (a) => {
  return seeded$m(a);
  // eslint-disable-next-line no-undef
  seeded$({ random_seed_in }); // never run, but here to "trick" calculang graph logic
};
////////// end seeded memo-loader code //////////



////////// start random memo-loader code //////////
//const random$m = memoize(999999, isEqual)(random$);
export const random$m = memoize(random$, JSON.stringify);
export const random = (a) => {
  return random$m(a);
  // eslint-disable-next-line no-undef
  random$({ random_seed_in }); // never run, but here to "trick" calculang graph logic
};
////////// end random memo-loader code //////////



////////// start frame memo-loader code //////////
//const frame$m = memoize(999999, isEqual)(frame$);
export const frame$m = memoize(frame$, JSON.stringify);
export const frame = (a) => {
  return frame$m(a);
  // eslint-disable-next-line no-undef
  frame$({ frame_in }); // never run, but here to "trick" calculang graph logic
};
////////// end frame memo-loader code //////////



////////// start star memo-loader code //////////
//const star$m = memoize(999999, isEqual)(star$);
export const star$m = memoize(star$, JSON.stringify);
export const star = (a) => {
  return star$m(a);
  // eslint-disable-next-line no-undef
  star$({ star_in }); // never run, but here to "trick" calculang graph logic
};
////////// end star memo-loader code //////////



////////// start x0 memo-loader code //////////
//const x0$m = memoize(999999, isEqual)(x0$);
export const x0$m = memoize(x0$, JSON.stringify);
export const x0 = (a) => {
  return x0$m(a);
  // eslint-disable-next-line no-undef
  x0$({ random_seed_in, star_in }); // never run, but here to "trick" calculang graph logic
};
////////// end x0 memo-loader code //////////



////////// start y0 memo-loader code //////////
//const y0$m = memoize(999999, isEqual)(y0$);
export const y0$m = memoize(y0$, JSON.stringify);
export const y0 = (a) => {
  return y0$m(a);
  // eslint-disable-next-line no-undef
  y0$({ random_seed_in, star_in }); // never run, but here to "trick" calculang graph logic
};
////////// end y0 memo-loader code //////////



////////// start z0 memo-loader code //////////
//const z0$m = memoize(999999, isEqual)(z0$);
export const z0$m = memoize(z0$, JSON.stringify);
export const z0 = (a) => {
  return z0$m(a);
  // eslint-disable-next-line no-undef
  z0$({ random_seed_in, star_in }); // never run, but here to "trick" calculang graph logic
};
////////// end z0 memo-loader code //////////



////////// start speed memo-loader code //////////
//const speed$m = memoize(999999, isEqual)(speed$);
export const speed$m = memoize(speed$, JSON.stringify);
export const speed = (a) => {
  return speed$m(a);
  // eslint-disable-next-line no-undef
  speed$({ speed_in }); // never run, but here to "trick" calculang graph logic
};
////////// end speed memo-loader code //////////



////////// start z memo-loader code //////////
//const z$m = memoize(999999, isEqual)(z$);
export const z$m = memoize(z$, JSON.stringify);
export const z = (a) => {
  return z$m(a);
  // eslint-disable-next-line no-undef
  z$({ frame_in, random_seed_in, star_in, speed_in }); // never run, but here to "trick" calculang graph logic
};
////////// end z memo-loader code //////////



////////// start visible memo-loader code //////////
//const visible$m = memoize(999999, isEqual)(visible$);
export const visible$m = memoize(visible$, JSON.stringify);
export const visible = (a) => {
  return visible$m(a);
  // eslint-disable-next-line no-undef
  visible$({ frame_in, random_seed_in, star_in, speed_in }); // never run, but here to "trick" calculang graph logic
};
////////// end visible memo-loader code //////////



////////// start x memo-loader code //////////
//const x$m = memoize(999999, isEqual)(x$);
export const x$m = memoize(x$, JSON.stringify);
export const x = (a) => {
  return x$m(a);
  // eslint-disable-next-line no-undef
  x$({ frame_in, random_seed_in, star_in, speed_in }); // never run, but here to "trick" calculang graph logic
};
////////// end x memo-loader code //////////



////////// start y memo-loader code //////////
//const y$m = memoize(999999, isEqual)(y$);
export const y$m = memoize(y$, JSON.stringify);
export const y = (a) => {
  return y$m(a);
  // eslint-disable-next-line no-undef
  y$({ frame_in, random_seed_in, star_in, speed_in }); // never run, but here to "trick" calculang graph logic
};
////////// end y memo-loader code //////////



////////// start px memo-loader code //////////
//const px$m = memoize(999999, isEqual)(px$);
export const px$m = memoize(px$, JSON.stringify);
export const px = (a) => {
  return px$m(a);
  // eslint-disable-next-line no-undef
  px$({ frame_in, random_seed_in, star_in, speed_in }); // never run, but here to "trick" calculang graph logic
};
////////// end px memo-loader code //////////



////////// start py memo-loader code //////////
//const py$m = memoize(999999, isEqual)(py$);
export const py$m = memoize(py$, JSON.stringify);
export const py = (a) => {
  return py$m(a);
  // eslint-disable-next-line no-undef
  py$({ frame_in, random_seed_in, star_in, speed_in }); // never run, but here to "trick" calculang graph logic
};
////////// end py memo-loader code //////////