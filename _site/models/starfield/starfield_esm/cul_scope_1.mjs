import { py } from "./cul_scope_0.mjs";import { px } from "./cul_scope_0.mjs";import { y } from "./cul_scope_0.mjs";import { x } from "./cul_scope_0.mjs";import { visible } from "./cul_scope_0.mjs";import { z } from "./cul_scope_0.mjs";import { speed } from "./cul_scope_0.mjs";import { z0 } from "./cul_scope_0.mjs";import { y0 } from "./cul_scope_0.mjs";import { x0 } from "./cul_scope_0.mjs";import { star } from "./cul_scope_0.mjs";import { frame } from "./cul_scope_0.mjs";import { random } from "./cul_scope_0.mjs";import { seeded } from "./cul_scope_0.mjs";import { random_seed } from "./cul_scope_0.mjs"; // This model is a inspired by the following made in p5.js by Daniel Shiffman http://codingtra.in
// https://editor.p5js.org/codingtrain/sketches/1wLHIck3T
// heavily simplified!

/* seeded random numbers: */
export const random_seed_ = ({ random_seed_in }) => random_seed_in;
import { prng_alea } from "./esm-seedrandom.mjs";
export const seeded_ = ({ random_seed_in }) => prng_alea(random_seed({ random_seed_in }));
export const random_ = ({ random_seed_in }) => seeded({ random_seed_in: random_seed({ random_seed_in }) })();
// alternative:
//export const random = () => Math.random();

export const frame_ = ({ frame_in }) => frame_in;

export const star_ = ({ star_in }) => star_in;

// star-level calcs:
export const x0_ = ({ random_seed_in, star_in }) => random({ random_seed_in, star_in: star({ star_in }), id: "x" }) * 100 - 50;
export const y0_ = ({ random_seed_in, star_in }) => random({ random_seed_in, star_in: star({ star_in }), id: "y" }) * 100 - 50;
export const z0_ = ({ random_seed_in, star_in }) => random({ random_seed_in, star_in: star({ star_in }), id: "z" }) * 50;

export const speed_ = ({ speed_in }) => speed_in;

// use for radius
export const z_ = ({ frame_in, random_seed_in, star_in, speed_in }) => {
  if (frame({ frame_in }) == 0) return z0({ random_seed_in, star_in });else
  if (z({ random_seed_in, star_in, speed_in, frame_in: frame({ frame_in }) - 1 }) - speed({ speed_in }) < 1) return 50; // "far away"
  else return z({ random_seed_in, star_in, speed_in, frame_in: frame({ frame_in }) - 1 }) - speed({ speed_in });
};

// only z gets updated in each frame

export const visible_ = ({ frame_in, random_seed_in, star_in, speed_in }) => z({ frame_in, random_seed_in, star_in, speed_in }) > 1;

export const x_ = ({ frame_in, random_seed_in, star_in, speed_in }) => {
  if (frame({ frame_in }) == 0) return x0({ random_seed_in, star_in });else
  if (!visible({ random_seed_in, star_in, speed_in, frame_in: frame({ frame_in }) - 1 })) return x0({ random_seed_in, star_in });else
  return x0({ random_seed_in, star_in }) / z({ frame_in, random_seed_in, star_in, speed_in }) * 50;
};

export const y_ = ({ frame_in, random_seed_in, star_in, speed_in }) => {
  if (frame({ frame_in }) == 0) return y0({ random_seed_in, star_in });else
  if (!visible({ random_seed_in, star_in, speed_in, frame_in: frame({ frame_in }) - 1 })) return y0({ random_seed_in, star_in });else
  return y0({ random_seed_in, star_in }) / z({ frame_in, random_seed_in, star_in, speed_in }) * 50;
};

export const px_ = ({ frame_in, random_seed_in, star_in, speed_in }) => {
  if (x({ frame_in, random_seed_in, star_in, speed_in }) == x0({ random_seed_in, star_in })) return x0({ random_seed_in, star_in });else
  return x({ random_seed_in, star_in, speed_in, frame_in: frame({ frame_in }) - 1 });
};
export const py_ = ({ frame_in, random_seed_in, star_in, speed_in }) => {
  if (y({ frame_in, random_seed_in, star_in, speed_in }) == y0({ random_seed_in, star_in })) return y0({ random_seed_in, star_in });else
  return y({ random_seed_in, star_in, speed_in, frame_in: frame({ frame_in }) - 1 });
};