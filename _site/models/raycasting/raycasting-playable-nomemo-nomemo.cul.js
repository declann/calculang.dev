// there were important perf changes here to ponder!
// overriding player x/y => ray level calcs depend on key calcs,
// versus populating player x/y with calcd positions so that key calcs don't propagate down to ray calcs

import {
  player_x,
  player_y,
  level_data,
  level_x,
  level_y,
  level,
  ray_steps,
  ray_angle,
  ray_x,
  ray_y,
  ray_value,
  ray_hit,
  ray_length,
  ray_hit_color_ as ray_hit_color_base,
  inverse_ray_length_ as inverse_ray_length_base,
  negative_inverse_ray_length,
  ray_level,
  ray,
  player,
  fov,
  angle,
  in_fov,
  level_player,
  level_player_ray,
  level_player_ray_fov,
  level_player_fov,
} from "./raycasting.cul.js";
export {
  inverse_ray_length_base, ray_hit_color_base,
  player_x,
  player_y,
  level_data,
  level_x,
  level_y,
  level,
  ray_steps,
  ray_angle,
  ray_x,
  ray_y,
  ray_value,
  ray_hit,
  ray_length,
  //ray_hit_color,
  //inverse_ray_length,
  negative_inverse_ray_length,
  ray_level,
  ray,
  player,
  fov,
  angle,
  in_fov,
  level_player,
  level_player_ray,
  level_player_ray_fov,
  level_player_fov,
};

// events processing

// big perf problems aren't fixed by using data_table_in. keys is still used in c-p, plus likely redundancy below // => only using this model for controls
// ^ old comment. This approach was far faster! Using actual modular model,
// player x/y are determined by keys, which adds complexity to ray-level calcs
// Alternatively to override, just supply player_x_in:
// other approaches?
// how many formulae are we really interested in - not that much!
// ray_lengths and colors 
  
export const inverse_ray_length = () => inverse_ray_length_base({player_x_in:player_x_calcd(),player_y_in:player_y_calcd()/*, fov_in:fov_calcd()*/})
export const ray_hit_color = () => ray_hit_color_base({ player_x_in: player_x_calcd(), player_y_in: player_y_calcd()/*, fov_in:fov_calcd()*/ })
  
//export const data_table = () => data_table_in;
export const keys = () => keys_in; // data_table();
export const time = () => time_in;
export const speed = () => speed_in;
export const fov_0 = () => fov_0_in;

export const last_key = () => keys()[time() - 1];
export const forwardness = () =>
  last_key() == "ArrowUp" ? 1 : last_key() == "ArrowDown" ? -1 : 0;

export const dfov = () =>
  last_key() == "ArrowLeft" ? -0.2 : last_key() == "ArrowRight" ? 0.2 : 0;

export const fov_calcd = () => {
  if (time() == 0) return fov_0();
  else
    return [
      fov_calcd({ time_in: time() - 1 })[0] + dfov(), // hmm
      fov_calcd({ time_in: time() - 1 })[1] + dfov(),
    ];
};

export const player_x_raw = () => {
  if (time() == 0) return 32;
  else
    return (
      player_x_raw({ time_in: time() - 1 }) +
      forwardness() * speed() * Math.cos((fov_calcd()[0] + fov_calcd()[1]) / 2)
    );
};
export const player_x_calcd = () => Math.round(player_x_raw());
export const player_y_calcd = () => Math.round(player_y_raw());

export const player_y_raw = () => {
  if (time() == 0) return 32;
  else
    return (
      player_y_raw({ time_in: time() - 1 }) +
      forwardness() * speed() * Math.sin((fov_calcd()[0] + fov_calcd()[1]) / 2)
    );
};
