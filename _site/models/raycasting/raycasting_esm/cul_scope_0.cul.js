import { range } from "underscore";

export const player_x = () => player_x_in;
export const player_y = () => player_y_in;

import level_data0 from "csv-loader!./level.csv";

export const level_data = () => level_data0;

export const level_x = () => Math.max(0, Math.min(63, level_x_in));
export const level_y = () => Math.max(0, Math.min(63, level_y_in));

export const level = () => +level_data()[level_y()][level_x()];

export const ray_steps = () => ray_steps_in;
export const ray_angle = () => ray_angle_in;

export const ray_x = () =>
  Math.round(player_x() + ray_steps() * Math.cos(ray_angle()));
export const ray_y = () =>
  Math.round(player_y() + ray_steps() * Math.sin(ray_angle()));

export const ray_value = () =>
  level({
    level_x_in: ray_x(),
    level_y_in: ray_y(),
  });

export const ray_hit = () =>
  ray_level().filter(({ ray_value }) => ray_value != 0)[0];

export const ray_length = () => ray_hit().i;
export const ray_hit_color = () => ray_hit().ray_value;

export const inverse_ray_length = () => 1 / ray_length();
export const negative_inverse_ray_length = () => -inverse_ray_length();

// if ray calcs depended on player x/y and angle, better than depending on keys and time?
export const ray_level = () =>
  range(0, 1.42 * 64, 1).map((ray_steps_in, i) => ({
    i,
    ray_value: ray_value({ ray_steps_in }),
    ray_x: ray_x(),
    ray_y: ray_y(),
  }));

export const ray = () => {
  if (ray_angle() == undefined) return 0;
  else {
    return ray_level().find(
      (d) => level_x() == d.ray_x && level_y() == d.ray_y && d.i <= ray_length()
    ) == undefined
      ? 0.8
      : 0;
  }
};

export const player = () =>
  level_x() == player_x() && level_y() == player_y() ? 0.5 : 0;

// field of view calcs
export const fov = () => fov_in;

export const angle = () =>
  // angle back to player from any cell in the level
  Math.atan2(level_y() - player_y(), level_x() - player_x());

// is a cell in the fov?
export const in_fov = () => angle() <= fov()[1] && angle() >= fov()[0];

// outputs for visual; these could/should be in a separate calculang model
export const level_player = () =>
  (level() != 0) * (1 + 0.5 * level()) + player() * 3;

export const level_player_ray = () =>
  (level() != 0) * (1 + 0.5 * level()) + player() * 3 - ray();

export const level_player_ray_fov = () =>
  (level() != 0) * (1 + 0.5 * level()) + player() * 3 - ray() + in_fov() * 0.5;

export const level_player_fov = () =>
  (level() != 0) * (1 + 0.5 * level()) + player() * 3 + in_fov() * 0.5;
