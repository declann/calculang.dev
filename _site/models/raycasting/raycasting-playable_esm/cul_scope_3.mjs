import { player_y_raw } from "./cul_scope_0.mjs";import { player_y_calcd } from "./cul_scope_0.mjs";import { player_x_calcd } from "./cul_scope_0.mjs";import { player_x_raw } from "./cul_scope_0.mjs";import { fov_calcd } from "./cul_scope_0.mjs";import { dfov } from "./cul_scope_0.mjs";import { forwardness } from "./cul_scope_0.mjs";import { last_key } from "./cul_scope_0.mjs";import { fov_0 } from "./cul_scope_0.mjs";import { speed } from "./cul_scope_0.mjs";import { time } from "./cul_scope_0.mjs";import { keys } from "./cul_scope_0.mjs";import { ray_hit_color } from "./cul_scope_0.mjs";import { inverse_ray_length } from "./cul_scope_0.mjs";import { level_player_fov } from "./cul_scope_0.mjs";import { level_player_ray_fov } from "./cul_scope_0.mjs";import { level_player_ray } from "./cul_scope_0.mjs";import { level_player } from "./cul_scope_0.mjs";import { in_fov } from "./cul_scope_0.mjs";import { angle } from "./cul_scope_0.mjs";import { fov } from "./cul_scope_0.mjs";import { player } from "./cul_scope_0.mjs";import { ray } from "./cul_scope_0.mjs";import { ray_level } from "./cul_scope_0.mjs";import { negative_inverse_ray_length } from "./cul_scope_0.mjs";import { inverse_ray_length_base } from "./cul_scope_0.mjs";import { ray_hit_color_base } from "./cul_scope_0.mjs";import { ray_length } from "./cul_scope_0.mjs";import { ray_hit } from "./cul_scope_0.mjs";import { ray_value } from "./cul_scope_0.mjs";import { ray_y } from "./cul_scope_0.mjs";import { ray_x } from "./cul_scope_0.mjs";import { ray_angle } from "./cul_scope_0.mjs";import { ray_steps } from "./cul_scope_0.mjs";import { level } from "./cul_scope_0.mjs";import { level_y } from "./cul_scope_0.mjs";import { level_x } from "./cul_scope_0.mjs";import { level_data } from "./cul_scope_0.mjs";import { player_y } from "./cul_scope_0.mjs";import { player_x } from "./cul_scope_0.mjs";import { range } from "underscore";

export const player_x_ = ({ player_x_in }) => player_x_in;
export const player_y_ = ({ player_y_in }) => player_y_in;

import level_data0 from "csv-loader!./level.csv";

export const level_data_ = ({}) => level_data0;

export const level_x_ = ({ level_x_in }) => Math.max(0, Math.min(63, level_x_in));
export const level_y_ = ({ level_y_in }) => Math.max(0, Math.min(63, level_y_in));

export const level_ = ({ level_y_in, level_x_in }) => +level_data({})[level_y({ level_y_in })][level_x({ level_x_in })];

export const ray_steps_ = ({ ray_steps_in }) => ray_steps_in;
export const ray_angle_ = ({ ray_angle_in }) => ray_angle_in;

export const ray_x_ = ({ player_x_in, ray_steps_in, ray_angle_in }) =>
Math.round(player_x({ player_x_in }) + ray_steps({ ray_steps_in }) * Math.cos(ray_angle({ ray_angle_in })));
export const ray_y_ = ({ player_y_in, ray_steps_in, ray_angle_in }) =>
Math.round(player_y({ player_y_in }) + ray_steps({ ray_steps_in }) * Math.sin(ray_angle({ ray_angle_in })));

export const ray_value_ = ({ player_x_in, ray_steps_in, ray_angle_in, player_y_in }) =>
level({
  level_x_in: ray_x({ player_x_in, ray_steps_in, ray_angle_in }),
  level_y_in: ray_y({ player_y_in, ray_steps_in, ray_angle_in }) });


export const ray_hit_ = ({ player_x_in, ray_angle_in, player_y_in }) =>
ray_level({ player_x_in, ray_angle_in, player_y_in }).filter(({ ray_value }) => ray_value != 0)[0];

export const ray_length_ = ({ player_x_in, ray_angle_in, player_y_in }) => ray_hit({ player_x_in, ray_angle_in, player_y_in }).i;
export const ray_hit_color_ = ({ player_x_in, ray_angle_in, player_y_in }) => ray_hit({ player_x_in, ray_angle_in, player_y_in }).ray_value;

export const inverse_ray_length_ = ({ player_x_in, ray_angle_in, player_y_in }) => 1 / ray_length({ player_x_in, ray_angle_in, player_y_in });
export const negative_inverse_ray_length_ = ({ ray_angle_in, time_in, keys_in, speed_in, fov_0_in }) => -inverse_ray_length({ ray_angle_in, time_in, keys_in, speed_in, fov_0_in });

// if ray calcs depended on player x/y and angle, better than depending on keys and time?
export const ray_level_ = ({ player_x_in, ray_angle_in, player_y_in }) =>
range(0, 1.42 * 64, 1).map((ray_steps_in, i) => ({
  i,
  ray_value: ray_value({ player_x_in, ray_angle_in, player_y_in, ray_steps_in }),
  ray_x: ray_x({ player_x_in, ray_steps_in, ray_angle_in }),
  ray_y: ray_y({ player_y_in, ray_steps_in, ray_angle_in })
}));

export const ray_ = ({ ray_angle_in, level_y_in, level_x_in, player_x_in, player_y_in }) => {
  if (ray_angle({ ray_angle_in }) == undefined) return 0;else
  {
    return ray_level({ player_x_in, ray_angle_in, player_y_in }).find(
    (d) => level_x({ level_x_in }) == d.ray_x && level_y({ level_y_in }) == d.ray_y && d.i <= ray_length({ player_x_in, ray_angle_in, player_y_in })) ==
    undefined ?
    0.8 :
    0;
  }
};

export const player_ = ({ level_x_in, player_x_in, level_y_in, player_y_in }) =>
level_x({ level_x_in }) == player_x({ player_x_in }) && level_y({ level_y_in }) == player_y({ player_y_in }) ? 0.5 : 0;

// field of view calcs
export const fov_ = ({ fov_in }) => fov_in;

export const angle_ = ({ level_y_in, player_y_in, level_x_in, player_x_in }) =>
// angle back to player from any cell in the level
Math.atan2(level_y({ level_y_in }) - player_y({ player_y_in }), level_x({ level_x_in }) - player_x({ player_x_in }));

// is a cell in the fov?
export const in_fov_ = ({ level_y_in, player_y_in, level_x_in, player_x_in, fov_in }) => angle({ level_y_in, player_y_in, level_x_in, player_x_in }) <= fov({ fov_in })[1] && angle({ level_y_in, player_y_in, level_x_in, player_x_in }) >= fov({ fov_in })[0];

// outputs for visual; these could/should be in a separate calculang model
export const level_player_ = ({ level_y_in, level_x_in, player_x_in, player_y_in }) =>
(level({ level_y_in, level_x_in }) != 0) * (1 + 0.5 * level({ level_y_in, level_x_in })) + player({ level_x_in, player_x_in, level_y_in, player_y_in }) * 3;

export const level_player_ray_ = ({ level_y_in, level_x_in, player_x_in, player_y_in, ray_angle_in }) =>
(level({ level_y_in, level_x_in }) != 0) * (1 + 0.5 * level({ level_y_in, level_x_in })) + player({ level_x_in, player_x_in, level_y_in, player_y_in }) * 3 - ray({ ray_angle_in, level_y_in, level_x_in, player_x_in, player_y_in });

export const level_player_ray_fov_ = ({ level_y_in, level_x_in, player_x_in, player_y_in, ray_angle_in, fov_in }) =>
(level({ level_y_in, level_x_in }) != 0) * (1 + 0.5 * level({ level_y_in, level_x_in })) + player({ level_x_in, player_x_in, level_y_in, player_y_in }) * 3 - ray({ ray_angle_in, level_y_in, level_x_in, player_x_in, player_y_in }) + in_fov({ level_y_in, player_y_in, level_x_in, player_x_in, fov_in }) * 0.5;

export const level_player_fov_ = ({ level_y_in, level_x_in, player_x_in, player_y_in, fov_in }) =>
(level({ level_y_in, level_x_in }) != 0) * (1 + 0.5 * level({ level_y_in, level_x_in })) + player({ level_x_in, player_x_in, level_y_in, player_y_in }) * 3 + in_fov({ level_y_in, player_y_in, level_x_in, player_x_in, fov_in }) * 0.5;