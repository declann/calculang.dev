import { shape_size_A } from "./cul_scope_2.mjs";import { shape_z_A } from "./cul_scope_2.mjs";import { shape_y_A } from "./cul_scope_2.mjs";import { shape_x_A } from "./cul_scope_2.mjs";import { shape_A } from "./cul_scope_2.mjs";import { shape_size } from "./cul_scope_2.mjs";import { shape_z } from "./cul_scope_2.mjs";import { shape_y } from "./cul_scope_2.mjs";import { shape_x } from "./cul_scope_2.mjs";import { shape } from "./cul_scope_2.mjs";import { sdf_xyz } from "./cul_scope_2.mjs";import { sdf } from "./cul_scope_0.mjs";import { operation } from "./cul_scope_0.mjs";import { sdf_B } from "./cul_scope_0.mjs";import { sdf_A } from "./cul_scope_0.mjs";import { sdf_ as sdf_xyz_ /* workaround for memo bug */ } from "./cul_scope_5.mjs";
export { sdf_xyz_ };

export const sdf_ = ({ i_in, shape_A_in, x_in, shape_x_A_in, y_in, shape_y_A_in, z_in, shape_z_A_in, shape_size_A_in }) => sdf_xyz({ i_in, shape_A_in, x_in, shape_x_A_in, y_in, shape_y_A_in, z_in, shape_z_A_in, shape_size_A_in });

// constrain sdf params to be _A inputs

export const shape_ = ({ shape_A_in }) => shape_A({ shape_A_in });
export const shape_x_ = ({ shape_x_A_in }) => shape_x_A({ shape_x_A_in });
export const shape_y_ = ({ shape_y_A_in }) => shape_y_A({ shape_y_A_in });
export const shape_z_ = ({ shape_z_A_in }) => shape_z_A({ shape_z_A_in });
export const shape_size_ = ({ shape_size_A_in }) => shape_size_A({ shape_size_A_in });

export const shape_A_ = ({ shape_A_in }) => shape_A_in;
export const shape_x_A_ = ({ shape_x_A_in }) => shape_x_A_in;
export const shape_y_A_ = ({ shape_y_A_in }) => shape_y_A_in;
export const shape_z_A_ = ({ shape_z_A_in }) => shape_z_A_in;
export const shape_size_A_ = ({ shape_size_A_in }) => shape_size_A_in;