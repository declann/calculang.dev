import { sdf } from "./cul_scope_0.mjs";import { operation } from "./cul_scope_0.mjs";import { sdf_B } from "./cul_scope_0.mjs";import { sdf_A } from "./cul_scope_0.mjs";
import { memoize } from 'underscore';
//import memoize from 'lru-memoize';
//import { isEqual } from 'underscore'; // TODO poor tree shaking support, or why is this impact so massive? Move to lodash/lodash-es?

// import/export non-to memo?

import { sdf_xyz_ as sdf_xyz$, sdf_ as sdf$, shape_ as shape$, shape_x_ as shape_x$, shape_y_ as shape_y$, shape_z_ as shape_z$, shape_size_ as shape_size$, shape_B_ as shape_B$, shape_x_B_ as shape_x_B$, shape_y_B_ as shape_y_B$, shape_z_B_ as shape_z_B$, shape_size_B_ as shape_size_B$ } from "./cul_scope_7.mjs"; // there is already-culed stuff in here, why? imports to memo loader include cul_scope_id, what logic should it apply RE passing forward? eliminate? Probably!





////////// start sdf_xyz memo-loader code //////////
//const sdf_xyz$m = memoize(999999, isEqual)(sdf_xyz$);
export const sdf_xyz$m = memoize(sdf_xyz$, JSON.stringify);
export const sdf_xyz = (a) => {
  return sdf_xyz$m(a);
  // eslint-disable-next-line no-undef
  sdf_xyz$({ i_in, shape_B_in, x_in, shape_x_B_in, y_in, shape_y_B_in, z_in, shape_z_B_in, shape_size_B_in }); // never run, but here to "trick" calculang graph logic
};
////////// end sdf_xyz memo-loader code //////////



////////// start sdf memo-loader code //////////
//const sdf$m = memoize(999999, isEqual)(sdf$);
export const sdf$m = memoize(sdf$, JSON.stringify);
export const sdf_ = (a) => {
  return sdf$m(a);
  // eslint-disable-next-line no-undef
  sdf$({ i_in, shape_B_in, x_in, shape_x_B_in, y_in, shape_y_B_in, z_in, shape_z_B_in, shape_size_B_in }); // never run, but here to "trick" calculang graph logic
};
////////// end sdf memo-loader code //////////



////////// start shape memo-loader code //////////
//const shape$m = memoize(999999, isEqual)(shape$);
export const shape$m = memoize(shape$, JSON.stringify);
export const shape = (a) => {
  return shape$m(a);
  // eslint-disable-next-line no-undef
  shape$({ shape_B_in }); // never run, but here to "trick" calculang graph logic
};
////////// end shape memo-loader code //////////



////////// start shape_x memo-loader code //////////
//const shape_x$m = memoize(999999, isEqual)(shape_x$);
export const shape_x$m = memoize(shape_x$, JSON.stringify);
export const shape_x = (a) => {
  return shape_x$m(a);
  // eslint-disable-next-line no-undef
  shape_x$({ shape_x_B_in }); // never run, but here to "trick" calculang graph logic
};
////////// end shape_x memo-loader code //////////



////////// start shape_y memo-loader code //////////
//const shape_y$m = memoize(999999, isEqual)(shape_y$);
export const shape_y$m = memoize(shape_y$, JSON.stringify);
export const shape_y = (a) => {
  return shape_y$m(a);
  // eslint-disable-next-line no-undef
  shape_y$({ shape_y_B_in }); // never run, but here to "trick" calculang graph logic
};
////////// end shape_y memo-loader code //////////



////////// start shape_z memo-loader code //////////
//const shape_z$m = memoize(999999, isEqual)(shape_z$);
export const shape_z$m = memoize(shape_z$, JSON.stringify);
export const shape_z = (a) => {
  return shape_z$m(a);
  // eslint-disable-next-line no-undef
  shape_z$({ shape_z_B_in }); // never run, but here to "trick" calculang graph logic
};
////////// end shape_z memo-loader code //////////



////////// start shape_size memo-loader code //////////
//const shape_size$m = memoize(999999, isEqual)(shape_size$);
export const shape_size$m = memoize(shape_size$, JSON.stringify);
export const shape_size = (a) => {
  return shape_size$m(a);
  // eslint-disable-next-line no-undef
  shape_size$({ shape_size_B_in }); // never run, but here to "trick" calculang graph logic
};
////////// end shape_size memo-loader code //////////



////////// start shape_B memo-loader code //////////
//const shape_B$m = memoize(999999, isEqual)(shape_B$);
export const shape_B$m = memoize(shape_B$, JSON.stringify);
export const shape_B = (a) => {
  return shape_B$m(a);
  // eslint-disable-next-line no-undef
  shape_B$({ shape_B_in }); // never run, but here to "trick" calculang graph logic
};
////////// end shape_B memo-loader code //////////



////////// start shape_x_B memo-loader code //////////
//const shape_x_B$m = memoize(999999, isEqual)(shape_x_B$);
export const shape_x_B$m = memoize(shape_x_B$, JSON.stringify);
export const shape_x_B = (a) => {
  return shape_x_B$m(a);
  // eslint-disable-next-line no-undef
  shape_x_B$({ shape_x_B_in }); // never run, but here to "trick" calculang graph logic
};
////////// end shape_x_B memo-loader code //////////



////////// start shape_y_B memo-loader code //////////
//const shape_y_B$m = memoize(999999, isEqual)(shape_y_B$);
export const shape_y_B$m = memoize(shape_y_B$, JSON.stringify);
export const shape_y_B = (a) => {
  return shape_y_B$m(a);
  // eslint-disable-next-line no-undef
  shape_y_B$({ shape_y_B_in }); // never run, but here to "trick" calculang graph logic
};
////////// end shape_y_B memo-loader code //////////



////////// start shape_z_B memo-loader code //////////
//const shape_z_B$m = memoize(999999, isEqual)(shape_z_B$);
export const shape_z_B$m = memoize(shape_z_B$, JSON.stringify);
export const shape_z_B = (a) => {
  return shape_z_B$m(a);
  // eslint-disable-next-line no-undef
  shape_z_B$({ shape_z_B_in }); // never run, but here to "trick" calculang graph logic
};
////////// end shape_z_B memo-loader code //////////



////////// start shape_size_B memo-loader code //////////
//const shape_size_B$m = memoize(999999, isEqual)(shape_size_B$);
export const shape_size_B$m = memoize(shape_size_B$, JSON.stringify);
export const shape_size_B = (a) => {
  return shape_size_B$m(a);
  // eslint-disable-next-line no-undef
  shape_size_B$({ shape_size_B_in }); // never run, but here to "trick" calculang graph logic
};
////////// end shape_size_B memo-loader code //////////