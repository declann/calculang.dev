import { i } from "./cul_scope_8.mjs";import { cube } from "./cul_scope_8.mjs";import { sphere } from "./cul_scope_8.mjs";import { z } from "./cul_scope_8.mjs";import { y } from "./cul_scope_8.mjs";import { x } from "./cul_scope_8.mjs";import { shape_size_B } from "./cul_scope_3.mjs";import { shape_z_B } from "./cul_scope_3.mjs";import { shape_y_B } from "./cul_scope_3.mjs";import { shape_x_B } from "./cul_scope_3.mjs";import { shape_B } from "./cul_scope_3.mjs";import { shape_size } from "./cul_scope_3.mjs";import { shape_z } from "./cul_scope_3.mjs";import { shape_y } from "./cul_scope_3.mjs";import { shape_x } from "./cul_scope_3.mjs";import { shape } from "./cul_scope_3.mjs";import { sdf_xyz } from "./cul_scope_3.mjs";import { sdf } from "./cul_scope_0.mjs";import { operation } from "./cul_scope_0.mjs";import { sdf_B } from "./cul_scope_0.mjs";import { sdf_A } from "./cul_scope_0.mjs";export const x_ = ({ x_in }) => x_in;
export const y_ = ({ y_in }) => y_in;
export const z_ = ({ z_in }) => z_in;


export const sphere_ = ({ x_in, shape_x_B_in, y_in, shape_y_B_in, z_in, shape_z_B_in, shape_size_B_in }) =>
Math.sqrt(
  (x({ x_in }) - shape_x({ shape_x_B_in })) ** 2 + (y({ y_in }) - shape_y({ shape_y_B_in })) ** 2 + (z({ z_in }) - shape_z({ shape_z_B_in })) ** 2
) - shape_size({ shape_size_B_in });

export const cube_ = ({ x_in, shape_x_B_in, y_in, shape_y_B_in, z_in, shape_z_B_in, shape_size_B_in }) =>
Math.max(
  Math.abs(x({ x_in }) - shape_x({ shape_x_B_in })),
  Math.abs(y({ y_in }) - shape_y({ shape_y_B_in })),
  Math.abs(z({ z_in }) - shape_z({ shape_z_B_in }))
) - shape_size({ shape_size_B_in });

export const shape_ = ({ shape_in }) => shape_in;
export const shape_x_ = ({ shape_x_in }) => shape_x_in;
export const shape_y_ = ({ shape_y_in }) => shape_y_in;
export const shape_z_ = ({ shape_z_in }) => shape_z_in;
export const shape_size_ = ({ shape_size_in }) => shape_size_in;

export const i_ = ({ i_in }) => i_in;

export const sdf_ = ({ i_in, shape_B_in, x_in, shape_x_B_in, y_in, shape_y_B_in, z_in, shape_z_B_in, shape_size_B_in }) => {
  if (i({ i_in }) <= 0) return 0; // ?

  if (shape({ shape_B_in }) == "sphere") return sphere({ x_in, shape_x_B_in, y_in, shape_y_B_in, z_in, shape_z_B_in, shape_size_B_in });else
  if (shape({ shape_B_in }) == "cube") return cube({ x_in, shape_x_B_in, y_in, shape_y_B_in, z_in, shape_z_B_in, shape_size_B_in });else
  if (shape({ shape_B_in }) == "cube_with_holes")
  return Math.max(
    Math.abs(x({ x_in })) - 0.3,
    Math.abs(y({ y_in })) - 0.3,
    Math.abs(z({ z_in })) - 0.3,
    -(1 + x({ x_in }) * x({ x_in }) + y({ y_in }) * y({ y_in }) + z({ z_in }) * z({ z_in })) / 2 + 0.57
  );else
  if (shape({ shape_B_in }) == "cube_and_torus")
  return Math.min(
    Math.max(
      Math.abs(x({ x_in })) - 0.3,
      Math.abs(y({ y_in })) - 0.3,
      Math.abs(z({ z_in })) - 0.3,
      -Math.sqrt(x({ x_in }) ** 2 + y({ y_in }) ** 2 + z({ z_in }) ** 2) + 0.375
    ),
    Math.sqrt(
      (Math.sqrt((x({ x_in }) - 0.25) ** 2 + (z({ z_in }) - 0.25) ** 2) - 0.25) ** 2 +
      (y({ y_in }) - 0.0) ** 2
    ) - 0.05
  );else
  if (shape({ shape_B_in }) == "teapot")
  return Math.min(
    Math.sqrt(
      (Math.sqrt(x({ x_in }) ** 2 + z({ z_in }) ** 2) - 0.3) ** 2 + (y({ y_in }) - 0.18) ** 2
    ) - 0.02,
    Math.sqrt(x({ x_in }) ** 2 + y({ y_in }) ** 2 * 2.5 + z({ z_in }) ** 2) - 0.4,
    Math.max(
      x({ x_in }) + y({ y_in }) - 0.15 - 0.05 - 0.5,
      -y({ y_in }) + 0.19 - 0.1,
      Math.sqrt(
        (Math.sqrt((x({ x_in }) - 0.55) ** 2 + (y({ y_in }) - 0.09) ** 2) - 0.1) ** 2 +
        (z({ z_in }) - 0.1) ** 2
      ) - 0.04
    ),
    Math.max(
      -(-y({ y_in }) + 0.19 - 0.1),
      Math.sqrt(
        (Math.sqrt((x({ x_in }) - 0.35) ** 2 + (y({ y_in }) - 0.09) ** 2) - 0.1) ** 2 +
        (z({ z_in }) - 0.1) ** 2
      ) - 0.04
    ),
    Math.sqrt(x({ x_in }) ** 2 + (y({ y_in }) - 0.27) ** 2 + z({ z_in }) ** 2) - 0.05
  );
};