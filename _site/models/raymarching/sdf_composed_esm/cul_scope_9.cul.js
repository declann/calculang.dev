export const x = () => x_in;
export const y = () => y_in;
export const z = () => z_in;


export const sphere = () =>
  Math.sqrt(
    (x() - shape_x()) ** 2 + (y() - shape_y()) ** 2 + (z() - shape_z()) ** 2
  ) - shape_size();

export const cube = () =>
  Math.max(
    Math.abs(x() - shape_x()),
    Math.abs(y() - shape_y()),
    Math.abs(z() - shape_z())
  ) - shape_size();

export const shape = () => shape_in;
export const shape_x = () => shape_x_in;
export const shape_y = () => shape_y_in;
export const shape_z = () => shape_z_in;
export const shape_size = () => shape_size_in;

export const i = () => i_in;

export const sdf = () => {
  if (i() <= 0) return 0; // ?

  if (shape() == "sphere") return sphere();
  else if (shape() == "cube") return cube();
  else if (shape() == "cube_with_holes")
    return Math.max(
      Math.abs(x()) - 0.3,
      Math.abs(y()) - 0.3,
      Math.abs(z()) - 0.3,
      -(1 + x() * x() + y() * y() + z() * z()) / 2 + 0.57
    );
  else if (shape() == "cube_and_torus")
    return Math.min(
      Math.max(
        Math.abs(x()) - 0.3,
        Math.abs(y()) - 0.3,
        Math.abs(z()) - 0.3,
        -Math.sqrt(x() ** 2 + y() ** 2 + z() ** 2) + 0.375
      ),
      Math.sqrt(
        (Math.sqrt((x() - 0.25) ** 2 + (z() - 0.25) ** 2) - 0.25) ** 2 +
          (y() - 0.0) ** 2
      ) - 0.05
    );
  else if (shape() == "teapot")
    return Math.min(
      Math.sqrt(
        (Math.sqrt(x() ** 2 + z() ** 2) - 0.3) ** 2 + (y() - 0.18) ** 2
      ) - 0.02,
      Math.sqrt(x() ** 2 + y() ** 2 * 2.5 + z() ** 2) - 0.4,
      Math.max(
        x() + y() - 0.15 - 0.05 - 0.5,
        -y() + 0.19 - 0.1,
        Math.sqrt(
          (Math.sqrt((x() - 0.55) ** 2 + (y() - 0.09) ** 2) - 0.1) ** 2 +
            (z() - 0.1) ** 2
        ) - 0.04
      ),
      Math.max(
        -(-y() + 0.19 - 0.1),
        Math.sqrt(
          (Math.sqrt((x() - 0.35) ** 2 + (y() - 0.09) ** 2) - 0.1) ** 2 +
            (z() - 0.1) ** 2
        ) - 0.04
      ),
      Math.sqrt(x() ** 2 + (y() - 0.27) ** 2 + z() ** 2) - 0.05
    );
};