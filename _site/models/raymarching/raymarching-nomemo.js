(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else {
		var a = factory();
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "row", function() { return row; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "col", function() { return col; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "cosAlpha", function() { return cosAlpha; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sinAlpha", function() { return sinAlpha; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "cosBeta", function() { return cosBeta; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sinBeta", function() { return sinBeta; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "camX", function() { return camX; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "camY", function() { return camY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "camZ", function() { return camZ; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pixelSize", function() { return pixelSize; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ux", function() { return ux; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "uz", function() { return uz; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "vx", function() { return vx; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "vy", function() { return vy; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "vz", function() { return vz; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "x0", function() { return x0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "y0", function() { return y0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "z0", function() { return z0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "matX0", function() { return matX0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "matX1", function() { return matX1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "matX2", function() { return matX2; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "matY0", function() { return matY0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "matY1", function() { return matY1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "matY2", function() { return matY2; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "matZ0", function() { return matZ0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "matZ1", function() { return matZ1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "matZ2", function() { return matZ2; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "nx", function() { return nx; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ny", function() { return ny; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "nz", function() { return nz; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "n", function() { return n; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "i", function() { return i; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "x", function() { return x; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "y", function() { return y; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "z", function() { return z; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "result1", function() { return result1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "result", function() { return result; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "brightness", function() { return brightness; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "brightness_2", function() { return brightness_2; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "luminosity_quantized_char", function() { return luminosity_quantized_char; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "raymarching_iterations", function() { return raymarching_iterations; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "screen_width", function() { return screen_width; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "screen_height", function() { return screen_height; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "xq", function() { return xq; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "yq", function() { return yq; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "alpha", function() { return alpha; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "beta", function() { return beta; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fov", function() { return fov; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "dist", function() { return dist; });
/* harmony import */ var _sdf_composed_cul_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3);
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "sdf", function() { return _sdf_composed_cul_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__["a"]; });

// dn
// from https://gist.github.com/pallada-92/b60f25d5a6aeddf7269d941bc35b6793
// tiny modifications, mainly paramaterise raymarching_iterations
// and some refactoring

/*
Copyright (c) 2023 Yaroslav Sergienko

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

// DN renamed rows, cols to screen_width,_height, not sure if better
// or if was useful old way

const row = ({ yq_in, screen_height_in }) => 1 - yq({ yq_in }) / screen_height({ screen_height_in });
const col = ({ xq_in, screen_width_in }) => xq({ xq_in }) / screen_width({ screen_width_in });

const cosAlpha = ({ alpha_in }) => Math.cos(alpha({ alpha_in }) * Math.PI / 180);
const sinAlpha = ({ alpha_in }) => Math.sin(alpha({ alpha_in }) * Math.PI / 180);
const cosBeta = ({ beta_in }) => Math.cos(beta({ beta_in }) * Math.PI / 180);
const sinBeta = ({ beta_in }) => Math.sin(beta({ beta_in }) * Math.PI / 180);

const camX = ({ dist_in, alpha_in, beta_in }) => dist({ dist_in }) * cosAlpha({ alpha_in }) * cosBeta({ beta_in });
const camY = ({ dist_in, beta_in }) => dist({ dist_in }) * sinBeta({ beta_in });
const camZ = ({ dist_in, alpha_in, beta_in }) => dist({ dist_in }) * sinAlpha({ alpha_in }) * cosBeta({ beta_in });

const pixelSize = ({ fov_in, screen_width_in }) =>
Math.tan(fov({ fov_in }) * Math.PI / 180 / 2) / ((screen_width({ screen_width_in }) - 1) / 2);

const ux = ({ fov_in, screen_width_in, alpha_in }) => -pixelSize({ fov_in, screen_width_in }) * sinAlpha({ alpha_in });
const uz = ({ fov_in, screen_width_in, alpha_in }) => +pixelSize({ fov_in, screen_width_in }) * cosAlpha({ alpha_in });
const vx = ({ fov_in, screen_width_in, alpha_in, beta_in }) => +pixelSize({ fov_in, screen_width_in }) * cosAlpha({ alpha_in }) * sinBeta({ beta_in });
const vy = ({ fov_in, screen_width_in, beta_in }) => -pixelSize({ fov_in, screen_width_in }) * cosBeta({ beta_in });
const vz = ({ fov_in, screen_width_in, alpha_in, beta_in }) => +pixelSize({ fov_in, screen_width_in }) * sinAlpha({ alpha_in }) * sinBeta({ beta_in });

const x0 = ({ alpha_in, beta_in }) => -cosAlpha({ alpha_in }) * cosBeta({ beta_in });
const y0 = ({ beta_in }) => -sinBeta({ beta_in });
const z0 = ({ alpha_in, beta_in }) => -sinAlpha({ alpha_in }) * cosBeta({ beta_in });

// matrix ....
const matX0 = ({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in }) =>
x0({ alpha_in, beta_in }) - screen_height({ screen_height_in }) / 2 * ux({ fov_in, screen_width_in, alpha_in }) - screen_width({ screen_width_in }) / 2 * vx({ fov_in, screen_width_in, alpha_in, beta_in }) + 0.02;
const matX1 = ({ fov_in, screen_width_in, alpha_in }) => ux({ fov_in, screen_width_in, alpha_in }) * screen_width({ screen_width_in });
const matX2 = ({ fov_in, screen_width_in, alpha_in, beta_in, screen_height_in }) => vx({ fov_in, screen_width_in, alpha_in, beta_in }) * screen_height({ screen_height_in });
const matY0 = ({ beta_in, screen_width_in, fov_in }) => y0({ beta_in }) - screen_width({ screen_width_in }) / 2 * vy({ fov_in, screen_width_in, beta_in }) + 0.05;
const matY1 = ({}) => 0;
const matY2 = ({ fov_in, screen_width_in, beta_in, screen_height_in }) => vy({ fov_in, screen_width_in, beta_in }) * screen_height({ screen_height_in });
const matZ0 = ({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in }) =>
z0({ alpha_in, beta_in }) - screen_height({ screen_height_in }) / 2 * uz({ fov_in, screen_width_in, alpha_in }) - screen_width({ screen_width_in }) / 2 * vz({ fov_in, screen_width_in, alpha_in, beta_in });
const matZ1 = ({ fov_in, screen_width_in, alpha_in }) => uz({ fov_in, screen_width_in, alpha_in }) * screen_width({ screen_width_in });
const matZ2 = ({ fov_in, screen_width_in, alpha_in, beta_in, screen_height_in }) => vz({ fov_in, screen_width_in, alpha_in, beta_in }) * screen_height({ screen_height_in });

const nx = ({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in }) => matX0({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in }) + col({ xq_in, screen_width_in }) * matX1({ fov_in, screen_width_in, alpha_in }) + row({ yq_in, screen_height_in }) * matX2({ fov_in, screen_width_in, alpha_in, beta_in, screen_height_in });
const ny = ({ beta_in, screen_width_in, fov_in, yq_in, screen_height_in }) => matY0({ beta_in, screen_width_in, fov_in }) + row({ yq_in, screen_height_in }) * matY2({ fov_in, screen_width_in, beta_in, screen_height_in });
const nz = ({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in }) => matZ0({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in }) + col({ xq_in, screen_width_in }) * matZ1({ fov_in, screen_width_in, alpha_in }) + row({ yq_in, screen_height_in }) * matZ2({ fov_in, screen_width_in, alpha_in, beta_in, screen_height_in });

const n = ({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in }) => Math.sqrt(nx({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in }) ** 2 + ny({ beta_in, screen_width_in, fov_in, yq_in, screen_height_in }) ** 2 + nz({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in }) ** 2) * 1.6;

const i = ({ i_in }) => i_in;

// if I dont override x,y,z, instead constrain them to = result_, then I won't need a sep. sdf_composed entrypoint for sdf viz, do this?
const /*result_*/x = ({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) => camX({ dist_in, alpha_in, beta_in }) + result({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) * nx({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in }) / n({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in });
const /*result_*/y = ({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) => camY({ dist_in, beta_in }) + result({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) * ny({ beta_in, screen_width_in, fov_in, yq_in, screen_height_in }) / n({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in });
const /*result_*/z = ({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) => camZ({ dist_in, alpha_in, beta_in }) + result({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) * nz({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in }) / n({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in });




const result1 = ({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) => {
  // this still depends on x,y,z
  // a perf hint ? would need to measure
  //for (let h = 0; h < i(); h += 5) result({ i_in: h });
  if (i({ i_in }) <= 0) return 0;else
  return result({ operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in, i_in: i({ i_in }) - 1 }) + Object(_sdf_composed_cul_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* sdf */ "a"])({ operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in, i_in: i({ i_in }) - 1 });
};

const result = ({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) => {
  if (i({ i_in }) <= 0) return 0;
  if (
   true &&
  i({ i_in }) > 2 &&
  result({ operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in, i_in: i({ i_in }) - 1 }) - result({ operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in, i_in: i({ i_in }) - 2 }) < 0.1)

  return result({ operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in, i_in: i({ i_in }) - 1 });
  if (i({ i_in }) > 2 && result({ operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in, i_in: i({ i_in }) - 1 }) > 3)
  return result({ operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in, i_in: i({ i_in }) - 1 });else
  return result1({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in });
};

const brightness = ({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) => Number(((result({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - 1.75) / 1.7).toFixed(2));
const brightness_2 = ({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) => Math.min(Math.max(brightness({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }), 0), 1);
const luminosity_quantized_char = ({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) => {
  const s = " .,-~:;=!*#$@";
  return s.split("")[Math.floor((1 - brightness_2({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in })) * s.length)];
};

// inputs:
const raymarching_iterations = ({ raymarching_iterations_in }) => raymarching_iterations_in;
const screen_width = ({ screen_width_in }) => screen_width_in;
const screen_height = ({ screen_height_in }) => screen_height_in;
const xq = ({ xq_in }) => xq_in;
const yq = ({ yq_in }) => yq_in;
const alpha = ({ alpha_in }) => alpha_in;
const beta = ({ beta_in }) => beta_in;
const fov = ({ fov_in }) => fov_in;
const dist = ({ dist_in }) => dist_in;

/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return sdf_; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return shape; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return shape_x; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return shape_y; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return shape_z; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return shape_size; });
/* unused harmony export shape_A */
/* unused harmony export shape_x_A */
/* unused harmony export shape_y_A */
/* unused harmony export shape_z_A */
/* unused harmony export shape_size_A */
/* harmony import */ var _sdf_composed_cul_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3);
/* harmony import */ var _sdf_B_cul_js_cul_scope_id_3_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2);
/* harmony import */ var _raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(0);
/* harmony import */ var _sdf_base_cul_cul_scope_id_4_cul_parent_scope_id_2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(5);



const sdf_ = ({ i_in, shape_A_in, dist_in, alpha_in, beta_in, operation_in, shape_B_in, shape_x_B_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_B_in, shape_z_B_in, shape_size_B_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in }) => Object(_sdf_base_cul_cul_scope_id_4_cul_parent_scope_id_2__WEBPACK_IMPORTED_MODULE_3__[/* sdf_ */ "a"])({ i_in, shape_A_in, dist_in, alpha_in, beta_in, operation_in, shape_B_in, shape_x_B_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_B_in, shape_z_B_in, shape_size_B_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in });

// constrain sdf params to be _A inputs

const shape = ({ shape_A_in }) => shape_A({ shape_A_in });
const shape_x = ({ shape_x_A_in }) => shape_x_A({ shape_x_A_in });
const shape_y = ({ shape_y_A_in }) => shape_y_A({ shape_y_A_in });
const shape_z = ({ shape_z_A_in }) => shape_z_A({ shape_z_A_in });
const shape_size = ({ shape_size_A_in }) => shape_size_A({ shape_size_A_in });

const shape_A = ({ shape_A_in }) => shape_A_in;
const shape_x_A = ({ shape_x_A_in }) => shape_x_A_in;
const shape_y_A = ({ shape_y_A_in }) => shape_y_A_in;
const shape_z_A = ({ shape_z_A_in }) => shape_z_A_in;
const shape_size_A = ({ shape_size_A_in }) => shape_size_A_in;

/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return sdf_; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return shape; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return shape_x; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return shape_y; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return shape_z; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return shape_size; });
/* unused harmony export shape_B */
/* unused harmony export shape_x_B */
/* unused harmony export shape_y_B */
/* unused harmony export shape_z_B */
/* unused harmony export shape_size_B */
/* harmony import */ var _sdf_composed_cul_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3);
/* harmony import */ var _sdf_A_cul_js_cul_scope_id_2_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1);
/* harmony import */ var _raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(0);
/* harmony import */ var _sdf_base_cul_cul_scope_id_5_cul_parent_scope_id_3__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(4);



const sdf_ = ({ i_in, shape_B_in, dist_in, alpha_in, beta_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) => Object(_sdf_base_cul_cul_scope_id_5_cul_parent_scope_id_3__WEBPACK_IMPORTED_MODULE_3__[/* sdf_ */ "a"])({ i_in, shape_B_in, dist_in, alpha_in, beta_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in });

// constrain sdf params to be _B inputs

const shape = ({ shape_B_in }) => shape_B({ shape_B_in });
const shape_x = ({ shape_x_B_in }) => shape_x_B({ shape_x_B_in });
const shape_y = ({ shape_y_B_in }) => shape_y_B({ shape_y_B_in });
const shape_z = ({ shape_z_B_in }) => shape_z_B({ shape_z_B_in });
const shape_size = ({ shape_size_B_in }) => shape_size_B({ shape_size_B_in });

const shape_B = ({ shape_B_in }) => shape_B_in;
const shape_x_B = ({ shape_x_B_in }) => shape_x_B_in;
const shape_y_B = ({ shape_y_B_in }) => shape_y_B_in;
const shape_z_B = ({ shape_z_B_in }) => shape_z_B_in;
const shape_size_B = ({ shape_size_B_in }) => shape_size_B_in;

/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export operation */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return sdf; });
/* harmony import */ var _raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var _sdf_A_cul_js_cul_scope_id_2_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1);
/* harmony import */ var _sdf_B_cul_js_cul_scope_id_3_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(2);






const operation = ({ operation_in }) => operation_in; // union, intersection, B-A, A-B

//export const sdf = () => {Math[operation()/* safety warning */](sdf_A(), sdf_B());

const sdf = ({ operation_in, i_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) => {
  if (operation({ operation_in }) == 'union') return Math.min(Object(_sdf_A_cul_js_cul_scope_id_2_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_1__[/* sdf_ */ "a"])({ i_in, shape_A_in, dist_in, alpha_in, beta_in, operation_in, shape_B_in, shape_x_B_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_B_in, shape_z_B_in, shape_size_B_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in }), Object(_sdf_B_cul_js_cul_scope_id_3_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_2__[/* sdf_ */ "a"])({ i_in, shape_B_in, dist_in, alpha_in, beta_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }));
  if (operation({ operation_in }) == 'intersection') return Math.max(Object(_sdf_A_cul_js_cul_scope_id_2_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_1__[/* sdf_ */ "a"])({ i_in, shape_A_in, dist_in, alpha_in, beta_in, operation_in, shape_B_in, shape_x_B_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_B_in, shape_z_B_in, shape_size_B_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in }), Object(_sdf_B_cul_js_cul_scope_id_3_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_2__[/* sdf_ */ "a"])({ i_in, shape_B_in, dist_in, alpha_in, beta_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }));
  if (operation({ operation_in }) == 'A-B') return Math.max(Object(_sdf_A_cul_js_cul_scope_id_2_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_1__[/* sdf_ */ "a"])({ i_in, shape_A_in, dist_in, alpha_in, beta_in, operation_in, shape_B_in, shape_x_B_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_B_in, shape_z_B_in, shape_size_B_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in }), -Object(_sdf_B_cul_js_cul_scope_id_3_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_2__[/* sdf_ */ "a"])({ i_in, shape_B_in, dist_in, alpha_in, beta_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in })); //sdf({operation_in: 'intersection'})
  if (operation({ operation_in }) == 'B-A') return Math.max(-Object(_sdf_A_cul_js_cul_scope_id_2_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_1__[/* sdf_ */ "a"])({ i_in, shape_A_in, dist_in, alpha_in, beta_in, operation_in, shape_B_in, shape_x_B_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_B_in, shape_z_B_in, shape_size_B_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in }), Object(_sdf_B_cul_js_cul_scope_id_3_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_2__[/* sdf_ */ "a"])({ i_in, shape_B_in, dist_in, alpha_in, beta_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }));
};

/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export x_ */
/* unused harmony export y_ */
/* unused harmony export z_ */
/* unused harmony export sphere */
/* unused harmony export cube */
/* unused harmony export shape_ */
/* unused harmony export shape_x_ */
/* unused harmony export shape_y_ */
/* unused harmony export shape_z_ */
/* unused harmony export shape_size_ */
/* unused harmony export i_ */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return sdf_; });
/* harmony import */ var _sdf_B_cul_js_cul_scope_id_3_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var _sdf_composed_cul_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3);
/* harmony import */ var _sdf_A_cul_js_cul_scope_id_2_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1);
/* harmony import */ var _raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(0);
const x_ = ({ x_in }) => x_in;
const y_ = ({ y_in }) => y_in;
const z_ = ({ z_in }) => z_in;


const sphere = ({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) =>
Math.sqrt(
  (Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["x"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - Object(_sdf_B_cul_js_cul_scope_id_3_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_0__[/* shape_x */ "d"])({ shape_x_B_in })) ** 2 + (Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["y"])({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - Object(_sdf_B_cul_js_cul_scope_id_3_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_0__[/* shape_y */ "e"])({ shape_y_B_in })) ** 2 + (Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["z"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - Object(_sdf_B_cul_js_cul_scope_id_3_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_0__[/* shape_z */ "f"])({ shape_z_B_in })) ** 2
) - Object(_sdf_B_cul_js_cul_scope_id_3_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_0__[/* shape_size */ "c"])({ shape_size_B_in });

const cube = ({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) =>
Math.max(
  Math.abs(Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["x"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - Object(_sdf_B_cul_js_cul_scope_id_3_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_0__[/* shape_x */ "d"])({ shape_x_B_in })),
  Math.abs(Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["y"])({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - Object(_sdf_B_cul_js_cul_scope_id_3_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_0__[/* shape_y */ "e"])({ shape_y_B_in })),
  Math.abs(Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["z"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - Object(_sdf_B_cul_js_cul_scope_id_3_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_0__[/* shape_z */ "f"])({ shape_z_B_in }))
) - Object(_sdf_B_cul_js_cul_scope_id_3_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_0__[/* shape_size */ "c"])({ shape_size_B_in });

const shape_ = ({ shape_in }) => shape_in;
const shape_x_ = ({ shape_x_in }) => shape_x_in;
const shape_y_ = ({ shape_y_in }) => shape_y_in;
const shape_z_ = ({ shape_z_in }) => shape_z_in;
const shape_size_ = ({ shape_size_in }) => shape_size_in;

const i_ = ({ i_in }) => i_in;

const sdf_ = ({ i_in, shape_B_in, dist_in, alpha_in, beta_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) => {
  if (Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["i"])({ i_in }) <= 0) return 0; // ?

  if (Object(_sdf_B_cul_js_cul_scope_id_3_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_0__[/* shape */ "b"])({ shape_B_in }) == "sphere") return sphere({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in });else
  if (Object(_sdf_B_cul_js_cul_scope_id_3_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_0__[/* shape */ "b"])({ shape_B_in }) == "cube") return cube({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in });else
  if (Object(_sdf_B_cul_js_cul_scope_id_3_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_0__[/* shape */ "b"])({ shape_B_in }) == "cube_with_holes")
  return Math.max(
    Math.abs(Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["x"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in })) - 0.3,
    Math.abs(Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["y"])({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in })) - 0.3,
    Math.abs(Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["z"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in })) - 0.3,
    -(1 + Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["x"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) * Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["x"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) + Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["y"])({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) * Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["y"])({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) + Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["z"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) * Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["z"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in })) / 2 + 0.57
  );else
  if (Object(_sdf_B_cul_js_cul_scope_id_3_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_0__[/* shape */ "b"])({ shape_B_in }) == "cube_and_torus")
  return Math.min(
    Math.max(
      Math.abs(Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["x"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in })) - 0.3,
      Math.abs(Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["y"])({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in })) - 0.3,
      Math.abs(Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["z"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in })) - 0.3,
      -Math.sqrt(Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["x"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) ** 2 + Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["y"])({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) ** 2 + Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["z"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) ** 2) + 0.375
    ),
    Math.sqrt(
      (Math.sqrt((Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["x"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - 0.25) ** 2 + (Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["z"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - 0.25) ** 2) - 0.25) ** 2 +
      (Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["y"])({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - 0.0) ** 2
    ) - 0.05
  );else
  if (Object(_sdf_B_cul_js_cul_scope_id_3_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_0__[/* shape */ "b"])({ shape_B_in }) == "teapot")
  return Math.min(
    Math.sqrt(
      (Math.sqrt(Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["x"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) ** 2 + Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["z"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) ** 2) - 0.3) ** 2 + (Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["y"])({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - 0.18) ** 2
    ) - 0.02,
    Math.sqrt(Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["x"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) ** 2 + Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["y"])({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) ** 2 * 2.5 + Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["z"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) ** 2) - 0.4,
    Math.max(
      Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["x"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) + Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["y"])({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - 0.15 - 0.05 - 0.5,
      -Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["y"])({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) + 0.19 - 0.1,
      Math.sqrt(
        (Math.sqrt((Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["x"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - 0.55) ** 2 + (Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["y"])({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - 0.09) ** 2) - 0.1) ** 2 +
        (Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["z"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - 0.1) ** 2
      ) - 0.04
    ),
    Math.max(
      -(-Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["y"])({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) + 0.19 - 0.1),
      Math.sqrt(
        (Math.sqrt((Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["x"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - 0.35) ** 2 + (Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["y"])({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - 0.09) ** 2) - 0.1) ** 2 +
        (Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["z"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - 0.1) ** 2
      ) - 0.04
    ),
    Math.sqrt(Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["x"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) ** 2 + (Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["y"])({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - 0.27) ** 2 + Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["z"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) ** 2) - 0.05
  );
};

/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export x_ */
/* unused harmony export y_ */
/* unused harmony export z_ */
/* unused harmony export sphere */
/* unused harmony export cube */
/* unused harmony export shape_ */
/* unused harmony export shape_x_ */
/* unused harmony export shape_y_ */
/* unused harmony export shape_z_ */
/* unused harmony export shape_size_ */
/* unused harmony export i_ */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return sdf_; });
/* harmony import */ var _sdf_A_cul_js_cul_scope_id_2_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1);
/* harmony import */ var _sdf_composed_cul_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3);
/* harmony import */ var _sdf_B_cul_js_cul_scope_id_3_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(2);
/* harmony import */ var _raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(0);
const x_ = ({ x_in }) => x_in;
const y_ = ({ y_in }) => y_in;
const z_ = ({ z_in }) => z_in;


const sphere = ({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) =>
Math.sqrt(
  (Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["x"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - Object(_sdf_A_cul_js_cul_scope_id_2_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_0__[/* shape_x */ "d"])({ shape_x_A_in })) ** 2 + (Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["y"])({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - Object(_sdf_A_cul_js_cul_scope_id_2_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_0__[/* shape_y */ "e"])({ shape_y_A_in })) ** 2 + (Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["z"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - Object(_sdf_A_cul_js_cul_scope_id_2_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_0__[/* shape_z */ "f"])({ shape_z_A_in })) ** 2
) - Object(_sdf_A_cul_js_cul_scope_id_2_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_0__[/* shape_size */ "c"])({ shape_size_A_in });

const cube = ({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) =>
Math.max(
  Math.abs(Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["x"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - Object(_sdf_A_cul_js_cul_scope_id_2_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_0__[/* shape_x */ "d"])({ shape_x_A_in })),
  Math.abs(Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["y"])({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - Object(_sdf_A_cul_js_cul_scope_id_2_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_0__[/* shape_y */ "e"])({ shape_y_A_in })),
  Math.abs(Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["z"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - Object(_sdf_A_cul_js_cul_scope_id_2_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_0__[/* shape_z */ "f"])({ shape_z_A_in }))
) - Object(_sdf_A_cul_js_cul_scope_id_2_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_0__[/* shape_size */ "c"])({ shape_size_A_in });

const shape_ = ({ shape_in }) => shape_in;
const shape_x_ = ({ shape_x_in }) => shape_x_in;
const shape_y_ = ({ shape_y_in }) => shape_y_in;
const shape_z_ = ({ shape_z_in }) => shape_z_in;
const shape_size_ = ({ shape_size_in }) => shape_size_in;

const i_ = ({ i_in }) => i_in;

const sdf_ = ({ i_in, shape_A_in, dist_in, alpha_in, beta_in, operation_in, shape_B_in, shape_x_B_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_B_in, shape_z_B_in, shape_size_B_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in }) => {
  if (Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["i"])({ i_in }) <= 0) return 0; // ?

  if (Object(_sdf_A_cul_js_cul_scope_id_2_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_0__[/* shape */ "b"])({ shape_A_in }) == "sphere") return sphere({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in });else
  if (Object(_sdf_A_cul_js_cul_scope_id_2_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_0__[/* shape */ "b"])({ shape_A_in }) == "cube") return cube({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in });else
  if (Object(_sdf_A_cul_js_cul_scope_id_2_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_0__[/* shape */ "b"])({ shape_A_in }) == "cube_with_holes")
  return Math.max(
    Math.abs(Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["x"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in })) - 0.3,
    Math.abs(Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["y"])({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in })) - 0.3,
    Math.abs(Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["z"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in })) - 0.3,
    -(1 + Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["x"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) * Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["x"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) + Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["y"])({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) * Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["y"])({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) + Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["z"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) * Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["z"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in })) / 2 + 0.57
  );else
  if (Object(_sdf_A_cul_js_cul_scope_id_2_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_0__[/* shape */ "b"])({ shape_A_in }) == "cube_and_torus")
  return Math.min(
    Math.max(
      Math.abs(Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["x"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in })) - 0.3,
      Math.abs(Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["y"])({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in })) - 0.3,
      Math.abs(Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["z"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in })) - 0.3,
      -Math.sqrt(Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["x"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) ** 2 + Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["y"])({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) ** 2 + Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["z"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) ** 2) + 0.375
    ),
    Math.sqrt(
      (Math.sqrt((Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["x"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - 0.25) ** 2 + (Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["z"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - 0.25) ** 2) - 0.25) ** 2 +
      (Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["y"])({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - 0.0) ** 2
    ) - 0.05
  );else
  if (Object(_sdf_A_cul_js_cul_scope_id_2_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_0__[/* shape */ "b"])({ shape_A_in }) == "teapot")
  return Math.min(
    Math.sqrt(
      (Math.sqrt(Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["x"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) ** 2 + Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["z"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) ** 2) - 0.3) ** 2 + (Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["y"])({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - 0.18) ** 2
    ) - 0.02,
    Math.sqrt(Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["x"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) ** 2 + Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["y"])({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) ** 2 * 2.5 + Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["z"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) ** 2) - 0.4,
    Math.max(
      Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["x"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) + Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["y"])({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - 0.15 - 0.05 - 0.5,
      -Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["y"])({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) + 0.19 - 0.1,
      Math.sqrt(
        (Math.sqrt((Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["x"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - 0.55) ** 2 + (Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["y"])({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - 0.09) ** 2) - 0.1) ** 2 +
        (Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["z"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - 0.1) ** 2
      ) - 0.04
    ),
    Math.max(
      -(-Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["y"])({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) + 0.19 - 0.1),
      Math.sqrt(
        (Math.sqrt((Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["x"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - 0.35) ** 2 + (Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["y"])({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - 0.09) ** 2) - 0.1) ** 2 +
        (Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["z"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - 0.1) ** 2
      ) - 0.04
    ),
    Math.sqrt(Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["x"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) ** 2 + (Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["y"])({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - 0.27) ** 2 + Object(_raymarching_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_3__["z"])({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) ** 2) - 0.05
  );
};

/***/ })
/******/ ]);
});
//# sourceMappingURL=raymarching-nomemo.js.map