import { sales, purchases, profit, sales_price, purchase_price, expenses } from "./cul_scope_1.mjs";
export { sales, purchases, profit, sales_price, purchase_price, expenses };

export const units = ({ sales_price_in }) =>
7 * 20000 - sales_price({ sales_price_in }) * 20000;