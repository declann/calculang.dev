import { sales, purchases, profit, sales_price, purchase_price, expenses, units_ as units_base } from "./cul_scope_1.mjs";
export { sales, purchases, profit, sales_price, purchase_price, expenses };

export const units = ({ units_in }) =>
units_base({ units_in }) * 0.9;