// TODO unit tests

// references: UCAR Simple Climate Model: https://scied.ucar.edu/interactive/simple-climate-model
// ClimateMARGO.jl

export const ppm_to_GtC = () => ppm_to_GtC_in; // 2.3 in UCAR, 2.13 in ClimateMARGO from https://web.archive.org/web/20170118004650/http://cdiac.ornl.gov/pns/convert.html

// [{emissions_rate, year_in}]
export const emissions_table = () => emissions_table_in;

export const emissions_rate = () => emissions_table().find(d => d.year_in == year()).emissions_rate; // gigatons carbon per year, not CO2 (=> no oxygen mass) // 10.5

export const year_0 = () => 2015;
export const temperature_0 = () => 14.65;
export const CO2_concentration_0 = () => 399.4;

export const climate_sensitivity = () => climate_sensitivity_in; // 3

export const year = () => year_in;

export const absorption = () => emissions_rate() * 0.45; // ocean, biosphere absorption (not atmosphere)

export const drawdown_factor = () => drawdown_factor_in;

export const drawdown = () =>
  drawdown_factor() * (CO2_concentration() * ppm_to_GtC());

export const CO2_concentration = () => {
  if (year() == year_0()) return CO2_concentration_0();
  else
    return (
      CO2_concentration({ year_in: year() - 1 }) + CO2_concentration_delta()
    );
};

export const net_carbon = () =>
  /* _rate? */ emissions_rate() - absorption() - drawdown(); // can be negative because of drawdown

export const CO2_concentration_delta = () =>
  net_carbon({ year_in: year() - 1 }) / ppm_to_GtC(); // https://github.com/ClimateMARGO/ClimateMARGO.jl/issues/86#issuecomment-1698107499 but my emissions rate is Gt Carbon (not CO2)

export const temperature = () => temperature_0() + temperature_delta();

export const concentration_factor = () =>
  CO2_concentration() / CO2_concentration_0();

export const temperature_delta = () =>
  Math.log2(concentration_factor()) * climate_sensitivity(); // doubling calibration
