import { temperature_delta } from "./cul_scope_0.mjs";import { concentration_factor } from "./cul_scope_0.mjs";import { temperature } from "./cul_scope_0.mjs";import { CO2_concentration_delta } from "./cul_scope_0.mjs";import { net_carbon } from "./cul_scope_0.mjs";import { CO2_concentration } from "./cul_scope_0.mjs";import { drawdown } from "./cul_scope_0.mjs";import { drawdown_factor } from "./cul_scope_0.mjs";import { absorption } from "./cul_scope_0.mjs";import { year } from "./cul_scope_0.mjs";import { climate_sensitivity } from "./cul_scope_0.mjs";import { CO2_concentration_0 } from "./cul_scope_0.mjs";import { temperature_0 } from "./cul_scope_0.mjs";import { year_0 } from "./cul_scope_0.mjs";import { emissions_rate } from "./cul_scope_0.mjs";import { emissions_table } from "./cul_scope_0.mjs";import { ppm_to_GtC } from "./cul_scope_0.mjs"; // TODO unit tests

// references: UCAR Simple Climate Model: https://scied.ucar.edu/interactive/simple-climate-model
// ClimateMARGO.jl

export const ppm_to_GtC_ = ({ ppm_to_GtC_in }) => ppm_to_GtC_in; // 2.3 in UCAR, 2.13 in ClimateMARGO from https://web.archive.org/web/20170118004650/http://cdiac.ornl.gov/pns/convert.html

// [{emissions_rate, year_in}]
export const emissions_table_ = ({ emissions_table_in }) => emissions_table_in;

export const emissions_rate_ = ({ emissions_table_in, year_in }) => emissions_table({ emissions_table_in }).find((d) => d.year_in == year({ year_in })).emissions_rate; // gigatons carbon per year, not CO2 (=> no oxygen mass) // 10.5

export const year_0_ = ({}) => 2015;
export const temperature_0_ = ({}) => 14.65;
export const CO2_concentration_0_ = ({}) => 399.4;

export const climate_sensitivity_ = ({ climate_sensitivity_in }) => climate_sensitivity_in; // 3

export const year_ = ({ year_in }) => year_in;

export const absorption_ = ({ emissions_table_in, year_in }) => emissions_rate({ emissions_table_in, year_in }) * 0.45; // ocean, biosphere absorption (not atmosphere)

export const drawdown_factor_ = ({ drawdown_factor_in }) => drawdown_factor_in;

export const drawdown_ = ({ drawdown_factor_in, year_in, emissions_table_in, ppm_to_GtC_in }) =>
drawdown_factor({ drawdown_factor_in }) * (CO2_concentration({ year_in, emissions_table_in, drawdown_factor_in, ppm_to_GtC_in }) * ppm_to_GtC({ ppm_to_GtC_in }));

export const CO2_concentration_ = ({ year_in, emissions_table_in, drawdown_factor_in, ppm_to_GtC_in }) => {
  if (year({ year_in }) == year_0({})) return CO2_concentration_0({});else

  return (
    CO2_concentration({ emissions_table_in, drawdown_factor_in, ppm_to_GtC_in, year_in: year({ year_in }) - 1 }) + CO2_concentration_delta({ emissions_table_in, year_in, drawdown_factor_in, ppm_to_GtC_in }));

};

export const net_carbon_ = ({ emissions_table_in, year_in, drawdown_factor_in, ppm_to_GtC_in }) =>
/* _rate? */emissions_rate({ emissions_table_in, year_in }) - absorption({ emissions_table_in, year_in }) - drawdown({ drawdown_factor_in, year_in, emissions_table_in, ppm_to_GtC_in }); // can be negative because of drawdown

export const CO2_concentration_delta_ = ({ emissions_table_in, year_in, drawdown_factor_in, ppm_to_GtC_in }) =>
net_carbon({ emissions_table_in, drawdown_factor_in, ppm_to_GtC_in, year_in: year({ year_in }) - 1 }) / ppm_to_GtC({ ppm_to_GtC_in }); // https://github.com/ClimateMARGO/ClimateMARGO.jl/issues/86#issuecomment-1698107499 but my emissions rate is Gt Carbon (not CO2)

export const temperature_ = ({ year_in, emissions_table_in, drawdown_factor_in, ppm_to_GtC_in, climate_sensitivity_in }) => temperature_0({}) + temperature_delta({ year_in, emissions_table_in, drawdown_factor_in, ppm_to_GtC_in, climate_sensitivity_in });

export const concentration_factor_ = ({ year_in, emissions_table_in, drawdown_factor_in, ppm_to_GtC_in }) =>
CO2_concentration({ year_in, emissions_table_in, drawdown_factor_in, ppm_to_GtC_in }) / CO2_concentration_0({});

export const temperature_delta_ = ({ year_in, emissions_table_in, drawdown_factor_in, ppm_to_GtC_in, climate_sensitivity_in }) =>
Math.log2(concentration_factor({ year_in, emissions_table_in, drawdown_factor_in, ppm_to_GtC_in })) * climate_sensitivity({ climate_sensitivity_in }); // doubling calibration