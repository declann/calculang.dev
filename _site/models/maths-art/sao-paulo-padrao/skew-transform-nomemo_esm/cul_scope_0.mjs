import {
  block_x,
  block_y,
  section_x,
  color,
  x_ as x0,
  y_ as y0 } from
"./cul_scope_1.mjs";
export { block_x, section_x, block_y, color };

export const skewness = ({ skewness_in }) => skewness_in;

// skew linear transformation
export const x = ({ y_in, skewness_in, x_in }) => y0({ y_in }) * skewness({ skewness_in }) + x0({ x_in });
export const y = ({ y_in }) => y0({ y_in });