
import { memoize } from 'underscore';
//import memoize from 'lru-memoize';
//import { isEqual } from 'underscore'; // TODO poor tree shaking support, or why is this impact so massive? Move to lodash/lodash-es?

// import/export non-to memo?

import { player_x_ as player_x$, player_y_ as player_y$, level_data_ as level_data$, level_x_ as level_x$, level_y_ as level_y$, level_ as level$, ray_steps_ as ray_steps$, ray_angle_ as ray_angle$, ray_x_ as ray_x$, ray_y_ as ray_y$, ray_value_ as ray_value$, ray_hit_ as ray_hit$, ray_length_ as ray_length$, ray_hit_color_base_ as ray_hit_color_base$, inverse_ray_length_base_ as inverse_ray_length_base$, negative_inverse_ray_length_ as negative_inverse_ray_length$, ray_level_ as ray_level$, ray_ as ray$, player_ as player$, fov_ as fov$, angle_ as angle$, in_fov_ as in_fov$, level_player_ as level_player$, level_player_ray_ as level_player_ray$, level_player_ray_fov_ as level_player_ray_fov$, level_player_fov_ as level_player_fov$, inverse_ray_length_ as inverse_ray_length$, ray_hit_color_ as ray_hit_color$, keys_ as keys$, time_ as time$, speed_ as speed$, fov_0_ as fov_0$, last_key_ as last_key$, forwardness_ as forwardness$, dfov_ as dfov$, fov_calcd_ as fov_calcd$, player_x_raw_ as player_x_raw$, player_x_calcd_ as player_x_calcd$, player_y_calcd_ as player_y_calcd$, player_y_raw_ as player_y_raw$ } from "./cul_scope_1.mjs"; // there is already-culed stuff in here, why? imports to memo loader include cul_scope_id, what logic should it apply RE passing forward? eliminate? Probably!





////////// start player_x memo-loader code //////////
//const player_x$m = memoize(999999, isEqual)(player_x$);
export const player_x$m = memoize(player_x$, JSON.stringify);
export const player_x = (a) => {
  return player_x$m(a);
  // eslint-disable-next-line no-undef
  player_x$({ player_x_in }); // never run, but here to "trick" calculang graph logic
};
////////// end player_x memo-loader code //////////



////////// start player_y memo-loader code //////////
//const player_y$m = memoize(999999, isEqual)(player_y$);
export const player_y$m = memoize(player_y$, JSON.stringify);
export const player_y = (a) => {
  return player_y$m(a);
  // eslint-disable-next-line no-undef
  player_y$({ player_y_in }); // never run, but here to "trick" calculang graph logic
};
////////// end player_y memo-loader code //////////



////////// start level_data memo-loader code //////////
//const level_data$m = memoize(999999, isEqual)(level_data$);
export const level_data$m = memoize(level_data$, JSON.stringify);
export const level_data = (a) => {
  return level_data$m(a);
  // eslint-disable-next-line no-undef
  level_data$({}); // never run, but here to "trick" calculang graph logic
};
////////// end level_data memo-loader code //////////



////////// start level_x memo-loader code //////////
//const level_x$m = memoize(999999, isEqual)(level_x$);
export const level_x$m = memoize(level_x$, JSON.stringify);
export const level_x = (a) => {
  return level_x$m(a);
  // eslint-disable-next-line no-undef
  level_x$({ level_x_in }); // never run, but here to "trick" calculang graph logic
};
////////// end level_x memo-loader code //////////



////////// start level_y memo-loader code //////////
//const level_y$m = memoize(999999, isEqual)(level_y$);
export const level_y$m = memoize(level_y$, JSON.stringify);
export const level_y = (a) => {
  return level_y$m(a);
  // eslint-disable-next-line no-undef
  level_y$({ level_y_in }); // never run, but here to "trick" calculang graph logic
};
////////// end level_y memo-loader code //////////



////////// start level memo-loader code //////////
//const level$m = memoize(999999, isEqual)(level$);
export const level$m = memoize(level$, JSON.stringify);
export const level = (a) => {
  return level$m(a);
  // eslint-disable-next-line no-undef
  level$({ level_y_in, level_x_in }); // never run, but here to "trick" calculang graph logic
};
////////// end level memo-loader code //////////



////////// start ray_steps memo-loader code //////////
//const ray_steps$m = memoize(999999, isEqual)(ray_steps$);
export const ray_steps$m = memoize(ray_steps$, JSON.stringify);
export const ray_steps = (a) => {
  return ray_steps$m(a);
  // eslint-disable-next-line no-undef
  ray_steps$({ ray_steps_in }); // never run, but here to "trick" calculang graph logic
};
////////// end ray_steps memo-loader code //////////



////////// start ray_angle memo-loader code //////////
//const ray_angle$m = memoize(999999, isEqual)(ray_angle$);
export const ray_angle$m = memoize(ray_angle$, JSON.stringify);
export const ray_angle = (a) => {
  return ray_angle$m(a);
  // eslint-disable-next-line no-undef
  ray_angle$({ ray_angle_in }); // never run, but here to "trick" calculang graph logic
};
////////// end ray_angle memo-loader code //////////



////////// start ray_x memo-loader code //////////
//const ray_x$m = memoize(999999, isEqual)(ray_x$);
export const ray_x$m = memoize(ray_x$, JSON.stringify);
export const ray_x = (a) => {
  return ray_x$m(a);
  // eslint-disable-next-line no-undef
  ray_x$({ player_x_in, ray_steps_in, ray_angle_in }); // never run, but here to "trick" calculang graph logic
};
////////// end ray_x memo-loader code //////////



////////// start ray_y memo-loader code //////////
//const ray_y$m = memoize(999999, isEqual)(ray_y$);
export const ray_y$m = memoize(ray_y$, JSON.stringify);
export const ray_y = (a) => {
  return ray_y$m(a);
  // eslint-disable-next-line no-undef
  ray_y$({ player_y_in, ray_steps_in, ray_angle_in }); // never run, but here to "trick" calculang graph logic
};
////////// end ray_y memo-loader code //////////



////////// start ray_value memo-loader code //////////
//const ray_value$m = memoize(999999, isEqual)(ray_value$);
export const ray_value$m = memoize(ray_value$, JSON.stringify);
export const ray_value = (a) => {
  return ray_value$m(a);
  // eslint-disable-next-line no-undef
  ray_value$({ player_x_in, ray_steps_in, ray_angle_in, player_y_in }); // never run, but here to "trick" calculang graph logic
};
////////// end ray_value memo-loader code //////////



////////// start ray_hit memo-loader code //////////
//const ray_hit$m = memoize(999999, isEqual)(ray_hit$);
export const ray_hit$m = memoize(ray_hit$, JSON.stringify);
export const ray_hit = (a) => {
  return ray_hit$m(a);
  // eslint-disable-next-line no-undef
  ray_hit$({ player_x_in, ray_angle_in, player_y_in }); // never run, but here to "trick" calculang graph logic
};
////////// end ray_hit memo-loader code //////////



////////// start ray_length memo-loader code //////////
//const ray_length$m = memoize(999999, isEqual)(ray_length$);
export const ray_length$m = memoize(ray_length$, JSON.stringify);
export const ray_length = (a) => {
  return ray_length$m(a);
  // eslint-disable-next-line no-undef
  ray_length$({ player_x_in, ray_angle_in, player_y_in }); // never run, but here to "trick" calculang graph logic
};
////////// end ray_length memo-loader code //////////



////////// start ray_hit_color_base memo-loader code //////////
//const ray_hit_color_base$m = memoize(999999, isEqual)(ray_hit_color_base$);
export const ray_hit_color_base$m = memoize(ray_hit_color_base$, JSON.stringify);
export const ray_hit_color_base = (a) => {
  return ray_hit_color_base$m(a);
  // eslint-disable-next-line no-undef
  ray_hit_color_base$({ player_x_in, ray_angle_in, player_y_in }); // never run, but here to "trick" calculang graph logic
};
////////// end ray_hit_color_base memo-loader code //////////



////////// start inverse_ray_length_base memo-loader code //////////
//const inverse_ray_length_base$m = memoize(999999, isEqual)(inverse_ray_length_base$);
export const inverse_ray_length_base$m = memoize(inverse_ray_length_base$, JSON.stringify);
export const inverse_ray_length_base = (a) => {
  return inverse_ray_length_base$m(a);
  // eslint-disable-next-line no-undef
  inverse_ray_length_base$({ player_x_in, ray_angle_in, player_y_in }); // never run, but here to "trick" calculang graph logic
};
////////// end inverse_ray_length_base memo-loader code //////////



////////// start negative_inverse_ray_length memo-loader code //////////
//const negative_inverse_ray_length$m = memoize(999999, isEqual)(negative_inverse_ray_length$);
export const negative_inverse_ray_length$m = memoize(negative_inverse_ray_length$, JSON.stringify);
export const negative_inverse_ray_length = (a) => {
  return negative_inverse_ray_length$m(a);
  // eslint-disable-next-line no-undef
  negative_inverse_ray_length$({ ray_angle_in, time_in, keys_in, speed_in, fov_0_in }); // never run, but here to "trick" calculang graph logic
};
////////// end negative_inverse_ray_length memo-loader code //////////



////////// start ray_level memo-loader code //////////
//const ray_level$m = memoize(999999, isEqual)(ray_level$);
export const ray_level$m = memoize(ray_level$, JSON.stringify);
export const ray_level = (a) => {
  return ray_level$m(a);
  // eslint-disable-next-line no-undef
  ray_level$({ player_x_in, ray_angle_in, player_y_in }); // never run, but here to "trick" calculang graph logic
};
////////// end ray_level memo-loader code //////////



////////// start ray memo-loader code //////////
//const ray$m = memoize(999999, isEqual)(ray$);
export const ray$m = memoize(ray$, JSON.stringify);
export const ray = (a) => {
  return ray$m(a);
  // eslint-disable-next-line no-undef
  ray$({ ray_angle_in, level_y_in, level_x_in, player_x_in, player_y_in }); // never run, but here to "trick" calculang graph logic
};
////////// end ray memo-loader code //////////



////////// start player memo-loader code //////////
//const player$m = memoize(999999, isEqual)(player$);
export const player$m = memoize(player$, JSON.stringify);
export const player = (a) => {
  return player$m(a);
  // eslint-disable-next-line no-undef
  player$({ level_x_in, player_x_in, level_y_in, player_y_in }); // never run, but here to "trick" calculang graph logic
};
////////// end player memo-loader code //////////



////////// start fov memo-loader code //////////
//const fov$m = memoize(999999, isEqual)(fov$);
export const fov$m = memoize(fov$, JSON.stringify);
export const fov = (a) => {
  return fov$m(a);
  // eslint-disable-next-line no-undef
  fov$({ fov_in }); // never run, but here to "trick" calculang graph logic
};
////////// end fov memo-loader code //////////



////////// start angle memo-loader code //////////
//const angle$m = memoize(999999, isEqual)(angle$);
export const angle$m = memoize(angle$, JSON.stringify);
export const angle = (a) => {
  return angle$m(a);
  // eslint-disable-next-line no-undef
  angle$({ level_y_in, player_y_in, level_x_in, player_x_in }); // never run, but here to "trick" calculang graph logic
};
////////// end angle memo-loader code //////////



////////// start in_fov memo-loader code //////////
//const in_fov$m = memoize(999999, isEqual)(in_fov$);
export const in_fov$m = memoize(in_fov$, JSON.stringify);
export const in_fov = (a) => {
  return in_fov$m(a);
  // eslint-disable-next-line no-undef
  in_fov$({ level_y_in, player_y_in, level_x_in, player_x_in, fov_in }); // never run, but here to "trick" calculang graph logic
};
////////// end in_fov memo-loader code //////////



////////// start level_player memo-loader code //////////
//const level_player$m = memoize(999999, isEqual)(level_player$);
export const level_player$m = memoize(level_player$, JSON.stringify);
export const level_player = (a) => {
  return level_player$m(a);
  // eslint-disable-next-line no-undef
  level_player$({ level_y_in, level_x_in, player_x_in, player_y_in }); // never run, but here to "trick" calculang graph logic
};
////////// end level_player memo-loader code //////////



////////// start level_player_ray memo-loader code //////////
//const level_player_ray$m = memoize(999999, isEqual)(level_player_ray$);
export const level_player_ray$m = memoize(level_player_ray$, JSON.stringify);
export const level_player_ray = (a) => {
  return level_player_ray$m(a);
  // eslint-disable-next-line no-undef
  level_player_ray$({ level_y_in, level_x_in, player_x_in, player_y_in, ray_angle_in }); // never run, but here to "trick" calculang graph logic
};
////////// end level_player_ray memo-loader code //////////



////////// start level_player_ray_fov memo-loader code //////////
//const level_player_ray_fov$m = memoize(999999, isEqual)(level_player_ray_fov$);
export const level_player_ray_fov$m = memoize(level_player_ray_fov$, JSON.stringify);
export const level_player_ray_fov = (a) => {
  return level_player_ray_fov$m(a);
  // eslint-disable-next-line no-undef
  level_player_ray_fov$({ level_y_in, level_x_in, player_x_in, player_y_in, ray_angle_in, fov_in }); // never run, but here to "trick" calculang graph logic
};
////////// end level_player_ray_fov memo-loader code //////////



////////// start level_player_fov memo-loader code //////////
//const level_player_fov$m = memoize(999999, isEqual)(level_player_fov$);
export const level_player_fov$m = memoize(level_player_fov$, JSON.stringify);
export const level_player_fov = (a) => {
  return level_player_fov$m(a);
  // eslint-disable-next-line no-undef
  level_player_fov$({ level_y_in, level_x_in, player_x_in, player_y_in, fov_in }); // never run, but here to "trick" calculang graph logic
};
////////// end level_player_fov memo-loader code //////////



////////// start inverse_ray_length memo-loader code //////////
//const inverse_ray_length$m = memoize(999999, isEqual)(inverse_ray_length$);
export const inverse_ray_length$m = memoize(inverse_ray_length$, JSON.stringify);
export const inverse_ray_length = (a) => {
  return inverse_ray_length$m(a);
  // eslint-disable-next-line no-undef
  inverse_ray_length$({ ray_angle_in, time_in, keys_in, speed_in, fov_0_in }); // never run, but here to "trick" calculang graph logic
};
////////// end inverse_ray_length memo-loader code //////////



////////// start ray_hit_color memo-loader code //////////
//const ray_hit_color$m = memoize(999999, isEqual)(ray_hit_color$);
export const ray_hit_color$m = memoize(ray_hit_color$, JSON.stringify);
export const ray_hit_color = (a) => {
  return ray_hit_color$m(a);
  // eslint-disable-next-line no-undef
  ray_hit_color$({ ray_angle_in, time_in, keys_in, speed_in, fov_0_in }); // never run, but here to "trick" calculang graph logic
};
////////// end ray_hit_color memo-loader code //////////



////////// start keys memo-loader code //////////
//const keys$m = memoize(999999, isEqual)(keys$);
export const keys$m = memoize(keys$, JSON.stringify);
export const keys = (a) => {
  return keys$m(a);
  // eslint-disable-next-line no-undef
  keys$({ keys_in }); // never run, but here to "trick" calculang graph logic
};
////////// end keys memo-loader code //////////



////////// start time memo-loader code //////////
//const time$m = memoize(999999, isEqual)(time$);
export const time$m = memoize(time$, JSON.stringify);
export const time = (a) => {
  return time$m(a);
  // eslint-disable-next-line no-undef
  time$({ time_in }); // never run, but here to "trick" calculang graph logic
};
////////// end time memo-loader code //////////



////////// start speed memo-loader code //////////
//const speed$m = memoize(999999, isEqual)(speed$);
export const speed$m = memoize(speed$, JSON.stringify);
export const speed = (a) => {
  return speed$m(a);
  // eslint-disable-next-line no-undef
  speed$({ speed_in }); // never run, but here to "trick" calculang graph logic
};
////////// end speed memo-loader code //////////



////////// start fov_0 memo-loader code //////////
//const fov_0$m = memoize(999999, isEqual)(fov_0$);
export const fov_0$m = memoize(fov_0$, JSON.stringify);
export const fov_0 = (a) => {
  return fov_0$m(a);
  // eslint-disable-next-line no-undef
  fov_0$({ fov_0_in }); // never run, but here to "trick" calculang graph logic
};
////////// end fov_0 memo-loader code //////////



////////// start last_key memo-loader code //////////
//const last_key$m = memoize(999999, isEqual)(last_key$);
export const last_key$m = memoize(last_key$, JSON.stringify);
export const last_key = (a) => {
  return last_key$m(a);
  // eslint-disable-next-line no-undef
  last_key$({ keys_in, time_in }); // never run, but here to "trick" calculang graph logic
};
////////// end last_key memo-loader code //////////



////////// start forwardness memo-loader code //////////
//const forwardness$m = memoize(999999, isEqual)(forwardness$);
export const forwardness$m = memoize(forwardness$, JSON.stringify);
export const forwardness = (a) => {
  return forwardness$m(a);
  // eslint-disable-next-line no-undef
  forwardness$({ keys_in, time_in }); // never run, but here to "trick" calculang graph logic
};
////////// end forwardness memo-loader code //////////



////////// start dfov memo-loader code //////////
//const dfov$m = memoize(999999, isEqual)(dfov$);
export const dfov$m = memoize(dfov$, JSON.stringify);
export const dfov = (a) => {
  return dfov$m(a);
  // eslint-disable-next-line no-undef
  dfov$({ keys_in, time_in }); // never run, but here to "trick" calculang graph logic
};
////////// end dfov memo-loader code //////////



////////// start fov_calcd memo-loader code //////////
//const fov_calcd$m = memoize(999999, isEqual)(fov_calcd$);
export const fov_calcd$m = memoize(fov_calcd$, JSON.stringify);
export const fov_calcd = (a) => {
  return fov_calcd$m(a);
  // eslint-disable-next-line no-undef
  fov_calcd$({ time_in, fov_0_in, keys_in }); // never run, but here to "trick" calculang graph logic
};
////////// end fov_calcd memo-loader code //////////



////////// start player_x_raw memo-loader code //////////
//const player_x_raw$m = memoize(999999, isEqual)(player_x_raw$);
export const player_x_raw$m = memoize(player_x_raw$, JSON.stringify);
export const player_x_raw = (a) => {
  return player_x_raw$m(a);
  // eslint-disable-next-line no-undef
  player_x_raw$({ time_in, keys_in, speed_in, fov_0_in }); // never run, but here to "trick" calculang graph logic
};
////////// end player_x_raw memo-loader code //////////



////////// start player_x_calcd memo-loader code //////////
//const player_x_calcd$m = memoize(999999, isEqual)(player_x_calcd$);
export const player_x_calcd$m = memoize(player_x_calcd$, JSON.stringify);
export const player_x_calcd = (a) => {
  return player_x_calcd$m(a);
  // eslint-disable-next-line no-undef
  player_x_calcd$({ time_in, keys_in, speed_in, fov_0_in }); // never run, but here to "trick" calculang graph logic
};
////////// end player_x_calcd memo-loader code //////////



////////// start player_y_calcd memo-loader code //////////
//const player_y_calcd$m = memoize(999999, isEqual)(player_y_calcd$);
export const player_y_calcd$m = memoize(player_y_calcd$, JSON.stringify);
export const player_y_calcd = (a) => {
  return player_y_calcd$m(a);
  // eslint-disable-next-line no-undef
  player_y_calcd$({ time_in, keys_in, speed_in, fov_0_in }); // never run, but here to "trick" calculang graph logic
};
////////// end player_y_calcd memo-loader code //////////



////////// start player_y_raw memo-loader code //////////
//const player_y_raw$m = memoize(999999, isEqual)(player_y_raw$);
export const player_y_raw$m = memoize(player_y_raw$, JSON.stringify);
export const player_y_raw = (a) => {
  return player_y_raw$m(a);
  // eslint-disable-next-line no-undef
  player_y_raw$({ time_in, keys_in, speed_in, fov_0_in }); // never run, but here to "trick" calculang graph logic
};
////////// end player_y_raw memo-loader code //////////