
    import { memoize } from 'underscore';
    //import memoize from 'lru-memoize';
    //import { isEqual } from 'underscore'; // TODO poor tree shaking support, or why is this impact so massive? Move to lodash/lodash-es?
    
    // import/export non-to memo?

    import { player_x_ as player_x$, player_y_ as player_y$, level_data_ as level_data$, level_x_ as level_x$, level_y_ as level_y$, level_ as level$, ray_steps_ as ray_steps$, ray_angle_ as ray_angle$, ray_x_ as ray_x$, ray_y_ as ray_y$, ray_value_ as ray_value$, ray_hit_ as ray_hit$, ray_length_ as ray_length$, ray_hit_color_ as ray_hit_color$, inverse_ray_length_ as inverse_ray_length$, negative_inverse_ray_length_ as negative_inverse_ray_length$, ray_level_ as ray_level$, ray_ as ray$, player_ as player$, fov_ as fov$, angle_ as angle$, in_fov_ as in_fov$, level_player_ as level_player$, level_player_ray_ as level_player_ray$, level_player_ray_fov_ as level_player_ray_fov$, level_player_fov_ as level_player_fov$ } from './raycasting.cul.js?&+memoed'; // there is already-culed stuff in here, why? imports to memo loader include cul_scope_id, what logic should it apply RE passing forward? eliminate? Probably!

    
    
    

////////// start player_x memo-loader code //////////
//const player_x$m = memoize(999999, isEqual)(player_x$);
export const player_x$m = memoize(player_x$, JSON.stringify);
export const player_x = (a) => {
  return player_x$m(a);
  // eslint-disable-next-line no-undef
  player_x$(); // never run, but here to "trick" calculang graph logic
};
////////// end player_x memo-loader code //////////



////////// start player_y memo-loader code //////////
//const player_y$m = memoize(999999, isEqual)(player_y$);
export const player_y$m = memoize(player_y$, JSON.stringify);
export const player_y = (a) => {
  return player_y$m(a);
  // eslint-disable-next-line no-undef
  player_y$(); // never run, but here to "trick" calculang graph logic
};
////////// end player_y memo-loader code //////////



////////// start level_data memo-loader code //////////
//const level_data$m = memoize(999999, isEqual)(level_data$);
export const level_data$m = memoize(level_data$, JSON.stringify);
export const level_data = (a) => {
  return level_data$m(a);
  // eslint-disable-next-line no-undef
  level_data$(); // never run, but here to "trick" calculang graph logic
};
////////// end level_data memo-loader code //////////



////////// start level_x memo-loader code //////////
//const level_x$m = memoize(999999, isEqual)(level_x$);
export const level_x$m = memoize(level_x$, JSON.stringify);
export const level_x = (a) => {
  return level_x$m(a);
  // eslint-disable-next-line no-undef
  level_x$(); // never run, but here to "trick" calculang graph logic
};
////////// end level_x memo-loader code //////////



////////// start level_y memo-loader code //////////
//const level_y$m = memoize(999999, isEqual)(level_y$);
export const level_y$m = memoize(level_y$, JSON.stringify);
export const level_y = (a) => {
  return level_y$m(a);
  // eslint-disable-next-line no-undef
  level_y$(); // never run, but here to "trick" calculang graph logic
};
////////// end level_y memo-loader code //////////



////////// start level memo-loader code //////////
//const level$m = memoize(999999, isEqual)(level$);
export const level$m = memoize(level$, JSON.stringify);
export const level = (a) => {
  return level$m(a);
  // eslint-disable-next-line no-undef
  level$(); // never run, but here to "trick" calculang graph logic
};
////////// end level memo-loader code //////////



////////// start ray_steps memo-loader code //////////
//const ray_steps$m = memoize(999999, isEqual)(ray_steps$);
export const ray_steps$m = memoize(ray_steps$, JSON.stringify);
export const ray_steps = (a) => {
  return ray_steps$m(a);
  // eslint-disable-next-line no-undef
  ray_steps$(); // never run, but here to "trick" calculang graph logic
};
////////// end ray_steps memo-loader code //////////



////////// start ray_angle memo-loader code //////////
//const ray_angle$m = memoize(999999, isEqual)(ray_angle$);
export const ray_angle$m = memoize(ray_angle$, JSON.stringify);
export const ray_angle = (a) => {
  return ray_angle$m(a);
  // eslint-disable-next-line no-undef
  ray_angle$(); // never run, but here to "trick" calculang graph logic
};
////////// end ray_angle memo-loader code //////////



////////// start ray_x memo-loader code //////////
//const ray_x$m = memoize(999999, isEqual)(ray_x$);
export const ray_x$m = memoize(ray_x$, JSON.stringify);
export const ray_x = (a) => {
  return ray_x$m(a);
  // eslint-disable-next-line no-undef
  ray_x$(); // never run, but here to "trick" calculang graph logic
};
////////// end ray_x memo-loader code //////////



////////// start ray_y memo-loader code //////////
//const ray_y$m = memoize(999999, isEqual)(ray_y$);
export const ray_y$m = memoize(ray_y$, JSON.stringify);
export const ray_y = (a) => {
  return ray_y$m(a);
  // eslint-disable-next-line no-undef
  ray_y$(); // never run, but here to "trick" calculang graph logic
};
////////// end ray_y memo-loader code //////////



////////// start ray_value memo-loader code //////////
//const ray_value$m = memoize(999999, isEqual)(ray_value$);
export const ray_value$m = memoize(ray_value$, JSON.stringify);
export const ray_value = (a) => {
  return ray_value$m(a);
  // eslint-disable-next-line no-undef
  ray_value$(); // never run, but here to "trick" calculang graph logic
};
////////// end ray_value memo-loader code //////////



////////// start ray_hit memo-loader code //////////
//const ray_hit$m = memoize(999999, isEqual)(ray_hit$);
export const ray_hit$m = memoize(ray_hit$, JSON.stringify);
export const ray_hit = (a) => {
  return ray_hit$m(a);
  // eslint-disable-next-line no-undef
  ray_hit$(); // never run, but here to "trick" calculang graph logic
};
////////// end ray_hit memo-loader code //////////



////////// start ray_length memo-loader code //////////
//const ray_length$m = memoize(999999, isEqual)(ray_length$);
export const ray_length$m = memoize(ray_length$, JSON.stringify);
export const ray_length = (a) => {
  return ray_length$m(a);
  // eslint-disable-next-line no-undef
  ray_length$(); // never run, but here to "trick" calculang graph logic
};
////////// end ray_length memo-loader code //////////



////////// start ray_hit_color memo-loader code //////////
//const ray_hit_color$m = memoize(999999, isEqual)(ray_hit_color$);
export const ray_hit_color$m = memoize(ray_hit_color$, JSON.stringify);
export const ray_hit_color = (a) => {
  return ray_hit_color$m(a);
  // eslint-disable-next-line no-undef
  ray_hit_color$(); // never run, but here to "trick" calculang graph logic
};
////////// end ray_hit_color memo-loader code //////////



////////// start inverse_ray_length memo-loader code //////////
//const inverse_ray_length$m = memoize(999999, isEqual)(inverse_ray_length$);
export const inverse_ray_length$m = memoize(inverse_ray_length$, JSON.stringify);
export const inverse_ray_length = (a) => {
  return inverse_ray_length$m(a);
  // eslint-disable-next-line no-undef
  inverse_ray_length$(); // never run, but here to "trick" calculang graph logic
};
////////// end inverse_ray_length memo-loader code //////////



////////// start negative_inverse_ray_length memo-loader code //////////
//const negative_inverse_ray_length$m = memoize(999999, isEqual)(negative_inverse_ray_length$);
export const negative_inverse_ray_length$m = memoize(negative_inverse_ray_length$, JSON.stringify);
export const negative_inverse_ray_length = (a) => {
  return negative_inverse_ray_length$m(a);
  // eslint-disable-next-line no-undef
  negative_inverse_ray_length$(); // never run, but here to "trick" calculang graph logic
};
////////// end negative_inverse_ray_length memo-loader code //////////



////////// start ray_level memo-loader code //////////
//const ray_level$m = memoize(999999, isEqual)(ray_level$);
export const ray_level$m = memoize(ray_level$, JSON.stringify);
export const ray_level = (a) => {
  return ray_level$m(a);
  // eslint-disable-next-line no-undef
  ray_level$(); // never run, but here to "trick" calculang graph logic
};
////////// end ray_level memo-loader code //////////



////////// start ray memo-loader code //////////
//const ray$m = memoize(999999, isEqual)(ray$);
export const ray$m = memoize(ray$, JSON.stringify);
export const ray = (a) => {
  return ray$m(a);
  // eslint-disable-next-line no-undef
  ray$(); // never run, but here to "trick" calculang graph logic
};
////////// end ray memo-loader code //////////



////////// start player memo-loader code //////////
//const player$m = memoize(999999, isEqual)(player$);
export const player$m = memoize(player$, JSON.stringify);
export const player = (a) => {
  return player$m(a);
  // eslint-disable-next-line no-undef
  player$(); // never run, but here to "trick" calculang graph logic
};
////////// end player memo-loader code //////////



////////// start fov memo-loader code //////////
//const fov$m = memoize(999999, isEqual)(fov$);
export const fov$m = memoize(fov$, JSON.stringify);
export const fov = (a) => {
  return fov$m(a);
  // eslint-disable-next-line no-undef
  fov$(); // never run, but here to "trick" calculang graph logic
};
////////// end fov memo-loader code //////////



////////// start angle memo-loader code //////////
//const angle$m = memoize(999999, isEqual)(angle$);
export const angle$m = memoize(angle$, JSON.stringify);
export const angle = (a) => {
  return angle$m(a);
  // eslint-disable-next-line no-undef
  angle$(); // never run, but here to "trick" calculang graph logic
};
////////// end angle memo-loader code //////////



////////// start in_fov memo-loader code //////////
//const in_fov$m = memoize(999999, isEqual)(in_fov$);
export const in_fov$m = memoize(in_fov$, JSON.stringify);
export const in_fov = (a) => {
  return in_fov$m(a);
  // eslint-disable-next-line no-undef
  in_fov$(); // never run, but here to "trick" calculang graph logic
};
////////// end in_fov memo-loader code //////////



////////// start level_player memo-loader code //////////
//const level_player$m = memoize(999999, isEqual)(level_player$);
export const level_player$m = memoize(level_player$, JSON.stringify);
export const level_player = (a) => {
  return level_player$m(a);
  // eslint-disable-next-line no-undef
  level_player$(); // never run, but here to "trick" calculang graph logic
};
////////// end level_player memo-loader code //////////



////////// start level_player_ray memo-loader code //////////
//const level_player_ray$m = memoize(999999, isEqual)(level_player_ray$);
export const level_player_ray$m = memoize(level_player_ray$, JSON.stringify);
export const level_player_ray = (a) => {
  return level_player_ray$m(a);
  // eslint-disable-next-line no-undef
  level_player_ray$(); // never run, but here to "trick" calculang graph logic
};
////////// end level_player_ray memo-loader code //////////



////////// start level_player_ray_fov memo-loader code //////////
//const level_player_ray_fov$m = memoize(999999, isEqual)(level_player_ray_fov$);
export const level_player_ray_fov$m = memoize(level_player_ray_fov$, JSON.stringify);
export const level_player_ray_fov = (a) => {
  return level_player_ray_fov$m(a);
  // eslint-disable-next-line no-undef
  level_player_ray_fov$(); // never run, but here to "trick" calculang graph logic
};
////////// end level_player_ray_fov memo-loader code //////////



////////// start level_player_fov memo-loader code //////////
//const level_player_fov$m = memoize(999999, isEqual)(level_player_fov$);
export const level_player_fov$m = memoize(level_player_fov$, JSON.stringify);
export const level_player_fov = (a) => {
  return level_player_fov$m(a);
  // eslint-disable-next-line no-undef
  level_player_fov$(); // never run, but here to "trick" calculang graph logic
};
////////// end level_player_fov memo-loader code //////////


    