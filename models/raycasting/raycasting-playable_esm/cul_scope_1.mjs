import { player_y_raw } from "./cul_scope_0.mjs";import { player_y_calcd } from "./cul_scope_0.mjs";import { player_x_calcd } from "./cul_scope_0.mjs";import { player_x_raw } from "./cul_scope_0.mjs";import { fov_calcd } from "./cul_scope_0.mjs";import { dfov } from "./cul_scope_0.mjs";import { forwardness } from "./cul_scope_0.mjs";import { last_key } from "./cul_scope_0.mjs";import { fov_0 } from "./cul_scope_0.mjs";import { speed } from "./cul_scope_0.mjs";import { time } from "./cul_scope_0.mjs";import { keys } from "./cul_scope_0.mjs";import { ray_hit_color } from "./cul_scope_0.mjs";import { inverse_ray_length } from "./cul_scope_0.mjs";import { level_player_fov } from "./cul_scope_0.mjs";import { level_player_ray_fov } from "./cul_scope_0.mjs";import { level_player_ray } from "./cul_scope_0.mjs";import { level_player } from "./cul_scope_0.mjs";import { in_fov } from "./cul_scope_0.mjs";import { angle } from "./cul_scope_0.mjs";import { fov } from "./cul_scope_0.mjs";import { player } from "./cul_scope_0.mjs";import { ray } from "./cul_scope_0.mjs";import { ray_level } from "./cul_scope_0.mjs";import { negative_inverse_ray_length } from "./cul_scope_0.mjs";import { inverse_ray_length_base } from "./cul_scope_0.mjs";import { ray_hit_color_base } from "./cul_scope_0.mjs";import { ray_length } from "./cul_scope_0.mjs";import { ray_hit } from "./cul_scope_0.mjs";import { ray_value } from "./cul_scope_0.mjs";import { ray_y } from "./cul_scope_0.mjs";import { ray_x } from "./cul_scope_0.mjs";import { ray_angle } from "./cul_scope_0.mjs";import { ray_steps } from "./cul_scope_0.mjs";import { level } from "./cul_scope_0.mjs";import { level_y } from "./cul_scope_0.mjs";import { level_x } from "./cul_scope_0.mjs";import { level_data } from "./cul_scope_0.mjs";import { player_y } from "./cul_scope_0.mjs";import { player_x } from "./cul_scope_0.mjs"; // there were important perf changes here to ponder!
// overriding player x/y => ray level calcs depend on key calcs,
// versus populating player x/y with calcd positions so that key calcs don't propagate down to ray calcs

import {
player_x_,
player_y_,
level_data_,
level_x_,
level_y_,
level_,
ray_steps_,
ray_angle_,
ray_x_,
ray_y_,
ray_value_,
ray_hit_,
ray_length_,
ray_hit_color_ as ray_hit_color_base_,
inverse_ray_length_ as inverse_ray_length_base_,
negative_inverse_ray_length_,
ray_level_,
ray_,
player_,
fov_,
angle_,
in_fov_,
level_player_,
level_player_ray_,
level_player_ray_fov_,
level_player_fov_ } from
"./cul_scope_2.mjs";
export {
inverse_ray_length_base_, ray_hit_color_base_,
player_x_,
player_y_,
level_data_,
level_x_,
level_y_,
level_,
ray_steps_,
ray_angle_,
ray_x_,
ray_y_,
ray_value_,
ray_hit_,
ray_length_,
//ray_hit_color,
//inverse_ray_length,
negative_inverse_ray_length_,
ray_level_,
ray_,
player_,
fov_,
angle_,
in_fov_,
level_player_,
level_player_ray_,
level_player_ray_fov_,
level_player_fov_ };


// events processing

// big perf problems aren't fixed by using data_table_in. keys is still used in c-p, plus likely redundancy below // => only using this model for controls
// ^ old comment. This approach was far faster! Using actual modular model,
// player x/y are determined by keys, which adds complexity to ray-level calcs
// Alternatively to override, just supply player_x_in:
// other approaches?
// how many formulae are we really interested in - not that much!
// ray_lengths and colors 

export const inverse_ray_length_ = ({ ray_angle_in, time_in, keys_in, speed_in, fov_0_in }) => inverse_ray_length_base({ ray_angle_in, player_x_in: player_x_calcd({ time_in, keys_in, speed_in, fov_0_in }), player_y_in: player_y_calcd({ time_in, keys_in, speed_in, fov_0_in }) /*, fov_in:fov_calcd()*/ });
export const ray_hit_color_ = ({ ray_angle_in, time_in, keys_in, speed_in, fov_0_in }) => ray_hit_color_base({ ray_angle_in, player_x_in: player_x_calcd({ time_in, keys_in, speed_in, fov_0_in }), player_y_in: player_y_calcd({ time_in, keys_in, speed_in, fov_0_in }) /*, fov_in:fov_calcd()*/ });

//export const data_table = () => data_table_in;
export const keys_ = ({ keys_in }) => keys_in; // data_table();
export const time_ = ({ time_in }) => time_in;
export const speed_ = ({ speed_in }) => speed_in;
export const fov_0_ = ({ fov_0_in }) => fov_0_in;

export const last_key_ = ({ keys_in, time_in }) => keys({ keys_in })[time({ time_in }) - 1];
export const forwardness_ = ({ keys_in, time_in }) =>
last_key({ keys_in, time_in }) == "ArrowUp" ? 1 : last_key({ keys_in, time_in }) == "ArrowDown" ? -1 : 0;

export const dfov_ = ({ keys_in, time_in }) =>
last_key({ keys_in, time_in }) == "ArrowLeft" ? -0.2 : last_key({ keys_in, time_in }) == "ArrowRight" ? 0.2 : 0;

export const fov_calcd_ = ({ time_in, fov_0_in, keys_in }) => {
  if (time({ time_in }) == 0) return fov_0({ fov_0_in });else

  return [
  fov_calcd({ fov_0_in, keys_in, time_in: time({ time_in }) - 1 })[0] + dfov({ keys_in, time_in }), // hmm
  fov_calcd({ fov_0_in, keys_in, time_in: time({ time_in }) - 1 })[1] + dfov({ keys_in, time_in })];

};

export const player_x_raw_ = ({ time_in, keys_in, speed_in, fov_0_in }) => {
  if (time({ time_in }) == 0) return 32;else

  return (
    player_x_raw({ keys_in, speed_in, fov_0_in, time_in: time({ time_in }) - 1 }) +
    forwardness({ keys_in, time_in }) * speed({ speed_in }) * Math.cos((fov_calcd({ time_in, fov_0_in, keys_in })[0] + fov_calcd({ time_in, fov_0_in, keys_in })[1]) / 2));

};
export const player_x_calcd_ = ({ time_in, keys_in, speed_in, fov_0_in }) => Math.round(player_x_raw({ time_in, keys_in, speed_in, fov_0_in }));
export const player_y_calcd_ = ({ time_in, keys_in, speed_in, fov_0_in }) => Math.round(player_y_raw({ time_in, keys_in, speed_in, fov_0_in }));

export const player_y_raw_ = ({ time_in, keys_in, speed_in, fov_0_in }) => {
  if (time({ time_in }) == 0) return 32;else

  return (
    player_y_raw({ keys_in, speed_in, fov_0_in, time_in: time({ time_in }) - 1 }) +
    forwardness({ keys_in, time_in }) * speed({ speed_in }) * Math.sin((fov_calcd({ time_in, fov_0_in, keys_in })[0] + fov_calcd({ time_in, fov_0_in, keys_in })[1]) / 2));

};