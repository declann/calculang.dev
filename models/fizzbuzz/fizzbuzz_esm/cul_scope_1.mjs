import { i } from "./cul_scope_0.mjs";import { fizzbuzz } from "./cul_scope_0.mjs";import { buzz } from "./cul_scope_0.mjs";import { fizz } from "./cul_scope_0.mjs";export const fizz_ = ({ i_in }) => i({ i_in }) % 3 == 0 ? 'Fizz' : '';
export const buzz_ = ({ i_in }) => i({ i_in }) % 5 == 0 ? 'Buzz' : '';

export const fizzbuzz_ = ({ i_in }) => fizz({ i_in }) + buzz({ i_in });

// inputs:
export const i_ = ({ i_in }) => i_in;