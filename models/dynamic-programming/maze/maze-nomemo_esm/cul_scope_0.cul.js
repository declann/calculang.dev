// https://www.youtube.com/watch?v=0yKf8TrLUOw&t=856s @ 16 minutes

// maze_in is an input 2-d matrix of values to collect. We need to go from top left to bottom right
// we can't walk backwards (but we can calculate backwards :)

// value in the maze for x,y
export const value = () => maze()[y()][x()];

// dynamic programming solution:

// maximum value collected after reaching/collected at a square in the maze
// for first row and first col easy: there is only one straight path
// otherwise we calculate maximum of approaches
export const max = () => {
  if (x() == 0 && y() == 0) return value();
  else if (x() == 0) return max({ y_in: y() - 1 }) + value();
  else if (y() == 0) return max({ x_in: x() - 1 }) + value();
  else
    return Math.max(max({ y_in: y() - 1 }), max({ x_in: x() - 1 })) + value();
};

// starting from the end
export const arrow = () => {
  if (x() == 0) return "^";
  else if (y() == 0) return "<";
  else return max({ y_in: y() - 1 }) > max({ x_in: x() - 1 }) ? "^" : "<";
};

// If pointed at determines inclusion in arrow_path
export const arrow_path = () => {
  if (x() == 14 && y() == 14) return arrow();
  else if (x() == 14 && pointed_at_from_below()) return arrow();
  else if (y() == 14 && pointed_at_from_right()) return arrow();
  else if (x() == 14 || y() == 14) return "";
  else if (pointed_at_from_right()) return arrow();
  else if (pointed_at_from_below()) return arrow();
  else return "";
};

// just for readability
export const pointed_at_from_below = () => arrow_path({ y_in: y() + 1 }) == "^";
export const pointed_at_from_right = () => arrow_path({ x_in: x() + 1 }) == "<";

// starting from the start
// due to direction change arrow is wrong,
// but run with it and use emojis
export const arrow_path_pretty = () => {
  if (x() == 0 && y() == 0) return "🐣";
  else if (x() == 14 && y() == 14) return "🏆";
  else return arrow_path() == "^" ? "⬇️" : arrow_path() == "<" ? "➡️‍" : "";
};

// it will be useful to try different approaches to path plotting, including recording the path during calculation; cleanly and careful about side-effects, hmmm
// also create context/credibility: show that other paths are inferior. But this wasn't my challenge for now, if you work on this shout!

// inputs:
export const maze = () => maze_in;
export const x = () => x_in;
export const y = () => y_in;
