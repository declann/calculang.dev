
import { memoize } from 'underscore';
//import memoize from 'lru-memoize';
//import { isEqual } from 'underscore'; // TODO poor tree shaking support, or why is this impact so massive? Move to lodash/lodash-es?

// import/export non-to memo?

import { value_ as value$, max_ as max$, arrow_ as arrow$, arrow_path_ as arrow_path$, pointed_at_from_below_ as pointed_at_from_below$, pointed_at_from_right_ as pointed_at_from_right$, arrow_path_pretty_ as arrow_path_pretty$, maze_ as maze$, x_ as x$, y_ as y$ } from "./cul_scope_1.mjs"; // there is already-culed stuff in here, why? imports to memo loader include cul_scope_id, what logic should it apply RE passing forward? eliminate? Probably!





////////// start value memo-loader code //////////
//const value$m = memoize(999999, isEqual)(value$);
export const value$m = memoize(value$, JSON.stringify);
export const value = (a) => {
  return value$m(a);
  // eslint-disable-next-line no-undef
  value$({ maze_in, y_in, x_in }); // never run, but here to "trick" calculang graph logic
};
////////// end value memo-loader code //////////



////////// start max memo-loader code //////////
//const max$m = memoize(999999, isEqual)(max$);
export const max$m = memoize(max$, JSON.stringify);
export const max = (a) => {
  return max$m(a);
  // eslint-disable-next-line no-undef
  max$({ x_in, y_in, maze_in }); // never run, but here to "trick" calculang graph logic
};
////////// end max memo-loader code //////////



////////// start arrow memo-loader code //////////
//const arrow$m = memoize(999999, isEqual)(arrow$);
export const arrow$m = memoize(arrow$, JSON.stringify);
export const arrow = (a) => {
  return arrow$m(a);
  // eslint-disable-next-line no-undef
  arrow$({ x_in, y_in, maze_in }); // never run, but here to "trick" calculang graph logic
};
////////// end arrow memo-loader code //////////



////////// start arrow_path memo-loader code //////////
//const arrow_path$m = memoize(999999, isEqual)(arrow_path$);
export const arrow_path$m = memoize(arrow_path$, JSON.stringify);
export const arrow_path = (a) => {
  return arrow_path$m(a);
  // eslint-disable-next-line no-undef
  arrow_path$({ x_in, y_in, maze_in }); // never run, but here to "trick" calculang graph logic
};
////////// end arrow_path memo-loader code //////////



////////// start pointed_at_from_below memo-loader code //////////
//const pointed_at_from_below$m = memoize(999999, isEqual)(pointed_at_from_below$);
export const pointed_at_from_below$m = memoize(pointed_at_from_below$, JSON.stringify);
export const pointed_at_from_below = (a) => {
  return pointed_at_from_below$m(a);
  // eslint-disable-next-line no-undef
  pointed_at_from_below$({ x_in, y_in, maze_in }); // never run, but here to "trick" calculang graph logic
};
////////// end pointed_at_from_below memo-loader code //////////



////////// start pointed_at_from_right memo-loader code //////////
//const pointed_at_from_right$m = memoize(999999, isEqual)(pointed_at_from_right$);
export const pointed_at_from_right$m = memoize(pointed_at_from_right$, JSON.stringify);
export const pointed_at_from_right = (a) => {
  return pointed_at_from_right$m(a);
  // eslint-disable-next-line no-undef
  pointed_at_from_right$({ x_in, y_in, maze_in }); // never run, but here to "trick" calculang graph logic
};
////////// end pointed_at_from_right memo-loader code //////////



////////// start arrow_path_pretty memo-loader code //////////
//const arrow_path_pretty$m = memoize(999999, isEqual)(arrow_path_pretty$);
export const arrow_path_pretty$m = memoize(arrow_path_pretty$, JSON.stringify);
export const arrow_path_pretty = (a) => {
  return arrow_path_pretty$m(a);
  // eslint-disable-next-line no-undef
  arrow_path_pretty$({ x_in, y_in, maze_in }); // never run, but here to "trick" calculang graph logic
};
////////// end arrow_path_pretty memo-loader code //////////



////////// start maze memo-loader code //////////
//const maze$m = memoize(999999, isEqual)(maze$);
export const maze$m = memoize(maze$, JSON.stringify);
export const maze = (a) => {
  return maze$m(a);
  // eslint-disable-next-line no-undef
  maze$({ maze_in }); // never run, but here to "trick" calculang graph logic
};
////////// end maze memo-loader code //////////



////////// start x memo-loader code //////////
//const x$m = memoize(999999, isEqual)(x$);
export const x$m = memoize(x$, JSON.stringify);
export const x = (a) => {
  return x$m(a);
  // eslint-disable-next-line no-undef
  x$({ x_in }); // never run, but here to "trick" calculang graph logic
};
////////// end x memo-loader code //////////



////////// start y memo-loader code //////////
//const y$m = memoize(999999, isEqual)(y$);
export const y$m = memoize(y$, JSON.stringify);
export const y = (a) => {
  return y$m(a);
  // eslint-disable-next-line no-undef
  y$({ y_in }); // never run, but here to "trick" calculang graph logic
};
////////// end y memo-loader code //////////