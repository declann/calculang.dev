import { contribution_charge_rate } from "./cul_scope_0.mjs";import { fund_value_0 } from "./cul_scope_0.mjs";import { unit_growth_rate } from "./cul_scope_0.mjs";import { emper_matching_rate } from "./cul_scope_0.mjs";import { empee_contribution_rate } from "./cul_scope_0.mjs";import { salary_inflation_rate } from "./cul_scope_0.mjs";import { salary_age_0 } from "./cul_scope_0.mjs";import { salary_0 } from "./cul_scope_0.mjs";import { retirement_age } from "./cul_scope_0.mjs";import { age_0 } from "./cul_scope_0.mjs";import { age } from "./cul_scope_0.mjs";import { salaries_per_retirement_fund } from "./cul_scope_0.mjs";import { retirement_fund_value } from "./cul_scope_0.mjs";import { salary } from "./cul_scope_0.mjs";import { emper_contribution } from "./cul_scope_0.mjs";import { empee_contribution_cost } from "./cul_scope_0.mjs";import { empee_contribution_tax_relief } from "./cul_scope_0.mjs";import { tax_credits } from "./cul_scope_0.mjs";import { pension_tax_relief_ratio } from "./cul_scope_0.mjs";import { accumulated_empee_contribution_tax_relief } from "./cul_scope_0.mjs";import { accumulated_empee_contributions } from "./cul_scope_0.mjs";import { empee_contribution } from "./cul_scope_0.mjs";import { missed_contribution_age } from "./cul_scope_0.mjs";import { unit_price } from "./cul_scope_0.mjs";import { contribution_units } from "./cul_scope_0.mjs";import { contributions } from "./cul_scope_0.mjs";import { management_charge_units } from "./cul_scope_0.mjs";import { management_charge } from "./cul_scope_0.mjs";import { management_charge_rate } from "./cul_scope_0.mjs";import { unit_balance } from "./cul_scope_0.mjs";import { check_cfs_vs_fund_value } from "./cul_scope_0.mjs";import { check_cfs } from "./cul_scope_0.mjs";import { contribution_charge } from "./cul_scope_0.mjs";import { fund_charges } from "./cul_scope_0.mjs";import { fund_growth } from "./cul_scope_0.mjs";import { fund_value } from "./cul_scope_0.mjs";import { income_tax } from "./cul_scope_0.mjs";
// disclaimer: This is a work-in-progress model released for some calculang/tooling demonstration purposes and numbers shouldn't be relied upon; there are known model issues.


import { income_tax_ } from "./cul_scope_2.mjs";
export { income_tax_ };

export const fund_value_ = ({ age_in, age_0_in, fund_value_0_in, unit_growth_rate_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in }) => unit_balance({ age_in, age_0_in, fund_value_0_in, unit_growth_rate_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in }) * unit_price({ age_in, age_0_in, unit_growth_rate_in }); // not allowing for multiple funds now

export const fund_growth_ = ({ age_in, age_0_in, fund_value_0_in, unit_growth_rate_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in }) =>
unit_balance({ age_0_in, fund_value_0_in, unit_growth_rate_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in, age_in: age({ age_in }) - 1 }) * unit_price({ age_in, age_0_in, unit_growth_rate_in }) -
unit_balance({ age_0_in, fund_value_0_in, unit_growth_rate_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in, age_in: age({ age_in }) - 1 }) * unit_price({ age_0_in, unit_growth_rate_in, age_in: age({ age_in }) - 1 });

export const fund_charges_ = ({ contribution_charge_rate_in, age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, fund_value_0_in, unit_growth_rate_in, management_charge_rate_in }) => contribution_charge({ contribution_charge_rate_in, age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in }) + management_charge({ age_in, age_0_in, fund_value_0_in, unit_growth_rate_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in });

export const contribution_charge_ = ({ contribution_charge_rate_in, age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in }) =>
-contribution_charge_rate({ contribution_charge_rate_in }) * (empee_contribution({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in }) + emper_contribution({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in }));

export const check_cfs_ = ({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, paye_band_id_in, gross_salary_in, pension_contribution_in, usc_band_id_in, emper_matching_rate_in, fund_value_0_in, unit_growth_rate_in, contribution_charge_rate_in, management_charge_rate_in }) =>
empee_contribution_cost({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in }) +
empee_contribution_tax_relief({ age_in, salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in, age_0_in, missed_contribution_age_in, empee_contribution_rate_in }) +
emper_contribution({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in }) +
fund_growth({ age_in, age_0_in, fund_value_0_in, unit_growth_rate_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in }) +
fund_charges({ contribution_charge_rate_in, age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, fund_value_0_in, unit_growth_rate_in, management_charge_rate_in });

export const check_cfs_vs_fund_value_ = ({ age_in, age_0_in, fund_value_0_in, unit_growth_rate_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in, paye_band_id_in, gross_salary_in, pension_contribution_in, usc_band_id_in }) =>
fund_value({ age_in, age_0_in, fund_value_0_in, unit_growth_rate_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in }) - fund_value({ age_0_in, fund_value_0_in, unit_growth_rate_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in, age_in: age({ age_in }) - 1 }) - check_cfs({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, paye_band_id_in, gross_salary_in, pension_contribution_in, usc_band_id_in, emper_matching_rate_in, fund_value_0_in, unit_growth_rate_in, contribution_charge_rate_in, management_charge_rate_in });

export const unit_balance_ = ({ age_in, age_0_in, fund_value_0_in, unit_growth_rate_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in }) => {
  if (age({ age_in }) <= age_0({ age_0_in }) - 1) return fund_value_0({ fund_value_0_in }) / unit_price({ age_in, age_0_in, unit_growth_rate_in });else

  return (
    unit_balance({ age_0_in, fund_value_0_in, unit_growth_rate_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in, age_in: age({ age_in }) - 1 }) +
    contribution_units({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, unit_growth_rate_in }) +
    management_charge_units({ age_in, age_0_in, fund_value_0_in, unit_growth_rate_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in }));

  // timing = premium received at start of year and allocated immediately
};

export const management_charge_rate_ = ({ management_charge_rate_in }) => management_charge_rate_in;

export const management_charge_ = ({ age_in, age_0_in, fund_value_0_in, unit_growth_rate_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in }) => {
  if (age({ age_in }) <= age_0({ age_0_in })) return 0;else
  return -fund_value({ age_0_in, fund_value_0_in, unit_growth_rate_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in, age_in: age({ age_in }) - 1 }) * management_charge_rate({ management_charge_rate_in });
};

export const management_charge_units_ = ({ age_in, age_0_in, fund_value_0_in, unit_growth_rate_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in }) => management_charge({ age_in, age_0_in, fund_value_0_in, unit_growth_rate_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in }) / unit_price({ age_in, age_0_in, unit_growth_rate_in });

export const contributions_ = ({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in }) => empee_contribution({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in }) + emper_contribution({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in });

export const contribution_units_ = ({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, unit_growth_rate_in }) =>
contributions({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in }) * (1 - contribution_charge_rate({ contribution_charge_rate_in })) / unit_price({ age_in, age_0_in, unit_growth_rate_in }); // todo, AVCs?

export const unit_price_ = ({ age_in, age_0_in, unit_growth_rate_in }) => {
  if (age({ age_in }) <= age_0({ age_0_in })) return 1;else
  return unit_price({ age_0_in, unit_growth_rate_in, age_in: age({ age_in }) - 1 }) * (1 + unit_growth_rate({ unit_growth_rate_in }));
};

export const missed_contribution_age_ = ({ missed_contribution_age_in }) =>
missed_contribution_age_in != undefined ? missed_contribution_age_in : -1;

export const empee_contribution_ = ({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in }) => {
  if (
  age({ age_in }) <= age_0({ age_0_in }) - 1 ||
  age({ age_in }) == retirement_age({ retirement_age_in }) ||
  age({ age_in }) == missed_contribution_age({ missed_contribution_age_in }))

  return 0;else
  return salary({ salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in, age_in: age({ age_in }) - 1 }) * empee_contribution_rate({ empee_contribution_rate_in });
};

export const accumulated_empee_contributions_ = ({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in }) => {
  if (age({ age_in }) == age_0({ age_0_in }) - 1) return 0;else

  return (
    accumulated_empee_contributions({ age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, age_in: age({ age_in }) - 1 }) +
    empee_contribution({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in }));

};

export const accumulated_empee_contribution_tax_relief_ = ({ age_in, age_0_in, paye_band_id_in, gross_salary_in, pension_contribution_in, usc_band_id_in, salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in, missed_contribution_age_in, empee_contribution_rate_in }) => {
  if (age({ age_in }) == age_0({ age_0_in }) - 1) return 0;else

  return (
    accumulated_empee_contribution_tax_relief({ age_0_in, paye_band_id_in, gross_salary_in, pension_contribution_in, usc_band_id_in, salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in, missed_contribution_age_in, empee_contribution_rate_in, age_in: age({ age_in }) - 1 }) +
    empee_contribution_tax_relief({ age_in, salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in, age_0_in, missed_contribution_age_in, empee_contribution_rate_in }));

};

export const pension_tax_relief_ratio_ = ({ age_in, salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in, age_0_in, missed_contribution_age_in, empee_contribution_rate_in }) =>
empee_contribution_tax_relief({ age_in, salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in, age_0_in, missed_contribution_age_in, empee_contribution_rate_in }) / empee_contribution({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in });

// assumption for tax relief calculation, 2024 values
export const tax_credits_ = ({}) => 1875 + 1875; // single person + empee paye tax creedits

export const empee_contribution_tax_relief_ = ({ age_in, salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in, age_0_in, missed_contribution_age_in, empee_contribution_rate_in }) =>
income_tax({ age_in,
  gross_salary_in: salary({ salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in, age_in: age({ age_in }) - 1 }),
  //tax_credits_in: 3000, // ex- I specified like this
  pension_contribution_in: 0 }
) -
income_tax({ age_in,
  gross_salary_in: salary({ salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in, age_in: age({ age_in }) - 1 }),
  //tax_credits_in: 3000, // ex- I specified like this
  pension_contribution_in: empee_contribution({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in }) }
);

export const empee_contribution_cost_ = ({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in }) =>
empee_contribution({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in }) - empee_contribution_tax_relief({ age_in, salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in, age_0_in, missed_contribution_age_in, empee_contribution_rate_in });

// affected by bug: depends on gross_salary_in, for some reason
// issue #102
/*export const accumulated_empee_contribution_tax_relief = () => {
  if (age() == age_0() - 1) return 0;
  else
    return (
      accumulated_empee_contribution_tax_relief({ age_in: age() - 1 }) +
      empee_contribution_tax_relief()
    );
};*/

export const emper_contribution_ = ({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in }) =>
empee_contribution({ age_in, age_0_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in }) * emper_matching_rate({ emper_matching_rate_in });

export const salary_ = ({ age_in, salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in }) => {
  // at end of year
  if (age({ age_in }) == salary_age_0({ salary_age_0_in }) - 1) return salary_0({ salary_0_in });else
  if (age({ age_in }) >= retirement_age({ retirement_age_in })) return 0;else
  if (age({ age_in }) >= salary_age_0({ salary_age_0_in })) return salary({ salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in, age_in: age({ age_in }) - 1 }) * (1 + salary_inflation_rate({ salary_inflation_rate_in }));else
  return salary({ salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in, age_in: age({ age_in }) + 1 }) / (1 + salary_inflation_rate({ salary_inflation_rate_in }));
};

export const retirement_fund_value_ = ({ age_0_in, fund_value_0_in, unit_growth_rate_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in }) =>
// at retirement:
fund_value({ age_0_in, fund_value_0_in, unit_growth_rate_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in, age_in: retirement_age({ retirement_age_in }) });

export const salaries_per_retirement_fund_ = ({ age_0_in, fund_value_0_in, unit_growth_rate_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in }) =>
retirement_fund_value({ age_0_in, fund_value_0_in, unit_growth_rate_in, retirement_age_in, missed_contribution_age_in, salary_age_0_in, salary_0_in, salary_inflation_rate_in, empee_contribution_rate_in, emper_matching_rate_in, contribution_charge_rate_in, management_charge_rate_in }) / salary({ salary_age_0_in, salary_0_in, retirement_age_in, salary_inflation_rate_in, age_in: retirement_age({ retirement_age_in }) - 1 });


// inputs:

// using age and age_0 (starting age) as inputs, rather than year/time and age_0.
export const age_ = ({ age_in }) => age_in; // input
export const age_0_ = ({ age_0_in }) => age_0_in;

export const retirement_age_ = ({ retirement_age_in }) => retirement_age_in;
export const salary_0_ = ({ salary_0_in }) => salary_0_in;
export const salary_age_0_ = ({ salary_age_0_in }) => salary_age_0_in; // salary benchmark age is detached from age_0 so that results for different age_0 (starting age) are comparable // maybe this is bad naming
export const salary_inflation_rate_ = ({ salary_inflation_rate_in }) => salary_inflation_rate_in;
export const empee_contribution_rate_ = ({ empee_contribution_rate_in }) => empee_contribution_rate_in;
export const emper_matching_rate_ = ({ emper_matching_rate_in }) => emper_matching_rate_in;

export const unit_growth_rate_ = ({ unit_growth_rate_in }) => unit_growth_rate_in;

export const fund_value_0_ = ({ fund_value_0_in }) => fund_value_0_in;

export const contribution_charge_rate_ = ({ contribution_charge_rate_in }) => contribution_charge_rate_in;