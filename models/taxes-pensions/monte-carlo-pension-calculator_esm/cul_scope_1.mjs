import { unit_growth_rate_mean } from "./cul_scope_0.mjs";import { unit_growth_rate_std_dev } from "./cul_scope_0.mjs";import { simulation } from "./cul_scope_0.mjs";import { random } from "./cul_scope_0.mjs";import { seeded } from "./cul_scope_0.mjs";import { random_seed } from "./cul_scope_0.mjs";import { z0 } from "./cul_scope_0.mjs";import { u2 } from "./cul_scope_0.mjs";import { u1 } from "./cul_scope_0.mjs";import { unit_growth_rate } from "./cul_scope_0.mjs";import { fund_value } from "./cul_scope_0.mjs";import { age } from "./cul_scope_0.mjs";import { retirement_fund_value } from "./cul_scope_0.mjs";import {
  retirement_fund_value_,
  age_,
  fund_value_ } from
"./cul_scope_2.mjs";
export { retirement_fund_value_, age_, fund_value_ };

// must compile with memoization and must pass appropriate params into random calls, to ensure purity

export const unit_growth_rate_ = ({ random_seed_in, age_in, simulation_in, unit_growth_rate_std_dev_in, unit_growth_rate_mean_in }) =>
z0({ random_seed_in, age_in, simulation_in }) * unit_growth_rate_std_dev({ unit_growth_rate_std_dev_in }) + unit_growth_rate_mean({ unit_growth_rate_mean_in });
// vs. uniform:
//random({ age_in: age(), simulation_in: simulation() }) * 0.1;

// box-muller transform to generate z0 a standard normally distributed number
// https://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform
export const u1_ = ({ random_seed_in, age_in, simulation_in }) =>
random({ random_seed_in, age_in: age({ age_in }), simulation_in: simulation({ simulation_in }), u: 1 });
export const u2_ = ({ random_seed_in, age_in, simulation_in }) =>
random({ random_seed_in, age_in: age({ age_in }), simulation_in: simulation({ simulation_in }), u: 2 });
export const z0_ = ({ random_seed_in, age_in, simulation_in }) =>
Math.sqrt(-2.0 * Math.log(u1({ random_seed_in, age_in, simulation_in }))) * Math.cos(2 * Math.PI * u2({ random_seed_in, age_in, simulation_in }));
// todo use z1 of previous generation?
//export const z1 = () => Math.sqrt(-2.0 * Math.log(u1())) * Math.sin(2 * Math.PI * u2());


/* seeded random numbers: */
export const random_seed_ = ({ random_seed_in }) => random_seed_in;
import { prng_alea } from "./esm-seedrandom.mjs";
export const seeded_ = ({ random_seed_in }) => prng_alea(random_seed({ random_seed_in }));
export const random_ = ({ random_seed_in }) => seeded({ random_seed_in: random_seed({ random_seed_in }) })();
// alternative:
//export const random = () => Math.random();

export const simulation_ = ({ simulation_in }) => simulation_in;
export const unit_growth_rate_std_dev_ = ({ unit_growth_rate_std_dev_in }) => unit_growth_rate_std_dev_in;
export const unit_growth_rate_mean_ = ({ unit_growth_rate_mean_in }) => unit_growth_rate_mean_in;