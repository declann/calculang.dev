import {
  retirement_fund_value,
  age,
  fund_value,
} from "./pension-calculator.cul.js";
export { retirement_fund_value, age, fund_value };

// must compile with memoization and must pass appropriate params into random calls, to ensure purity

export const unit_growth_rate = () =>
  z0() * unit_growth_rate_std_dev() + unit_growth_rate_mean();
// vs. uniform:
//random({ age_in: age(), simulation_in: simulation() }) * 0.1;

// box-muller transform to generate z0 a standard normally distributed number
// https://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform
export const u1 = () =>
  random({ age_in: age(), simulation_in: simulation(), u: 1 });
export const u2 = () =>
  random({ age_in: age(), simulation_in: simulation(), u: 2 });
export const z0 = () =>
  Math.sqrt(-2.0 * Math.log(u1())) * Math.cos(2 * Math.PI * u2());
// todo use z1 of previous generation?
//export const z1 = () => Math.sqrt(-2.0 * Math.log(u1())) * Math.sin(2 * Math.PI * u2());


/* seeded random numbers: */
export const random_seed = () => random_seed_in;
import { prng_alea } from "./esm-seedrandom.mjs";
export const seeded = () => prng_alea(random_seed());
export const random = () => seeded({ random_seed_in: random_seed() })();
// alternative:
//export const random = () => Math.random();

export const simulation = () => simulation_in;
export const unit_growth_rate_std_dev = () => unit_growth_rate_std_dev_in;
export const unit_growth_rate_mean = () => unit_growth_rate_mean_in;
