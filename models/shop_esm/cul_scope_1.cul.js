export const sales = () =>
  units() * sales_price();

export const purchases = () =>
  units() * purchase_price();

export const profit = () =>
  sales() - purchases() - expenses();

// inputs:
export const sales_price = () => sales_price_in;
export const purchase_price = () => purchase_price_in;
export const units = () => units_in;
export const expenses = () => expenses_in;