import { y } from "./cul_scope_0.mjs";import { x } from "./cul_scope_0.mjs";import { step } from "./cul_scope_0.mjs";import { ray_angle } from "./cul_scope_0.mjs";import { f } from "./cul_scope_0.mjs";import { speed } from "./cul_scope_0.mjs";import { player_y } from "./cul_scope_0.mjs";import { player_x } from "./cul_scope_0.mjs";import { player_angle } from "./cul_scope_0.mjs";import { leftness } from "./cul_scope_0.mjs";import { forwardness } from "./cul_scope_0.mjs";import { key_right } from "./cul_scope_0.mjs";import { key_left } from "./cul_scope_0.mjs";import { key_down } from "./cul_scope_0.mjs";import { key_up } from "./cul_scope_0.mjs";import { keys } from "./cul_scope_0.mjs";import { keys_stream_version } from "./cul_scope_0.mjs";import { r_inverse_length } from "./cul_scope_0.mjs";import { r_length } from "./cul_scope_0.mjs";import { angle_offset1 } from "./cul_scope_0.mjs";import { angle_offset0 } from "./cul_scope_0.mjs";import { r_hit_step } from "./cul_scope_0.mjs";import { r_hit_v } from "./cul_scope_0.mjs";import { r_hit_y } from "./cul_scope_0.mjs";import { r_hit_x } from "./cul_scope_0.mjs";import { r_hit_object } from "./cul_scope_0.mjs";import { r_hit_h_or_v } from "./cul_scope_0.mjs";import { rv_hit_length } from "./cul_scope_0.mjs";import { rv_hit_step } from "./cul_scope_0.mjs";import { rv_hit_v } from "./cul_scope_0.mjs";import { rv_hit_y } from "./cul_scope_0.mjs";import { rv_hit_x } from "./cul_scope_0.mjs";import { rv_hit_object } from "./cul_scope_0.mjs";import { rvv } from "./cul_scope_0.mjs";import { rvy } from "./cul_scope_0.mjs";import { rvx } from "./cul_scope_0.mjs";import { rh_hit_length } from "./cul_scope_0.mjs";import { rh_hit_step } from "./cul_scope_0.mjs";import { rh_hit_v } from "./cul_scope_0.mjs";import { rh_hit_y } from "./cul_scope_0.mjs";import { rh_hit_x } from "./cul_scope_0.mjs";import { rh_hit_object } from "./cul_scope_0.mjs";import { rhv } from "./cul_scope_0.mjs";import { rhy } from "./cul_scope_0.mjs";import { rhx } from "./cul_scope_0.mjs";import { map_check } from "./cul_scope_0.mjs";import { ray_looking_right } from "./cul_scope_0.mjs";import { ray_looking_up } from "./cul_scope_0.mjs";import { inverse_tan_ray_angle } from "./cul_scope_0.mjs";import { tan_ray_angle } from "./cul_scope_0.mjs";import { range } from "underscore";

// trig helpers:

export const tan_ray_angle_ = ({ ray_angle_in }) => Math.tan(ray_angle({ ray_angle_in }));
export const inverse_tan_ray_angle_ = ({ ray_angle_in }) => 1 / tan_ray_angle({ ray_angle_in });

export const ray_looking_up_ = ({ ray_angle_in }) => Math.sin(ray_angle({ ray_angle_in })) > 0;
export const ray_looking_right_ = ({ ray_angle_in }) => Math.cos(ray_angle({ ray_angle_in })) > 0;

export const map_check_ = ({ x_in, y_in }) => {
  if (
  Math.floor(x({ x_in }) - 0.01 /* *10 */) == 3 &&
  Math.floor(y({ y_in }) - 0.01 /* *10 */) == 3)

  return 1;else
  if (Math.floor(x({ x_in }) - 0) == 3 && Math.floor(y({ y_in }) - 0) == 3) return 1;else
  if (y({ y_in }) <= 2 || x({ x_in }) >= 5 || x({ x_in }) <= 0.1 || y({ y_in }) >= 7) return 1;else
  return 0;
};

// for rays:

// ray calcs are split into 2 types of formula. I might consolidate these in fut.
// h=horizontal, v=vertical

// horizontal::

export const rhx_ = ({ step_in, f_in, keys_stream_version_in, keys_stream_function_in, ray_angle_in }) => {
  if (step({ step_in }) == 0)
  return player_x({ f_in, keys_stream_version_in, keys_stream_function_in }) + (player_y({ f_in, keys_stream_version_in, keys_stream_function_in }) - rhy({ ray_angle_in, step_in, f_in, keys_stream_version_in, keys_stream_function_in })) * inverse_tan_ray_angle({ ray_angle_in });else

  return (
    rhx({ f_in, keys_stream_version_in, keys_stream_function_in, ray_angle_in, step_in: step({ step_in }) - 1 }) +
    (ray_looking_up({ ray_angle_in }) ? 1 : -1) * inverse_tan_ray_angle({ ray_angle_in }));

};

export const rhy_ = ({ ray_angle_in, step_in, f_in, keys_stream_version_in, keys_stream_function_in }) => {
  if (ray_looking_up({ ray_angle_in })) {
    if (step({ step_in }) == 0) return Math.floor(player_y({ f_in, keys_stream_version_in, keys_stream_function_in }));else
    return rhy({ ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in, step_in: step({ step_in }) - 1 }) - 1;
  } else {
    if (step({ step_in }) == 0) return Math.floor(player_y({ f_in, keys_stream_version_in, keys_stream_function_in })) + 1;else
    return rhy({ ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in, step_in: step({ step_in }) - 1 }) + 1;
  }
};

export const rhv_ = ({ step_in, f_in, keys_stream_version_in, keys_stream_function_in, ray_angle_in }) => map_check({ x_in: rhx({ step_in, f_in, keys_stream_version_in, keys_stream_function_in, ray_angle_in }), y_in: rhy({ ray_angle_in, step_in, f_in, keys_stream_version_in, keys_stream_function_in }) });

// the first hit; returns {rhx,rhy,rhv,step_in}
export const rh_hit_object_ = ({ f_in, keys_stream_version_in, keys_stream_function_in, ray_angle_in }) => {
  // I need to summarise h steps
  // alternative use an accumulator cul approach?
  return range(0, 8.1).reduce(
    (acc, step_in) => {
      if (acc.step_in == -1 && rhv({ f_in, keys_stream_version_in, keys_stream_function_in, ray_angle_in, step_in }) != 0)
        // do I need to pass step_in, or context provided?
        return {
          x: rhx({ f_in, keys_stream_version_in, keys_stream_function_in, ray_angle_in, step_in }),
          y: rhy({ ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in, step_in }),
          v: rhv({ f_in, keys_stream_version_in, keys_stream_function_in, ray_angle_in, step_in }),
          step_in
        };else
      return acc;
    },
    { step_in: -1 }
  );
};

// ray-level results (independent of step):
export const rh_hit_x_ = ({ f_in, keys_stream_version_in, keys_stream_function_in, ray_angle_in }) => rh_hit_object({ f_in, keys_stream_version_in, keys_stream_function_in, ray_angle_in }).x;
export const rh_hit_y_ = ({ f_in, keys_stream_version_in, keys_stream_function_in, ray_angle_in }) => rh_hit_object({ f_in, keys_stream_version_in, keys_stream_function_in, ray_angle_in }).y;
export const rh_hit_v_ = ({ f_in, keys_stream_version_in, keys_stream_function_in, ray_angle_in }) => rh_hit_object({ f_in, keys_stream_version_in, keys_stream_function_in, ray_angle_in }).v;
export const rh_hit_step_ = ({ f_in, keys_stream_version_in, keys_stream_function_in, ray_angle_in }) => rh_hit_object({ f_in, keys_stream_version_in, keys_stream_function_in, ray_angle_in }).step_in;

export const rh_hit_length_ = ({ x_in, y_in, f_in, keys_stream_version_in, keys_stream_function_in, ray_angle_in }) =>
// Pythagoras' theorem
Math.sqrt((rh_hit_x({ f_in, keys_stream_version_in, keys_stream_function_in, ray_angle_in }) - player_x({ f_in, keys_stream_version_in, keys_stream_function_in })) ** 2 + (rh_hit_y({ f_in, keys_stream_version_in, keys_stream_function_in, ray_angle_in }) - player_y({ f_in, keys_stream_version_in, keys_stream_function_in })) ** 2);

// vertical::

export const rvx_ = ({ ray_angle_in, step_in, f_in, keys_stream_version_in, keys_stream_function_in }) => {
  if (ray_looking_right({ ray_angle_in })) {
    if (step({ step_in }) == 0) return Math.floor(player_x({ f_in, keys_stream_version_in, keys_stream_function_in })) + 1;else
    return rvx({ ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in, step_in: step({ step_in }) - 1 }) + 1;
  } else {
    if (step({ step_in }) == 0) return Math.floor(player_x({ f_in, keys_stream_version_in, keys_stream_function_in }));else
    return rvx({ ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in, step_in: step({ step_in }) - 1 }) - 1;
  }
};

export const rvy_ = ({ step_in, f_in, keys_stream_version_in, keys_stream_function_in, ray_angle_in }) => {
  if (step({ step_in }) == 0) return player_y({ f_in, keys_stream_version_in, keys_stream_function_in }) + (player_x({ f_in, keys_stream_version_in, keys_stream_function_in }) - rvx({ ray_angle_in, step_in, f_in, keys_stream_version_in, keys_stream_function_in })) * tan_ray_angle({ ray_angle_in });else

  return (
    rvy({ f_in, keys_stream_version_in, keys_stream_function_in, ray_angle_in, step_in: step({ step_in }) - 1 }) +
    (ray_looking_right({ ray_angle_in }) ? -1 : 1) * tan_ray_angle({ ray_angle_in }));

};

export const rvv_ = ({ ray_angle_in, step_in, f_in, keys_stream_version_in, keys_stream_function_in }) => map_check({ x_in: rvx({ ray_angle_in, step_in, f_in, keys_stream_version_in, keys_stream_function_in }), y_in: rvy({ step_in, f_in, keys_stream_version_in, keys_stream_function_in, ray_angle_in }) });

// the first hit in this direction; returns {rvx,rvy,rvv,step_in}
export const rv_hit_object_ = ({ ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in }) => {
  // I need to summarise h steps
  // alternative use an accumulator cul approach? How to answer, this vs. loops?
  return range(0, 8.1).reduce(
    (acc, step_in) => {
      if (acc.step_in == -1 && rvv({ ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in, step_in }) != 0)
        // do I need to pass step_in, or context provided?
        return {
          x: rvx({ ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in, step_in }),
          y: rvy({ f_in, keys_stream_version_in, keys_stream_function_in, ray_angle_in, step_in }),
          v: rvv({ ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in, step_in }),
          step_in
        };else
      return acc;
    },
    { step_in: -1 }
  );
};

// ray-level results (independent of step):
export const rv_hit_x_ = ({ ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in }) => rv_hit_object({ ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in }).x;
export const rv_hit_y_ = ({ ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in }) => rv_hit_object({ ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in }).y;
export const rv_hit_v_ = ({ ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in }) => rv_hit_object({ ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in }).v;
export const rv_hit_step_ = ({ ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in }) => rv_hit_object({ ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in }).step_in;

export const rv_hit_length_ = ({ x_in, y_in, ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in }) =>
// Pythagoras' theorem
Math.sqrt((rv_hit_x({ ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in }) - player_x({ f_in, keys_stream_version_in, keys_stream_function_in })) ** 2 + (rv_hit_y({ ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in }) - player_y({ f_in, keys_stream_version_in, keys_stream_function_in })) ** 2);

// ray-level results (independent of direction):

export const r_hit_h_or_v_ = ({ x_in, y_in, ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in }) =>
rv_hit_length({ x_in, y_in, ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in }) < rh_hit_length({ x_in, y_in, f_in, keys_stream_version_in, keys_stream_function_in, ray_angle_in }) ? "v" : "h";

export const r_hit_object_ = ({ x_in, y_in, ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in }) =>
r_hit_h_or_v({ x_in, y_in, ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in }) == "v" ? rv_hit_object({ ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in }) : rh_hit_object({ f_in, keys_stream_version_in, keys_stream_function_in, ray_angle_in });
export const r_hit_x_ = ({ x_in, y_in, ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in }) => r_hit_object({ x_in, y_in, ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in }).x;
export const r_hit_y_ = ({ x_in, y_in, ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in }) => r_hit_object({ x_in, y_in, ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in }).y;
export const r_hit_v_ = ({ x_in, y_in, ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in }) => r_hit_object({ x_in, y_in, ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in }).v;
export const r_hit_step_ = ({ x_in, y_in, ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in }) => r_hit_object({ x_in, y_in, ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in }).step_in;

export const angle_offset0_ = ({ f_in, keys_stream_version_in, keys_stream_function_in, ray_angle_in }) => player_angle({ f_in, keys_stream_version_in, keys_stream_function_in }) - ray_angle({ ray_angle_in });
export const angle_offset1_ = ({ f_in, keys_stream_version_in, keys_stream_function_in, ray_angle_in }) => {
  // redundant?!
  if (angle_offset0({ f_in, keys_stream_version_in, keys_stream_function_in, ray_angle_in }) < 0) return angle_offset0({ f_in, keys_stream_version_in, keys_stream_function_in, ray_angle_in }) + 2 * Math.PI;
  if (angle_offset0({ f_in, keys_stream_version_in, keys_stream_function_in, ray_angle_in }) > 2 * Math.PI) return angle_offset0({ f_in, keys_stream_version_in, keys_stream_function_in, ray_angle_in }) - 2 * Math.PI;
};

export const r_length_ = ({ x_in, y_in, ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in }) =>
(r_hit_h_or_v({ x_in, y_in, ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in }) == "v" ? rv_hit_length({ x_in, y_in, ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in }) : rh_hit_length({ x_in, y_in, f_in, keys_stream_version_in, keys_stream_function_in, ray_angle_in })) *
Math.cos(angle_offset0({ f_in, keys_stream_version_in, keys_stream_function_in, ray_angle_in })); // trig needs guards! // I think I need Math.abs! Cos(10) is neg

export const r_inverse_length_ = ({ x_in, y_in, ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in }) => 1 / r_length({ x_in, y_in, ray_angle_in, f_in, keys_stream_version_in, keys_stream_function_in });


export const keys_stream_function = ({ keys_stream_function_in }) => keys_stream_function_in;
export const keys_stream_version_ = ({ keys_stream_version_in }) => keys_stream_version_in;

export const keys_ = ({ keys_stream_version_in, keys_stream_function_in, f_in }) => {
  keys_stream_version({ keys_stream_version_in });
  return keys_stream_function({ keys_stream_function_in })().filter((d) => d.frame == f({ f_in }));
  //
};


export const key_up_ = ({ keys_stream_version_in, keys_stream_function_in, f_in }) =>
keys({ keys_stream_version_in, keys_stream_function_in, f_in }).length ?
keys({ keys_stream_version_in, keys_stream_function_in, f_in }).find((d) => d.key == "ArrowUp") ?
true :
false :
false;


export const key_down_ = ({ keys_stream_version_in, keys_stream_function_in, f_in }) =>
keys({ keys_stream_version_in, keys_stream_function_in, f_in }).length ?
keys({ keys_stream_version_in, keys_stream_function_in, f_in }).find((d) => d.key == "ArrowDown") ?
true :
false :
false;




export const key_left_ = ({ keys_stream_version_in, keys_stream_function_in, f_in }) =>
keys({ keys_stream_version_in, keys_stream_function_in, f_in }).length ?
keys({ keys_stream_version_in, keys_stream_function_in, f_in }).find((d) => d.key == "ArrowLeft") ?
true :
false :
false;


export const key_right_ = ({ keys_stream_version_in, keys_stream_function_in, f_in }) =>
keys({ keys_stream_version_in, keys_stream_function_in, f_in }).length ?
keys({ keys_stream_version_in, keys_stream_function_in, f_in }).find((d) => d.key == "ArrowRight") ?
true :
false :
false;

export const forwardness_ = ({ keys_stream_version_in, keys_stream_function_in, f_in }) =>
key_up({ keys_stream_version_in, keys_stream_function_in, f_in }) ? 1 : key_down({ keys_stream_version_in, keys_stream_function_in, f_in }) ? -1 : 0;


export const leftness_ = ({ keys_stream_version_in, keys_stream_function_in, f_in }) =>
key_left({ keys_stream_version_in, keys_stream_function_in, f_in }) ? 1 : key_right({ keys_stream_version_in, keys_stream_function_in, f_in }) ? -1 : 0;



export const player_angle_ = ({ f_in, keys_stream_version_in, keys_stream_function_in }) => {
  if (f({ f_in }) <= 0) return 0.8;else

  return (
    player_angle({ keys_stream_version_in, keys_stream_function_in, f_in: f({ f_in }) - 1 }) +
    leftness({ keys_stream_version_in, keys_stream_function_in, f_in }) * 0.15);

};




export const player_x_ = ({ f_in, keys_stream_version_in, keys_stream_function_in }) => {
  if (f({ f_in }) <= 0) return 1.5;else

  return (
    player_x({ keys_stream_version_in, keys_stream_function_in, f_in: f({ f_in }) - 1 }) +
    forwardness({ keys_stream_version_in, keys_stream_function_in, f_in }) * speed({}) * Math.cos(player_angle({ f_in, keys_stream_version_in, keys_stream_function_in })));

};

export const player_y_ = ({ f_in, keys_stream_version_in, keys_stream_function_in }) => {
  if (f({ f_in }) <= 0) return 5;else

  return (
    player_y({ keys_stream_version_in, keys_stream_function_in, f_in: f({ f_in }) - 1 }) +
    forwardness({ keys_stream_version_in, keys_stream_function_in, f_in }) * speed({}) * Math.sin(player_angle({ f_in, keys_stream_version_in, keys_stream_function_in })) * -1 // origin top left
  );
};

export const speed_ = ({}) => 0.5;

// inputs:
export const f_ = ({ f_in }) => f_in;

//export const player_x = () => player_x_in;
//export const player_y = () => player_y_in;
//export const player_angle_ = () => player_angle_in;

export const ray_angle_ = ({ ray_angle_in }) => ray_angle_in;

export const step_ = ({ step_in }) => Math.max(0, step_in);

// for map checking:

export const x_ = ({ x_in }) => x_in;
export const y_ = ({ y_in }) => y_in;