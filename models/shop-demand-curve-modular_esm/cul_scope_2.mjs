import { units } from "./cul_scope_0.mjs";import { expenses } from "./cul_scope_0.mjs";import { purchase_price } from "./cul_scope_0.mjs";import { sales_price } from "./cul_scope_0.mjs";import { profit } from "./cul_scope_0.mjs";import { purchases } from "./cul_scope_0.mjs";import { sales } from "./cul_scope_0.mjs";
import { memoize } from 'underscore';
//import memoize from 'lru-memoize';
//import { isEqual } from 'underscore'; // TODO poor tree shaking support, or why is this impact so massive? Move to lodash/lodash-es?

// import/export non-to memo?

import { sales_ as sales$, purchases_ as purchases$, profit_ as profit$, sales_price_ as sales_price$, purchase_price_ as purchase_price$, units_ as units$, expenses_ as expenses$ } from "./cul_scope_3.mjs"; // there is already-culed stuff in here, why? imports to memo loader include cul_scope_id, what logic should it apply RE passing forward? eliminate? Probably!





////////// start sales memo-loader code //////////
//const sales$m = memoize(999999, isEqual)(sales$);
export const sales$m = memoize(sales$, JSON.stringify);
export const sales_ = (a) => {
  return sales$m(a);
  // eslint-disable-next-line no-undef
  sales$({ sales_price_in }); // never run, but here to "trick" calculang graph logic
};
////////// end sales memo-loader code //////////



////////// start purchases memo-loader code //////////
//const purchases$m = memoize(999999, isEqual)(purchases$);
export const purchases$m = memoize(purchases$, JSON.stringify);
export const purchases_ = (a) => {
  return purchases$m(a);
  // eslint-disable-next-line no-undef
  purchases$({ sales_price_in, purchase_price_in }); // never run, but here to "trick" calculang graph logic
};
////////// end purchases memo-loader code //////////



////////// start profit memo-loader code //////////
//const profit$m = memoize(999999, isEqual)(profit$);
export const profit$m = memoize(profit$, JSON.stringify);
export const profit_ = (a) => {
  return profit$m(a);
  // eslint-disable-next-line no-undef
  profit$({ sales_price_in, purchase_price_in, expenses_in }); // never run, but here to "trick" calculang graph logic
};
////////// end profit memo-loader code //////////



////////// start sales_price memo-loader code //////////
//const sales_price$m = memoize(999999, isEqual)(sales_price$);
export const sales_price$m = memoize(sales_price$, JSON.stringify);
export const sales_price_ = (a) => {
  return sales_price$m(a);
  // eslint-disable-next-line no-undef
  sales_price$({ sales_price_in }); // never run, but here to "trick" calculang graph logic
};
////////// end sales_price memo-loader code //////////



////////// start purchase_price memo-loader code //////////
//const purchase_price$m = memoize(999999, isEqual)(purchase_price$);
export const purchase_price$m = memoize(purchase_price$, JSON.stringify);
export const purchase_price_ = (a) => {
  return purchase_price$m(a);
  // eslint-disable-next-line no-undef
  purchase_price$({ purchase_price_in }); // never run, but here to "trick" calculang graph logic
};
////////// end purchase_price memo-loader code //////////



////////// start units memo-loader code //////////
//const units$m = memoize(999999, isEqual)(units$);
export const units$m = memoize(units$, JSON.stringify);
export const units_ = (a) => {
  return units$m(a);
  // eslint-disable-next-line no-undef
  units$({ units_in }); // never run, but here to "trick" calculang graph logic
};
////////// end units memo-loader code //////////



////////// start expenses memo-loader code //////////
//const expenses$m = memoize(999999, isEqual)(expenses$);
export const expenses$m = memoize(expenses$, JSON.stringify);
export const expenses_ = (a) => {
  return expenses$m(a);
  // eslint-disable-next-line no-undef
  expenses$({ expenses_in }); // never run, but here to "trick" calculang graph logic
};
////////// end expenses memo-loader code //////////