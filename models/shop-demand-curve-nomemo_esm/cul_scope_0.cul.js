// shop model, formulae:
export const sales = () =>
  units() * sales_price();

export const purchases = () =>
  units() * purchase_price();

export const profit = () =>
  sales() - purchases() - expenses();

export const units = () =>
  7 * 20000 - sales_price() * 20000;

// inputs:
export const sales_price = () => sales_price_in;
export const purchase_price = () => purchase_price_in;
export const expenses = () => expenses_in;