import { units } from "./cul_scope_0.mjs"; // shop model, formulae:
export const sales = ({ sales_price_in }) =>
units({ sales_price_in }) * sales_price({ sales_price_in });

export const purchases = ({ sales_price_in, purchase_price_in }) =>
units({ sales_price_in }) * purchase_price({ purchase_price_in });

export const profit = ({ sales_price_in, purchase_price_in, expenses_in }) =>
sales({ sales_price_in }) - purchases({ sales_price_in, purchase_price_in }) - expenses({ expenses_in });

// inputs:
export const sales_price = ({ sales_price_in }) => sales_price_in;
export const purchase_price = ({ purchase_price_in }) => purchase_price_in;
export const units_ = ({ units_in }) => units_in;
export const expenses = ({ expenses_in }) => expenses_in;