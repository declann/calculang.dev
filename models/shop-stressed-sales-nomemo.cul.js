import { sales, purchases, profit, sales_price, purchase_price, expenses, units_ as units_base } from './shop.cul.js';
export { sales, purchases, profit, sales_price, purchase_price, expenses };

export const units = () =>
  units_base() * 0.9;