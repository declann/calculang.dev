// This model is a inspired by the following made in p5.js by Daniel Shiffman http://codingtra.in
// https://editor.p5js.org/codingtrain/sketches/1wLHIck3T
// heavily simplified!

/* seeded random numbers: */
export const random_seed = ({ random_seed_in }) => random_seed_in;
import { prng_alea } from "./esm-seedrandom.mjs";
export const seeded = ({ random_seed_in }) => prng_alea(random_seed({ random_seed_in }));
export const random = ({ random_seed_in }) => seeded({ random_seed_in: random_seed({ random_seed_in }) })();
// alternative:
//export const random = () => Math.random();

export const frame = ({ frame_in }) => frame_in;

export const star = ({ star_in }) => star_in;

// star-level calcs:
export const x0 = ({ random_seed_in, star_in }) => random({ random_seed_in, star_in: star({ star_in }), id: "x" }) * 100 - 50;
export const y0 = ({ random_seed_in, star_in }) => random({ random_seed_in, star_in: star({ star_in }), id: "y" }) * 100 - 50;
export const z0 = ({ random_seed_in, star_in }) => random({ random_seed_in, star_in: star({ star_in }), id: "z" }) * 50;

export const speed = ({ speed_in }) => speed_in;

// use for radius
export const z = ({ frame_in, random_seed_in, star_in, speed_in }) => {
  if (frame({ frame_in }) == 0) return z0({ random_seed_in, star_in });else
  if (z({ random_seed_in, star_in, speed_in, frame_in: frame({ frame_in }) - 1 }) - speed({ speed_in }) < 1) return 50; // "far away"
  else return z({ random_seed_in, star_in, speed_in, frame_in: frame({ frame_in }) - 1 }) - speed({ speed_in });
};

// only z gets updated in each frame

export const visible = ({ frame_in, random_seed_in, star_in, speed_in }) => z({ frame_in, random_seed_in, star_in, speed_in }) > 1;

export const x = ({ frame_in, random_seed_in, star_in, speed_in }) => {
  if (frame({ frame_in }) == 0) return x0({ random_seed_in, star_in });else
  if (!visible({ random_seed_in, star_in, speed_in, frame_in: frame({ frame_in }) - 1 })) return x0({ random_seed_in, star_in });else
  return x0({ random_seed_in, star_in }) / z({ frame_in, random_seed_in, star_in, speed_in }) * 50;
};

export const y = ({ frame_in, random_seed_in, star_in, speed_in }) => {
  if (frame({ frame_in }) == 0) return y0({ random_seed_in, star_in });else
  if (!visible({ random_seed_in, star_in, speed_in, frame_in: frame({ frame_in }) - 1 })) return y0({ random_seed_in, star_in });else
  return y0({ random_seed_in, star_in }) / z({ frame_in, random_seed_in, star_in, speed_in }) * 50;
};

export const px = ({ frame_in, random_seed_in, star_in, speed_in }) => {
  if (x({ frame_in, random_seed_in, star_in, speed_in }) == x0({ random_seed_in, star_in })) return x0({ random_seed_in, star_in });else
  return x({ random_seed_in, star_in, speed_in, frame_in: frame({ frame_in }) - 1 });
};
export const py = ({ frame_in, random_seed_in, star_in, speed_in }) => {
  if (y({ frame_in, random_seed_in, star_in, speed_in }) == y0({ random_seed_in, star_in })) return y0({ random_seed_in, star_in });else
  return y({ random_seed_in, star_in, speed_in, frame_in: frame({ frame_in }) - 1 });
};