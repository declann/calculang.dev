// This model is a inspired by the following made in p5.js by Daniel Shiffman http://codingtra.in
// https://editor.p5js.org/codingtrain/sketches/1wLHIck3T
// heavily simplified!

/* seeded random numbers: */
export const random_seed = () => random_seed_in;
import { prng_alea } from "./esm-seedrandom.mjs";
export const seeded = () => prng_alea(random_seed());
export const random = () => seeded({ random_seed_in: random_seed() })();
// alternative:
//export const random = () => Math.random();

export const frame = () => frame_in;

export const star = () => star_in;

// star-level calcs:
export const x0 = () => random({ star_in: star(), id: "x" }) * 100 - 50;
export const y0 = () => random({ star_in: star(), id: "y" }) * 100 - 50;
export const z0 = () => random({ star_in: star(), id: "z" }) * 50;

export const speed = () => speed_in;

// use for radius
export const z = () => {
  if (frame() == 0) return z0();
  else if (z({ frame_in: frame() - 1 }) - speed() < 1) return 50; // "far away"
  else return z({ frame_in: frame() - 1 }) - speed();
};

// only z gets updated in each frame

export const visible = () => z() > 1;

export const x = () => {
  if (frame() == 0) return x0();
  else if (!visible({ frame_in: frame() - 1 })) return x0();
  else return (x0() / z()) * 50;
};

export const y = () => {
  if (frame() == 0) return y0();
  else if (!visible({ frame_in: frame() - 1 })) return y0();
  else return (y0() / z()) * 50;
};

export const px = () => {
  if (x() == x0()) return x0();
  else return x({ frame_in: frame() - 1 });
};
export const py = () => {
  if (y() == y0()) return y0();
  else return y({ frame_in: frame() - 1 });
};
