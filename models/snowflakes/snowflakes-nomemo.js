(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else {
		var a = factory();
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return angle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return x; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return y; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return time; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return p; });
/* unused harmony export f */
/* unused harmony export w */
/* unused harmony export radius_ */
/* unused harmony export initial_angle_ */
/* unused harmony export size_ */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return width; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return height; });
/* harmony import */ var _snowflakes_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1);
 // this model defines dynamics of a falling snowflake

const angle = ({ w_in, f_in, p_in }) => w({ w_in }) * time({ f_in }) + Object(_snowflakes_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_0__["initial_angle"])({ p_in });

const x = ({ width_in, p_in, w_in, f_in }) => width({ width_in }) / 2 + Object(_snowflakes_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_0__["radius"])({ p_in, width_in }) * Math.sin(angle({ w_in, f_in, p_in }));

const y = ({ f_in, height_in, p_in }) => {
  if (f({ f_in }) == 0 || y({ height_in, p_in, f_in: f({ f_in }) - 1 }) > height({ height_in }) + 20) return -10;else
  return y({ height_in, p_in, f_in: f({ f_in }) - 1 }) + Math.pow(Object(_snowflakes_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_0__["size"])({ p_in }), 0.5);
};

const time = ({ f_in }) => f({ f_in }) / 30;

// inputs:
const p = ({ p_in }) => p_in; // point or snowflake
const f = ({ f_in }) => f_in; // frame
const w = ({ w_in }) => w_in; // angular speed
const radius_ = ({ radius_in }) => radius_in; // radius of sine wave (or amplitude in x)
const initial_angle_ = ({ initial_angle_in }) => initial_angle_in;
const size_ = ({ size_in }) => size_in;
const width = ({ width_in }) => width_in;
const height = ({ height_in }) => height_in;

/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "radius", function() { return radius; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initial_angle", function() { return initial_angle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "size", function() { return size; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "random", function() { return random; });
/* harmony import */ var _snowflake_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "time", function() { return _snowflake_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__["d"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "width", function() { return _snowflake_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__["e"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "height", function() { return _snowflake_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__["b"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "angle", function() { return _snowflake_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__["a"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "x", function() { return _snowflake_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__["f"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "y", function() { return _snowflake_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__["g"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "p", function() { return _snowflake_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__["c"]; });

// this model defines the random distributions of inputs used for different snowflakes.
// I separate from the pure dynamics of a falling snowflake, below




// radius of sine wave (or amplitude in x)
const radius = ({ p_in, width_in }) =>
Math.sqrt(random({ p_in: Object(_snowflake_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* p */ "c"])({ p_in }) }) * Math.pow(Object(_snowflake_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* width */ "e"])({ width_in }) / 2, 2));

const initial_angle = ({ p_in }) => random({ p_in: Object(_snowflake_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* p */ "c"])({ p_in }), v: 2 }) * 2 * Math.PI;

const size = ({ p_in }) =>
random({ p_in: Object(_snowflake_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* p */ "c"])({ p_in }), v: 0 }) * 2 + random({ p_in: Object(_snowflake_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* p */ "c"])({ p_in }), v: 1 }) * 3;

// must compile with memoization and must pass appropriate params into random, to ensure purity
const random = ({}) => Math.random();

/* for seeded random numbers use:
import { prng_alea } from "./esm-seedrandom.mjs";
export const random = () => prng_alea("seed123456")();*/

/***/ })
/******/ ]);
});
//# sourceMappingURL=snowflakes-nomemo.js.map