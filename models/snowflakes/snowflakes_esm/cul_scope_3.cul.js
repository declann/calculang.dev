// this model defines dynamics of a falling snowflake

export const angle = () => w() * time() + initial_angle();

export const x = () => width() / 2 + radius() * Math.sin(angle());

export const y = () => {
  if (f() == 0 || y({ f_in: f() - 1 }) > height() + 20) return -10;
  else return y({ f_in: f() - 1 }) + Math.pow(size(), 0.5);
};

export const time = () => f() / 30;

// inputs:
export const p = () => p_in; // point or snowflake
export const f = () => f_in; // frame
export const w = () => w_in; // angular speed
export const radius = () => radius_in; // radius of sine wave (or amplitude in x)
export const initial_angle = () => initial_angle_in;
export const size = () => size_in;
export const width = () => width_in;
export const height = () => height_in;
