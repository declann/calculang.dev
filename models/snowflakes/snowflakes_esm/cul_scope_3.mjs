import { w } from "./cul_scope_2.mjs";import { f } from "./cul_scope_2.mjs";import { random } from "./cul_scope_0.mjs";import { size } from "./cul_scope_0.mjs";import { initial_angle } from "./cul_scope_0.mjs";import { radius } from "./cul_scope_0.mjs";import { p } from "./cul_scope_0.mjs";import { y } from "./cul_scope_0.mjs";import { x } from "./cul_scope_0.mjs";import { angle } from "./cul_scope_0.mjs";import { height } from "./cul_scope_0.mjs";import { width } from "./cul_scope_0.mjs";import { time } from "./cul_scope_0.mjs"; // this model defines dynamics of a falling snowflake

export const angle_ = ({ w_in, f_in, p_in }) => w({ w_in }) * time({ f_in }) + initial_angle({ p_in });

export const x_ = ({ width_in, p_in, w_in, f_in }) => width({ width_in }) / 2 + radius({ p_in, width_in }) * Math.sin(angle({ w_in, f_in, p_in }));

export const y_ = ({ f_in, height_in, p_in }) => {
  if (f({ f_in }) == 0 || y({ height_in, p_in, f_in: f({ f_in }) - 1 }) > height({ height_in }) + 20) return -10;else
  return y({ height_in, p_in, f_in: f({ f_in }) - 1 }) + Math.pow(size({ p_in }), 0.5);
};

export const time_ = ({ f_in }) => f({ f_in }) / 30;

// inputs:
export const p_ = ({ p_in }) => p_in; // point or snowflake
export const f_ = ({ f_in }) => f_in; // frame
export const w_ = ({ w_in }) => w_in; // angular speed
export const radius_ = ({ radius_in }) => radius_in; // radius of sine wave (or amplitude in x)
export const initial_angle_ = ({ initial_angle_in }) => initial_angle_in;
export const size_ = ({ size_in }) => size_in;
export const width_ = ({ width_in }) => width_in;
export const height_ = ({ height_in }) => height_in;