export const x = ({ t_in, angle_in, power_in, drag_coefficient_in }) => {
  if (t({ t_in }) == 0) return 0;else
  return x({ angle_in, power_in, drag_coefficient_in, t_in: t({ t_in }) - 1 }) + dx({ angle_in, power_in, t_in, drag_coefficient_in });
};

export const dx = ({ angle_in, power_in, t_in, drag_coefficient_in }) => Math.sin(angle({ angle_in }) * Math.PI / 180) * power({ power_in }) - drag_x({ t_in, angle_in, power_in, drag_coefficient_in });

// linear drag // test
export const drag_x = ({ t_in, angle_in, power_in, drag_coefficient_in }) => x({ angle_in, power_in, drag_coefficient_in, t_in: t({ t_in }) - 1 }) * drag_coefficient({ drag_coefficient_in });
export const drag_y = ({ t_in, angle_in, power_in, g_in, drag_coefficient_in }) => y({ angle_in, power_in, g_in, drag_coefficient_in, t_in: t({ t_in }) - 1 }) * drag_coefficient({ drag_coefficient_in });

export const y = ({ t_in, angle_in, power_in, g_in, drag_coefficient_in }) => {
  if (t({ t_in }) == 0) return 0;else
  return y({ angle_in, power_in, g_in, drag_coefficient_in, t_in: t({ t_in }) - 1 }) + dy({ angle_in, power_in, t_in, g_in, drag_coefficient_in });
};

export const dy = ({ angle_in, power_in, t_in, g_in, drag_coefficient_in }) =>
Math.cos(angle({ angle_in }) * Math.PI / 180) * power({ power_in }) - t({ t_in }) * g({ g_in }) - drag_y({ t_in, angle_in, power_in, g_in, drag_coefficient_in });

// inputs:
export const t = ({ t_in }) => t_in;
export const angle = ({ angle_in }) => angle_in;
export const power = ({ power_in }) => power_in;
export const g = ({ g_in }) => g_in;
export const drag_coefficient = ({ drag_coefficient_in }) => drag_coefficient_in;