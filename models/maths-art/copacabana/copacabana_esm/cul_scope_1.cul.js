// a trick for consistent random here breaks all the rules!, but it works as long as memo is on
// for a different, (better) approach see random seed approach in monte-carlo-pi example
// (or here: https://observablehq.com/@declann/monte-carlo-pi?collection=@declann/calculang)

export const x = () => Math.random() * 40 - 20 + p() * 0; // no random seed here, => we need memo on for consistency
export const y = () => Math.random() * 40 - 20 + p() * 0;

export const p = () => p_in; // point

export const waveA = () =>
  (Math.sin(x()) + Math.sin(x()) * 0.6 * (1 - Math.abs(Math.sin(x())))) * 3 + 0;
export const waveB = () =>
  (Math.sin(x()) + Math.sin(x()) * 0.6 * (1 - Math.abs(Math.sin(x())))) * 3 + 4; // same as waveA with a 4 offset.. (this isn't faithful)

export const color = () => {
  //if (y() > 0 || 1) return y() > waveA() && y() < waveB() ? 1 : 0;
  return Math.floor((y() - waveA()) / (waveB() - waveA()));
};
