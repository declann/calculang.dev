import { y } from "./cul_scope_0.mjs";import { x } from "./cul_scope_0.mjs";import { skewness } from "./cul_scope_0.mjs";export const x_ = ({ x_in }) => x_in;
export const y_ = ({ y_in }) => y_in;

export const block_x = ({ y_in, skewness_in, x_in }) => Math.floor(x({ y_in, skewness_in, x_in }) / 4);

export const section_x = ({ y_in, skewness_in, x_in }) => Math.floor(x({ y_in, skewness_in, x_in })) % 4; // 0,1,2,3

export const block_y = ({ y_in, skewness_in, x_in }) => {
  if (section_x({ y_in, skewness_in, x_in }) == 0) return Math.floor(y({ y_in }));
  if (section_x({ y_in, skewness_in, x_in }) == 1 || section_x({ y_in, skewness_in, x_in }) == 2)
  return Math.floor(y({ y_in }) - (0.5 - (0.5 * x({ y_in, skewness_in, x_in }) - 0.5 * block_x({ y_in, skewness_in, x_in }) * 4)));
  if (section_x({ y_in, skewness_in, x_in }) == 3) return Math.floor(y({ y_in })) + 1;
  return console.error(); // shouldn't occur
};

export const color = ({ y_in, skewness_in, x_in }) => -block_x({ y_in, skewness_in, x_in }) - block_x({ y_in, skewness_in, x_in }) % 2 - block_y({ y_in, skewness_in, x_in });