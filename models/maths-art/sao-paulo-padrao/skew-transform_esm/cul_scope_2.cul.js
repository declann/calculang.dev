
    import { memoize } from 'underscore';
    //import memoize from 'lru-memoize';
    //import { isEqual } from 'underscore'; // TODO poor tree shaking support, or why is this impact so massive? Move to lodash/lodash-es?
    
    // import/export non-to memo?

    import { x_ as x$, y_ as y$, block_x_ as block_x$, section_x_ as section_x$, block_y_ as block_y$, color_ as color$ } from './base-pattern.cul.js?&+memoed'; // there is already-culed stuff in here, why? imports to memo loader include cul_scope_id, what logic should it apply RE passing forward? eliminate? Probably!

    
    
    

////////// start x memo-loader code //////////
//const x$m = memoize(999999, isEqual)(x$);
export const x$m = memoize(x$, JSON.stringify);
export const x = (a) => {
  return x$m(a);
  // eslint-disable-next-line no-undef
  x$(); // never run, but here to "trick" calculang graph logic
};
////////// end x memo-loader code //////////



////////// start y memo-loader code //////////
//const y$m = memoize(999999, isEqual)(y$);
export const y$m = memoize(y$, JSON.stringify);
export const y = (a) => {
  return y$m(a);
  // eslint-disable-next-line no-undef
  y$(); // never run, but here to "trick" calculang graph logic
};
////////// end y memo-loader code //////////



////////// start block_x memo-loader code //////////
//const block_x$m = memoize(999999, isEqual)(block_x$);
export const block_x$m = memoize(block_x$, JSON.stringify);
export const block_x = (a) => {
  return block_x$m(a);
  // eslint-disable-next-line no-undef
  block_x$(); // never run, but here to "trick" calculang graph logic
};
////////// end block_x memo-loader code //////////



////////// start section_x memo-loader code //////////
//const section_x$m = memoize(999999, isEqual)(section_x$);
export const section_x$m = memoize(section_x$, JSON.stringify);
export const section_x = (a) => {
  return section_x$m(a);
  // eslint-disable-next-line no-undef
  section_x$(); // never run, but here to "trick" calculang graph logic
};
////////// end section_x memo-loader code //////////



////////// start block_y memo-loader code //////////
//const block_y$m = memoize(999999, isEqual)(block_y$);
export const block_y$m = memoize(block_y$, JSON.stringify);
export const block_y = (a) => {
  return block_y$m(a);
  // eslint-disable-next-line no-undef
  block_y$(); // never run, but here to "trick" calculang graph logic
};
////////// end block_y memo-loader code //////////



////////// start color memo-loader code //////////
//const color$m = memoize(999999, isEqual)(color$);
export const color$m = memoize(color$, JSON.stringify);
export const color = (a) => {
  return color$m(a);
  // eslint-disable-next-line no-undef
  color$(); // never run, but here to "trick" calculang graph logic
};
////////// end color memo-loader code //////////


    