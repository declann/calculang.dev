(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else {
		var a = factory();
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "skewness", function() { return skewness; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "x", function() { return x; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "y", function() { return y; });
/* harmony import */ var _base_pattern_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1);
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "block_x", function() { return _base_pattern_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__["a"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "section_x", function() { return _base_pattern_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__["d"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "block_y", function() { return _base_pattern_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__["b"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "color", function() { return _base_pattern_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__["c"]; });




const skewness = ({ skewness_in }) => skewness_in;

// skew linear transformation
const x = ({ y_in, skewness_in, x_in }) => Object(_base_pattern_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* y_ */ "f"])({ y_in }) * skewness({ skewness_in }) + Object(_base_pattern_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* x_ */ "e"])({ x_in });
const y = ({ y_in }) => Object(_base_pattern_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* y_ */ "f"])({ y_in });

/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return x_; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return y_; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return block_x; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return section_x; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return block_y; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return color; });
/* harmony import */ var _skew_transform_cul_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
const x_ = ({ x_in }) => x_in;
const y_ = ({ y_in }) => y_in;

const block_x = ({ y_in, skewness_in, x_in }) => Math.floor(Object(_skew_transform_cul_js__WEBPACK_IMPORTED_MODULE_0__["x"])({ y_in, skewness_in, x_in }) / 4);

const section_x = ({ y_in, skewness_in, x_in }) => Math.floor(Object(_skew_transform_cul_js__WEBPACK_IMPORTED_MODULE_0__["x"])({ y_in, skewness_in, x_in })) % 4; // 0,1,2,3

const block_y = ({ y_in, skewness_in, x_in }) => {
  if (section_x({ y_in, skewness_in, x_in }) == 0) return Math.floor(Object(_skew_transform_cul_js__WEBPACK_IMPORTED_MODULE_0__["y"])({ y_in }));
  if (section_x({ y_in, skewness_in, x_in }) == 1 || section_x({ y_in, skewness_in, x_in }) == 2)
  return Math.floor(Object(_skew_transform_cul_js__WEBPACK_IMPORTED_MODULE_0__["y"])({ y_in }) - (0.5 - (0.5 * Object(_skew_transform_cul_js__WEBPACK_IMPORTED_MODULE_0__["x"])({ y_in, skewness_in, x_in }) - 0.5 * block_x({ y_in, skewness_in, x_in }) * 4)));
  if (section_x({ y_in, skewness_in, x_in }) == 3) return Math.floor(Object(_skew_transform_cul_js__WEBPACK_IMPORTED_MODULE_0__["y"])({ y_in })) + 1;
  return console.error(); // shouldn't occur
};

const color = ({ y_in, skewness_in, x_in }) => -block_x({ y_in, skewness_in, x_in }) - block_x({ y_in, skewness_in, x_in }) % 2 - block_y({ y_in, skewness_in, x_in });

/***/ })
/******/ ]);
});
//# sourceMappingURL=skew-transform.js.map