import {
  block_x,
  block_y,
  section_x,
  color,
  x_ as x0,
  y_ as y0,
} from "./base-pattern.cul.js";
export { block_x, section_x, block_y, color };

export const skewness = () => skewness_in;

// skew linear transformation
export const x = () => y0() * skewness() + x0();
export const y = () => y0();
