(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else {
		var a = factory();
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "date", function() { return date; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "year", function() { return year; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isLeap", function() { return isLeap; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "daysInYear", function() { return daysInYear; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "daysIntoYearStartTomorrow", function() { return daysIntoYearStartTomorrow; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "daysIntoYearStartToday", function() { return daysIntoYearStartToday; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "daysIntoYear", function() { return daysIntoYear; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fractionIntoYear", function() { return fractionIntoYear; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fractionLeftInYear", function() { return fractionLeftInYear; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "timing", function() { return timing; });
// date_in is the only input, then fractionIntoYear({date_in}) does the calculation for any date (see below)
const date = ({ date_in }) => date_in;

// formulae:

const year = ({ date_in }) => date({ date_in }).getFullYear(); // e.g. 2023

// leap year if Feb has 29 days,
// from https://stackoverflow.com/a/43819507
const isLeap = ({ date_in }) => new Date(year({ date_in }), 1, 29).getDate() === 29;

const daysInYear = ({ date_in }) => isLeap({ date_in }) ? 366 : 365;

// no Daylight Savings Time allowance because
// "UTC ... is not subject to DST" \o/
// from https://stackoverflow.com/a/40975730
const daysIntoYearStartTomorrow = ({ date_in }) =>
(Date.UTC(date({ date_in }).getFullYear(), date({ date_in }).getMonth(), date({ date_in }).getDate()) -
Date.UTC(date({ date_in }).getFullYear(), 0, 0)) /
24 /
60 /
60 /
1000;

const daysIntoYearStartToday = ({ date_in }) => daysIntoYearStartTomorrow({ date_in }) - 1;

const daysIntoYear = ({ timing_in, date_in }) =>
timing({ timing_in }) == "start of today" ?
daysIntoYearStartToday({ date_in }) :
daysIntoYearStartTomorrow({ date_in });

const fractionIntoYear = ({ timing_in, date_in }) => daysIntoYear({ timing_in, date_in }) / daysInYear({ date_in });

const fractionLeftInYear = ({ timing_in, date_in }) => 1 - fractionIntoYear({ timing_in, date_in });

//inputs:
const timing = ({ timing_in }) => timing_in; // "start of today" or "start of tomorrow"

/***/ })
/******/ ]);
});
//# sourceMappingURL=year-remaining-nomemo.js.map