import { sdf_ as sdf_A } from "./cul_scope_1.mjs";
export { sdf_A };

import { sdf_ as sdf_B } from "./cul_scope_2.mjs";
export { sdf_B };

export const operation = ({ operation_in }) => operation_in; // union, intersection, B-A, A-B

//export const sdf = () => {Math[operation()/* safety warning */](sdf_A(), sdf_B());

export const sdf = ({ operation_in, i_in, shape_A_in, x_in, shape_x_A_in, y_in, shape_y_A_in, z_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) => {
  if (operation({ operation_in }) == 'union') return Math.min(sdf_A({ i_in, shape_A_in, x_in, shape_x_A_in, y_in, shape_y_A_in, z_in, shape_z_A_in, shape_size_A_in }), sdf_B({ i_in, shape_B_in, x_in, shape_x_B_in, y_in, shape_y_B_in, z_in, shape_z_B_in, shape_size_B_in }));
  if (operation({ operation_in }) == 'intersection') return Math.max(sdf_A({ i_in, shape_A_in, x_in, shape_x_A_in, y_in, shape_y_A_in, z_in, shape_z_A_in, shape_size_A_in }), sdf_B({ i_in, shape_B_in, x_in, shape_x_B_in, y_in, shape_y_B_in, z_in, shape_z_B_in, shape_size_B_in }));
  if (operation({ operation_in }) == 'A-B') return Math.max(sdf_A({ i_in, shape_A_in, x_in, shape_x_A_in, y_in, shape_y_A_in, z_in, shape_z_A_in, shape_size_A_in }), -sdf_B({ i_in, shape_B_in, x_in, shape_x_B_in, y_in, shape_y_B_in, z_in, shape_z_B_in, shape_size_B_in })); //sdf({operation_in: 'intersection'})
  if (operation({ operation_in }) == 'B-A') return Math.max(-sdf_A({ i_in, shape_A_in, x_in, shape_x_A_in, y_in, shape_y_A_in, z_in, shape_z_A_in, shape_size_A_in }), sdf_B({ i_in, shape_B_in, x_in, shape_x_B_in, y_in, shape_y_B_in, z_in, shape_z_B_in, shape_size_B_in }));
};