import { sdf } from "./cul_scope_0.mjs";import { operation } from "./cul_scope_0.mjs";import { sdf_A } from "./cul_scope_1.mjs";import { sdf_ as sdf_xyz /* workaround for memo bug */ } from "./cul_scope_4.mjs";
export { sdf_xyz };

export const sdf_ = ({ i_in, shape_B_in, x_in, shape_x_B_in, y_in, shape_y_B_in, z_in, shape_z_B_in, shape_size_B_in }) => sdf_xyz({ i_in, shape_B_in, x_in, shape_x_B_in, y_in, shape_y_B_in, z_in, shape_z_B_in, shape_size_B_in });

// constrain sdf params to be _B inputs

export const shape = ({ shape_B_in }) => shape_B({ shape_B_in });
export const shape_x = ({ shape_x_B_in }) => shape_x_B({ shape_x_B_in });
export const shape_y = ({ shape_y_B_in }) => shape_y_B({ shape_y_B_in });
export const shape_z = ({ shape_z_B_in }) => shape_z_B({ shape_z_B_in });
export const shape_size = ({ shape_size_B_in }) => shape_size_B({ shape_size_B_in });

export const shape_B = ({ shape_B_in }) => shape_B_in;
export const shape_x_B = ({ shape_x_B_in }) => shape_x_B_in;
export const shape_y_B = ({ shape_y_B_in }) => shape_y_B_in;
export const shape_z_B = ({ shape_z_B_in }) => shape_z_B_in;
export const shape_size_B = ({ shape_size_B_in }) => shape_size_B_in;