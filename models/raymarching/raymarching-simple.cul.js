// see https://gist.github.com/pallada-92/b60f25d5a6aeddf7269d941bc35b6793

/*
Copyright (c) 2023 Yaroslav Sergienko

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

export const screen_width = () => 50;
export const screen_height = () => 50;
export const xq = () => xq_in;
export const yq = () => yq_in;
export const row = () => yq() / screen_height();
export const col = () => xq() / screen_width();
export const nx = () => 0.04 + col() * 0.56 + row() * 0.15;
export const ny = () => 0.85 + row() * -0.52;
export const nz = () => 0.74 + col() * -0.27 + row() * 0.30;
export const n = () => Math.sqrt(nx() ** 2 + ny() ** 2 + nz() ** 2) * 1.6;
export const v = () => {
    const n_ = n();
    const kx_ = nx() / n_;
    const ky_ = ny() / n_;
    const kz_ = nz() / n_;
    let res = 0;
    for (let i=0; i<100; i++) {
        const x = -0.61 + res * kx_;
        const y = -0.90 + res * ky_;
        const z = -1.24 + res * kz_;
        const sdf = Math.max(
            Math.abs(x) - 0.3,
            Math.abs(y) - 0.3,
            Math.abs(z) - 0.3,
            - (1 + x * x + y * y + z * z) / 2 + 0.57
        );
        res += sdf;
    }
    return res;
}
export const brightness = () => Number(((v() - 1.75) / 1.70).toFixed(2));
export const brightness_2 = () => Math.min(Math.max(brightness(), 0), 1);
export const luminosity_quantized_char = () => {
    const s = " .,-~:;=!*#$@";
    return s.split('')[Math.floor((1 - brightness_2()) * s.length)];
}