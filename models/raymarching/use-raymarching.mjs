import { v, result } from './raymarching_esm/cul_scope_0.mjs'

console.log(v({ raymarching_iterations_in: 10, dist_in: 1.88, alpha_in: 69, beta_in: 38, screen_height_in: 10, fov_in: 34, screen_width_in: 10, xq_in: 5, yq_in: 5, sdf_shape_in: 'cube_and_torus' }))



console.log(result({i_in:11, dist_in:1.88, alpha_in: 69, beta_in: 38, screen_height_in:10, fov_in:34, screen_width_in:10, xq_in:5, yq_in:5, sdf_shape_in:'cube_and_torus'}))


console.log(result({i_in:1100, dist_in:1.88, alpha_in: 69, beta_in: 38, screen_height_in:10, fov_in:34, screen_width_in:10, xq_in:5, yq_in:5, sdf_shape_in:'cube_and_torus'}))