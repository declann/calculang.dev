// dn
// from https://gist.github.com/pallada-92/b60f25d5a6aeddf7269d941bc35b6793
// tiny modifications, mainly paramaterise raymarching_iterations
// and some refactoring

/*
Copyright (c) 2023 Yaroslav Sergienko

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

// DN renamed rows, cols to screen_width,_height, not sure if better
// or if was useful old way

export const row = ({ yq_in, screen_height_in }) => 1 - yq({ yq_in }) / screen_height({ screen_height_in });
export const col = ({ xq_in, screen_width_in }) => xq({ xq_in }) / screen_width({ screen_width_in });

export const cosAlpha = ({ alpha_in }) => Math.cos(alpha({ alpha_in }) * Math.PI / 180);
export const sinAlpha = ({ alpha_in }) => Math.sin(alpha({ alpha_in }) * Math.PI / 180);
export const cosBeta = ({ beta_in }) => Math.cos(beta({ beta_in }) * Math.PI / 180);
export const sinBeta = ({ beta_in }) => Math.sin(beta({ beta_in }) * Math.PI / 180);

export const camX = ({ dist_in, alpha_in, beta_in }) => dist({ dist_in }) * cosAlpha({ alpha_in }) * cosBeta({ beta_in });
export const camY = ({ dist_in, beta_in }) => dist({ dist_in }) * sinBeta({ beta_in });
export const camZ = ({ dist_in, alpha_in, beta_in }) => dist({ dist_in }) * sinAlpha({ alpha_in }) * cosBeta({ beta_in });

export const pixelSize = ({ fov_in, screen_width_in }) =>
Math.tan(fov({ fov_in }) * Math.PI / 180 / 2) / ((screen_width({ screen_width_in }) - 1) / 2);

export const ux = ({ fov_in, screen_width_in, alpha_in }) => -pixelSize({ fov_in, screen_width_in }) * sinAlpha({ alpha_in });
export const uz = ({ fov_in, screen_width_in, alpha_in }) => +pixelSize({ fov_in, screen_width_in }) * cosAlpha({ alpha_in });
export const vx = ({ fov_in, screen_width_in, alpha_in, beta_in }) => +pixelSize({ fov_in, screen_width_in }) * cosAlpha({ alpha_in }) * sinBeta({ beta_in });
export const vy = ({ fov_in, screen_width_in, beta_in }) => -pixelSize({ fov_in, screen_width_in }) * cosBeta({ beta_in });
export const vz = ({ fov_in, screen_width_in, alpha_in, beta_in }) => +pixelSize({ fov_in, screen_width_in }) * sinAlpha({ alpha_in }) * sinBeta({ beta_in });

export const x0 = ({ alpha_in, beta_in }) => -cosAlpha({ alpha_in }) * cosBeta({ beta_in });
export const y0 = ({ beta_in }) => -sinBeta({ beta_in });
export const z0 = ({ alpha_in, beta_in }) => -sinAlpha({ alpha_in }) * cosBeta({ beta_in });

// matrix ....
export const matX0 = ({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in }) =>
x0({ alpha_in, beta_in }) - screen_height({ screen_height_in }) / 2 * ux({ fov_in, screen_width_in, alpha_in }) - screen_width({ screen_width_in }) / 2 * vx({ fov_in, screen_width_in, alpha_in, beta_in }) + 0.02;
export const matX1 = ({ fov_in, screen_width_in, alpha_in }) => ux({ fov_in, screen_width_in, alpha_in }) * screen_width({ screen_width_in });
export const matX2 = ({ fov_in, screen_width_in, alpha_in, beta_in, screen_height_in }) => vx({ fov_in, screen_width_in, alpha_in, beta_in }) * screen_height({ screen_height_in });
export const matY0 = ({ beta_in, screen_width_in, fov_in }) => y0({ beta_in }) - screen_width({ screen_width_in }) / 2 * vy({ fov_in, screen_width_in, beta_in }) + 0.05;
export const matY1 = ({}) => 0;
export const matY2 = ({ fov_in, screen_width_in, beta_in, screen_height_in }) => vy({ fov_in, screen_width_in, beta_in }) * screen_height({ screen_height_in });
export const matZ0 = ({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in }) =>
z0({ alpha_in, beta_in }) - screen_height({ screen_height_in }) / 2 * uz({ fov_in, screen_width_in, alpha_in }) - screen_width({ screen_width_in }) / 2 * vz({ fov_in, screen_width_in, alpha_in, beta_in });
export const matZ1 = ({ fov_in, screen_width_in, alpha_in }) => uz({ fov_in, screen_width_in, alpha_in }) * screen_width({ screen_width_in });
export const matZ2 = ({ fov_in, screen_width_in, alpha_in, beta_in, screen_height_in }) => vz({ fov_in, screen_width_in, alpha_in, beta_in }) * screen_height({ screen_height_in });

export const nx = ({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in }) => matX0({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in }) + col({ xq_in, screen_width_in }) * matX1({ fov_in, screen_width_in, alpha_in }) + row({ yq_in, screen_height_in }) * matX2({ fov_in, screen_width_in, alpha_in, beta_in, screen_height_in });
export const ny = ({ beta_in, screen_width_in, fov_in, yq_in, screen_height_in }) => matY0({ beta_in, screen_width_in, fov_in }) + row({ yq_in, screen_height_in }) * matY2({ fov_in, screen_width_in, beta_in, screen_height_in });
export const nz = ({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in }) => matZ0({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in }) + col({ xq_in, screen_width_in }) * matZ1({ fov_in, screen_width_in, alpha_in }) + row({ yq_in, screen_height_in }) * matZ2({ fov_in, screen_width_in, alpha_in, beta_in, screen_height_in });

export const n = ({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in }) => Math.sqrt(nx({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in }) ** 2 + ny({ beta_in, screen_width_in, fov_in, yq_in, screen_height_in }) ** 2 + nz({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in }) ** 2) * 1.6;

export const i = ({ i_in }) => i_in;

// if I dont override x,y,z, instead constrain them to = result_, then I won't need a sep. sdf_composed entrypoint for sdf viz, do this?
export const /*result_*/x = ({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) => camX({ dist_in, alpha_in, beta_in }) + result({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) * nx({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in }) / n({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in });
export const /*result_*/y = ({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) => camY({ dist_in, beta_in }) + result({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) * ny({ beta_in, screen_width_in, fov_in, yq_in, screen_height_in }) / n({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in });
export const /*result_*/z = ({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) => camZ({ dist_in, alpha_in, beta_in }) + result({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) * nz({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in }) / n({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in });

import { sdf } from "./cul_scope_1.mjs";
export { sdf };

export const result1 = ({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) => {
  // this still depends on x,y,z
  // a perf hint ? would need to measure
  //for (let h = 0; h < i(); h += 5) result({ i_in: h });
  if (i({ i_in }) <= 0) return 0;else
  return result({ operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in, i_in: i({ i_in }) - 1 }) + sdf({ operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in, i_in: i({ i_in }) - 1 });
};

export const result = ({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) => {
  if (i({ i_in }) <= 0) return 0;
  if (
  1 &&
  i({ i_in }) > 2 &&
  result({ operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in, i_in: i({ i_in }) - 1 }) - result({ operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in, i_in: i({ i_in }) - 2 }) < 0.1)

  return result({ operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in, i_in: i({ i_in }) - 1 });
  if (i({ i_in }) > 2 && result({ operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in, i_in: i({ i_in }) - 1 }) > 3)
  return result({ operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in, i_in: i({ i_in }) - 1 });else
  return result1({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in });
};

export const brightness = ({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) => Number(((result({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - 1.75) / 1.7).toFixed(2));
export const brightness_2 = ({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) => Math.min(Math.max(brightness({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }), 0), 1);
export const luminosity_quantized_char = ({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) => {
  const s = " .,-~:;=!*#$@";
  return s.split("")[Math.floor((1 - brightness_2({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in })) * s.length)];
};

// inputs:
export const raymarching_iterations = ({ raymarching_iterations_in }) => raymarching_iterations_in;
export const screen_width = ({ screen_width_in }) => screen_width_in;
export const screen_height = ({ screen_height_in }) => screen_height_in;
export const xq = ({ xq_in }) => xq_in;
export const yq = ({ yq_in }) => yq_in;
export const alpha = ({ alpha_in }) => alpha_in;
export const beta = ({ beta_in }) => beta_in;
export const fov = ({ fov_in }) => fov_in;
export const dist = ({ dist_in }) => dist_in;