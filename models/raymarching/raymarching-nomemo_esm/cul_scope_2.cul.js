import { sdf_ as sdf_xyz /* workaround for memo bug */ } from "./sdf_base.cul";
export { sdf_xyz };
  
export const sdf = () => sdf_xyz();

// constrain sdf params to be _A inputs

export const shape = () => shape_A();
export const shape_x = () => shape_x_A();
export const shape_y = () => shape_y_A();
export const shape_z = () => shape_z_A();
export const shape_size = () => shape_size_A();

export const shape_A = () => shape_A_in;
export const shape_x_A = () => shape_x_A_in;
export const shape_y_A = () => shape_y_A_in;
export const shape_z_A = () => shape_z_A_in;
export const shape_size_A = () => shape_size_A_in;
