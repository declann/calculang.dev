(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else {
		var a = factory();
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return sdf_; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return shape; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return shape_x; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return shape_y; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return shape_z; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return shape_size; });
/* unused harmony export shape_A */
/* unused harmony export shape_x_A */
/* unused harmony export shape_y_A */
/* unused harmony export shape_z_A */
/* unused harmony export shape_size_A */
/* harmony import */ var _sdf_composed_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var _sdf_B_cul_js_cul_scope_id_2_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1);
/* harmony import */ var _sdf_base_cul_cul_scope_id_3_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4);



const sdf_ = ({ i_in, shape_A_in, x_in, shape_x_A_in, y_in, shape_y_A_in, z_in, shape_z_A_in, shape_size_A_in }) => Object(_sdf_base_cul_cul_scope_id_3_cul_parent_scope_id_1__WEBPACK_IMPORTED_MODULE_2__[/* sdf_ */ "a"])({ i_in, shape_A_in, x_in, shape_x_A_in, y_in, shape_y_A_in, z_in, shape_z_A_in, shape_size_A_in });

// constrain sdf params to be _A inputs

const shape = ({ shape_A_in }) => shape_A({ shape_A_in });
const shape_x = ({ shape_x_A_in }) => shape_x_A({ shape_x_A_in });
const shape_y = ({ shape_y_A_in }) => shape_y_A({ shape_y_A_in });
const shape_z = ({ shape_z_A_in }) => shape_z_A({ shape_z_A_in });
const shape_size = ({ shape_size_A_in }) => shape_size_A({ shape_size_A_in });

const shape_A = ({ shape_A_in }) => shape_A_in;
const shape_x_A = ({ shape_x_A_in }) => shape_x_A_in;
const shape_y_A = ({ shape_y_A_in }) => shape_y_A_in;
const shape_z_A = ({ shape_z_A_in }) => shape_z_A_in;
const shape_size_A = ({ shape_size_A_in }) => shape_size_A_in;

/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return sdf_; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return shape; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return shape_x; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return shape_y; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return shape_z; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return shape_size; });
/* unused harmony export shape_B */
/* unused harmony export shape_x_B */
/* unused harmony export shape_y_B */
/* unused harmony export shape_z_B */
/* unused harmony export shape_size_B */
/* harmony import */ var _sdf_composed_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var _sdf_A_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(0);
/* harmony import */ var _sdf_base_cul_cul_scope_id_4_cul_parent_scope_id_2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3);



const sdf_ = ({ i_in, shape_B_in, x_in, shape_x_B_in, y_in, shape_y_B_in, z_in, shape_z_B_in, shape_size_B_in }) => Object(_sdf_base_cul_cul_scope_id_4_cul_parent_scope_id_2__WEBPACK_IMPORTED_MODULE_2__[/* sdf_ */ "a"])({ i_in, shape_B_in, x_in, shape_x_B_in, y_in, shape_y_B_in, z_in, shape_z_B_in, shape_size_B_in });

// constrain sdf params to be _B inputs

const shape = ({ shape_B_in }) => shape_B({ shape_B_in });
const shape_x = ({ shape_x_B_in }) => shape_x_B({ shape_x_B_in });
const shape_y = ({ shape_y_B_in }) => shape_y_B({ shape_y_B_in });
const shape_z = ({ shape_z_B_in }) => shape_z_B({ shape_z_B_in });
const shape_size = ({ shape_size_B_in }) => shape_size_B({ shape_size_B_in });

const shape_B = ({ shape_B_in }) => shape_B_in;
const shape_x_B = ({ shape_x_B_in }) => shape_x_B_in;
const shape_y_B = ({ shape_y_B_in }) => shape_y_B_in;
const shape_z_B = ({ shape_z_B_in }) => shape_z_B_in;
const shape_size_B = ({ shape_size_B_in }) => shape_size_B_in;

/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "operation", function() { return operation; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sdf", function() { return sdf; });
/* harmony import */ var _sdf_A_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "sdf_A", function() { return _sdf_A_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__["a"]; });

/* harmony import */ var _sdf_B_cul_js_cul_scope_id_2_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1);
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "sdf_B", function() { return _sdf_B_cul_js_cul_scope_id_2_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_1__["a"]; });







const operation = ({ operation_in }) => operation_in; // union, intersection, B-A, A-B

//export const sdf = () => {Math[operation()/* safety warning */](sdf_A(), sdf_B());

const sdf = ({ operation_in, i_in, shape_A_in, x_in, shape_x_A_in, y_in, shape_y_A_in, z_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) => {
  if (operation({ operation_in }) == 'union') return Math.min(Object(_sdf_A_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* sdf_ */ "a"])({ i_in, shape_A_in, x_in, shape_x_A_in, y_in, shape_y_A_in, z_in, shape_z_A_in, shape_size_A_in }), Object(_sdf_B_cul_js_cul_scope_id_2_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_1__[/* sdf_ */ "a"])({ i_in, shape_B_in, x_in, shape_x_B_in, y_in, shape_y_B_in, z_in, shape_z_B_in, shape_size_B_in }));
  if (operation({ operation_in }) == 'intersection') return Math.max(Object(_sdf_A_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* sdf_ */ "a"])({ i_in, shape_A_in, x_in, shape_x_A_in, y_in, shape_y_A_in, z_in, shape_z_A_in, shape_size_A_in }), Object(_sdf_B_cul_js_cul_scope_id_2_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_1__[/* sdf_ */ "a"])({ i_in, shape_B_in, x_in, shape_x_B_in, y_in, shape_y_B_in, z_in, shape_z_B_in, shape_size_B_in }));
  if (operation({ operation_in }) == 'A-B') return Math.max(Object(_sdf_A_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* sdf_ */ "a"])({ i_in, shape_A_in, x_in, shape_x_A_in, y_in, shape_y_A_in, z_in, shape_z_A_in, shape_size_A_in }), -Object(_sdf_B_cul_js_cul_scope_id_2_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_1__[/* sdf_ */ "a"])({ i_in, shape_B_in, x_in, shape_x_B_in, y_in, shape_y_B_in, z_in, shape_z_B_in, shape_size_B_in })); //sdf({operation_in: 'intersection'})
  if (operation({ operation_in }) == 'B-A') return Math.max(-Object(_sdf_A_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* sdf_ */ "a"])({ i_in, shape_A_in, x_in, shape_x_A_in, y_in, shape_y_A_in, z_in, shape_z_A_in, shape_size_A_in }), Object(_sdf_B_cul_js_cul_scope_id_2_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_1__[/* sdf_ */ "a"])({ i_in, shape_B_in, x_in, shape_x_B_in, y_in, shape_y_B_in, z_in, shape_z_B_in, shape_size_B_in }));
};

/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export x */
/* unused harmony export y */
/* unused harmony export z */
/* unused harmony export sphere */
/* unused harmony export cube */
/* unused harmony export shape_ */
/* unused harmony export shape_x_ */
/* unused harmony export shape_y_ */
/* unused harmony export shape_z_ */
/* unused harmony export shape_size_ */
/* unused harmony export i */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return sdf_; });
/* harmony import */ var _sdf_B_cul_js_cul_scope_id_2_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1);
/* harmony import */ var _sdf_composed_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2);
/* harmony import */ var _sdf_A_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(0);
const x = ({ x_in }) => x_in;
const y = ({ y_in }) => y_in;
const z = ({ z_in }) => z_in;


const sphere = ({ x_in, shape_x_B_in, y_in, shape_y_B_in, z_in, shape_z_B_in, shape_size_B_in }) =>
Math.sqrt(
  (x({ x_in }) - Object(_sdf_B_cul_js_cul_scope_id_2_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* shape_x */ "d"])({ shape_x_B_in })) ** 2 + (y({ y_in }) - Object(_sdf_B_cul_js_cul_scope_id_2_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* shape_y */ "e"])({ shape_y_B_in })) ** 2 + (z({ z_in }) - Object(_sdf_B_cul_js_cul_scope_id_2_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* shape_z */ "f"])({ shape_z_B_in })) ** 2
) - Object(_sdf_B_cul_js_cul_scope_id_2_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* shape_size */ "c"])({ shape_size_B_in });

const cube = ({ x_in, shape_x_B_in, y_in, shape_y_B_in, z_in, shape_z_B_in, shape_size_B_in }) =>
Math.max(
  Math.abs(x({ x_in }) - Object(_sdf_B_cul_js_cul_scope_id_2_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* shape_x */ "d"])({ shape_x_B_in })),
  Math.abs(y({ y_in }) - Object(_sdf_B_cul_js_cul_scope_id_2_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* shape_y */ "e"])({ shape_y_B_in })),
  Math.abs(z({ z_in }) - Object(_sdf_B_cul_js_cul_scope_id_2_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* shape_z */ "f"])({ shape_z_B_in }))
) - Object(_sdf_B_cul_js_cul_scope_id_2_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* shape_size */ "c"])({ shape_size_B_in });

const shape_ = ({ shape_in }) => shape_in;
const shape_x_ = ({ shape_x_in }) => shape_x_in;
const shape_y_ = ({ shape_y_in }) => shape_y_in;
const shape_z_ = ({ shape_z_in }) => shape_z_in;
const shape_size_ = ({ shape_size_in }) => shape_size_in;

const i = ({ i_in }) => i_in;

const sdf_ = ({ i_in, shape_B_in, x_in, shape_x_B_in, y_in, shape_y_B_in, z_in, shape_z_B_in, shape_size_B_in }) => {
  if (i({ i_in }) <= 0) return 0; // ?

  if (Object(_sdf_B_cul_js_cul_scope_id_2_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* shape */ "b"])({ shape_B_in }) == "sphere") return sphere({ x_in, shape_x_B_in, y_in, shape_y_B_in, z_in, shape_z_B_in, shape_size_B_in });else
  if (Object(_sdf_B_cul_js_cul_scope_id_2_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* shape */ "b"])({ shape_B_in }) == "cube") return cube({ x_in, shape_x_B_in, y_in, shape_y_B_in, z_in, shape_z_B_in, shape_size_B_in });else
  if (Object(_sdf_B_cul_js_cul_scope_id_2_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* shape */ "b"])({ shape_B_in }) == "cube_with_holes")
  return Math.max(
    Math.abs(x({ x_in })) - 0.3,
    Math.abs(y({ y_in })) - 0.3,
    Math.abs(z({ z_in })) - 0.3,
    -(1 + x({ x_in }) * x({ x_in }) + y({ y_in }) * y({ y_in }) + z({ z_in }) * z({ z_in })) / 2 + 0.57
  );else
  if (Object(_sdf_B_cul_js_cul_scope_id_2_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* shape */ "b"])({ shape_B_in }) == "cube_and_torus")
  return Math.min(
    Math.max(
      Math.abs(x({ x_in })) - 0.3,
      Math.abs(y({ y_in })) - 0.3,
      Math.abs(z({ z_in })) - 0.3,
      -Math.sqrt(x({ x_in }) ** 2 + y({ y_in }) ** 2 + z({ z_in }) ** 2) + 0.375
    ),
    Math.sqrt(
      (Math.sqrt((x({ x_in }) - 0.25) ** 2 + (z({ z_in }) - 0.25) ** 2) - 0.25) ** 2 +
      (y({ y_in }) - 0.0) ** 2
    ) - 0.05
  );else
  if (Object(_sdf_B_cul_js_cul_scope_id_2_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* shape */ "b"])({ shape_B_in }) == "teapot")
  return Math.min(
    Math.sqrt(
      (Math.sqrt(x({ x_in }) ** 2 + z({ z_in }) ** 2) - 0.3) ** 2 + (y({ y_in }) - 0.18) ** 2
    ) - 0.02,
    Math.sqrt(x({ x_in }) ** 2 + y({ y_in }) ** 2 * 2.5 + z({ z_in }) ** 2) - 0.4,
    Math.max(
      x({ x_in }) + y({ y_in }) - 0.15 - 0.05 - 0.5,
      -y({ y_in }) + 0.19 - 0.1,
      Math.sqrt(
        (Math.sqrt((x({ x_in }) - 0.55) ** 2 + (y({ y_in }) - 0.09) ** 2) - 0.1) ** 2 +
        (z({ z_in }) - 0.1) ** 2
      ) - 0.04
    ),
    Math.max(
      -(-y({ y_in }) + 0.19 - 0.1),
      Math.sqrt(
        (Math.sqrt((x({ x_in }) - 0.35) ** 2 + (y({ y_in }) - 0.09) ** 2) - 0.1) ** 2 +
        (z({ z_in }) - 0.1) ** 2
      ) - 0.04
    ),
    Math.sqrt(x({ x_in }) ** 2 + (y({ y_in }) - 0.27) ** 2 + z({ z_in }) ** 2) - 0.05
  );
};

/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export x */
/* unused harmony export y */
/* unused harmony export z */
/* unused harmony export sphere */
/* unused harmony export cube */
/* unused harmony export shape_ */
/* unused harmony export shape_x_ */
/* unused harmony export shape_y_ */
/* unused harmony export shape_z_ */
/* unused harmony export shape_size_ */
/* unused harmony export i */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return sdf_; });
/* harmony import */ var _sdf_A_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var _sdf_composed_nomemo_cul_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2);
/* harmony import */ var _sdf_B_cul_js_cul_scope_id_2_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1);
const x = ({ x_in }) => x_in;
const y = ({ y_in }) => y_in;
const z = ({ z_in }) => z_in;


const sphere = ({ x_in, shape_x_A_in, y_in, shape_y_A_in, z_in, shape_z_A_in, shape_size_A_in }) =>
Math.sqrt(
  (x({ x_in }) - Object(_sdf_A_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* shape_x */ "d"])({ shape_x_A_in })) ** 2 + (y({ y_in }) - Object(_sdf_A_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* shape_y */ "e"])({ shape_y_A_in })) ** 2 + (z({ z_in }) - Object(_sdf_A_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* shape_z */ "f"])({ shape_z_A_in })) ** 2
) - Object(_sdf_A_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* shape_size */ "c"])({ shape_size_A_in });

const cube = ({ x_in, shape_x_A_in, y_in, shape_y_A_in, z_in, shape_z_A_in, shape_size_A_in }) =>
Math.max(
  Math.abs(x({ x_in }) - Object(_sdf_A_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* shape_x */ "d"])({ shape_x_A_in })),
  Math.abs(y({ y_in }) - Object(_sdf_A_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* shape_y */ "e"])({ shape_y_A_in })),
  Math.abs(z({ z_in }) - Object(_sdf_A_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* shape_z */ "f"])({ shape_z_A_in }))
) - Object(_sdf_A_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* shape_size */ "c"])({ shape_size_A_in });

const shape_ = ({ shape_in }) => shape_in;
const shape_x_ = ({ shape_x_in }) => shape_x_in;
const shape_y_ = ({ shape_y_in }) => shape_y_in;
const shape_z_ = ({ shape_z_in }) => shape_z_in;
const shape_size_ = ({ shape_size_in }) => shape_size_in;

const i = ({ i_in }) => i_in;

const sdf_ = ({ i_in, shape_A_in, x_in, shape_x_A_in, y_in, shape_y_A_in, z_in, shape_z_A_in, shape_size_A_in }) => {
  if (i({ i_in }) <= 0) return 0; // ?

  if (Object(_sdf_A_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* shape */ "b"])({ shape_A_in }) == "sphere") return sphere({ x_in, shape_x_A_in, y_in, shape_y_A_in, z_in, shape_z_A_in, shape_size_A_in });else
  if (Object(_sdf_A_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* shape */ "b"])({ shape_A_in }) == "cube") return cube({ x_in, shape_x_A_in, y_in, shape_y_A_in, z_in, shape_z_A_in, shape_size_A_in });else
  if (Object(_sdf_A_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* shape */ "b"])({ shape_A_in }) == "cube_with_holes")
  return Math.max(
    Math.abs(x({ x_in })) - 0.3,
    Math.abs(y({ y_in })) - 0.3,
    Math.abs(z({ z_in })) - 0.3,
    -(1 + x({ x_in }) * x({ x_in }) + y({ y_in }) * y({ y_in }) + z({ z_in }) * z({ z_in })) / 2 + 0.57
  );else
  if (Object(_sdf_A_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* shape */ "b"])({ shape_A_in }) == "cube_and_torus")
  return Math.min(
    Math.max(
      Math.abs(x({ x_in })) - 0.3,
      Math.abs(y({ y_in })) - 0.3,
      Math.abs(z({ z_in })) - 0.3,
      -Math.sqrt(x({ x_in }) ** 2 + y({ y_in }) ** 2 + z({ z_in }) ** 2) + 0.375
    ),
    Math.sqrt(
      (Math.sqrt((x({ x_in }) - 0.25) ** 2 + (z({ z_in }) - 0.25) ** 2) - 0.25) ** 2 +
      (y({ y_in }) - 0.0) ** 2
    ) - 0.05
  );else
  if (Object(_sdf_A_cul_js_cul_scope_id_1_cul_parent_scope_id_0__WEBPACK_IMPORTED_MODULE_0__[/* shape */ "b"])({ shape_A_in }) == "teapot")
  return Math.min(
    Math.sqrt(
      (Math.sqrt(x({ x_in }) ** 2 + z({ z_in }) ** 2) - 0.3) ** 2 + (y({ y_in }) - 0.18) ** 2
    ) - 0.02,
    Math.sqrt(x({ x_in }) ** 2 + y({ y_in }) ** 2 * 2.5 + z({ z_in }) ** 2) - 0.4,
    Math.max(
      x({ x_in }) + y({ y_in }) - 0.15 - 0.05 - 0.5,
      -y({ y_in }) + 0.19 - 0.1,
      Math.sqrt(
        (Math.sqrt((x({ x_in }) - 0.55) ** 2 + (y({ y_in }) - 0.09) ** 2) - 0.1) ** 2 +
        (z({ z_in }) - 0.1) ** 2
      ) - 0.04
    ),
    Math.max(
      -(-y({ y_in }) + 0.19 - 0.1),
      Math.sqrt(
        (Math.sqrt((x({ x_in }) - 0.35) ** 2 + (y({ y_in }) - 0.09) ** 2) - 0.1) ** 2 +
        (z({ z_in }) - 0.1) ** 2
      ) - 0.04
    ),
    Math.sqrt(x({ x_in }) ** 2 + (y({ y_in }) - 0.27) ** 2 + z({ z_in }) ** 2) - 0.05
  );
};

/***/ })
/******/ ]);
});
//# sourceMappingURL=sdf_composed-nomemo.js.map