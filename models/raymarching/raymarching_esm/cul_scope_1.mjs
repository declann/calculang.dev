import { dist } from "./cul_scope_0.mjs";import { fov } from "./cul_scope_0.mjs";import { beta } from "./cul_scope_0.mjs";import { alpha } from "./cul_scope_0.mjs";import { yq } from "./cul_scope_0.mjs";import { xq } from "./cul_scope_0.mjs";import { screen_height } from "./cul_scope_0.mjs";import { screen_width } from "./cul_scope_0.mjs";import { raymarching_iterations } from "./cul_scope_0.mjs";import { luminosity_quantized_char } from "./cul_scope_0.mjs";import { brightness_2 } from "./cul_scope_0.mjs";import { brightness } from "./cul_scope_0.mjs";import { result } from "./cul_scope_0.mjs";import { result1 } from "./cul_scope_0.mjs";import { sdf } from "./cul_scope_0.mjs";import { z } from "./cul_scope_0.mjs";import { y } from "./cul_scope_0.mjs";import { x } from "./cul_scope_0.mjs";import { i } from "./cul_scope_0.mjs";import { n } from "./cul_scope_0.mjs";import { nz } from "./cul_scope_0.mjs";import { ny } from "./cul_scope_0.mjs";import { nx } from "./cul_scope_0.mjs";import { matZ2 } from "./cul_scope_0.mjs";import { matZ1 } from "./cul_scope_0.mjs";import { matZ0 } from "./cul_scope_0.mjs";import { matY2 } from "./cul_scope_0.mjs";import { matY1 } from "./cul_scope_0.mjs";import { matY0 } from "./cul_scope_0.mjs";import { matX2 } from "./cul_scope_0.mjs";import { matX1 } from "./cul_scope_0.mjs";import { matX0 } from "./cul_scope_0.mjs";import { z0 } from "./cul_scope_0.mjs";import { y0 } from "./cul_scope_0.mjs";import { x0 } from "./cul_scope_0.mjs";import { vz } from "./cul_scope_0.mjs";import { vy } from "./cul_scope_0.mjs";import { vx } from "./cul_scope_0.mjs";import { uz } from "./cul_scope_0.mjs";import { ux } from "./cul_scope_0.mjs";import { pixelSize } from "./cul_scope_0.mjs";import { camZ } from "./cul_scope_0.mjs";import { camY } from "./cul_scope_0.mjs";import { camX } from "./cul_scope_0.mjs";import { sinBeta } from "./cul_scope_0.mjs";import { cosBeta } from "./cul_scope_0.mjs";import { sinAlpha } from "./cul_scope_0.mjs";import { cosAlpha } from "./cul_scope_0.mjs";import { col } from "./cul_scope_0.mjs";import { row } from "./cul_scope_0.mjs"; // dn
// from https://gist.github.com/pallada-92/b60f25d5a6aeddf7269d941bc35b6793
// tiny modifications, mainly paramaterise raymarching_iterations
// and some refactoring

/*
Copyright (c) 2023 Yaroslav Sergienko

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

// DN renamed rows, cols to screen_width,_height, not sure if better
// or if was useful old way

export const row_ = ({ yq_in, screen_height_in }) => 1 - yq({ yq_in }) / screen_height({ screen_height_in });
export const col_ = ({ xq_in, screen_width_in }) => xq({ xq_in }) / screen_width({ screen_width_in });

export const cosAlpha_ = ({ alpha_in }) => Math.cos(alpha({ alpha_in }) * Math.PI / 180);
export const sinAlpha_ = ({ alpha_in }) => Math.sin(alpha({ alpha_in }) * Math.PI / 180);
export const cosBeta_ = ({ beta_in }) => Math.cos(beta({ beta_in }) * Math.PI / 180);
export const sinBeta_ = ({ beta_in }) => Math.sin(beta({ beta_in }) * Math.PI / 180);

export const camX_ = ({ dist_in, alpha_in, beta_in }) => dist({ dist_in }) * cosAlpha({ alpha_in }) * cosBeta({ beta_in });
export const camY_ = ({ dist_in, beta_in }) => dist({ dist_in }) * sinBeta({ beta_in });
export const camZ_ = ({ dist_in, alpha_in, beta_in }) => dist({ dist_in }) * sinAlpha({ alpha_in }) * cosBeta({ beta_in });

export const pixelSize_ = ({ fov_in, screen_width_in }) =>
Math.tan(fov({ fov_in }) * Math.PI / 180 / 2) / ((screen_width({ screen_width_in }) - 1) / 2);

export const ux_ = ({ fov_in, screen_width_in, alpha_in }) => -pixelSize({ fov_in, screen_width_in }) * sinAlpha({ alpha_in });
export const uz_ = ({ fov_in, screen_width_in, alpha_in }) => +pixelSize({ fov_in, screen_width_in }) * cosAlpha({ alpha_in });
export const vx_ = ({ fov_in, screen_width_in, alpha_in, beta_in }) => +pixelSize({ fov_in, screen_width_in }) * cosAlpha({ alpha_in }) * sinBeta({ beta_in });
export const vy_ = ({ fov_in, screen_width_in, beta_in }) => -pixelSize({ fov_in, screen_width_in }) * cosBeta({ beta_in });
export const vz_ = ({ fov_in, screen_width_in, alpha_in, beta_in }) => +pixelSize({ fov_in, screen_width_in }) * sinAlpha({ alpha_in }) * sinBeta({ beta_in });

export const x0_ = ({ alpha_in, beta_in }) => -cosAlpha({ alpha_in }) * cosBeta({ beta_in });
export const y0_ = ({ beta_in }) => -sinBeta({ beta_in });
export const z0_ = ({ alpha_in, beta_in }) => -sinAlpha({ alpha_in }) * cosBeta({ beta_in });

// matrix ....
export const matX0_ = ({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in }) =>
x0({ alpha_in, beta_in }) - screen_height({ screen_height_in }) / 2 * ux({ fov_in, screen_width_in, alpha_in }) - screen_width({ screen_width_in }) / 2 * vx({ fov_in, screen_width_in, alpha_in, beta_in }) + 0.02;
export const matX1_ = ({ fov_in, screen_width_in, alpha_in }) => ux({ fov_in, screen_width_in, alpha_in }) * screen_width({ screen_width_in });
export const matX2_ = ({ fov_in, screen_width_in, alpha_in, beta_in, screen_height_in }) => vx({ fov_in, screen_width_in, alpha_in, beta_in }) * screen_height({ screen_height_in });
export const matY0_ = ({ beta_in, screen_width_in, fov_in }) => y0({ beta_in }) - screen_width({ screen_width_in }) / 2 * vy({ fov_in, screen_width_in, beta_in }) + 0.05;
export const matY1_ = ({}) => 0;
export const matY2_ = ({ fov_in, screen_width_in, beta_in, screen_height_in }) => vy({ fov_in, screen_width_in, beta_in }) * screen_height({ screen_height_in });
export const matZ0_ = ({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in }) =>
z0({ alpha_in, beta_in }) - screen_height({ screen_height_in }) / 2 * uz({ fov_in, screen_width_in, alpha_in }) - screen_width({ screen_width_in }) / 2 * vz({ fov_in, screen_width_in, alpha_in, beta_in });
export const matZ1_ = ({ fov_in, screen_width_in, alpha_in }) => uz({ fov_in, screen_width_in, alpha_in }) * screen_width({ screen_width_in });
export const matZ2_ = ({ fov_in, screen_width_in, alpha_in, beta_in, screen_height_in }) => vz({ fov_in, screen_width_in, alpha_in, beta_in }) * screen_height({ screen_height_in });

export const nx_ = ({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in }) => matX0({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in }) + col({ xq_in, screen_width_in }) * matX1({ fov_in, screen_width_in, alpha_in }) + row({ yq_in, screen_height_in }) * matX2({ fov_in, screen_width_in, alpha_in, beta_in, screen_height_in });
export const ny_ = ({ beta_in, screen_width_in, fov_in, yq_in, screen_height_in }) => matY0({ beta_in, screen_width_in, fov_in }) + row({ yq_in, screen_height_in }) * matY2({ fov_in, screen_width_in, beta_in, screen_height_in });
export const nz_ = ({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in }) => matZ0({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in }) + col({ xq_in, screen_width_in }) * matZ1({ fov_in, screen_width_in, alpha_in }) + row({ yq_in, screen_height_in }) * matZ2({ fov_in, screen_width_in, alpha_in, beta_in, screen_height_in });

export const n_ = ({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in }) => Math.sqrt(nx({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in }) ** 2 + ny({ beta_in, screen_width_in, fov_in, yq_in, screen_height_in }) ** 2 + nz({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in }) ** 2) * 1.6;

export const i_ = ({ i_in }) => i_in;

// if I dont override x,y,z, instead constrain them to = result_, then I won't need a sep. sdf_composed entrypoint for sdf viz, do this?
export const /*result_*/x_ = ({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) => camX({ dist_in, alpha_in, beta_in }) + result({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) * nx({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in }) / n({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in });
export const /*result_*/y_ = ({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) => camY({ dist_in, beta_in }) + result({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) * ny({ beta_in, screen_width_in, fov_in, yq_in, screen_height_in }) / n({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in });
export const /*result_*/z_ = ({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) => camZ({ dist_in, alpha_in, beta_in }) + result({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) * nz({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in }) / n({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in });

import { sdf_ } from "./cul_scope_2.mjs";
export { sdf_ };

export const result1_ = ({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) => {
  // this still depends on x,y,z
  // a perf hint ? would need to measure
  //for (let h = 0; h < i(); h += 5) result({ i_in: h });
  if (i({ i_in }) <= 0) return 0;else
  return result({ operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in, i_in: i({ i_in }) - 1 }) + sdf({ operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in, i_in: i({ i_in }) - 1 });
};

export const result_ = ({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) => {
  if (i({ i_in }) <= 0) return 0;
  if (
  1 &&
  i({ i_in }) > 2 &&
  result({ operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in, i_in: i({ i_in }) - 1 }) - result({ operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in, i_in: i({ i_in }) - 2 }) < 0.1)

  return result({ operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in, i_in: i({ i_in }) - 1 });
  if (i({ i_in }) > 2 && result({ operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in, i_in: i({ i_in }) - 1 }) > 3)
  return result({ operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in, i_in: i({ i_in }) - 1 });else
  return result1({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in });
};

export const brightness_ = ({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) => Number(((result({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) - 1.75) / 1.7).toFixed(2));
export const brightness_2_ = ({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) => Math.min(Math.max(brightness({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }), 0), 1);
export const luminosity_quantized_char_ = ({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }) => {
  const s = " .,-~:;=!*#$@";
  return s.split("")[Math.floor((1 - brightness_2({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in })) * s.length)];
};

// inputs:
export const raymarching_iterations_ = ({ raymarching_iterations_in }) => raymarching_iterations_in;
export const screen_width_ = ({ screen_width_in }) => screen_width_in;
export const screen_height_ = ({ screen_height_in }) => screen_height_in;
export const xq_ = ({ xq_in }) => xq_in;
export const yq_ = ({ yq_in }) => yq_in;
export const alpha_ = ({ alpha_in }) => alpha_in;
export const beta_ = ({ beta_in }) => beta_in;
export const fov_ = ({ fov_in }) => fov_in;
export const dist_ = ({ dist_in }) => dist_in;