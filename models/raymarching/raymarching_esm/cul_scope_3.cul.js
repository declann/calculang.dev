import { sdf_ as sdf_A } from "./sdf_A.cul.js";
export { sdf_A };

import { sdf_ as sdf_B } from "./sdf_B.cul.js";
export { sdf_B };

export const operation = () => operation_in; // union, intersection, B-A, A-B
  
//export const sdf = () => {Math[operation()/* safety warning */](sdf_A(), sdf_B());

export const sdf = () => {
  if (operation() == 'union') return Math.min(sdf_A(), sdf_B());
  if (operation() == 'intersection') return Math.max(sdf_A(), sdf_B());
  if (operation() == 'A-B') return Math.max(sdf_A(), -sdf_B()); //sdf({operation_in: 'intersection'})
  if (operation() == 'B-A') return Math.max(-sdf_A(), sdf_B());
}
