// dn
// from https://gist.github.com/pallada-92/b60f25d5a6aeddf7269d941bc35b6793
// tiny modifications, mainly paramaterise raymarching_iterations
// and some refactoring

/*
Copyright (c) 2023 Yaroslav Sergienko

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

// DN renamed rows, cols to screen_width,_height, not sure if better
// or if was useful old way

export const row = () => 1 - yq() / screen_height();
export const col = () => xq() / screen_width();

export const cosAlpha = () => Math.cos((alpha() * Math.PI) / 180);
export const sinAlpha = () => Math.sin((alpha() * Math.PI) / 180);
export const cosBeta = () => Math.cos((beta() * Math.PI) / 180);
export const sinBeta = () => Math.sin((beta() * Math.PI) / 180);

export const camX = () => dist() * cosAlpha() * cosBeta();
export const camY = () => dist() * sinBeta();
export const camZ = () => dist() * sinAlpha() * cosBeta();

export const pixelSize = () =>
  Math.tan((fov() * Math.PI) / 180 / 2) / ((screen_width() - 1) / 2);

export const ux = () => -pixelSize() * sinAlpha();
export const uz = () => +pixelSize() * cosAlpha();
export const vx = () => +pixelSize() * cosAlpha() * sinBeta();
export const vy = () => -pixelSize() * cosBeta();
export const vz = () => +pixelSize() * sinAlpha() * sinBeta();

export const x0 = () => -cosAlpha() * cosBeta();
export const y0 = () => -sinBeta();
export const z0 = () => -sinAlpha() * cosBeta();

// matrix ....
export const matX0 = () =>
  x0() - (screen_height() / 2) * ux() - (screen_width() / 2) * vx() + 0.02;
export const matX1 = () => ux() * screen_width();
export const matX2 = () => vx() * screen_height();
export const matY0 = () => y0() - (screen_width() / 2) * vy() + 0.05;
export const matY1 = () => 0;
export const matY2 = () => vy() * screen_height();
export const matZ0 = () =>
  z0() - (screen_height() / 2) * uz() - (screen_width() / 2) * vz();
export const matZ1 = () => uz() * screen_width();
export const matZ2 = () => vz() * screen_height();

export const nx = () => matX0() + col() * matX1() + row() * matX2();
export const ny = () => matY0() + row() * matY2();
export const nz = () => matZ0() + col() * matZ1() + row() * matZ2();

export const n = () => Math.sqrt(nx() ** 2 + ny() ** 2 + nz() ** 2) * 1.6;

export const i = () => i_in;

// if I dont override x,y,z, instead constrain them to = result_, then I won't need a sep. sdf_composed entrypoint for sdf viz, do this?
export const /*result_*/ x = () => camX() + (result() * nx()) / n();
export const /*result_*/ y = () => camY() + (result() * ny()) / n();
export const /*result_*/ z = () => camZ() + (result() * nz()) / n();

import { sdf } from "./sdf_composed.cul";
export { sdf };

export const result1 = () => {
  // this still depends on x,y,z
  // a perf hint ? would need to measure
  //for (let h = 0; h < i(); h += 5) result({ i_in: h });
  if (i() <= 0) return 0;
  else return result({ i_in: i() - 1 }) + sdf({ i_in: i() - 1 });
};

export const result = () => {
  if (i() <= 0) return 0;
  if (
    1 &&
    i() > 2 &&
    result({ i_in: i() - 1 }) - result({ i_in: i() - 2 }) < 0.1
  )
    return result({ i_in: i() - 1 });
  if (i() > 2 && result({ i_in: i() - 1 }) > 3)
    return result({ i_in: i() - 1 });
  else return result1();
};

export const brightness = () => Number(((result() - 1.75) / 1.7).toFixed(2));
export const brightness_2 = () => Math.min(Math.max(brightness(), 0), 1);
export const luminosity_quantized_char = () => {
  const s = " .,-~:;=!*#$@";
  return s.split("")[Math.floor((1 - brightness_2()) * s.length)];
};

// inputs:
export const raymarching_iterations = () => raymarching_iterations_in;
export const screen_width = () => screen_width_in;
export const screen_height = () => screen_height_in;
export const xq = () => xq_in;
export const yq = () => yq_in;
export const alpha = () => alpha_in;
export const beta = () => beta_in;
export const fov = () => fov_in;
export const dist = () => dist_in;
