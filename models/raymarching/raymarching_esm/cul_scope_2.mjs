import { dist } from "./cul_scope_0.mjs";import { fov } from "./cul_scope_0.mjs";import { beta } from "./cul_scope_0.mjs";import { alpha } from "./cul_scope_0.mjs";import { yq } from "./cul_scope_0.mjs";import { xq } from "./cul_scope_0.mjs";import { screen_height } from "./cul_scope_0.mjs";import { screen_width } from "./cul_scope_0.mjs";import { raymarching_iterations } from "./cul_scope_0.mjs";import { luminosity_quantized_char } from "./cul_scope_0.mjs";import { brightness_2 } from "./cul_scope_0.mjs";import { brightness } from "./cul_scope_0.mjs";import { result } from "./cul_scope_0.mjs";import { result1 } from "./cul_scope_0.mjs";import { sdf } from "./cul_scope_0.mjs";import { z } from "./cul_scope_0.mjs";import { y } from "./cul_scope_0.mjs";import { x } from "./cul_scope_0.mjs";import { i } from "./cul_scope_0.mjs";import { n } from "./cul_scope_0.mjs";import { nz } from "./cul_scope_0.mjs";import { ny } from "./cul_scope_0.mjs";import { nx } from "./cul_scope_0.mjs";import { matZ2 } from "./cul_scope_0.mjs";import { matZ1 } from "./cul_scope_0.mjs";import { matZ0 } from "./cul_scope_0.mjs";import { matY2 } from "./cul_scope_0.mjs";import { matY1 } from "./cul_scope_0.mjs";import { matY0 } from "./cul_scope_0.mjs";import { matX2 } from "./cul_scope_0.mjs";import { matX1 } from "./cul_scope_0.mjs";import { matX0 } from "./cul_scope_0.mjs";import { z0 } from "./cul_scope_0.mjs";import { y0 } from "./cul_scope_0.mjs";import { x0 } from "./cul_scope_0.mjs";import { vz } from "./cul_scope_0.mjs";import { vy } from "./cul_scope_0.mjs";import { vx } from "./cul_scope_0.mjs";import { uz } from "./cul_scope_0.mjs";import { ux } from "./cul_scope_0.mjs";import { pixelSize } from "./cul_scope_0.mjs";import { camZ } from "./cul_scope_0.mjs";import { camY } from "./cul_scope_0.mjs";import { camX } from "./cul_scope_0.mjs";import { sinBeta } from "./cul_scope_0.mjs";import { cosBeta } from "./cul_scope_0.mjs";import { sinAlpha } from "./cul_scope_0.mjs";import { cosAlpha } from "./cul_scope_0.mjs";import { col } from "./cul_scope_0.mjs";import { row } from "./cul_scope_0.mjs";
import { memoize } from 'underscore';
//import memoize from 'lru-memoize';
//import { isEqual } from 'underscore'; // TODO poor tree shaking support, or why is this impact so massive? Move to lodash/lodash-es?

// import/export non-to memo?

import { sdf_A_ as sdf_A$, sdf_B_ as sdf_B$, operation_ as operation$, sdf_ as sdf$ } from "./cul_scope_3.mjs"; // there is already-culed stuff in here, why? imports to memo loader include cul_scope_id, what logic should it apply RE passing forward? eliminate? Probably!





////////// start sdf_A memo-loader code //////////
//const sdf_A$m = memoize(999999, isEqual)(sdf_A$);
export const sdf_A$m = memoize(sdf_A$, JSON.stringify);
export const sdf_A = (a) => {
  return sdf_A$m(a);
  // eslint-disable-next-line no-undef
  sdf_A$({ i_in, shape_A_in, dist_in, alpha_in, beta_in, operation_in, shape_B_in, shape_x_B_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_B_in, shape_z_B_in, shape_size_B_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in }); // never run, but here to "trick" calculang graph logic
};
////////// end sdf_A memo-loader code //////////



////////// start sdf_B memo-loader code //////////
//const sdf_B$m = memoize(999999, isEqual)(sdf_B$);
export const sdf_B$m = memoize(sdf_B$, JSON.stringify);
export const sdf_B = (a) => {
  return sdf_B$m(a);
  // eslint-disable-next-line no-undef
  sdf_B$({ i_in, shape_B_in, dist_in, alpha_in, beta_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }); // never run, but here to "trick" calculang graph logic
};
////////// end sdf_B memo-loader code //////////



////////// start operation memo-loader code //////////
//const operation$m = memoize(999999, isEqual)(operation$);
export const operation$m = memoize(operation$, JSON.stringify);
export const operation = (a) => {
  return operation$m(a);
  // eslint-disable-next-line no-undef
  operation$({ operation_in }); // never run, but here to "trick" calculang graph logic
};
////////// end operation memo-loader code //////////



////////// start sdf memo-loader code //////////
//const sdf$m = memoize(999999, isEqual)(sdf$);
export const sdf$m = memoize(sdf$, JSON.stringify);
export const sdf_ = (a) => {
  return sdf$m(a);
  // eslint-disable-next-line no-undef
  sdf$({ operation_in, i_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }); // never run, but here to "trick" calculang graph logic
};
////////// end sdf memo-loader code //////////