
import { memoize } from 'underscore';
//import memoize from 'lru-memoize';
//import { isEqual } from 'underscore'; // TODO poor tree shaking support, or why is this impact so massive? Move to lodash/lodash-es?

// import/export non-to memo?

import { row_ as row$, col_ as col$, cosAlpha_ as cosAlpha$, sinAlpha_ as sinAlpha$, cosBeta_ as cosBeta$, sinBeta_ as sinBeta$, camX_ as camX$, camY_ as camY$, camZ_ as camZ$, pixelSize_ as pixelSize$, ux_ as ux$, uz_ as uz$, vx_ as vx$, vy_ as vy$, vz_ as vz$, x0_ as x0$, y0_ as y0$, z0_ as z0$, matX0_ as matX0$, matX1_ as matX1$, matX2_ as matX2$, matY0_ as matY0$, matY1_ as matY1$, matY2_ as matY2$, matZ0_ as matZ0$, matZ1_ as matZ1$, matZ2_ as matZ2$, nx_ as nx$, ny_ as ny$, nz_ as nz$, n_ as n$, i_ as i$, x_ as x$, y_ as y$, z_ as z$, sdf_ as sdf$, result1_ as result1$, result_ as result$, brightness_ as brightness$, brightness_2_ as brightness_2$, luminosity_quantized_char_ as luminosity_quantized_char$, raymarching_iterations_ as raymarching_iterations$, screen_width_ as screen_width$, screen_height_ as screen_height$, xq_ as xq$, yq_ as yq$, alpha_ as alpha$, beta_ as beta$, fov_ as fov$, dist_ as dist$ } from "./cul_scope_1.mjs"; // there is already-culed stuff in here, why? imports to memo loader include cul_scope_id, what logic should it apply RE passing forward? eliminate? Probably!





////////// start row memo-loader code //////////
//const row$m = memoize(999999, isEqual)(row$);
export const row$m = memoize(row$, JSON.stringify);
export const row = (a) => {
  return row$m(a);
  // eslint-disable-next-line no-undef
  row$({ yq_in, screen_height_in }); // never run, but here to "trick" calculang graph logic
};
////////// end row memo-loader code //////////



////////// start col memo-loader code //////////
//const col$m = memoize(999999, isEqual)(col$);
export const col$m = memoize(col$, JSON.stringify);
export const col = (a) => {
  return col$m(a);
  // eslint-disable-next-line no-undef
  col$({ xq_in, screen_width_in }); // never run, but here to "trick" calculang graph logic
};
////////// end col memo-loader code //////////



////////// start cosAlpha memo-loader code //////////
//const cosAlpha$m = memoize(999999, isEqual)(cosAlpha$);
export const cosAlpha$m = memoize(cosAlpha$, JSON.stringify);
export const cosAlpha = (a) => {
  return cosAlpha$m(a);
  // eslint-disable-next-line no-undef
  cosAlpha$({ alpha_in }); // never run, but here to "trick" calculang graph logic
};
////////// end cosAlpha memo-loader code //////////



////////// start sinAlpha memo-loader code //////////
//const sinAlpha$m = memoize(999999, isEqual)(sinAlpha$);
export const sinAlpha$m = memoize(sinAlpha$, JSON.stringify);
export const sinAlpha = (a) => {
  return sinAlpha$m(a);
  // eslint-disable-next-line no-undef
  sinAlpha$({ alpha_in }); // never run, but here to "trick" calculang graph logic
};
////////// end sinAlpha memo-loader code //////////



////////// start cosBeta memo-loader code //////////
//const cosBeta$m = memoize(999999, isEqual)(cosBeta$);
export const cosBeta$m = memoize(cosBeta$, JSON.stringify);
export const cosBeta = (a) => {
  return cosBeta$m(a);
  // eslint-disable-next-line no-undef
  cosBeta$({ beta_in }); // never run, but here to "trick" calculang graph logic
};
////////// end cosBeta memo-loader code //////////



////////// start sinBeta memo-loader code //////////
//const sinBeta$m = memoize(999999, isEqual)(sinBeta$);
export const sinBeta$m = memoize(sinBeta$, JSON.stringify);
export const sinBeta = (a) => {
  return sinBeta$m(a);
  // eslint-disable-next-line no-undef
  sinBeta$({ beta_in }); // never run, but here to "trick" calculang graph logic
};
////////// end sinBeta memo-loader code //////////



////////// start camX memo-loader code //////////
//const camX$m = memoize(999999, isEqual)(camX$);
export const camX$m = memoize(camX$, JSON.stringify);
export const camX = (a) => {
  return camX$m(a);
  // eslint-disable-next-line no-undef
  camX$({ dist_in, alpha_in, beta_in }); // never run, but here to "trick" calculang graph logic
};
////////// end camX memo-loader code //////////



////////// start camY memo-loader code //////////
//const camY$m = memoize(999999, isEqual)(camY$);
export const camY$m = memoize(camY$, JSON.stringify);
export const camY = (a) => {
  return camY$m(a);
  // eslint-disable-next-line no-undef
  camY$({ dist_in, beta_in }); // never run, but here to "trick" calculang graph logic
};
////////// end camY memo-loader code //////////



////////// start camZ memo-loader code //////////
//const camZ$m = memoize(999999, isEqual)(camZ$);
export const camZ$m = memoize(camZ$, JSON.stringify);
export const camZ = (a) => {
  return camZ$m(a);
  // eslint-disable-next-line no-undef
  camZ$({ dist_in, alpha_in, beta_in }); // never run, but here to "trick" calculang graph logic
};
////////// end camZ memo-loader code //////////



////////// start pixelSize memo-loader code //////////
//const pixelSize$m = memoize(999999, isEqual)(pixelSize$);
export const pixelSize$m = memoize(pixelSize$, JSON.stringify);
export const pixelSize = (a) => {
  return pixelSize$m(a);
  // eslint-disable-next-line no-undef
  pixelSize$({ fov_in, screen_width_in }); // never run, but here to "trick" calculang graph logic
};
////////// end pixelSize memo-loader code //////////



////////// start ux memo-loader code //////////
//const ux$m = memoize(999999, isEqual)(ux$);
export const ux$m = memoize(ux$, JSON.stringify);
export const ux = (a) => {
  return ux$m(a);
  // eslint-disable-next-line no-undef
  ux$({ fov_in, screen_width_in, alpha_in }); // never run, but here to "trick" calculang graph logic
};
////////// end ux memo-loader code //////////



////////// start uz memo-loader code //////////
//const uz$m = memoize(999999, isEqual)(uz$);
export const uz$m = memoize(uz$, JSON.stringify);
export const uz = (a) => {
  return uz$m(a);
  // eslint-disable-next-line no-undef
  uz$({ fov_in, screen_width_in, alpha_in }); // never run, but here to "trick" calculang graph logic
};
////////// end uz memo-loader code //////////



////////// start vx memo-loader code //////////
//const vx$m = memoize(999999, isEqual)(vx$);
export const vx$m = memoize(vx$, JSON.stringify);
export const vx = (a) => {
  return vx$m(a);
  // eslint-disable-next-line no-undef
  vx$({ fov_in, screen_width_in, alpha_in, beta_in }); // never run, but here to "trick" calculang graph logic
};
////////// end vx memo-loader code //////////



////////// start vy memo-loader code //////////
//const vy$m = memoize(999999, isEqual)(vy$);
export const vy$m = memoize(vy$, JSON.stringify);
export const vy = (a) => {
  return vy$m(a);
  // eslint-disable-next-line no-undef
  vy$({ fov_in, screen_width_in, beta_in }); // never run, but here to "trick" calculang graph logic
};
////////// end vy memo-loader code //////////



////////// start vz memo-loader code //////////
//const vz$m = memoize(999999, isEqual)(vz$);
export const vz$m = memoize(vz$, JSON.stringify);
export const vz = (a) => {
  return vz$m(a);
  // eslint-disable-next-line no-undef
  vz$({ fov_in, screen_width_in, alpha_in, beta_in }); // never run, but here to "trick" calculang graph logic
};
////////// end vz memo-loader code //////////



////////// start x0 memo-loader code //////////
//const x0$m = memoize(999999, isEqual)(x0$);
export const x0$m = memoize(x0$, JSON.stringify);
export const x0 = (a) => {
  return x0$m(a);
  // eslint-disable-next-line no-undef
  x0$({ alpha_in, beta_in }); // never run, but here to "trick" calculang graph logic
};
////////// end x0 memo-loader code //////////



////////// start y0 memo-loader code //////////
//const y0$m = memoize(999999, isEqual)(y0$);
export const y0$m = memoize(y0$, JSON.stringify);
export const y0 = (a) => {
  return y0$m(a);
  // eslint-disable-next-line no-undef
  y0$({ beta_in }); // never run, but here to "trick" calculang graph logic
};
////////// end y0 memo-loader code //////////



////////// start z0 memo-loader code //////////
//const z0$m = memoize(999999, isEqual)(z0$);
export const z0$m = memoize(z0$, JSON.stringify);
export const z0 = (a) => {
  return z0$m(a);
  // eslint-disable-next-line no-undef
  z0$({ alpha_in, beta_in }); // never run, but here to "trick" calculang graph logic
};
////////// end z0 memo-loader code //////////



////////// start matX0 memo-loader code //////////
//const matX0$m = memoize(999999, isEqual)(matX0$);
export const matX0$m = memoize(matX0$, JSON.stringify);
export const matX0 = (a) => {
  return matX0$m(a);
  // eslint-disable-next-line no-undef
  matX0$({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in }); // never run, but here to "trick" calculang graph logic
};
////////// end matX0 memo-loader code //////////



////////// start matX1 memo-loader code //////////
//const matX1$m = memoize(999999, isEqual)(matX1$);
export const matX1$m = memoize(matX1$, JSON.stringify);
export const matX1 = (a) => {
  return matX1$m(a);
  // eslint-disable-next-line no-undef
  matX1$({ fov_in, screen_width_in, alpha_in }); // never run, but here to "trick" calculang graph logic
};
////////// end matX1 memo-loader code //////////



////////// start matX2 memo-loader code //////////
//const matX2$m = memoize(999999, isEqual)(matX2$);
export const matX2$m = memoize(matX2$, JSON.stringify);
export const matX2 = (a) => {
  return matX2$m(a);
  // eslint-disable-next-line no-undef
  matX2$({ fov_in, screen_width_in, alpha_in, beta_in, screen_height_in }); // never run, but here to "trick" calculang graph logic
};
////////// end matX2 memo-loader code //////////



////////// start matY0 memo-loader code //////////
//const matY0$m = memoize(999999, isEqual)(matY0$);
export const matY0$m = memoize(matY0$, JSON.stringify);
export const matY0 = (a) => {
  return matY0$m(a);
  // eslint-disable-next-line no-undef
  matY0$({ beta_in, screen_width_in, fov_in }); // never run, but here to "trick" calculang graph logic
};
////////// end matY0 memo-loader code //////////



////////// start matY1 memo-loader code //////////
//const matY1$m = memoize(999999, isEqual)(matY1$);
export const matY1$m = memoize(matY1$, JSON.stringify);
export const matY1 = (a) => {
  return matY1$m(a);
  // eslint-disable-next-line no-undef
  matY1$({}); // never run, but here to "trick" calculang graph logic
};
////////// end matY1 memo-loader code //////////



////////// start matY2 memo-loader code //////////
//const matY2$m = memoize(999999, isEqual)(matY2$);
export const matY2$m = memoize(matY2$, JSON.stringify);
export const matY2 = (a) => {
  return matY2$m(a);
  // eslint-disable-next-line no-undef
  matY2$({ fov_in, screen_width_in, beta_in, screen_height_in }); // never run, but here to "trick" calculang graph logic
};
////////// end matY2 memo-loader code //////////



////////// start matZ0 memo-loader code //////////
//const matZ0$m = memoize(999999, isEqual)(matZ0$);
export const matZ0$m = memoize(matZ0$, JSON.stringify);
export const matZ0 = (a) => {
  return matZ0$m(a);
  // eslint-disable-next-line no-undef
  matZ0$({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in }); // never run, but here to "trick" calculang graph logic
};
////////// end matZ0 memo-loader code //////////



////////// start matZ1 memo-loader code //////////
//const matZ1$m = memoize(999999, isEqual)(matZ1$);
export const matZ1$m = memoize(matZ1$, JSON.stringify);
export const matZ1 = (a) => {
  return matZ1$m(a);
  // eslint-disable-next-line no-undef
  matZ1$({ fov_in, screen_width_in, alpha_in }); // never run, but here to "trick" calculang graph logic
};
////////// end matZ1 memo-loader code //////////



////////// start matZ2 memo-loader code //////////
//const matZ2$m = memoize(999999, isEqual)(matZ2$);
export const matZ2$m = memoize(matZ2$, JSON.stringify);
export const matZ2 = (a) => {
  return matZ2$m(a);
  // eslint-disable-next-line no-undef
  matZ2$({ fov_in, screen_width_in, alpha_in, beta_in, screen_height_in }); // never run, but here to "trick" calculang graph logic
};
////////// end matZ2 memo-loader code //////////



////////// start nx memo-loader code //////////
//const nx$m = memoize(999999, isEqual)(nx$);
export const nx$m = memoize(nx$, JSON.stringify);
export const nx = (a) => {
  return nx$m(a);
  // eslint-disable-next-line no-undef
  nx$({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in }); // never run, but here to "trick" calculang graph logic
};
////////// end nx memo-loader code //////////



////////// start ny memo-loader code //////////
//const ny$m = memoize(999999, isEqual)(ny$);
export const ny$m = memoize(ny$, JSON.stringify);
export const ny = (a) => {
  return ny$m(a);
  // eslint-disable-next-line no-undef
  ny$({ beta_in, screen_width_in, fov_in, yq_in, screen_height_in }); // never run, but here to "trick" calculang graph logic
};
////////// end ny memo-loader code //////////



////////// start nz memo-loader code //////////
//const nz$m = memoize(999999, isEqual)(nz$);
export const nz$m = memoize(nz$, JSON.stringify);
export const nz = (a) => {
  return nz$m(a);
  // eslint-disable-next-line no-undef
  nz$({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in }); // never run, but here to "trick" calculang graph logic
};
////////// end nz memo-loader code //////////



////////// start n memo-loader code //////////
//const n$m = memoize(999999, isEqual)(n$);
export const n$m = memoize(n$, JSON.stringify);
export const n = (a) => {
  return n$m(a);
  // eslint-disable-next-line no-undef
  n$({ alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in }); // never run, but here to "trick" calculang graph logic
};
////////// end n memo-loader code //////////



////////// start i memo-loader code //////////
//const i$m = memoize(999999, isEqual)(i$);
export const i$m = memoize(i$, JSON.stringify);
export const i = (a) => {
  return i$m(a);
  // eslint-disable-next-line no-undef
  i$({ i_in }); // never run, but here to "trick" calculang graph logic
};
////////// end i memo-loader code //////////



////////// start x memo-loader code //////////
//const x$m = memoize(999999, isEqual)(x$);
export const x$m = memoize(x$, JSON.stringify);
export const x = (a) => {
  return x$m(a);
  // eslint-disable-next-line no-undef
  x$({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, shape_x_A_in, screen_width_in, fov_in, yq_in, screen_height_in, xq_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }); // never run, but here to "trick" calculang graph logic
};
////////// end x memo-loader code //////////



////////// start y memo-loader code //////////
//const y$m = memoize(999999, isEqual)(y$);
export const y$m = memoize(y$, JSON.stringify);
export const y = (a) => {
  return y$m(a);
  // eslint-disable-next-line no-undef
  y$({ dist_in, beta_in, i_in, operation_in, shape_A_in, alpha_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }); // never run, but here to "trick" calculang graph logic
};
////////// end y memo-loader code //////////



////////// start z memo-loader code //////////
//const z$m = memoize(999999, isEqual)(z$);
export const z$m = memoize(z$, JSON.stringify);
export const z = (a) => {
  return z$m(a);
  // eslint-disable-next-line no-undef
  z$({ dist_in, alpha_in, beta_in, i_in, operation_in, shape_A_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }); // never run, but here to "trick" calculang graph logic
};
////////// end z memo-loader code //////////



////////// start sdf memo-loader code //////////
//const sdf$m = memoize(999999, isEqual)(sdf$);
export const sdf$m = memoize(sdf$, JSON.stringify);
export const sdf = (a) => {
  return sdf$m(a);
  // eslint-disable-next-line no-undef
  sdf$({ operation_in, i_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }); // never run, but here to "trick" calculang graph logic
};
////////// end sdf memo-loader code //////////



////////// start result1 memo-loader code //////////
//const result1$m = memoize(999999, isEqual)(result1$);
export const result1$m = memoize(result1$, JSON.stringify);
export const result1 = (a) => {
  return result1$m(a);
  // eslint-disable-next-line no-undef
  result1$({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }); // never run, but here to "trick" calculang graph logic
};
////////// end result1 memo-loader code //////////



////////// start result memo-loader code //////////
//const result$m = memoize(999999, isEqual)(result$);
export const result$m = memoize(result$, JSON.stringify);
export const result = (a) => {
  return result$m(a);
  // eslint-disable-next-line no-undef
  result$({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }); // never run, but here to "trick" calculang graph logic
};
////////// end result memo-loader code //////////



////////// start brightness memo-loader code //////////
//const brightness$m = memoize(999999, isEqual)(brightness$);
export const brightness$m = memoize(brightness$, JSON.stringify);
export const brightness = (a) => {
  return brightness$m(a);
  // eslint-disable-next-line no-undef
  brightness$({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }); // never run, but here to "trick" calculang graph logic
};
////////// end brightness memo-loader code //////////



////////// start brightness_2 memo-loader code //////////
//const brightness_2$m = memoize(999999, isEqual)(brightness_2$);
export const brightness_2$m = memoize(brightness_2$, JSON.stringify);
export const brightness_2 = (a) => {
  return brightness_2$m(a);
  // eslint-disable-next-line no-undef
  brightness_2$({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }); // never run, but here to "trick" calculang graph logic
};
////////// end brightness_2 memo-loader code //////////



////////// start luminosity_quantized_char memo-loader code //////////
//const luminosity_quantized_char$m = memoize(999999, isEqual)(luminosity_quantized_char$);
export const luminosity_quantized_char$m = memoize(luminosity_quantized_char$, JSON.stringify);
export const luminosity_quantized_char = (a) => {
  return luminosity_quantized_char$m(a);
  // eslint-disable-next-line no-undef
  luminosity_quantized_char$({ i_in, operation_in, shape_A_in, dist_in, alpha_in, beta_in, screen_height_in, fov_in, screen_width_in, xq_in, yq_in, shape_x_A_in, shape_y_A_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }); // never run, but here to "trick" calculang graph logic
};
////////// end luminosity_quantized_char memo-loader code //////////



////////// start raymarching_iterations memo-loader code //////////
//const raymarching_iterations$m = memoize(999999, isEqual)(raymarching_iterations$);
export const raymarching_iterations$m = memoize(raymarching_iterations$, JSON.stringify);
export const raymarching_iterations = (a) => {
  return raymarching_iterations$m(a);
  // eslint-disable-next-line no-undef
  raymarching_iterations$({ raymarching_iterations_in }); // never run, but here to "trick" calculang graph logic
};
////////// end raymarching_iterations memo-loader code //////////



////////// start screen_width memo-loader code //////////
//const screen_width$m = memoize(999999, isEqual)(screen_width$);
export const screen_width$m = memoize(screen_width$, JSON.stringify);
export const screen_width = (a) => {
  return screen_width$m(a);
  // eslint-disable-next-line no-undef
  screen_width$({ screen_width_in }); // never run, but here to "trick" calculang graph logic
};
////////// end screen_width memo-loader code //////////



////////// start screen_height memo-loader code //////////
//const screen_height$m = memoize(999999, isEqual)(screen_height$);
export const screen_height$m = memoize(screen_height$, JSON.stringify);
export const screen_height = (a) => {
  return screen_height$m(a);
  // eslint-disable-next-line no-undef
  screen_height$({ screen_height_in }); // never run, but here to "trick" calculang graph logic
};
////////// end screen_height memo-loader code //////////



////////// start xq memo-loader code //////////
//const xq$m = memoize(999999, isEqual)(xq$);
export const xq$m = memoize(xq$, JSON.stringify);
export const xq = (a) => {
  return xq$m(a);
  // eslint-disable-next-line no-undef
  xq$({ xq_in }); // never run, but here to "trick" calculang graph logic
};
////////// end xq memo-loader code //////////



////////// start yq memo-loader code //////////
//const yq$m = memoize(999999, isEqual)(yq$);
export const yq$m = memoize(yq$, JSON.stringify);
export const yq = (a) => {
  return yq$m(a);
  // eslint-disable-next-line no-undef
  yq$({ yq_in }); // never run, but here to "trick" calculang graph logic
};
////////// end yq memo-loader code //////////



////////// start alpha memo-loader code //////////
//const alpha$m = memoize(999999, isEqual)(alpha$);
export const alpha$m = memoize(alpha$, JSON.stringify);
export const alpha = (a) => {
  return alpha$m(a);
  // eslint-disable-next-line no-undef
  alpha$({ alpha_in }); // never run, but here to "trick" calculang graph logic
};
////////// end alpha memo-loader code //////////



////////// start beta memo-loader code //////////
//const beta$m = memoize(999999, isEqual)(beta$);
export const beta$m = memoize(beta$, JSON.stringify);
export const beta = (a) => {
  return beta$m(a);
  // eslint-disable-next-line no-undef
  beta$({ beta_in }); // never run, but here to "trick" calculang graph logic
};
////////// end beta memo-loader code //////////



////////// start fov memo-loader code //////////
//const fov$m = memoize(999999, isEqual)(fov$);
export const fov$m = memoize(fov$, JSON.stringify);
export const fov = (a) => {
  return fov$m(a);
  // eslint-disable-next-line no-undef
  fov$({ fov_in }); // never run, but here to "trick" calculang graph logic
};
////////// end fov memo-loader code //////////



////////// start dist memo-loader code //////////
//const dist$m = memoize(999999, isEqual)(dist$);
export const dist$m = memoize(dist$, JSON.stringify);
export const dist = (a) => {
  return dist$m(a);
  // eslint-disable-next-line no-undef
  dist$({ dist_in }); // never run, but here to "trick" calculang graph logic
};
////////// end dist memo-loader code //////////