import { sdf_ as sdf_xyz /* workaround for memo bug */  } from "./sdf_base.cul.js";
export { sdf_xyz };
  
export const sdf = () => sdf_xyz();

// constrain sdf params to be _B inputs

export const shape = () => shape_B();
export const shape_x = () => shape_x_B();
export const shape_y = () => shape_y_B();
export const shape_z = () => shape_z_B();
export const shape_size = () => shape_size_B();

export const shape_B = () => shape_B_in;
export const shape_x_B = () => shape_x_B_in;
export const shape_y_B = () => shape_y_B_in;
export const shape_z_B = () => shape_z_B_in;
export const shape_size_B = () => shape_size_B_in;
