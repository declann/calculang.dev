
import { memoize } from 'underscore';
//import memoize from 'lru-memoize';
//import { isEqual } from 'underscore'; // TODO poor tree shaking support, or why is this impact so massive? Move to lodash/lodash-es?

// import/export non-to memo?

import { sdf_A_ as sdf_A$, sdf_B_ as sdf_B$, operation_ as operation$, sdf_ as sdf$ } from "./cul_scope_1.mjs"; // there is already-culed stuff in here, why? imports to memo loader include cul_scope_id, what logic should it apply RE passing forward? eliminate? Probably!





////////// start sdf_A memo-loader code //////////
//const sdf_A$m = memoize(999999, isEqual)(sdf_A$);
export const sdf_A$m = memoize(sdf_A$, JSON.stringify);
export const sdf_A = (a) => {
  return sdf_A$m(a);
  // eslint-disable-next-line no-undef
  sdf_A$({ i_in, shape_A_in, x_in, shape_x_A_in, y_in, shape_y_A_in, z_in, shape_z_A_in, shape_size_A_in }); // never run, but here to "trick" calculang graph logic
};
////////// end sdf_A memo-loader code //////////



////////// start sdf_B memo-loader code //////////
//const sdf_B$m = memoize(999999, isEqual)(sdf_B$);
export const sdf_B$m = memoize(sdf_B$, JSON.stringify);
export const sdf_B = (a) => {
  return sdf_B$m(a);
  // eslint-disable-next-line no-undef
  sdf_B$({ i_in, shape_B_in, x_in, shape_x_B_in, y_in, shape_y_B_in, z_in, shape_z_B_in, shape_size_B_in }); // never run, but here to "trick" calculang graph logic
};
////////// end sdf_B memo-loader code //////////



////////// start operation memo-loader code //////////
//const operation$m = memoize(999999, isEqual)(operation$);
export const operation$m = memoize(operation$, JSON.stringify);
export const operation = (a) => {
  return operation$m(a);
  // eslint-disable-next-line no-undef
  operation$({ operation_in }); // never run, but here to "trick" calculang graph logic
};
////////// end operation memo-loader code //////////



////////// start sdf memo-loader code //////////
//const sdf$m = memoize(999999, isEqual)(sdf$);
export const sdf$m = memoize(sdf$, JSON.stringify);
export const sdf = (a) => {
  return sdf$m(a);
  // eslint-disable-next-line no-undef
  sdf$({ operation_in, i_in, shape_A_in, x_in, shape_x_A_in, y_in, shape_y_A_in, z_in, shape_z_A_in, shape_size_A_in, shape_B_in, shape_x_B_in, shape_y_B_in, shape_z_B_in, shape_size_B_in }); // never run, but here to "trick" calculang graph logic
};
////////// end sdf memo-loader code //////////