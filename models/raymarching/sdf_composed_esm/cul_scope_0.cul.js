
    import { memoize } from 'underscore';
    //import memoize from 'lru-memoize';
    //import { isEqual } from 'underscore'; // TODO poor tree shaking support, or why is this impact so massive? Move to lodash/lodash-es?
    
    // import/export non-to memo?

    import { sdf_A_ as sdf_A$, sdf_B_ as sdf_B$, operation_ as operation$, sdf_ as sdf$ } from './sdf_composed.cul.js?+memoed'; // there is already-culed stuff in here, why? imports to memo loader include cul_scope_id, what logic should it apply RE passing forward? eliminate? Probably!

    
    
    

////////// start sdf_A memo-loader code //////////
//const sdf_A$m = memoize(999999, isEqual)(sdf_A$);
export const sdf_A$m = memoize(sdf_A$, JSON.stringify);
export const sdf_A = (a) => {
  return sdf_A$m(a);
  // eslint-disable-next-line no-undef
  sdf_A$(); // never run, but here to "trick" calculang graph logic
};
////////// end sdf_A memo-loader code //////////



////////// start sdf_B memo-loader code //////////
//const sdf_B$m = memoize(999999, isEqual)(sdf_B$);
export const sdf_B$m = memoize(sdf_B$, JSON.stringify);
export const sdf_B = (a) => {
  return sdf_B$m(a);
  // eslint-disable-next-line no-undef
  sdf_B$(); // never run, but here to "trick" calculang graph logic
};
////////// end sdf_B memo-loader code //////////



////////// start operation memo-loader code //////////
//const operation$m = memoize(999999, isEqual)(operation$);
export const operation$m = memoize(operation$, JSON.stringify);
export const operation = (a) => {
  return operation$m(a);
  // eslint-disable-next-line no-undef
  operation$(); // never run, but here to "trick" calculang graph logic
};
////////// end operation memo-loader code //////////



////////// start sdf memo-loader code //////////
//const sdf$m = memoize(999999, isEqual)(sdf$);
export const sdf$m = memoize(sdf$, JSON.stringify);
export const sdf = (a) => {
  return sdf$m(a);
  // eslint-disable-next-line no-undef
  sdf$(); // never run, but here to "trick" calculang graph logic
};
////////// end sdf memo-loader code //////////


    