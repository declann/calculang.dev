import { sdf } from "./cul_scope_0.mjs";import { operation } from "./cul_scope_0.mjs";import { sdf_B } from "./cul_scope_0.mjs";import { sdf_A } from "./cul_scope_0.mjs";
import { memoize } from 'underscore';
//import memoize from 'lru-memoize';
//import { isEqual } from 'underscore'; // TODO poor tree shaking support, or why is this impact so massive? Move to lodash/lodash-es?

// import/export non-to memo?

import { sdf_xyz_ as sdf_xyz$, sdf_ as sdf$, shape_ as shape$, shape_x_ as shape_x$, shape_y_ as shape_y$, shape_z_ as shape_z$, shape_size_ as shape_size$, shape_A_ as shape_A$, shape_x_A_ as shape_x_A$, shape_y_A_ as shape_y_A$, shape_z_A_ as shape_z_A$, shape_size_A_ as shape_size_A$ } from "./cul_scope_4.mjs"; // there is already-culed stuff in here, why? imports to memo loader include cul_scope_id, what logic should it apply RE passing forward? eliminate? Probably!





////////// start sdf_xyz memo-loader code //////////
//const sdf_xyz$m = memoize(999999, isEqual)(sdf_xyz$);
export const sdf_xyz$m = memoize(sdf_xyz$, JSON.stringify);
export const sdf_xyz = (a) => {
  return sdf_xyz$m(a);
  // eslint-disable-next-line no-undef
  sdf_xyz$({ i_in, shape_A_in, x_in, shape_x_A_in, y_in, shape_y_A_in, z_in, shape_z_A_in, shape_size_A_in }); // never run, but here to "trick" calculang graph logic
};
////////// end sdf_xyz memo-loader code //////////



////////// start sdf memo-loader code //////////
//const sdf$m = memoize(999999, isEqual)(sdf$);
export const sdf$m = memoize(sdf$, JSON.stringify);
export const sdf_ = (a) => {
  return sdf$m(a);
  // eslint-disable-next-line no-undef
  sdf$({ i_in, shape_A_in, x_in, shape_x_A_in, y_in, shape_y_A_in, z_in, shape_z_A_in, shape_size_A_in }); // never run, but here to "trick" calculang graph logic
};
////////// end sdf memo-loader code //////////



////////// start shape memo-loader code //////////
//const shape$m = memoize(999999, isEqual)(shape$);
export const shape$m = memoize(shape$, JSON.stringify);
export const shape = (a) => {
  return shape$m(a);
  // eslint-disable-next-line no-undef
  shape$({ shape_A_in }); // never run, but here to "trick" calculang graph logic
};
////////// end shape memo-loader code //////////



////////// start shape_x memo-loader code //////////
//const shape_x$m = memoize(999999, isEqual)(shape_x$);
export const shape_x$m = memoize(shape_x$, JSON.stringify);
export const shape_x = (a) => {
  return shape_x$m(a);
  // eslint-disable-next-line no-undef
  shape_x$({ shape_x_A_in }); // never run, but here to "trick" calculang graph logic
};
////////// end shape_x memo-loader code //////////



////////// start shape_y memo-loader code //////////
//const shape_y$m = memoize(999999, isEqual)(shape_y$);
export const shape_y$m = memoize(shape_y$, JSON.stringify);
export const shape_y = (a) => {
  return shape_y$m(a);
  // eslint-disable-next-line no-undef
  shape_y$({ shape_y_A_in }); // never run, but here to "trick" calculang graph logic
};
////////// end shape_y memo-loader code //////////



////////// start shape_z memo-loader code //////////
//const shape_z$m = memoize(999999, isEqual)(shape_z$);
export const shape_z$m = memoize(shape_z$, JSON.stringify);
export const shape_z = (a) => {
  return shape_z$m(a);
  // eslint-disable-next-line no-undef
  shape_z$({ shape_z_A_in }); // never run, but here to "trick" calculang graph logic
};
////////// end shape_z memo-loader code //////////



////////// start shape_size memo-loader code //////////
//const shape_size$m = memoize(999999, isEqual)(shape_size$);
export const shape_size$m = memoize(shape_size$, JSON.stringify);
export const shape_size = (a) => {
  return shape_size$m(a);
  // eslint-disable-next-line no-undef
  shape_size$({ shape_size_A_in }); // never run, but here to "trick" calculang graph logic
};
////////// end shape_size memo-loader code //////////



////////// start shape_A memo-loader code //////////
//const shape_A$m = memoize(999999, isEqual)(shape_A$);
export const shape_A$m = memoize(shape_A$, JSON.stringify);
export const shape_A = (a) => {
  return shape_A$m(a);
  // eslint-disable-next-line no-undef
  shape_A$({ shape_A_in }); // never run, but here to "trick" calculang graph logic
};
////////// end shape_A memo-loader code //////////



////////// start shape_x_A memo-loader code //////////
//const shape_x_A$m = memoize(999999, isEqual)(shape_x_A$);
export const shape_x_A$m = memoize(shape_x_A$, JSON.stringify);
export const shape_x_A = (a) => {
  return shape_x_A$m(a);
  // eslint-disable-next-line no-undef
  shape_x_A$({ shape_x_A_in }); // never run, but here to "trick" calculang graph logic
};
////////// end shape_x_A memo-loader code //////////



////////// start shape_y_A memo-loader code //////////
//const shape_y_A$m = memoize(999999, isEqual)(shape_y_A$);
export const shape_y_A$m = memoize(shape_y_A$, JSON.stringify);
export const shape_y_A = (a) => {
  return shape_y_A$m(a);
  // eslint-disable-next-line no-undef
  shape_y_A$({ shape_y_A_in }); // never run, but here to "trick" calculang graph logic
};
////////// end shape_y_A memo-loader code //////////



////////// start shape_z_A memo-loader code //////////
//const shape_z_A$m = memoize(999999, isEqual)(shape_z_A$);
export const shape_z_A$m = memoize(shape_z_A$, JSON.stringify);
export const shape_z_A = (a) => {
  return shape_z_A$m(a);
  // eslint-disable-next-line no-undef
  shape_z_A$({ shape_z_A_in }); // never run, but here to "trick" calculang graph logic
};
////////// end shape_z_A memo-loader code //////////



////////// start shape_size_A memo-loader code //////////
//const shape_size_A$m = memoize(999999, isEqual)(shape_size_A$);
export const shape_size_A$m = memoize(shape_size_A$, JSON.stringify);
export const shape_size_A = (a) => {
  return shape_size_A$m(a);
  // eslint-disable-next-line no-undef
  shape_size_A$({ shape_size_A_in }); // never run, but here to "trick" calculang graph logic
};
////////// end shape_size_A memo-loader code //////////