import { shape_size_B } from "./cul_scope_3.mjs";import { shape_z_B } from "./cul_scope_3.mjs";import { shape_y_B } from "./cul_scope_3.mjs";import { shape_x_B } from "./cul_scope_3.mjs";import { shape_B } from "./cul_scope_3.mjs";import { shape_size } from "./cul_scope_3.mjs";import { shape_z } from "./cul_scope_3.mjs";import { shape_y } from "./cul_scope_3.mjs";import { shape_x } from "./cul_scope_3.mjs";import { shape } from "./cul_scope_3.mjs";import { sdf_xyz } from "./cul_scope_3.mjs";import { sdf } from "./cul_scope_0.mjs";import { operation } from "./cul_scope_0.mjs";import { sdf_B } from "./cul_scope_0.mjs";import { sdf_A } from "./cul_scope_0.mjs";import { sdf_ as sdf_xyz_ /* workaround for memo bug */ } from "./cul_scope_8.mjs";
export { sdf_xyz_ };

export const sdf_ = ({ i_in, shape_B_in, x_in, shape_x_B_in, y_in, shape_y_B_in, z_in, shape_z_B_in, shape_size_B_in }) => sdf_xyz({ i_in, shape_B_in, x_in, shape_x_B_in, y_in, shape_y_B_in, z_in, shape_z_B_in, shape_size_B_in });

// constrain sdf params to be _B inputs

export const shape_ = ({ shape_B_in }) => shape_B({ shape_B_in });
export const shape_x_ = ({ shape_x_B_in }) => shape_x_B({ shape_x_B_in });
export const shape_y_ = ({ shape_y_B_in }) => shape_y_B({ shape_y_B_in });
export const shape_z_ = ({ shape_z_B_in }) => shape_z_B({ shape_z_B_in });
export const shape_size_ = ({ shape_size_B_in }) => shape_size_B({ shape_size_B_in });

export const shape_B_ = ({ shape_B_in }) => shape_B_in;
export const shape_x_B_ = ({ shape_x_B_in }) => shape_x_B_in;
export const shape_y_B_ = ({ shape_y_B_in }) => shape_y_B_in;
export const shape_z_B_ = ({ shape_z_B_in }) => shape_z_B_in;
export const shape_size_B_ = ({ shape_size_B_in }) => shape_size_B_in;