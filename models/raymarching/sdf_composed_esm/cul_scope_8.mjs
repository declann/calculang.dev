import { shape_size_B } from "./cul_scope_3.mjs";import { shape_z_B } from "./cul_scope_3.mjs";import { shape_y_B } from "./cul_scope_3.mjs";import { shape_x_B } from "./cul_scope_3.mjs";import { shape_B } from "./cul_scope_3.mjs";import { shape_size } from "./cul_scope_3.mjs";import { shape_z } from "./cul_scope_3.mjs";import { shape_y } from "./cul_scope_3.mjs";import { shape_x } from "./cul_scope_3.mjs";import { shape } from "./cul_scope_3.mjs";import { sdf_xyz } from "./cul_scope_3.mjs";import { sdf } from "./cul_scope_0.mjs";import { operation } from "./cul_scope_0.mjs";import { sdf_B } from "./cul_scope_0.mjs";import { sdf_A } from "./cul_scope_0.mjs";
import { memoize } from 'underscore';
//import memoize from 'lru-memoize';
//import { isEqual } from 'underscore'; // TODO poor tree shaking support, or why is this impact so massive? Move to lodash/lodash-es?

// import/export non-to memo?

import { x_ as x$, y_ as y$, z_ as z$, sphere_ as sphere$, cube_ as cube$, shape_ as shape$, shape_x_ as shape_x$, shape_y_ as shape_y$, shape_z_ as shape_z$, shape_size_ as shape_size$, i_ as i$, sdf_ as sdf$ } from "./cul_scope_9.mjs"; // there is already-culed stuff in here, why? imports to memo loader include cul_scope_id, what logic should it apply RE passing forward? eliminate? Probably!





////////// start x memo-loader code //////////
//const x$m = memoize(999999, isEqual)(x$);
export const x$m = memoize(x$, JSON.stringify);
export const x = (a) => {
  return x$m(a);
  // eslint-disable-next-line no-undef
  x$({ x_in }); // never run, but here to "trick" calculang graph logic
};
////////// end x memo-loader code //////////



////////// start y memo-loader code //////////
//const y$m = memoize(999999, isEqual)(y$);
export const y$m = memoize(y$, JSON.stringify);
export const y = (a) => {
  return y$m(a);
  // eslint-disable-next-line no-undef
  y$({ y_in }); // never run, but here to "trick" calculang graph logic
};
////////// end y memo-loader code //////////



////////// start z memo-loader code //////////
//const z$m = memoize(999999, isEqual)(z$);
export const z$m = memoize(z$, JSON.stringify);
export const z = (a) => {
  return z$m(a);
  // eslint-disable-next-line no-undef
  z$({ z_in }); // never run, but here to "trick" calculang graph logic
};
////////// end z memo-loader code //////////



////////// start sphere memo-loader code //////////
//const sphere$m = memoize(999999, isEqual)(sphere$);
export const sphere$m = memoize(sphere$, JSON.stringify);
export const sphere = (a) => {
  return sphere$m(a);
  // eslint-disable-next-line no-undef
  sphere$({ x_in, shape_x_B_in, y_in, shape_y_B_in, z_in, shape_z_B_in, shape_size_B_in }); // never run, but here to "trick" calculang graph logic
};
////////// end sphere memo-loader code //////////



////////// start cube memo-loader code //////////
//const cube$m = memoize(999999, isEqual)(cube$);
export const cube$m = memoize(cube$, JSON.stringify);
export const cube = (a) => {
  return cube$m(a);
  // eslint-disable-next-line no-undef
  cube$({ x_in, shape_x_B_in, y_in, shape_y_B_in, z_in, shape_z_B_in, shape_size_B_in }); // never run, but here to "trick" calculang graph logic
};
////////// end cube memo-loader code //////////



////////// start shape memo-loader code //////////
//const shape$m = memoize(999999, isEqual)(shape$);
export const shape$m = memoize(shape$, JSON.stringify);
export const shape_ = (a) => {
  return shape$m(a);
  // eslint-disable-next-line no-undef
  shape$({ shape_in }); // never run, but here to "trick" calculang graph logic
};
////////// end shape memo-loader code //////////



////////// start shape_x memo-loader code //////////
//const shape_x$m = memoize(999999, isEqual)(shape_x$);
export const shape_x$m = memoize(shape_x$, JSON.stringify);
export const shape_x_ = (a) => {
  return shape_x$m(a);
  // eslint-disable-next-line no-undef
  shape_x$({ shape_x_in }); // never run, but here to "trick" calculang graph logic
};
////////// end shape_x memo-loader code //////////



////////// start shape_y memo-loader code //////////
//const shape_y$m = memoize(999999, isEqual)(shape_y$);
export const shape_y$m = memoize(shape_y$, JSON.stringify);
export const shape_y_ = (a) => {
  return shape_y$m(a);
  // eslint-disable-next-line no-undef
  shape_y$({ shape_y_in }); // never run, but here to "trick" calculang graph logic
};
////////// end shape_y memo-loader code //////////



////////// start shape_z memo-loader code //////////
//const shape_z$m = memoize(999999, isEqual)(shape_z$);
export const shape_z$m = memoize(shape_z$, JSON.stringify);
export const shape_z_ = (a) => {
  return shape_z$m(a);
  // eslint-disable-next-line no-undef
  shape_z$({ shape_z_in }); // never run, but here to "trick" calculang graph logic
};
////////// end shape_z memo-loader code //////////



////////// start shape_size memo-loader code //////////
//const shape_size$m = memoize(999999, isEqual)(shape_size$);
export const shape_size$m = memoize(shape_size$, JSON.stringify);
export const shape_size_ = (a) => {
  return shape_size$m(a);
  // eslint-disable-next-line no-undef
  shape_size$({ shape_size_in }); // never run, but here to "trick" calculang graph logic
};
////////// end shape_size memo-loader code //////////



////////// start i memo-loader code //////////
//const i$m = memoize(999999, isEqual)(i$);
export const i$m = memoize(i$, JSON.stringify);
export const i = (a) => {
  return i$m(a);
  // eslint-disable-next-line no-undef
  i$({ i_in }); // never run, but here to "trick" calculang graph logic
};
////////// end i memo-loader code //////////



////////// start sdf memo-loader code //////////
//const sdf$m = memoize(999999, isEqual)(sdf$);
export const sdf$m = memoize(sdf$, JSON.stringify);
export const sdf_ = (a) => {
  return sdf$m(a);
  // eslint-disable-next-line no-undef
  sdf$({ i_in, shape_B_in, x_in, shape_x_B_in, y_in, shape_y_B_in, z_in, shape_z_B_in, shape_size_B_in }); // never run, but here to "trick" calculang graph logic
};
////////// end sdf memo-loader code //////////