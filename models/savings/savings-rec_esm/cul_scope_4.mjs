import { interest_rate } from "./cul_scope_0.mjs";import { actual_interest_rate_co } from "./cul_scope_0.mjs";import { actual_interest_rates } from "./cul_scope_0.mjs";import { year } from "./cul_scope_0.mjs";import { annual_payment } from "./cul_scope_0.mjs";import { interest_rate_expected } from "./cul_scope_0.mjs";import { duration } from "./cul_scope_0.mjs";import { interest } from "./cul_scope_0.mjs";import { deposits } from "./cul_scope_0.mjs";import { balance } from "./cul_scope_0.mjs";
import { memoize } from 'underscore';
//import memoize from 'lru-memoize';
//import { isEqual } from 'underscore'; // TODO poor tree shaking support, or why is this impact so massive? Move to lodash/lodash-es?

// import/export non-to memo?

import { balance_ as balance$, deposits_ as deposits$, interest_ as interest$, year_ as year$, annual_payment_ as annual_payment$, duration_ as duration$, interest_rate_ as interest_rate$ } from "./cul_scope_5.mjs"; // there is already-culed stuff in here, why? imports to memo loader include cul_scope_id, what logic should it apply RE passing forward? eliminate? Probably!





////////// start balance memo-loader code //////////
//const balance$m = memoize(999999, isEqual)(balance$);
export const balance$m = memoize(balance$, JSON.stringify);
export const balance_ = (a) => {
  return balance$m(a);
  // eslint-disable-next-line no-undef
  balance$({ year_in, actual_interest_rate_co_in }); // never run, but here to "trick" calculang graph logic
};
////////// end balance memo-loader code //////////



////////// start deposits memo-loader code //////////
//const deposits$m = memoize(999999, isEqual)(deposits$);
export const deposits$m = memoize(deposits$, JSON.stringify);
export const deposits_ = (a) => {
  return deposits$m(a);
  // eslint-disable-next-line no-undef
  deposits$({ year_in }); // never run, but here to "trick" calculang graph logic
};
////////// end deposits memo-loader code //////////



////////// start interest memo-loader code //////////
//const interest$m = memoize(999999, isEqual)(interest$);
export const interest$m = memoize(interest$, JSON.stringify);
export const interest_ = (a) => {
  return interest$m(a);
  // eslint-disable-next-line no-undef
  interest$({ year_in, actual_interest_rate_co_in }); // never run, but here to "trick" calculang graph logic
};
////////// end interest memo-loader code //////////



////////// start year memo-loader code //////////
//const year$m = memoize(999999, isEqual)(year$);
export const year$m = memoize(year$, JSON.stringify);
export const year_ = (a) => {
  return year$m(a);
  // eslint-disable-next-line no-undef
  year$({ year_in }); // never run, but here to "trick" calculang graph logic
};
////////// end year memo-loader code //////////



////////// start annual_payment memo-loader code //////////
//const annual_payment$m = memoize(999999, isEqual)(annual_payment$);
export const annual_payment$m = memoize(annual_payment$, JSON.stringify);
export const annual_payment_ = (a) => {
  return annual_payment$m(a);
  // eslint-disable-next-line no-undef
  annual_payment$({ annual_payment_in }); // never run, but here to "trick" calculang graph logic
};
////////// end annual_payment memo-loader code //////////



////////// start duration memo-loader code //////////
//const duration$m = memoize(999999, isEqual)(duration$);
export const duration$m = memoize(duration$, JSON.stringify);
export const duration_ = (a) => {
  return duration$m(a);
  // eslint-disable-next-line no-undef
  duration$({ duration_in }); // never run, but here to "trick" calculang graph logic
};
////////// end duration memo-loader code //////////



////////// start interest_rate memo-loader code //////////
//const interest_rate$m = memoize(999999, isEqual)(interest_rate$);
export const interest_rate$m = memoize(interest_rate$, JSON.stringify);
export const interest_rate_ = (a) => {
  return interest_rate$m(a);
  // eslint-disable-next-line no-undef
  interest_rate$({ interest_rate_in }); // never run, but here to "trick" calculang graph logic
};
////////// end interest_rate memo-loader code //////////