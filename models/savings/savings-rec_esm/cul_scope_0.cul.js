
    import { memoize } from 'underscore';
    //import memoize from 'lru-memoize';
    //import { isEqual } from 'underscore'; // TODO poor tree shaking support, or why is this impact so massive? Move to lodash/lodash-es?
    
    // import/export non-to memo?

    import { balance_ as balance$, deposits_ as deposits$, interest_ as interest$, duration_ as duration$, interest_rate_expected_ as interest_rate_expected$, annual_payment_ as annual_payment$, year_ as year$, actual_interest_rates_ as actual_interest_rates$, actual_interest_rate_co_ as actual_interest_rate_co$, interest_rate_ as interest_rate$ } from './savings-rec.cul.js?+memoed'; // there is already-culed stuff in here, why? imports to memo loader include cul_scope_id, what logic should it apply RE passing forward? eliminate? Probably!

    
    
    

////////// start balance memo-loader code //////////
//const balance$m = memoize(999999, isEqual)(balance$);
export const balance$m = memoize(balance$, JSON.stringify);
export const balance = (a) => {
  return balance$m(a);
  // eslint-disable-next-line no-undef
  balance$(); // never run, but here to "trick" calculang graph logic
};
////////// end balance memo-loader code //////////



////////// start deposits memo-loader code //////////
//const deposits$m = memoize(999999, isEqual)(deposits$);
export const deposits$m = memoize(deposits$, JSON.stringify);
export const deposits = (a) => {
  return deposits$m(a);
  // eslint-disable-next-line no-undef
  deposits$(); // never run, but here to "trick" calculang graph logic
};
////////// end deposits memo-loader code //////////



////////// start interest memo-loader code //////////
//const interest$m = memoize(999999, isEqual)(interest$);
export const interest$m = memoize(interest$, JSON.stringify);
export const interest = (a) => {
  return interest$m(a);
  // eslint-disable-next-line no-undef
  interest$(); // never run, but here to "trick" calculang graph logic
};
////////// end interest memo-loader code //////////



////////// start duration memo-loader code //////////
//const duration$m = memoize(999999, isEqual)(duration$);
export const duration$m = memoize(duration$, JSON.stringify);
export const duration = (a) => {
  return duration$m(a);
  // eslint-disable-next-line no-undef
  duration$(); // never run, but here to "trick" calculang graph logic
};
////////// end duration memo-loader code //////////



////////// start interest_rate_expected memo-loader code //////////
//const interest_rate_expected$m = memoize(999999, isEqual)(interest_rate_expected$);
export const interest_rate_expected$m = memoize(interest_rate_expected$, JSON.stringify);
export const interest_rate_expected = (a) => {
  return interest_rate_expected$m(a);
  // eslint-disable-next-line no-undef
  interest_rate_expected$(); // never run, but here to "trick" calculang graph logic
};
////////// end interest_rate_expected memo-loader code //////////



////////// start annual_payment memo-loader code //////////
//const annual_payment$m = memoize(999999, isEqual)(annual_payment$);
export const annual_payment$m = memoize(annual_payment$, JSON.stringify);
export const annual_payment = (a) => {
  return annual_payment$m(a);
  // eslint-disable-next-line no-undef
  annual_payment$(); // never run, but here to "trick" calculang graph logic
};
////////// end annual_payment memo-loader code //////////



////////// start year memo-loader code //////////
//const year$m = memoize(999999, isEqual)(year$);
export const year$m = memoize(year$, JSON.stringify);
export const year = (a) => {
  return year$m(a);
  // eslint-disable-next-line no-undef
  year$(); // never run, but here to "trick" calculang graph logic
};
////////// end year memo-loader code //////////



////////// start actual_interest_rates memo-loader code //////////
//const actual_interest_rates$m = memoize(999999, isEqual)(actual_interest_rates$);
export const actual_interest_rates$m = memoize(actual_interest_rates$, JSON.stringify);
export const actual_interest_rates = (a) => {
  return actual_interest_rates$m(a);
  // eslint-disable-next-line no-undef
  actual_interest_rates$(); // never run, but here to "trick" calculang graph logic
};
////////// end actual_interest_rates memo-loader code //////////



////////// start actual_interest_rate_co memo-loader code //////////
//const actual_interest_rate_co$m = memoize(999999, isEqual)(actual_interest_rate_co$);
export const actual_interest_rate_co$m = memoize(actual_interest_rate_co$, JSON.stringify);
export const actual_interest_rate_co = (a) => {
  return actual_interest_rate_co$m(a);
  // eslint-disable-next-line no-undef
  actual_interest_rate_co$(); // never run, but here to "trick" calculang graph logic
};
////////// end actual_interest_rate_co memo-loader code //////////



////////// start interest_rate memo-loader code //////////
//const interest_rate$m = memoize(999999, isEqual)(interest_rate$);
export const interest_rate$m = memoize(interest_rate$, JSON.stringify);
export const interest_rate = (a) => {
  return interest_rate$m(a);
  // eslint-disable-next-line no-undef
  interest_rate$(); // never run, but here to "trick" calculang graph logic
};
////////// end interest_rate memo-loader code //////////


    