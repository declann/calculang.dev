import { interest_rate } from "./cul_scope_0.mjs";import { actual_interest_rate_co } from "./cul_scope_0.mjs";import { actual_interest_rates } from "./cul_scope_0.mjs";import { year } from "./cul_scope_0.mjs";import { annual_payment } from "./cul_scope_0.mjs";import { interest_rate_expected } from "./cul_scope_0.mjs";import { duration } from "./cul_scope_0.mjs";import { interest } from "./cul_scope_0.mjs";import { deposits } from "./cul_scope_0.mjs";import { balance } from "./cul_scope_0.mjs";import { balance_, deposits_, interest_ } from "./cul_scope_4.mjs";
export { balance_, deposits_, interest_ };

export const annual_payment_ = ({}) => 1000;
export const duration_ = ({}) => 5;
export const interest_rate_ = ({}) => 0.02;