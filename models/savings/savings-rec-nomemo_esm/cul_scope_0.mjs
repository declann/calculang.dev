import { balance, deposits, interest, duration, interest_rate_ as interest_rate_expected, annual_payment } from "./cul_scope_1.mjs";
export { balance, deposits, interest, duration, interest_rate_expected, annual_payment };

export const year = ({ year_in }) => year_in;

export const actual_interest_rates = ({}) => [0.02, 0.03, 0.03, 0.00, 0.01, 0.01];

export const actual_interest_rate_co = ({ actual_interest_rate_co_in }) => actual_interest_rate_co_in;

export const interest_rate = ({ year_in, actual_interest_rate_co_in }) => {
  if (year({ year_in }) > actual_interest_rate_co({ actual_interest_rate_co_in }))
  return interest_rate_expected({});else

  return actual_interest_rates({})[year({ year_in })];
};